package bancodedados;

import java.util.ArrayList;
import br.ufrn.dimap.pairg.karutakanji.android.R;

import com.karutakanji.DadosPartidasAnteriores;
import com.karutakanji.ModoCasual;
import com.karutakanji.VerMaceteKanjiActivity;

import lojinha.ConcreteDAOAcessaComprasMaceteKanji;
import lojinha.ConcreteDAOAcessaDinheiroDoJogador;
import lojinha.DAOAcessaComprasMaceteKanji;
import lojinha.DAOAcessaDinheiroDoJogador;
import lojinha.MaceteKanjiParaListviewSelecionavel;
import android.R.array;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AdapterListViewDasUltimasPartidasCompeticao extends ArrayAdapter<String> 
{
	private ArrayList<String> arrayListDadosPartidas; //somente o texto que aparece nos botoes
	private Context contextoAplicacao;
	
	public AdapterListViewDasUltimasPartidasCompeticao(Context contextoAplicacao, int textViewResourceId,
			ArrayList<String> arrayListDadosPartidas) 
	{
		super(contextoAplicacao, textViewResourceId, arrayListDadosPartidas);
		this.arrayListDadosPartidas = arrayListDadosPartidas;
		
		this.contextoAplicacao = contextoAplicacao;
	}

	
	
	private class ViewHolderDadosPartidasAnteriores {
		   TextView texto_um_dado_partida_anterior;
		  }
	
	
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	  
	   ViewHolderDadosPartidasAnteriores holder = null;
	   Log.v("ConvertView", String.valueOf(position));
	  
	   if (convertView == null) {
		   
	   LayoutInflater vi = (LayoutInflater)contextoAplicacao.getSystemService(
	     Context.LAYOUT_INFLATER_SERVICE);
	   convertView = vi.inflate(R.layout.item_lista_dados_partidas_anteriores, null);
	   
	   TextView texto_um_dado_partida_anterior = (TextView) convertView.findViewById(R.id.texto_um_dado_partida_anterior);
	   this.mudarFonteTextView_texto_um_dado_partida_anterior(texto_um_dado_partida_anterior);
	   texto_um_dado_partida_anterior.setText(arrayListDadosPartidas.get(position));
	   
	   /*COISAS NECESSARIAS PARA FAZER AS LINHAS DA LISTVIEW FICAREM VERMELHAS OU NORMAIS
	   if((position & 1) != 0)
	   {
		   layoutDeUmaLinhaDoBuscarSalas.setBackgroundResource(R.drawable.red_header);
		   textoUsername.setTextColor(Color.parseColor("#FFFFFF"));
		   //imagemTituloDoJogador.setTextColor(Color.parseColor("#FFFFFF"));
	   }
	   else
	   {
		   layoutDeUmaLinhaDoBuscarSalas.setBackgroundResource(R.drawable.white_header);
		   textoUsername.setTextColor(Color.parseColor("#000000"));
		   //imagemTituloDoJogador.setTextColor(Color.parseColor("#000000"));
	   }*/
	  
	   holder = new ViewHolderDadosPartidasAnteriores();
	   holder.texto_um_dado_partida_anterior = (TextView) convertView.findViewById(R.id.texto_um_dado_partida_anterior);
	   
	   convertView.setTag(holder);
	   }
	   else 
	   {
		   //ANDREWS ADICIONOU
	        holder = (ViewHolderDadosPartidasAnteriores) convertView.getTag();
	       
	      
	 	   
	       TextView texto_um_dado_partida_anterior = (TextView) convertView.findViewById(R.id.texto_um_dado_partida_anterior);
	 	   this.mudarFonteTextView_texto_um_dado_partida_anterior(texto_um_dado_partida_anterior);
	 	  texto_um_dado_partida_anterior.setText(arrayListDadosPartidas.get(position));
	 	   
	 	  /*COISAS NECESSARIAS PARA FAZER AS LINHAS DA LISTVIEW FICAREM VERMELHAS OU NORMAIS
	 	   if((position & 1) != 0)
	 	   {
	 		   layoutDeUmaLinhaDoBuscarSalas.setBackgroundResource(R.drawable.red_header);
	 		   textoUsername.setTextColor(Color.parseColor("#FFFFFF"));
	 		   //textoTituloDoJogador.setTextColor(Color.parseColor("#FFFFFF"));
	 	   }
	 	   else
	 	   {
	 		   layoutDeUmaLinhaDoBuscarSalas.setBackgroundResource(R.drawable.white_header);
	 		   textoUsername.setTextColor(Color.parseColor("#000000"));
	 		   //textoTituloDoJogador.setTextColor(Color.parseColor("#000000"));
	 	   }*/
	 	  
	 	   //holder = new ViewHolderSalasCriadas();
	 	   holder.texto_um_dado_partida_anterior = (TextView) convertView.findViewById(R.id.texto_um_dado_partida_anterior);
	 	  
	 	   
	 	   //convertView.setTag(holder);
	 	   
	    }
	   
	  
	   return convertView;
	  
	  }
	  
	  
	  
	  private void mudarFonteTextView_texto_um_dado_partida_anterior(TextView textView)
	  {
		  String fontPath = "fonts/Wonton.ttf";
		  Typeface tf = Typeface.createFromAsset(contextoAplicacao.getAssets(), fontPath);
		  textView.setTypeface(tf);
	  }
	  

}
