package bancodedados;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.dimap.pairg.karutakanji.android.R;

import com.karutakanji.ModoCasual;


import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

//essa classe serve apenas para mudar a fonte do listview que mostra as palavras de uma partida anterior
public class AdapterListViewPalavrasUmaPartidaAnterior extends ArrayAdapter<String>
{
	private String[] listaPalavrasPartidaAnterior;
	private Context contextoAplicacao;
	
	public AdapterListViewPalavrasUmaPartidaAnterior(Context contextoAplicacao, int textViewResourceId,
			String[] listaPalavrasPartidaAnterior) 
	{
		super(contextoAplicacao, textViewResourceId, listaPalavrasPartidaAnterior);
		this.listaPalavrasPartidaAnterior = listaPalavrasPartidaAnterior;
		this.contextoAplicacao = contextoAplicacao;
		
	}

	
	private class ViewHolderPalavrasPartidaAnterior {
		   TextView item_lista_partida_anterior;
		  }
	
	
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	  
		  ViewHolderPalavrasPartidaAnterior holder = null;
	   Log.v("ConvertView", String.valueOf(position));
	  
	   if (convertView == null) {
		   
	   LayoutInflater vi = (LayoutInflater)contextoAplicacao.getSystemService(
	     Context.LAYOUT_INFLATER_SERVICE);
	   convertView = vi.inflate(R.layout.item_lista_palavras_partida_anterior, null);
	   
	   //vamos mudar a posicao do balao de chat para esquerda ou direita
	  
	   TextView item_lista_partida_anterior = (TextView) convertView.findViewById(R.id.item_lista_partida_anterior);
	   this.mudarFonteTextViewMensagemChat(item_lista_partida_anterior);
	   item_lista_partida_anterior.setText(listaPalavrasPartidaAnterior[position]);
	   
	   /*COISAS NECESSARIAS PARA FAZER AS LINHAS DA LISTVIEW FICAREM VERMELHAS OU NORMAIS
	   if((position & 1) != 0)
	   {
		   layoutDeUmaLinhaDoBuscarSalas.setBackgroundResource(R.drawable.red_header);
		   textoUsername.setTextColor(Color.parseColor("#FFFFFF"));
		   //imagemTituloDoJogador.setTextColor(Color.parseColor("#FFFFFF"));
	   }
	   else
	   {
		   layoutDeUmaLinhaDoBuscarSalas.setBackgroundResource(R.drawable.white_header);
		   textoUsername.setTextColor(Color.parseColor("#000000"));
		   //imagemTituloDoJogador.setTextColor(Color.parseColor("#000000"));
	   }*/
	  
	   holder = new ViewHolderPalavrasPartidaAnterior();
	   holder.item_lista_partida_anterior = (TextView) convertView.findViewById(R.id.item_lista_partida_anterior);
	   
	   convertView.setTag(holder);
	   }
	   else 
	   {
		   //ANDREWS ADICIONOU
	        holder = (ViewHolderPalavrasPartidaAnterior) convertView.getTag();
	       
	      
	 	   
	       TextView item_lista_partida_anterior = (TextView) convertView.findViewById(R.id.item_lista_partida_anterior);
	 	   this.mudarFonteTextViewMensagemChat(item_lista_partida_anterior);
	 	  item_lista_partida_anterior.setText(listaPalavrasPartidaAnterior[position]);
	 	   
	 	  /*COISAS NECESSARIAS PARA FAZER AS LINHAS DA LISTVIEW FICAREM VERMELHAS OU NORMAIS
	 	   if((position & 1) != 0)
	 	   {
	 		   layoutDeUmaLinhaDoBuscarSalas.setBackgroundResource(R.drawable.red_header);
	 		   textoUsername.setTextColor(Color.parseColor("#FFFFFF"));
	 		   //textoTituloDoJogador.setTextColor(Color.parseColor("#FFFFFF"));
	 	   }
	 	   else
	 	   {
	 		   layoutDeUmaLinhaDoBuscarSalas.setBackgroundResource(R.drawable.white_header);
	 		   textoUsername.setTextColor(Color.parseColor("#000000"));
	 		   //textoTituloDoJogador.setTextColor(Color.parseColor("#000000"));
	 	   }*/
	 	  
	 	   //holder = new ViewHolderSalasCriadas();
	 	   holder.item_lista_partida_anterior = (TextView) convertView.findViewById(R.id.item_lista_partida_anterior);
	 	  
	 	   
	 	   //convertView.setTag(holder);
	 	   
	    }
	   
	  
	   return convertView;
	  
	  }
	  
	  
	  
	  private void mudarFonteTextViewMensagemChat(TextView textView)
	  {
		  String fontPath = "fonts/mvboli.ttf";
		  Typeface tf = Typeface.createFromAsset(contextoAplicacao.getAssets(), fontPath);
		  textView.setTypeface(tf);
	  }

}
