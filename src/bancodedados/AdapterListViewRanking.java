package bancodedados;

import java.util.ArrayList;
import br.ufrn.dimap.pairg.karutakanji.android.R;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterListViewRanking extends ArrayAdapter<DadosRankingCompeticao> 
{
	private ArrayList<DadosRankingCompeticao> arrayListDadosRanking;
	private Context contextoAplicacao;
	
	public AdapterListViewRanking(Context contextoAplicacao, int textViewResourceId,
		    ArrayList<DadosRankingCompeticao> usuarios_ranking) 
	{
		super(contextoAplicacao, textViewResourceId, usuarios_ranking);
		this.arrayListDadosRanking = new ArrayList<DadosRankingCompeticao>();
		this.arrayListDadosRanking.addAll(usuarios_ranking);
		this.contextoAplicacao = contextoAplicacao;
	}

	public ArrayList<DadosRankingCompeticao> getArrayListDadosRanking() {
		return arrayListDadosRanking;
	}

	public void setArrayListDadosRanking(
			ArrayList<DadosRankingCompeticao> arrayListDadosRanking) {
		this.arrayListDadosRanking = arrayListDadosRanking;
	}
	
	
	private class ViewHolderRanking {
		   TextView posicao_do_jogador_ranking;
		   TextView nome_usuario_ranking;
		   TextView pontuacao_ranking;
		   TextView vitorias_ranking;
		   TextView derrotas_ranking;
		  }
	
	
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	  
	   ViewHolderRanking holder = null;
	   Log.v("ConvertView", String.valueOf(position));
	  
	   convertView = null;
	   
	   if (convertView == null) {
		   
	   LayoutInflater vi = (LayoutInflater)contextoAplicacao.getSystemService(
	     Context.LAYOUT_INFLATER_SERVICE);
	   convertView = vi.inflate(R.layout.item_lista_ranking, null);
	   
	   LinearLayout layoutDeUmaLinhaDoRanking = (LinearLayout) convertView.findViewById(R.id.uma_linha_ranking);
	   TextView texto_posicao_do_jogador_ranking = (TextView) convertView.findViewById(R.id.posicao_do_jogador_ranking);
	   
	   String fontPath = "fonts/mvboli.ttf";
	   Typeface tf = Typeface.createFromAsset(contextoAplicacao.getAssets(), fontPath);
	   this.mudarFonteTextView(texto_posicao_do_jogador_ranking,tf);
	   TextView nome_usuario_ranking = (TextView) convertView.findViewById(R.id.nome_usuario_ranking);
	   this.mudarFonteTextView(nome_usuario_ranking,tf);
	   TextView pontuacao_ranking = (TextView) convertView.findViewById(R.id.pontuacao_ranking);
	   this.mudarFonteTextView(pontuacao_ranking,tf);
	   TextView vitorias_ranking = (TextView) convertView.findViewById(R.id.vitorias_ranking);
	   this.mudarFonteTextView(vitorias_ranking,tf);
	   TextView derrotas_ranking = (TextView) convertView.findViewById(R.id.derrotas_ranking);
	   this.mudarFonteTextView(derrotas_ranking,tf);
	   
	   /*COISAS NECESSARIAS PARA FAZER AS LINHAS DA LISTVIEW FICAREM VERMELHAS OU NORMAIS
	   if((position & 1) != 0)
	   {
		   layoutDeUmaLinhaDoRanking.setBackgroundResource(R.drawable.red_header);
		   texto_posicao_do_jogador_ranking.setTextColor(Color.parseColor("#FFFFFF"));
		   nome_usuario_ranking.setTextColor(Color.parseColor("#FFFFFF"));
		   pontuacao_ranking.setTextColor(Color.parseColor("#FFFFFF"));
		   vitorias_ranking.setTextColor(Color.parseColor("#FFFFFF"));
		   derrotas_ranking.setTextColor(Color.parseColor("#FFFFFF"));
	   }
	   else
	   {
		   layoutDeUmaLinhaDoRanking.setBackgroundResource(R.drawable.white_header);
		   texto_posicao_do_jogador_ranking.setTextColor(Color.parseColor("#000000"));
		   nome_usuario_ranking.setTextColor(Color.parseColor("#000000"));
		   pontuacao_ranking.setTextColor(Color.parseColor("#000000"));
		   vitorias_ranking.setTextColor(Color.parseColor("#000000"));
		   derrotas_ranking.setTextColor(Color.parseColor("#000000"));
	   }*/
	  
	   holder = new ViewHolderRanking();
	   holder.posicao_do_jogador_ranking = (TextView) convertView.findViewById(R.id.posicao_do_jogador_ranking);
	   holder.nome_usuario_ranking = (TextView) convertView.findViewById(R.id.nome_usuario_ranking);
	   holder.pontuacao_ranking = (TextView) convertView.findViewById(R.id.pontuacao_ranking);
	   holder.vitorias_ranking = (TextView) convertView.findViewById(R.id.vitorias_ranking);
	   holder.derrotas_ranking = (TextView) convertView.findViewById(R.id.derrotas_ranking);
	  
	   
	   convertView.setTag(holder);
	   final DadosRankingCompeticao umDadosRanking = arrayListDadosRanking.get(position);
	   holder.derrotas_ranking.setText(String.valueOf(umDadosRanking.getDerrotas_competicao()));
	   holder.vitorias_ranking.setText(String.valueOf(umDadosRanking.getVitorias_competicao()));
	   String umNomeUsuario = umDadosRanking.getNome_usuario();
	   if(umNomeUsuario.length() > 12)
	   {
		   umNomeUsuario = umNomeUsuario.substring(0, 9) + "...";
	   }
	   holder.nome_usuario_ranking.setText(umNomeUsuario);
	   holder.pontuacao_ranking.setText(String.valueOf(umDadosRanking.getPontuacao_total()));
	   holder.posicao_do_jogador_ranking.setText(String.valueOf(umDadosRanking.getPosicao_no_ranking()));
	  
	   if(umDadosRanking.getPosicao_no_ranking() == 1 || umDadosRanking.getPosicao_no_ranking() == 2 || umDadosRanking.getPosicao_no_ranking() == 3)
	   {
		   //o background do linearlayout tem de mudar paraas faixas de primeiro, segundo ou terceiro lugar
		   LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.uma_linha_ranking);
		   int drawableFaixaASerUsada = 0;
				   
		   if(umDadosRanking.getPosicao_no_ranking() == 1)
		   {
			   drawableFaixaASerUsada = R.drawable.rankingfaixaprimeirolugar;
		   }
		   else if(umDadosRanking.getPosicao_no_ranking() == 2)
		   {
			   drawableFaixaASerUsada = R.drawable.rankingfaixasegundolugar;
		   }
		   else
		   {
			   drawableFaixaASerUsada = R.drawable.rankingfaixaterceirolugar;
		   }
		   
		   linearLayout.setBackgroundResource(drawableFaixaASerUsada);
	   }
	  
	   }
	   else 
	   {
		   //ANDREWS ADICIONOU
	        holder = (ViewHolderRanking) convertView.getTag();
	       
	        LinearLayout layoutDeUmaLinhaDoRanking = (LinearLayout) convertView.findViewById(R.id.uma_linha_ranking);
	        String fontPath = "fonts/mvboli.ttf";
			  Typeface tf = Typeface.createFromAsset(contextoAplicacao.getAssets(), fontPath);
			  
	        TextView texto_posicao_do_jogador_ranking = (TextView) convertView.findViewById(R.id.posicao_do_jogador_ranking);
	 	   this.mudarFonteTextView(texto_posicao_do_jogador_ranking,tf);
	 	   TextView nome_usuario_ranking = (TextView) convertView.findViewById(R.id.nome_usuario_ranking);
	 	   this.mudarFonteTextView(nome_usuario_ranking,tf);
	 	   TextView pontuacao_ranking = (TextView) convertView.findViewById(R.id.pontuacao_ranking);
	 	   this.mudarFonteTextView(pontuacao_ranking,tf);
	 	   TextView vitorias_ranking = (TextView) convertView.findViewById(R.id.vitorias_ranking);
	 	   this.mudarFonteTextView(vitorias_ranking,tf);
	 	   TextView derrotas_ranking = (TextView) convertView.findViewById(R.id.derrotas_ranking);
	 	   this.mudarFonteTextView(derrotas_ranking,tf);
	 	   
	 	  if((position & 1) != 0)
		   {
			   layoutDeUmaLinhaDoRanking.setBackgroundResource(R.drawable.red_header);
			   texto_posicao_do_jogador_ranking.setTextColor(Color.parseColor("#FFFFFF"));
			   nome_usuario_ranking.setTextColor(Color.parseColor("#FFFFFF"));
			   pontuacao_ranking.setTextColor(Color.parseColor("#FFFFFF"));
			   vitorias_ranking.setTextColor(Color.parseColor("#FFFFFF"));
			   derrotas_ranking.setTextColor(Color.parseColor("#FFFFFF"));
		   }
		   else
		   {
			   layoutDeUmaLinhaDoRanking.setBackgroundResource(R.drawable.white_header);
			   texto_posicao_do_jogador_ranking.setTextColor(Color.parseColor("#000000"));
			   nome_usuario_ranking.setTextColor(Color.parseColor("#000000"));
			   pontuacao_ranking.setTextColor(Color.parseColor("#000000"));
			   vitorias_ranking.setTextColor(Color.parseColor("#000000"));
			   derrotas_ranking.setTextColor(Color.parseColor("#000000"));
		   }
	 	  
	 	 holder.posicao_do_jogador_ranking = (TextView) convertView.findViewById(R.id.posicao_do_jogador_ranking);
		   holder.nome_usuario_ranking = (TextView) convertView.findViewById(R.id.nome_usuario_ranking);
		   //holder.pontuacao_ranking = (TextView) convertView.findViewById(R.id.pontuacao_ranking);
		   //holder.vitorias_ranking = (TextView) convertView.findViewById(R.id.vitorias_ranking);
		   //holder.derrotas_ranking = (TextView) convertView.findViewById(R.id.derrotas_ranking);
	    }
	   
	  
	   return convertView;
	  
	  }
	  
	  
	  private void mudarFonteTextView(TextView textView, Typeface tf)
	  {
		  textView.setTypeface(tf);
	  }
	  

}
