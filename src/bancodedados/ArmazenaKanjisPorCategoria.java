package bancodedados;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class ArmazenaKanjisPorCategoria {
	private HashMap<Integer, LinkedList<KanjiTreinar>> kanjisSeparadosPorCategoria; // o id da categoria
	private static ArmazenaKanjisPorCategoria singleton;
	
	private ArmazenaKanjisPorCategoria()
	{
		kanjisSeparadosPorCategoria = new HashMap<Integer, LinkedList<KanjiTreinar>>();
	}
	
	public static ArmazenaKanjisPorCategoria pegarInstancia()
	{
		if(singleton == null)
		{
			singleton = new ArmazenaKanjisPorCategoria();
		}
		return singleton;
	}
	
	/**
	 * retorna a lista de kanjis pra treinar ou null se ainda n�o tem uma lista com essa categoria
	 * @param categoria
	 * @return
	 */
	public LinkedList<KanjiTreinar> getListaKanjisTreinar(int id_categoria)
	{
		return this.kanjisSeparadosPorCategoria.get(id_categoria);
	}
	
	public void adicionarKanjiACategoria(Integer id_categoria, KanjiTreinar kanjiPraTreinar)
	{
		LinkedList<KanjiTreinar> kanjisDaCategoria = this.kanjisSeparadosPorCategoria.get(id_categoria);
		if(kanjisDaCategoria == null)
		{
			//ainda n�o temos nenhuma lista de kanjis da categoria solicitada.
			kanjisDaCategoria = new LinkedList<KanjiTreinar>();
			kanjisDaCategoria.add(kanjiPraTreinar);
			this.kanjisSeparadosPorCategoria.put(id_categoria, kanjisDaCategoria);
		}
		else
		{
			//j� temos uma lista de kanjis da categoria. vamos ver se o kanji � repetido
			boolean kanjiJaEstahNaLista = false;
			for(int i = 0; i < kanjisDaCategoria.size(); i++)
			{
				String kanjiJahNaLista = kanjisDaCategoria.get(i).getKanji();
				String kanjiParaAdicionar = kanjiPraTreinar.getKanji();
				if(kanjiJahNaLista.compareTo(kanjiParaAdicionar) == 0)
				{
					kanjiJaEstahNaLista = true;
				}
			}
			
			if(kanjiJaEstahNaLista == false)
			{
				//kanji nao estah na lista, vamos add ele
				kanjisDaCategoria.add(kanjiPraTreinar);
				this.kanjisSeparadosPorCategoria.put(id_categoria, kanjisDaCategoria);
			}
		}
	}
	
	
	/*com base no texto do kanji e na categoria, irei achar o objeto KanjiTreinar*/
	public KanjiTreinar acharKanji(int idCategoria, String kanji)
	{
		LinkedList<KanjiTreinar> kanjisDaCategoria = this.kanjisSeparadosPorCategoria.get(idCategoria);
		kanjisDaCategoria = kanjisDaCategoria;
		
		
		for(int i = 0; i < kanjisDaCategoria.size(); i++)
		{
			KanjiTreinar umKanji = kanjisDaCategoria.get(i);
			if(umKanji.getKanji().compareTo(kanji) == 0)
			{
				//achei o kanji
				return umKanji;
			}
		}
		
		return null;
		
	}
	
	/*PAREI AQUI TAVA CRIANDO UM CARA PRA ARMAZENAR OS KANJIS DA CATEGORIA
	PRA QUANDO EU RETREIVE OS KANJIS VIA MYASYNKTASK ELE ARMAZENAR.
	http://stackoverflow.com/questions/13196234/simple-parse-json-from-url-on-android*/
	
	public void esvaziarTodoOHashmap()
	{
		this.kanjisSeparadosPorCategoria.clear();
	}
	
	public int quantasPalavrasTemACategoria(int id_categoria)
	{
		
		if(this.kanjisSeparadosPorCategoria.containsKey(id_categoria) == true)
		{
			LinkedList<KanjiTreinar> palavrasDaCategoria = this.kanjisSeparadosPorCategoria.get(id_categoria);
			return palavrasDaCategoria.size();
		}
		else
		{
			return 0;
		}
	}

}
