package bancodedados;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

/*o modo Treinamento n�o deveria precisar de internet para jogar. Por isso, ao inves de carregarmos as categorias do bd 
 * toda vez que o jogador quiser treinar, carregaremos a lista toda de palavras do BD a cada
 * 3 iniciadas que o usu�rio faz na aplica��o. 
 * Nessa classe tb armazenamos as vezes iniciadas e a versao atual do sistema*/
public class ArmazenaTudoParaJogoOffline 
{
	private static ArmazenaTudoParaJogoOffline instancia;
	
	private ArmazenaTudoParaJogoOffline()
	{
		
	}
	
	public static ArmazenaTudoParaJogoOffline getInstance()
	{
		if(instancia == null)
		{
			instancia = new ArmazenaTudoParaJogoOffline();
		}
		
		return instancia;
	}
	
	/*para casos de falta de internet, o usuario deveria pelo menos poder usar o modo treinamento. Por isso vamos carregar da memoria locl a versao atual do sistema*/
	public String getVersaoMaisRecenteDoSistemaLocalmente(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("versao_mais_recente", Context.MODE_PRIVATE);
		String versaoMaisRecente = configuracoesSalvar.getString("versao_mais_recente", "");
		return versaoMaisRecente;
	}
	
	public void armazenarVersaoMaisRecenteDoSistemaLocalmente(Context contextoAplicacao, String versaoMaisRecente)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("versao_mais_recente", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		editorConfig.putString("versao_mais_recente", versaoMaisRecente);
		editorConfig.commit();
	}
	
	public int getQuantasVezesAAplicacaoFoiReiniciada(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("quantas_vezes_a_aplicacao_foi_reiniciada", Context.MODE_PRIVATE);
		int quantasVezesAAplicacaoFoiIniciada = configuracoesSalvar.getInt("quantas_vezes_a_aplicacao_foi_reiniciada", 0);
		return quantasVezesAAplicacaoFoiIniciada;
	}
	
	public void aumentarQuantasVezesAAplicacaoFoiReiniciada(Context contextoAplicacao) {
		int quantasVezesAAplicacaoFoiIniciada = this.getQuantasVezesAAplicacaoFoiReiniciada(contextoAplicacao);
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("quantas_vezes_a_aplicacao_foi_reiniciada", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		int novoQuantasVezes = quantasVezesAAplicacaoFoiIniciada + 1;
		editorConfig.putInt("quantas_vezes_a_aplicacao_foi_reiniciada", novoQuantasVezes);
		editorConfig.commit();
	}
	
	//formato da string salva:
	//Formato de cada palavra: KANJI|categoria|hiragana|traducao|dificuldade|jlpt
	//Ex:Au|cotidiano|...;Kau|...
	public void salvarListaDePalavrasParaUsoFuturo(Context contextoAplicacao, String linguaDaListaDePalavras)
	{
		LinkedList<KanjiTreinar> todosOsKanjisDeTodasAsCategorias = 
										pegarTodosOsKanjisDeTodasAsCategorias();
		String stringTodosOsKanjisDeTodasAsCategorias = "";
		
		for(int i = 0; i < todosOsKanjisDeTodasAsCategorias.size(); i++)
		{
			KanjiTreinar umKanji = todosOsKanjisDeTodasAsCategorias.get(i);
			stringTodosOsKanjisDeTodasAsCategorias = 
					stringTodosOsKanjisDeTodasAsCategorias +
					umKanji.getKanji() + "|" + 
					umKanji.getCategoriaAssociada() + "|" +
					umKanji.getHiraganaDoKanji() + "|" +
					umKanji.getTraducaoEmPortugues() + "|" +
					String.valueOf(umKanji.getDificuldadeDoKanji()) + "|" +
					umKanji.getJlptAssociado() + "|" +
					umKanji.getIdDoKanji() + ";";
		}
		
		
		HashMap<Integer,Categoria> hashmapCategorias = 
				SingletonArmazenaCategoriasDoJogo.getInstance().getCategoriasESeusIds();
		Iterator<Integer> iteradorIdsCategorias = hashmapCategorias.keySet().iterator();
		String todasAsCategorias = "";
		while(iteradorIdsCategorias.hasNext() == true)
		{
			int id_categoria = iteradorIdsCategorias.next();
			Categoria umaCategoria = hashmapCategorias.get(id_categoria);
			todasAsCategorias = todasAsCategorias + String.valueOf(umaCategoria.getId()) + "|" + umaCategoria.getNome() + "|" +
					umaCategoria.getDescricao();
			if(iteradorIdsCategorias.hasNext() == true)
			{
				todasAsCategorias = todasAsCategorias + ";";
			}
		}
		
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("lista_de_palavras", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		editorConfig.putString("lista_de_palavras", stringTodosOsKanjisDeTodasAsCategorias);
		
		SharedPreferences configuracoesSalvarCategorias = contextoAplicacao.getSharedPreferences("lista_de_categorias", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfigCategorias = configuracoesSalvarCategorias.edit();
		editorConfigCategorias.putString("lista_de_categorias", todasAsCategorias);
		
		SharedPreferences configuracoesSalvarLinguaCategorias = contextoAplicacao.getSharedPreferences("lingua_da_lista_de_palavras", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfigLinguaCategorias = configuracoesSalvarLinguaCategorias.edit();
		editorConfigLinguaCategorias.putString("lingua_da_lista_de_palavras", linguaDaListaDePalavras);
		
		editorConfig.commit();
		editorConfigCategorias.commit();
		editorConfigLinguaCategorias.commit();
	}
	
	
	
	private LinkedList<KanjiTreinar> pegarTodosOsKanjisDeTodasAsCategorias()
	{
		SingletonArmazenaCategoriasDoJogo conheceCategoriasDoJogo = SingletonArmazenaCategoriasDoJogo.getInstance();
		
		LinkedList<String> categorias = 
				conheceCategoriasDoJogo.getCategoriasArmazenadas();
		LinkedList<KanjiTreinar> todosOsKanjisDeTodasAsCategorias = new LinkedList<KanjiTreinar>();
		
		for(int i = 0; i < categorias.size(); i++)
		{
			String umaCategoria = categorias.get(i); 
			int id_categoria = conheceCategoriasDoJogo.pegarIdDaCategoria(umaCategoria);
			LinkedList<KanjiTreinar> kanjisDaCategoria = 
					ArmazenaKanjisPorCategoria.pegarInstancia().getListaKanjisTreinar(id_categoria);
			for(int j = 0; j < kanjisDaCategoria.size(); j++)
			{
				KanjiTreinar umKanji = kanjisDaCategoria.get(j);
				todosOsKanjisDeTodasAsCategorias.add(umKanji);
			}
		}
		
		return todosOsKanjisDeTodasAsCategorias;
	}
	
	public String carregarLinguaDaListaDePalavrasSalvaAnteriormente(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("lingua_da_lista_de_palavras", Context.MODE_PRIVATE);
		String lingua_da_lista_de_palavras = configuracoesSalvar.getString("lingua_da_lista_de_palavras", "");
		return lingua_da_lista_de_palavras;
	}
	
	public boolean carregarListasDePalavrasSalvasAnteriormente(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("lista_de_palavras", Context.MODE_PRIVATE);
		String todosOsKanjisDeTodasAsCategorias = configuracoesSalvar.getString("lista_de_palavras", "");
		
		SharedPreferences configuracoesSalvarCategorias = contextoAplicacao.getSharedPreferences("lista_de_categorias", Context.MODE_PRIVATE);
		String todasAsCategorias = configuracoesSalvarCategorias.getString("lista_de_categorias", "");
		
		if(todosOsKanjisDeTodasAsCategorias.length() == 0)
		{
			//nao deu para acessar essa lista salva anteriormente, entao nao faremos nada
			return false;
		}
		else
		{
			try
			{
				String cadaCategoriaSeparada[] = todasAsCategorias.split(";");
				
				SingletonArmazenaCategoriasDoJogo.getInstance().esvaziarListaCategorias();
				for(int j = 0; j < cadaCategoriaSeparada.length; j++)
				{
					String umaCategoria = cadaCategoriaSeparada[j];
					String[] cadaCoisaQueFormaCategoriaSeparado = umaCategoria.split("\\|");
					int idCategoria = Integer.valueOf(cadaCoisaQueFormaCategoriaSeparado[0]);
					String nomeCategoria = cadaCoisaQueFormaCategoriaSeparado[1];
					String descricaoCategoria = cadaCoisaQueFormaCategoriaSeparado[2];
					Categoria objetoCategoria = new Categoria(idCategoria,nomeCategoria,descricaoCategoria);
					SingletonArmazenaCategoriasDoJogo.getInstance().armazenarNovaCategoria(idCategoria, objetoCategoria);
				}
				
				String cadaPalavraSeparada[] = todosOsKanjisDeTodasAsCategorias.split(";");
				for(int i = 0; i < cadaPalavraSeparada.length; i++)
				{
					String umaPalavra = cadaPalavraSeparada[i];
					String[] cadaCoisaQueFormaKanjiTreinarSeparado = umaPalavra.split("\\|");
					String kanji = cadaCoisaQueFormaKanjiTreinarSeparado[0];
					String categoria = cadaCoisaQueFormaKanjiTreinarSeparado[1];
					String hiragana = cadaCoisaQueFormaKanjiTreinarSeparado[2];
					String traducao = cadaCoisaQueFormaKanjiTreinarSeparado[3];
					String dificuldade = cadaCoisaQueFormaKanjiTreinarSeparado[4];
					int dificuldadeEmInt = Integer.valueOf(dificuldade);
					String jlpt = cadaCoisaQueFormaKanjiTreinarSeparado[5];
					String idDoKanji = cadaCoisaQueFormaKanjiTreinarSeparado[6];
					
					KanjiTreinar umKanji = new KanjiTreinar(jlpt, categoria, kanji, traducao, hiragana, dificuldadeEmInt,idDoKanji); 
					
					int id_categoria = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(categoria);
					
					ArmazenaKanjisPorCategoria.pegarInstancia().adicionarKanjiACategoria(id_categoria, umKanji);
					
				}
				
				
				return true;
			}
			catch(Exception e)
			{
				//se qualquer erro ocorreu, devemos tentar pegar a lista de palavras e categorias do servidor mesmo
				String mensagemErro = e.getMessage();
				mensagemErro = mensagemErro + "";
				return false;
			}
		}
	}
	
	
	//da primeira vez que o usuario enra no jogo, ele deve fazer login ou se cadastrar
	public void salvarLoginUsuarioParaUsoFuturo(Context contextoAplicacao, String nome_usuario, String email_usuario,String senha_usuario)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("login_usuario", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		
		if(email_usuario == null)
		{
			email_usuario = "";
		}
		editorConfig.putString("nome_usuario", nome_usuario);
		editorConfig.putString("email_usuario", email_usuario);
		editorConfig.putString("senha_usuario", senha_usuario);
		editorConfig.commit();
	}
	
	public LinkedList<String> getLoginUsuarioLocalmente(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("login_usuario", Context.MODE_PRIVATE);
		String nome_usuario = configuracoesSalvar.getString("nome_usuario", "");
		String email_usuario = configuracoesSalvar.getString("email_usuario", "");
		String senha_usuario = configuracoesSalvar.getString("senha_usuario", "");
		
		LinkedList<String> nomeEEMailUsuarioESenha = new LinkedList<String>();
		nomeEEMailUsuarioESenha.add(nome_usuario);
		nomeEEMailUsuarioESenha.add(email_usuario);
		nomeEEMailUsuarioESenha.add(senha_usuario);
		
		return nomeEEMailUsuarioESenha;
	}
	
	/*devemos mostrar todos os dados de login do usuario preenchidos na tela de login para uma segunda vez que ele abrir o app?*/
	public void salvarLembrarDeMim(Context contextoAplicacao, boolean lembrar_de_mim)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("lembrar_de_mim", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		
		editorConfig.putBoolean("lembrar_de_mim", lembrar_de_mim);
		editorConfig.commit();
	}
	
	public Boolean getLembrarDeMim(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("lembrar_de_mim", Context.MODE_PRIVATE);
		Boolean lembrar_de_mim = configuracoesSalvar.getBoolean("lembrar_de_mim", false);
		
		return lembrar_de_mim;
	}
	
	/*devemos mostrar todos os dados de login do usuario preenchidos na tela de login para uma segunda vez que ele abrir o app?*/
	public void salvarManterLogado(Context contextoAplicacao, boolean manter_logado)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("manter_logado", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		
		editorConfig.putBoolean("manter_logado", manter_logado);
		editorConfig.commit();
	}
	
	public Boolean getManterLogado(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("manter_logado", Context.MODE_PRIVATE);
		Boolean manter_logado = configuracoesSalvar.getBoolean("manter_logado", false);
		
		return manter_logado;
	}
	
	/*armazena se o usu�rio gostaria de se logar usando a conta da google novamente mais tarde*/
	public void salvarManterLogadoNaContaDaGoogle(Context contextoAplicacao, boolean manter_logado)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("manter_logado_na_conta_da_google", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		
		editorConfig.putBoolean("manter_logado_na_conta_da_google", manter_logado);
		editorConfig.commit();
	}
	
	public Boolean getManterLogadoNaContaDaGoogle(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("manter_logado_na_conta_da_google", Context.MODE_PRIVATE);
		Boolean manter_logado = configuracoesSalvar.getBoolean("manter_logado_na_conta_da_google", false);
		
		return manter_logado;
	}
	
	/*a lingua pode ser br ou en por exemplo*/
	public void salvarLinguaDoJogo(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("lingua_do_jogo", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		
		Resources res = contextoAplicacao.getResources();
        Locale myLocale = res.getConfiguration().locale;
		if(myLocale != null)
		{
			String language = myLocale.getLanguage();
			editorConfig.putString("lingua_do_jogo", language);
			editorConfig.commit();
		}
	}
	
	public String carregarLinguaDoJogo(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("lingua_do_jogo", Context.MODE_PRIVATE);
		String lingua_do_jogo = configuracoesSalvar.getString("lingua_do_jogo", "");
		
		return lingua_do_jogo;
		
	}
	
	
}
