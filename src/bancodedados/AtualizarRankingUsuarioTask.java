package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import br.ufrn.dimap.pairg.karutakanji.android.R;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

public class AtualizarRankingUsuarioTask extends AsyncTask<String, String, String>
{
	private Activity activity;
	
	public AtualizarRankingUsuarioTask(Activity activity)
	{
		this.activity = activity;
	}
	@Override
	protected String doInBackground(String... nomeUsuario) 
	{
		String nome_usuario = nomeUsuario[0];
		SingletonArmazenaRankingUsuario conheceRankingUsuario = 
								SingletonArmazenaRankingUsuario.getInstance();
		int vitorias_competicao = conheceRankingUsuario.getVitorias_competicao();
		int derrotas_competicao = conheceRankingUsuario.getDerrotas_competicao();
		int pontuacao_total = conheceRankingUsuario.getPontuacao_total();
		String dan_do_usuario = conheceRankingUsuario.getDan_do_usuario();
		
		String url_pegarusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuariopornome.php";//android nao aceita localhost, tem de ser seu IP
		String url_atualizarrankingusuario = "http://server.karutakanji.pairg.dimap.ufrn.br/app/atualizarrankingusuario.php";
		
		//String url_select = "http://192.168.0.110/amit/inserirpartidanolog.php";//android nao aceita localhost, tem de ser seu IP
		
		try
		{
			//primeiro vamos pegar o id do usuario com base no seu nome
			HttpClient httpClient0 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs0 = new ArrayList<NameValuePair>();
            HttpPost httpPost0 = new HttpPost(url_pegarusuariopornome);
            nameValuePairs0.add(new BasicNameValuePair("nome_usuario", nome_usuario));
            
            httpPost0.setEntity(new UrlEncodedFormEntity(nameValuePairs0,"UTF-8"));
            HttpResponse httpResponse0 = httpClient0.execute(httpPost0); 
            HttpEntity httpEntity0 = httpResponse0.getEntity();
            InputStream inputStream0;
            inputStream0 = httpEntity0.getContent();	
            String id_usuario = this.extrairIdDoUsuarioQueQueremosAchar(inputStream0);
			
			//agora vamos criar a sala sem as categorias
			HttpClient httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            HttpPost httpPost = new HttpPost(url_atualizarrankingusuario);
            nameValuePairs.add(new BasicNameValuePair("id_usuario", id_usuario));
            
            //antes de incluir o dan do usuario no bd, devemos transformar de volta no que ele era antes de obtermos. Ou seja: Ranking_1,Ranking_2...
            String Ranking_1 = activity.getResources().getString(R.string.Ranking_1);
            String Ranking_2 = activity.getResources().getString(R.string.Ranking_2);
            String Ranking_3 = activity.getResources().getString(R.string.Ranking_3);
            String Ranking_4 = activity.getResources().getString(R.string.Ranking_4);
            String Ranking_5 = activity.getResources().getString(R.string.Ranking_5);
            String Ranking_6 = activity.getResources().getString(R.string.Ranking_6);
            String Ranking_7 = activity.getResources().getString(R.string.Ranking_7);
            String Ranking_8 = activity.getResources().getString(R.string.Ranking_8);
            String Ranking_9 = activity.getResources().getString(R.string.Ranking_9);
            String Ranking_10 = activity.getResources().getString(R.string.Ranking_10);
            String Ranking_11 = activity.getResources().getString(R.string.Ranking_11);
            String Ranking_12 = activity.getResources().getString(R.string.Ranking_12);
            
            if(dan_do_usuario.compareTo(Ranking_1) == 0)
            {
            	dan_do_usuario = "Ranking_1";
            }
            else if(dan_do_usuario.compareTo(Ranking_2) == 0)
            {
            	dan_do_usuario = "Ranking_2";
            }
            else if(dan_do_usuario.compareTo(Ranking_3) == 0)
            {
            	dan_do_usuario = "Ranking_3";
            }
            else if(dan_do_usuario.compareTo(Ranking_4) == 0)
            {
            	dan_do_usuario = "Ranking_4";
            }
            else if(dan_do_usuario.compareTo(Ranking_5) == 0)
            {
            	dan_do_usuario = "Ranking_5";
            }
            else if(dan_do_usuario.compareTo(Ranking_6) == 0)
            {
            	dan_do_usuario = "Ranking_6";
            }
            else if(dan_do_usuario.compareTo(Ranking_7) == 0)
            {
            	dan_do_usuario = "Ranking_7";
            }
            else if(dan_do_usuario.compareTo(Ranking_8) == 0)
            {
            	dan_do_usuario = "Ranking_8";
            }
            else if(dan_do_usuario.compareTo(Ranking_9) == 0)
            {
            	dan_do_usuario = "Ranking_9";
            }
            else if(dan_do_usuario.compareTo(Ranking_10) == 0)
            {
            	dan_do_usuario = "Ranking_10";
            }
            else if(dan_do_usuario.compareTo(Ranking_11) == 0)
            {
            	dan_do_usuario = "Ranking_11";
            }
            else
            {
            	dan_do_usuario = "Ranking_12";
            }
            
            nameValuePairs.add(new BasicNameValuePair("dan_do_usuario", dan_do_usuario));
            nameValuePairs.add(new BasicNameValuePair("pontuacao_total", String.valueOf(pontuacao_total)));
            nameValuePairs.add(new BasicNameValuePair("vitorias_competicao", String.valueOf(vitorias_competicao)));
            nameValuePairs.add(new BasicNameValuePair("derrotas_competicao", String.valueOf(derrotas_competicao)));
            
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost); 
             
            
		}
		catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		 
		return "";
	}
	
	@Override
	protected void onPostExecute(String s) 
	{
		
	}
	
	
	private String extrairIdDoUsuarioQueQueremosAchar(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_usuario = jObject.getString("id_usuario");
           
            return id_usuario;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}
	
}
