package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.ufrn.dimap.pairg.karutakanji.android.R;

import com.karutakanji.ModoCasual;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

public class BuscaSalasModoCasualComArgumentoTask extends AsyncTask<String, String, Void>
{
	private InputStream inputStream = null;
	private String result = ""; 
	private ProgressDialog loadingDaTelaEmEspera;//eh o dialog da tela em espera pelo resultado do web service
	private ModoCasual activityQueEsperaAtePegarAsSalas;

	public BuscaSalasModoCasualComArgumentoTask(ProgressDialog loadingDaTela, ModoCasual activityQueEsperaAteRequestTerminar)
	{
		this.loadingDaTelaEmEspera = loadingDaTela;
		this.activityQueEsperaAtePegarAsSalas = activityQueEsperaAteRequestTerminar;
	}

	@Override
	protected Void doInBackground(String... arg0) 
	{
		//LCC: http://10.9.99.239/amit/pegarjlptjson.php
				//sala de aula: http://10.5.26.127/amit/pegarjlptjson.php

		       //String url_select = "http://app.karutakanji.pairg.dimap.ufrn.br/pegarjlptjson.php";//android nao aceita localhost, tem de ser seu IP
		String url_select = "http://server.karutakanji.pairg.dimap.ufrn.br/app/";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		
		if(arg0[0].compareTo("nomeusuario") == 0)
		{
			//iremos pesquisar por nome do usuario
			url_select = url_select + "pegarsalasmodocasualpornomeusuario.php";
			String nome_usuario = arg0[1];
			nameValuePairs.add(new BasicNameValuePair("nome_usuario", nome_usuario));
		}
		else if(arg0[0].compareTo("duracao") == 0)
		{
			//iremos pesquisar por email
			url_select = url_select + "pegarsalasmodocasualporduracao.php";
			String duracao = arg0[1];
			nameValuePairs.add(new BasicNameValuePair("duracao", duracao));
		}
		else if(arg0[0].compareTo("dan") == 0)
		{
			//iremos pesquisar por dan, agora o dan passado eh o normal. Devemos traduzir o dan para o dan do BD
			url_select = url_select + "pegarsalasmodocasualpordan.php";
			String dan_normal = arg0[1];
			String dan = "";
			final String iniciante = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.iniciante);
			final String intermediario = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.intermediario);
			final String dan1 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan1);
			final String dan2 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan2);
			final String dan3 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan3);
			final String dan4 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan4);
			final String dan5 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan5);
			final String dan6 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan6);
			final String dan7 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan7);
			final String dan8 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan8);
			final String dan9 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan9);
			final String dan10 = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.dan10);
			
			if(dan_normal.compareTo(iniciante) == 0)
			{
				dan = "Ranking_1";
			}
			else if(dan_normal.compareTo(intermediario) == 0)
			{
				dan = "Ranking_2";
			}
			else if(dan_normal.compareTo(dan1) == 0)
			{
				dan = "Ranking_3";
			}
			else if(dan_normal.compareTo(dan2) == 0)
			{
				dan = "Ranking_4";
			}
			else if(dan_normal.compareTo(dan3) == 0)
			{
				dan = "Ranking_5";
			}
			else if(dan_normal.compareTo(dan4) == 0)
			{
				dan = "Ranking_6";
			}
			else if(dan_normal.compareTo(dan5) == 0)
			{
				dan = "Ranking_7";
			}
			else if(dan_normal.compareTo(dan6) == 0)
			{
				dan = "Ranking_8";
			}
			else if(dan_normal.compareTo(dan7) == 0)
			{
				dan = "Ranking_9";
			}
			else if(dan_normal.compareTo(dan8) == 0)
			{
				dan = "Ranking_10";
			}
			else if(dan_normal.compareTo(dan9) == 0)
			{
				dan = "Ranking_11";
			}
			else
			{
				dan = "Ranking_12";
			}
			
			nameValuePairs.add(new BasicNameValuePair("dan", dan));
		}
		else if(arg0[0].compareTo("categorias") == 0)
		{
			//iremos pesquisar por email
			url_select = url_select + "pegarsalasmodocasualporcategorias.php";
			String categorias = arg0[1];
			nameValuePairs.add(new BasicNameValuePair("categoriasselecionadas", categorias));
		}
		
		//String url_select = "http://192.168.0.110/amit/pegarjlptjson.php";

        try {
            // Set up HTTP post

            // HttpClient is more then less deprecated. Need to change to URLConnection
            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url_select);
            
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            //String mensagemEntity1 = EntityUtils.toString(httpEntity);
            //mensagemEntity1 = mensagemEntity1 + "";
            
            // Read content & Log
            inputStream = httpEntity.getContent();
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
        // Convert response to string using String Builder
        try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            result = sBuilder.toString();
            
            
            /*if(result.contains("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"))
            {
            	String sqlEnviada = result.split("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")[1];
            	result = result.split("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")[2];
            	result = result + "";
            }*/

        } catch (Exception e) {
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            ((ActivityQueEsperaAtePegarOsKanjis) this.activityQueEsperaAtePegarAsSalas).procedimentoConexaoFalhou();
        }
        
		return null;		
	}
	
	 protected void onPostExecute(Void v) {
	        //parse JSON data
	        try {
	            JSONArray jArray = new JSONArray(result); 
	            LinkedList<SalaModoCasual> salasModoCasual = new LinkedList<SalaModoCasual>();
	            int tamanhojArray = jArray.length();
	            tamanhojArray = tamanhojArray + 0;
	            for(int i=0; i < jArray.length(); i++) {

	                JSONObject jObject = jArray.getJSONObject(i);
	                
	                int id_sala = jObject.getInt("id_sala");
	                String categorias_juntas = jObject.getString("categorias_juntas");
	                String categorias_juntas_ingles = jObject.getString("categorias_juntas_ingles");
	                String categorias_juntas_japones = jObject.getString("categorias_juntas_japones");
	                String email_do_criador = jObject.getString("nome_usuario");
	                String dan_do_criador = jObject.getString("dan_do_criador");
	                
	                if(dan_do_criador.compareTo("Ranking_1") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_1);
	                }
	                else if(dan_do_criador.compareTo("Ranking_2") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_2);
	                }
	                else if(dan_do_criador.compareTo("Ranking_3") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_3);
	                }
	                else if(dan_do_criador.compareTo("Ranking_4") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_4);
	                }
	                else if(dan_do_criador.compareTo("Ranking_5") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_5);
	                }
	                else if(dan_do_criador.compareTo("Ranking_6") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_6);
	                }
	                else if(dan_do_criador.compareTo("Ranking_7") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_7);
	                }
	                else if(dan_do_criador.compareTo("Ranking_8") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_8);
	                }
	                else if(dan_do_criador.compareTo("Ranking_9") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_9);
	                }
	                else if(dan_do_criador.compareTo("Ranking_10") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_10);
	                }
	                else if(dan_do_criador.compareTo("Ranking_11") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_11);
	                }
	                else
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_12);
	                }
	                
	                String quantas_rodadas = jObject.getString("quantas_rodadas");
	                
	                SalaModoCasual novaSalaModoCasual = new SalaModoCasual();
	                novaSalaModoCasual.setId_sala(id_sala);
	                
	                Resources res = activityQueEsperaAtePegarAsSalas.getResources();
	                Locale myLocale = res.getConfiguration().locale;
	        		if(myLocale != null)
	        		{
	        			String language = myLocale.getLanguage();
	        			if(myLocale.getLanguage().compareTo("en") == 0)
	        		    {
	        		    	//ingles
	        				novaSalaModoCasual.setCategoriasJuntas(categorias_juntas_ingles);
	        		    }
	        			else if(myLocale.getLanguage().compareTo("jp") == 0)
	        		    {
	        		    	//japones
	        				novaSalaModoCasual.setCategoriasJuntas(categorias_juntas_japones);
	        		    }
	        			else if(myLocale.getLanguage().compareTo("pt") == 0)
	        			{
	        				novaSalaModoCasual.setCategoriasJuntas(categorias_juntas);
	        			}
	        		    else // en
	        		    {
	        		    	novaSalaModoCasual.setCategoriasJuntas(categorias_juntas_ingles);
	        		    }
	        		}
	        		else
	        		{
	        			novaSalaModoCasual.setCategoriasJuntas(categorias_juntas_ingles);
	        		}
	        		
	                novaSalaModoCasual.setDanDoCriador(dan_do_criador);
	                novaSalaModoCasual.setNomeDoCriador(email_do_criador);
	                novaSalaModoCasual.setQuantasRodadas(quantas_rodadas);
	                
	                salasModoCasual.add(novaSalaModoCasual);

	            } // End Loop
	            
	            this.activityQueEsperaAtePegarAsSalas.mostrarListaComSalasAposCarregar(salasModoCasual,false);
	            this.loadingDaTelaEmEspera.dismiss();
	           
	        } catch (JSONException e) {
	            Log.e("JSONException", "Error: " + e.toString());
	            this.loadingDaTelaEmEspera.dismiss();
	        }
	        catch (java.lang.IndexOutOfBoundsException e) {
	            Log.e("JSONException", "Error: " + e.toString());
	            String mensagem = e.getMessage();
	            e.getCause().toString();
	            this.loadingDaTelaEmEspera.dismiss();
	        }

	    }


}
