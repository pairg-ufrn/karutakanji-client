package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.ufrn.dimap.pairg.karutakanji.android.R;

import com.karutakanji.ModoCasual;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

public class BuscaSalasModoCasualTask extends AsyncTask<String, String, Void>
{
	private InputStream inputStream = null;
	private String result = ""; 
	private ProgressDialog loadingDaTelaEmEspera;//eh o dialog da tela em espera pelo resultado do web service
	private ModoCasual activityQueEsperaAtePegarAsSalas;

	public BuscaSalasModoCasualTask(ProgressDialog loadingDaTela, ModoCasual activityQueEsperaAteRequestTerminar)
	{
		this.loadingDaTelaEmEspera = loadingDaTela;
		this.activityQueEsperaAtePegarAsSalas = activityQueEsperaAteRequestTerminar;
	}

	@Override
	protected Void doInBackground(String... arg0) 
	{
		//LCC: http://10.9.99.239/amit/pegarjlptjson.php
				//sala de aula: http://10.5.26.127/amit/pegarjlptjson.php

		       //String url_select = "http://app.karutakanji.pairg.dimap.ufrn.br/pegarjlptjson.php";//android nao aceita localhost, tem de ser seu IP
				String url_select = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarsalasmodocasualjson.php";
				//String url_select = "http://192.168.0.110/amit/pegarjlptjson.php";
		       
				ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

		        try {
		            // Set up HTTP post

		            // HttpClient is more then less deprecated. Need to change to URLConnection
		            HttpClient httpClient = new DefaultHttpClient();

		            HttpPost httpPost = new HttpPost(url_select);
		            httpPost.setEntity(new UrlEncodedFormEntity(param));
		            HttpResponse httpResponse = httpClient.execute(httpPost);
		            HttpEntity httpEntity = httpResponse.getEntity();

		            // Read content & Log
		            inputStream = httpEntity.getContent();
		        } catch (UnsupportedEncodingException e1) {
		            Log.e("UnsupportedEncodingException", e1.toString());
		            e1.printStackTrace();
		        } catch (ClientProtocolException e2) {
		            Log.e("ClientProtocolException", e2.toString());
		            e2.printStackTrace();
		        } catch (IllegalStateException e3) {
		            Log.e("IllegalStateException", e3.toString());
		            e3.printStackTrace();
		        } catch (IOException e4) {
		            Log.e("IOException", e4.toString());
		            e4.printStackTrace();
		        }
		        // Convert response to string using String Builder
		        try {
		            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
		            StringBuilder sBuilder = new StringBuilder();

		            String line = null;
		            while ((line = bReader.readLine()) != null) {
		            	if(line.startsWith("<meta") == false)//pula linha de metadados
		            	{
		            		 sBuilder.append(line + "\n");
		            	}
		               
		            }

		            inputStream.close();
		            result = sBuilder.toString();

		        } catch (Exception e) {
		            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
		            ((ActivityQueEsperaAtePegarOsKanjis) this.activityQueEsperaAtePegarAsSalas).procedimentoConexaoFalhou();
		        }
		        
				return null;
	}
	
	 protected void onPostExecute(Void v) {
	        //parse JSON data
	        try {
	            JSONArray jArray = new JSONArray(result); 
	            LinkedList<SalaModoCasual> salasModoCasual = new LinkedList<SalaModoCasual>();
	            for(int i=0; i < jArray.length(); i++) {

	                JSONObject jObject = jArray.getJSONObject(i);
	                
	                int id_sala = jObject.getInt("id_sala");
	                String categorias_juntas = jObject.getString("categorias_juntas");
	                String categorias_juntas_ingles = jObject.getString("categorias_juntas_ingles");
	                String categorias_juntas_japones = jObject.getString("categorias_juntas_japones");
	                String nome_do_criador = jObject.getString("nome_usuario");
	                String dan_do_criador = jObject.getString("dan_do_criador");
	                
	                if(dan_do_criador.compareTo("Ranking_1") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_1);
	                }
	                else if(dan_do_criador.compareTo("Ranking_2") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_2);
	                }
	                else if(dan_do_criador.compareTo("Ranking_3") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_3);
	                }
	                else if(dan_do_criador.compareTo("Ranking_4") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_4);
	                }
	                else if(dan_do_criador.compareTo("Ranking_5") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_5);
	                }
	                else if(dan_do_criador.compareTo("Ranking_6") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_6);
	                }
	                else if(dan_do_criador.compareTo("Ranking_7") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_7);
	                }
	                else if(dan_do_criador.compareTo("Ranking_8") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_8);
	                }
	                else if(dan_do_criador.compareTo("Ranking_9") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_9);
	                }
	                else if(dan_do_criador.compareTo("Ranking_10") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_10);
	                }
	                else if(dan_do_criador.compareTo("Ranking_11") == 0)
	                {
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_11);
	                }
	                else
	                {
	                	dan_do_criador = dan_do_criador + "";
	                	dan_do_criador = activityQueEsperaAtePegarAsSalas.getResources().getString(R.string.Ranking_12);
	                }
	                
	                String quantas_rodadas = jObject.getString("quantas_rodadas");
	                
	                SalaModoCasual novaSalaModoCasual = new SalaModoCasual();
	                novaSalaModoCasual.setId_sala(id_sala);
	                
	                Resources res = activityQueEsperaAtePegarAsSalas.getResources();
	                Locale myLocale = res.getConfiguration().locale;
	                if(myLocale != null)
	        		{
	        			String language = myLocale.getLanguage();
	        			if(myLocale.getLanguage().compareTo("en") == 0)
	        		    {
	        		    	//ingles
	        				novaSalaModoCasual.setCategoriasJuntas(categorias_juntas_ingles);
	        		    }
	        			else if(myLocale.getLanguage().compareTo("jp") == 0)
	        		    {
	        		    	//japones
	        				novaSalaModoCasual.setCategoriasJuntas(categorias_juntas_japones);
	        		    }
	        		    else if(myLocale.getLanguage().compareTo("pt") == 0)// br
	        		    {
	        		    	novaSalaModoCasual.setCategoriasJuntas(categorias_juntas);
	        		    }
	        		    else
	        		    {
	        		    	novaSalaModoCasual.setCategoriasJuntas(categorias_juntas_ingles);
	        		    }
	        		}
	        		else
	        		{
	        			novaSalaModoCasual.setCategoriasJuntas(categorias_juntas_ingles);
	        		}
	                
	                
	                novaSalaModoCasual.setDanDoCriador(dan_do_criador);
	                novaSalaModoCasual.setNomeDoCriador(nome_do_criador);
	                novaSalaModoCasual.setQuantasRodadas(quantas_rodadas);
	                
	                salasModoCasual.add(novaSalaModoCasual);

	            } // End Loop
	            
	            //vamos reverter a ordem das salas, para coloca-las do mais recente pro menos
	            LinkedList<SalaModoCasual> salasModoCasualRevert = new LinkedList<SalaModoCasual>();
	            Iterator<SalaModoCasual> iteradorReverso = salasModoCasual.descendingIterator();
	            while(iteradorReverso.hasNext() == true)
	            {
	            	SalaModoCasual umaSala = iteradorReverso.next();
	            	salasModoCasualRevert.add(umaSala);
	            }
	            
	            if(activityQueEsperaAtePegarAsSalas.getSalasAbertas() == null)
	            {
	            	//nao ha listas de salas anteriores para comparar
	            	this.activityQueEsperaAtePegarAsSalas.mostrarListaComSalasAposCarregar(salasModoCasualRevert,false);
	            	fazerDialogDesaparecerSeAindaExistir();
	            }
	            else
	            {
	            	//sera que a GUI precisa mesmo ser atualizada? E a lista de salas obtidas eh a mesma da apresentada?
	            	LinkedList<SalaModoCasual> salasMostradasNaGui = this.activityQueEsperaAtePegarAsSalas.getSalasAbertas();
	            	if(salasMostradasNaGui.size() == salasModoCasualRevert.size())
	            	{
	            		//mesmo tamanho, mas serah que tem os mesmos elementos?
	            		for(int j = 0; j < salasModoCasualRevert.size(); j++)
	            		{
	            			int idUmaSalaModoCasual = salasModoCasualRevert.get(j).getId_sala();
	            			int idSalaJaExistiaNaGui = salasMostradasNaGui.get(j).getId_sala();
	            			
	            			if(idSalaJaExistiaNaGui != idUmaSalaModoCasual)
	            			{
	            				//tem de atualizar a gui. As salas deveriam estar ate na mesma ordem
	            				this.activityQueEsperaAtePegarAsSalas.mostrarListaComSalasAposCarregar(salasModoCasualRevert,true);
	            				fazerDialogDesaparecerSeAindaExistir();
	    			            break;
	            			}
	            		}
	            	}
	            	else
	            	{
	            		//as salas obtidas sao diferentes das mostradas na gui. Vamos ter de atualizar a GUI
	            		if(salasMostradasNaGui.size() < salasModoCasualRevert.size())
        				{
        					//alertar novas salas surgiram
        					this.activityQueEsperaAtePegarAsSalas.mostrarListaComSalasAposCarregar(salasModoCasualRevert,true);
        				}
        				else
        				{
        					//algumas salas sairam, entao n precisa alert
        					this.activityQueEsperaAtePegarAsSalas.mostrarListaComSalasAposCarregar(salasModoCasualRevert,false);
        				}
	            		
	            		fazerDialogDesaparecerSeAindaExistir();
	            	}
	            }
	           
	        } catch (JSONException e) {
	            Log.e("JSONException", "Error: " + e.toString());
	            fazerDialogDesaparecerSeAindaExistir();
	        }

	    }
	 
	 @SuppressLint("NewApi")
	private void fazerDialogDesaparecerSeAindaExistir()
	 {
		 if(this.loadingDaTelaEmEspera != null) {
		        if(loadingDaTelaEmEspera.isShowing()) { //check if dialog is showing.

		            //get the Context object that was used to great the dialog
		            Context context = ((ContextWrapper)loadingDaTelaEmEspera.getContext()).getBaseContext();

		            //if the Context used here was an activity AND it hasn't been finished or destroyed
		            //then dismiss it
		            if(context instanceof Activity) { 
		                if(!((Activity)context).isFinishing() && !((Activity)context).isDestroyed()) 
		                	loadingDaTelaEmEspera.dismiss();
		            } else //if the Context used wasnt an Activity, then dismiss it too
		            	loadingDaTelaEmEspera.dismiss();
		        }
		        loadingDaTelaEmEspera = null;
		    }
	 }
}
