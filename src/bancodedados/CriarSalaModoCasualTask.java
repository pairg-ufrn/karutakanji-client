package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import br.ufrn.dimap.pairg.karutakanji.android.R;

import com.karutakanji.ModoCasual;

import android.os.AsyncTask;
import android.util.Log;

public class CriarSalaModoCasualTask extends AsyncTask<SalaModoCasual, String, String>
{
	private ModoCasual telaModoCasual;
	private int id_sala_em_int; //toda sala criada tem um id, mas ele eh criado pelo BD e nao pelo user
	
	public CriarSalaModoCasualTask(ModoCasual telaModoCasual)
	{
		this.telaModoCasual = telaModoCasual;
	}
	@Override
	protected String doInBackground(SalaModoCasual... salaModoCasual) 
	{
		SalaModoCasual umaSalaModoCasual = salaModoCasual[0];
		String danCriador = umaSalaModoCasual.getDanDoCriador();
		String categoriasselecionadas = umaSalaModoCasual.getCategoriasJuntas();
		String rodadas = umaSalaModoCasual.getQuantasRodadas();
		String sala_aberta = "sim";
		
		String url_criarSala = "http://server.karutakanji.pairg.dimap.ufrn.br/app/criarsalamodocasual.php";//android nao aceita localhost, tem de ser seu IP
		String url_pegarIdSalaCriada = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegaridsalacasualcriadapelousuario.php";
		String url_inserirCategoriasSalaCriada = "http://server.karutakanji.pairg.dimap.ufrn.br/app/criarsalamodocasualcategorias.php";
		String url_pegarusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuariopornome.php";//android nao aceita localhost, tem de ser seu IP
		
		//String url_select = "http://192.168.0.110/amit/inserirpartidanolog.php";//android nao aceita localhost, tem de ser seu IP
		
		try
		{
			//primeiro vamos pegar o id do usuario com base no seu nome
			LinkedList<String> nomeEEMailUsuario = ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(telaModoCasual);
			String nomeCriador = nomeEEMailUsuario.get(0);
			HttpClient httpClient0 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs0 = new ArrayList<NameValuePair>();
            HttpPost httpPost0 = new HttpPost(url_pegarusuariopornome);
            nameValuePairs0.add(new BasicNameValuePair("nome_usuario", nomeCriador));
            
            httpPost0.setEntity(new UrlEncodedFormEntity(nameValuePairs0,"UTF-8"));
            HttpResponse httpResponse0 = httpClient0.execute(httpPost0); 
            HttpEntity httpEntity0 = httpResponse0.getEntity();
            InputStream inputStream0;
            inputStream0 = httpEntity0.getContent();	
            String id_usuario = this.extrairIdDoUsuarioQueQueremosAchar(inputStream0);
			
			//agora vamos criar a sala sem as categorias
			HttpClient httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            HttpPost httpPost = new HttpPost(url_criarSala);
            nameValuePairs.add(new BasicNameValuePair("id_usuario", id_usuario));
            
            //TEMOS DE MUDAR O DAN DO CRIADOR PARA O QUE ESTAH DE ACORDO COM O BANCO DE DADOS
            final String iniciante = telaModoCasual.getResources().getString(R.string.iniciante);
			final String intermediario = telaModoCasual.getResources().getString(R.string.intermediario);
			final String dan1 = telaModoCasual.getResources().getString(R.string.dan1);
			final String dan2 = telaModoCasual.getResources().getString(R.string.dan2);
			final String dan3 = telaModoCasual.getResources().getString(R.string.dan3);
			final String dan4 = telaModoCasual.getResources().getString(R.string.dan4);
			final String dan5 = telaModoCasual.getResources().getString(R.string.dan5);
			final String dan6 = telaModoCasual.getResources().getString(R.string.dan6);
			final String dan7 = telaModoCasual.getResources().getString(R.string.dan7);
			final String dan8 = telaModoCasual.getResources().getString(R.string.dan8);
			final String dan9 = telaModoCasual.getResources().getString(R.string.dan9);
			final String dan10 = telaModoCasual.getResources().getString(R.string.dan10);
			
			if(danCriador.compareTo(iniciante) == 0)
			{
				danCriador = "Ranking_1";
			}
			else if(danCriador.compareTo(intermediario) == 0)
			{
				danCriador = "Ranking_2";
			}
			else if(danCriador.compareTo(dan1) == 0)
			{
				danCriador = "Ranking_3";
			}
			else if(danCriador.compareTo(dan2) == 0)
			{
				danCriador = "Ranking_4";
			}
			else if(danCriador.compareTo(dan3) == 0)
			{
				danCriador = "Ranking_5";
			}
			else if(danCriador.compareTo(dan4) == 0)
			{
				danCriador = "Ranking_6";
			}
			else if(danCriador.compareTo(dan5) == 0)
			{
				danCriador = "Ranking_7";
			}
			else if(danCriador.compareTo(dan6) == 0)
			{
				danCriador = "Ranking_8";
			}
			else if(danCriador.compareTo(dan7) == 0)
			{
				danCriador = "Ranking_9";
			}
			else if(danCriador.compareTo(dan8) == 0)
			{
				danCriador = "Ranking_10";
			}
			else if(danCriador.compareTo(dan9) == 0)
			{
				danCriador = "Ranking_11";
			}
			else
			{
				danCriador = "Ranking_12";
			}
            
            nameValuePairs.add(new BasicNameValuePair("dan_do_criador", danCriador));
            nameValuePairs.add(new BasicNameValuePair("quantas_rodadas", rodadas));
            nameValuePairs.add(new BasicNameValuePair("sala_aberta", sala_aberta));
            
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost); 
            
            
            //agora vamos pegar o id da sala que acabamos de criar
            HttpClient httpClient2 = new DefaultHttpClient();

            ArrayList<NameValuePair> nameValuePairs2 = new ArrayList<NameValuePair>();
            HttpPost httpPost2 = new HttpPost(url_pegarIdSalaCriada);
            nameValuePairs2.add(new BasicNameValuePair("id_usuario", id_usuario));
            
            httpPost2.setEntity(new UrlEncodedFormEntity(nameValuePairs2,"UTF-8"));
            HttpResponse httpResponse2 = httpClient2.execute(httpPost2); 
            HttpEntity httpEntity = httpResponse2.getEntity();

            InputStream inputStream;
            inputStream = httpEntity.getContent();
            this.obterIdSalaDoResultadoDoBd(inputStream);
            
            
            //agora que obtemos o id da sala, podemos finalmente terminar de criar a sala colocando as categorias no BD tb
            //as categorias que colocaremos no banco nao sao os nomes delas, mas sim os ids
            String idsCategoriasSelecionadas = extrairIdsCategoriasSelecionadas(categoriasselecionadas);
            HttpClient httpClient3 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs3 = new ArrayList<NameValuePair>();
            HttpPost httpPost3 = new HttpPost(url_inserirCategoriasSalaCriada);
            nameValuePairs3.add(new BasicNameValuePair("categoriasselecionadas", idsCategoriasSelecionadas));
            nameValuePairs3.add(new BasicNameValuePair("id_sala", Integer.toString(this.id_sala_em_int)));
            
            httpPost3.setEntity(new UrlEncodedFormEntity(nameValuePairs3,"UTF-8"));
            HttpResponse httpResponse3 = httpClient3.execute(httpPost3); 
            
            
		}
		catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		 
		return "";
	}
	
	@Override
	protected void onPostExecute(String s) 
	{
		this.telaModoCasual.iniciarAberturaDeSalaCasualComId(id_sala_em_int);
	}
	
	private void obterIdSalaDoResultadoDoBd(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_sala = jObject.getString("id_sala");
            this.id_sala_em_int = Integer.valueOf(id_sala);

        } catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw e;
        }
	}
	
	private String extrairIdDoUsuarioQueQueremosAchar(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_usuario = jObject.getString("id_usuario");
           
            return id_usuario;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}
	
	/*de uma string cheia de categorias separadas por , nos iremos pegar os ids delas*/
	private String extrairIdsCategoriasSelecionadas(String categoriasSelecionadas)
	{
		SingletonArmazenaCategoriasDoJogo conheceAsCategoriasDoJogo = 
									SingletonArmazenaCategoriasDoJogo.getInstance();
		String[] categoriasSeparadas = categoriasSelecionadas.split(",");
		String idsCategoriasSeparadasPorVirgula = "";
		for(int i = 0; i < categoriasSeparadas.length; i++)
		{
			String umaCategoria = categoriasSeparadas[i];
			int idCategoria = conheceAsCategoriasDoJogo.pegarIdDaCategoria(umaCategoria);
			idsCategoriasSeparadasPorVirgula = idsCategoriasSeparadasPorVirgula + idCategoria;
			
			if(i < categoriasSeparadas.length - 1)
			{
				idsCategoriasSeparadasPorVirgula = idsCategoriasSeparadasPorVirgula + ",";
			}
		}
		
		return idsCategoriasSeparadasPorVirgula;
	}
}
