package bancodedados;

import java.util.LinkedList;

public class DadosPartidaParaOLog 
{
	private String nome_usuario; //se bem que vou precisar do id do usuario, nao do nome
	private String data;
	private String id_categorias_tudo_junto_separado_por_virgula; //pode ser mais de uma categoria separadas por , Ex: "1, 2,..."
	private int pontuacao;
	private LinkedList<KanjiTreinar> palavrasAcertadas;
	private LinkedList<KanjiTreinar> palavrasErradas;
	private LinkedList<KanjiTreinar> palavrasJogadas;
	private String nome_adversario;
	private String voceGanhouOuPerdeu; //ganhou,perdeu ou empatou
	
	public String getnome_usuario() {
		return nome_usuario;
	}
	public void setnome_usuario(String nome_usuario) {
		this.nome_usuario = nome_usuario;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getid_categorias_tudo_junto_separado_por_virgula() {
		return id_categorias_tudo_junto_separado_por_virgula;
	}
	public void setid_categorias_tudo_junto_separado_por_virgula(String id_categorias_tudo_junto_separado_por_virgula) {
		this.id_categorias_tudo_junto_separado_por_virgula = id_categorias_tudo_junto_separado_por_virgula;
	}
	public int getPontuacao() {
		return pontuacao;
	}
	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}
	public LinkedList<KanjiTreinar> getPalavrasAcertadas() {
		return palavrasAcertadas;
	}
	public void setPalavrasAcertadas(LinkedList<KanjiTreinar> palavrasAcertadas) {
		this.palavrasAcertadas = palavrasAcertadas;
	}
	public LinkedList<KanjiTreinar> getPalavrasErradas() {
		return palavrasErradas;
	}
	public void setPalavrasErradas(LinkedList<KanjiTreinar> palavrasErradas) {
		this.palavrasErradas = palavrasErradas;
	}
	public LinkedList<KanjiTreinar> getPalavrasJogadas() {
		return palavrasJogadas;
	}
	public void setPalavrasJogadas(LinkedList<KanjiTreinar> palavrasJogadas) {
		this.palavrasJogadas = palavrasJogadas;
	}
	public String getnome_adversario() {
		return nome_adversario;
	}
	public void setnome_adversario(String nome_adversario) {
		this.nome_adversario = nome_adversario;
	}
	public String getVoceGanhouOuPerdeu() {
		return voceGanhouOuPerdeu;
	}
	public void setVoceGanhouOuPerdeu(String voceGanhouOuPerdeu) {
		this.voceGanhouOuPerdeu = voceGanhouOuPerdeu;
	}

}
