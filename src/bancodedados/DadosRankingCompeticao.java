package bancodedados;


/*apos a task que pega os 20 melhores usuarios terminar seu servic, ela vai criando varios objetos dessa classe e envia para a tela que mostra o ranking*/
public class DadosRankingCompeticao 
{
	private String nome_usuario;
	private int posicao_no_ranking;
	private int pontuacao_total;
	private int vitorias_competicao;
	private int derrotas_competicao;
	
	public String getNome_usuario() {
		return nome_usuario;
	}
	public void setNome_usuario(String nome_usuario) {
		this.nome_usuario = nome_usuario;
	}
	public int getPosicao_no_ranking() {
		return posicao_no_ranking;
	}
	public void setPosicao_no_ranking(int posicao_no_ranking) {
		this.posicao_no_ranking = posicao_no_ranking;
	}
	public int getPontuacao_total() {
		return pontuacao_total;
	}
	public void setPontuacao_total(int pontuacao_total) {
		this.pontuacao_total = pontuacao_total;
	}
	public int getVitorias_competicao() {
		return vitorias_competicao;
	}
	public void setVitorias_competicao(int vitorias_competicao) {
		this.vitorias_competicao = vitorias_competicao;
	}
	public int getDerrotas_competicao() {
		return derrotas_competicao;
	}
	public void setDerrotas_competicao(int derrotas_competicao) {
		this.derrotas_competicao = derrotas_competicao;
	}
	

}
