package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.karutakanji.MainActivity;

import android.os.AsyncTask;
import android.util.Log;

public class EnviarDadosDaPartidaCompeticaoParaLogTask extends AsyncTask<DadosPartidaParaOLog, String, String> 
{
	
	@Override
	protected String doInBackground(DadosPartidaParaOLog... dadosPartida) 
	{
		DadosPartidaParaOLog umDadosPartida = dadosPartida[0];
		String categorias_tudo_junto_separado_por_virgula = umDadosPartida.getid_categorias_tudo_junto_separado_por_virgula();
		String nome_usuario = umDadosPartida.getnome_usuario();
		String nome_adversario = umDadosPartida.getnome_adversario();
		String data = umDadosPartida.getData();
		String pontuacao = String.valueOf(umDadosPartida.getPontuacao());
		
		LinkedList<KanjiTreinar> palavrasAcertadas = umDadosPartida.getPalavrasAcertadas();
		LinkedList<KanjiTreinar> palavrasErradas = umDadosPartida.getPalavrasErradas();
		LinkedList<KanjiTreinar> palavrasJogadas = umDadosPartida.getPalavrasJogadas();
		
		String voceganhououperdeu = umDadosPartida.getVoceGanhouOuPerdeu();
		
		String url_inserirpartidanolog = "http://server.karutakanji.pairg.dimap.ufrn.br/app/inserirpartidanologcompeticao.php";//android nao aceita localhost, tem de ser seu IP
		String url_inserirpartidanologcategorias = "http://server.karutakanji.pairg.dimap.ufrn.br/app/inserirpartidanologcategoriascompeticao.php";
		String url_pegarusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuariopornome.php";
		String url_inserirpartidanologpalavras = "http://server.karutakanji.pairg.dimap.ufrn.br/app/inserirpartidanologpalavrascompeticao.php";
		String url_pegaridpartidacasualatravesdenomeusuario = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegaridpartidacompeticaoatravesdenomeusuario.php";
		//String url_select = "http://192.168.0.110/amit/inserirpartidanolog.php";//android nao aceita localhost, tem de ser seu IP
		
		try
		{
			//primeiro, vamos pegar o id do usuario
			HttpClient httpClient0 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs0 = new ArrayList<NameValuePair>();
            HttpPost httpPost0 = new HttpPost(url_pegarusuariopornome);
            nameValuePairs0.add(new BasicNameValuePair("nome_usuario", nome_usuario));
            
            httpPost0.setEntity(new UrlEncodedFormEntity(nameValuePairs0,"UTF-8"));
            HttpResponse httpResponse0 = httpClient0.execute(httpPost0); 
            HttpEntity httpEntity0 = httpResponse0.getEntity();
            InputStream inputStream0;
            inputStream0 = httpEntity0.getContent();	
            String id_usuario = this.extrairIdDoUsuarioQueQueremosAchar(inputStream0);
			
			//agora vamos pegar o id do adversario
            HttpClient httpClient1 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
            HttpPost httpPost1 = new HttpPost(url_pegarusuariopornome);
            nameValuePairs1.add(new BasicNameValuePair("nome_usuario", nome_adversario));
            
            httpPost1.setEntity(new UrlEncodedFormEntity(nameValuePairs1,"UTF-8"));
            HttpResponse httpResponse1 = httpClient1.execute(httpPost1); 
            HttpEntity httpEntity1 = httpResponse1.getEntity();
            InputStream inputStream1;
            inputStream1 = httpEntity1.getContent();	
            String id_adversario = this.extrairIdDoUsuarioQueQueremosAchar(inputStream1);
            
            //agora vamos inserir um novo valor na tabela partidas
            
            HttpClient httpClient2 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs2 = new ArrayList<NameValuePair>();
            HttpPost httpPost2 = new HttpPost(url_inserirpartidanolog);
            nameValuePairs2.add(new BasicNameValuePair("id_usuario", id_usuario));
            nameValuePairs2.add(new BasicNameValuePair("data", data));
            nameValuePairs2.add(new BasicNameValuePair("pontuacao", pontuacao));
            nameValuePairs2.add(new BasicNameValuePair("id_adversario", id_adversario));
            nameValuePairs2.add(new BasicNameValuePair("voceganhououperdeu", voceganhououperdeu));
            
            httpPost2.setEntity(new UrlEncodedFormEntity(nameValuePairs2,"UTF-8"));
            httpClient2.execute(httpPost2); 
            
            //agora vamos pegar o id da partida que acabamos de criar
            
            HttpClient httpClient3 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs3 = new ArrayList<NameValuePair>();
            HttpPost httpPost3 = new HttpPost(url_pegaridpartidacasualatravesdenomeusuario);
            nameValuePairs3.add(new BasicNameValuePair("nome_usuario", nome_usuario));
            
            httpPost3.setEntity(new UrlEncodedFormEntity(nameValuePairs3,"UTF-8"));
            HttpResponse httpResponse3 = httpClient3.execute(httpPost3); 
            HttpEntity httpEntity3 = httpResponse3.getEntity();
            InputStream inputStream3;
            inputStream3 = httpEntity3.getContent();	
            String id_partida = this.extrairIdDaPartidaQueQueremosAchar(inputStream3);
            
            
            //agora vamos adicionar uma nova entrada na tabela partidas_e_categoria
            HttpClient httpClient4 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs4 = new ArrayList<NameValuePair>();
            HttpPost httpPost4 = new HttpPost(url_inserirpartidanologcategorias);
            nameValuePairs4.add(new BasicNameValuePair("id_partida", id_partida));
            nameValuePairs4.add(new BasicNameValuePair("categorias", categorias_tudo_junto_separado_por_virgula));
            
            httpPost4.setEntity(new UrlEncodedFormEntity(nameValuePairs4,"UTF-8"));
            httpClient4.execute(httpPost4); 
            
            //agora vamos sair formando 3 strings: palavras, categorias e treinadaerradaouacertadas. Cada um deles terah o mesmo tamanho e englobam todas as palavras referentes a partida. Seja errada ou treinada ou acertada
            //string palavras: as palavras da partida(em kanji)
            //string categorias: os ids das categorias associadas a cada palavra
            //string treinadaerradaouacertadas: se a palavra foi errada, treinada ou acertada
            String palavras = "";
            String categorias = "";
            String treinadaerradaouacertadas = "";
            SingletonArmazenaCategoriasDoJogo conheceCategorias = 
            					SingletonArmazenaCategoriasDoJogo.getInstance();
            for(int j = 0; j < palavrasAcertadas.size(); j++)
            {
            	KanjiTreinar umaPalavraAcertada = palavrasAcertadas.get(j);
            	palavras = palavras + umaPalavraAcertada.getKanji() + ",";
            	String categoriaUmaPalavra = umaPalavraAcertada.getCategoriaAssociada();
            	String id_categoria = String.valueOf(conheceCategorias.pegarIdDaCategoria(categoriaUmaPalavra));
            	
            	categorias = categorias + id_categoria + ",";
            	treinadaerradaouacertadas = treinadaerradaouacertadas + "acertada" + ",";
            }
            for(int k = 0; k < palavrasErradas.size(); k++)
            {
            	KanjiTreinar umaPalavraErrada = palavrasErradas.get(k);
            	palavras = palavras + umaPalavraErrada.getKanji() + ",";
            	String categoriaUmaPalavra = umaPalavraErrada.getCategoriaAssociada();
            	String id_categoria = String.valueOf(conheceCategorias.pegarIdDaCategoria(categoriaUmaPalavra));
            	
            	categorias = categorias + id_categoria + ",";
            	treinadaerradaouacertadas = treinadaerradaouacertadas + "errada" + ",";
            }
            for(int l = 0; l < palavrasJogadas.size(); l++)
            {
            	KanjiTreinar umaPalavraTreinada = palavrasJogadas.get(l);
            	palavras = palavras + umaPalavraTreinada.getKanji();
            	String categoriaUmaPalavra = umaPalavraTreinada.getCategoriaAssociada();
            	String id_categoria = String.valueOf(conheceCategorias.pegarIdDaCategoria(categoriaUmaPalavra));
            	
            	categorias = categorias + id_categoria;
            	treinadaerradaouacertadas = treinadaerradaouacertadas + "treinada";
            	
            	if(l < palavrasJogadas.size() - 1)
            	{
            		palavras = palavras + ",";
            		categorias = categorias + ",";
            		treinadaerradaouacertadas = treinadaerradaouacertadas + ",";
            	}
            }
            
            //agora finalmente vamos concluir a criacao de um log de partidas inserindo um valor novo na tabela partidas_palavras
            HttpClient httpClient5 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs5 = new ArrayList<NameValuePair>();
            HttpPost httpPost5 = new HttpPost(url_inserirpartidanologpalavras);
            nameValuePairs5.add(new BasicNameValuePair("categorias", categorias));
            nameValuePairs5.add(new BasicNameValuePair("palavras", palavras));
            nameValuePairs5.add(new BasicNameValuePair("treinadaerradaouacertadas", treinadaerradaouacertadas));
            nameValuePairs5.add(new BasicNameValuePair("id_partida", id_partida));
            
            httpPost5.setEntity(new UrlEncodedFormEntity(nameValuePairs5,"UTF-8"));
            httpClient5.execute(httpPost5); 
		}
		catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
		catch(Exception e)
		{
			Log.e("Exception", e.toString());
            e.printStackTrace();
		}
		
		 
		return "";
	}
	
	private String extrairIdDoUsuarioQueQueremosAchar(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_usuario = jObject.getString("id_usuario");
           
            return id_usuario;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}
	
	private String extrairIdDaPartidaQueQueremosAchar(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_partida = jObject.getString("ultima_partida");
           
            return id_partida;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}
	

}
