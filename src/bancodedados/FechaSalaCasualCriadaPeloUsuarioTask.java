package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.karutakanji.ModoCasual;

import android.os.AsyncTask;
import android.util.Log;

/*em algum momento, seja pela desistencia do usuario ou porque a partida comecou, serah necessario tirar a sala criada anteriormente pelo usuario do BD*/
public class FechaSalaCasualCriadaPeloUsuarioTask extends AsyncTask<String, String, String>
{
	private ModoCasual modoCasual;
	
	public FechaSalaCasualCriadaPeloUsuarioTask(ModoCasual modoCasual)
	{
		this.modoCasual = modoCasual;
	}
	
	@Override
	protected String doInBackground(String... emailUsuario) 
	{
		
		try
		{
			String url_pegarusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuariopornome.php";//android nao aceita localhost, tem de ser seu IP
			
			//primeiro vamos pegar o id do usuario com base no seu email
			LinkedList<String> nomeEEMailUsuario = ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(this.modoCasual);
			String nomeCriador = nomeEEMailUsuario.get(0);
			HttpClient httpClient0 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs0 = new ArrayList<NameValuePair>();
	        HttpPost httpPost0 = new HttpPost(url_pegarusuariopornome);
	        nameValuePairs0.add(new BasicNameValuePair("nome_usuario", nomeCriador));
	        
	        httpPost0.setEntity(new UrlEncodedFormEntity(nameValuePairs0,"UTF-8"));
	        HttpResponse httpResponse0 = httpClient0.execute(httpPost0); 
	        HttpEntity httpEntity0 = httpResponse0.getEntity();
	        InputStream inputStream0;
	        inputStream0 = httpEntity0.getContent();	
	        String id_usuario = this.extrairIdDoUsuarioQueQueremosAchar(inputStream0);
			
			
			String url_select = "http://server.karutakanji.pairg.dimap.ufrn.br/app/fechasalacasualcriadapelousuario.php";//android nao aceita localhost, tem de ser seu IP
			//String url_select = "http://192.168.0.110/amit/inserirpartidanolog.php";//android nao aceita localhost, tem de ser seu IP
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url_select);
            nameValuePairs.add(new BasicNameValuePair("id_usuario", id_usuario));
            	
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost); 
		}
		catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
		catch (Exception e)
		{
			Log.e("IOException", e.toString());
			e.printStackTrace();
		}
		
		 
		return "";
	}
	
	private String extrairIdDoUsuarioQueQueremosAchar(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_usuario = jObject.getString("id_usuario");
           
            return id_usuario;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}
}
