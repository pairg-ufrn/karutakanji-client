package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class InserirPartidaTreinamentoNoLogTask extends AsyncTask<String, String, String>
{
	private String nome_usuario;
	private String data_partida;
	private String hora_partida;
	private String ate_que_nivel_chegou;
	private String idsKanjisSeparadosPorVirgula;
	private String idsCategoriasKanjisSeparadosPorVirgula;
	private String seAcertouKanjisSeparadosPorVirgula; //sim,n�o,sim...
	private int id_partida;
	
	public InserirPartidaTreinamentoNoLogTask(String nome_usuario, String data_partida, String hora_partida,
			String ate_que_nivel_chegou, String idsKanjisSeparadosPorVirgula, String idsCategoriasKanjisSeparadosPorVirgula, 
															String seAcertouKanjisSeparadosPorVirgula)
	{
		this.nome_usuario = nome_usuario;
		this.data_partida = data_partida;
		this.hora_partida = hora_partida;
		this.ate_que_nivel_chegou = ate_que_nivel_chegou;
		this.idsKanjisSeparadosPorVirgula = idsKanjisSeparadosPorVirgula;
		this.idsCategoriasKanjisSeparadosPorVirgula = idsCategoriasKanjisSeparadosPorVirgula;
		this.seAcertouKanjisSeparadosPorVirgula = seAcertouKanjisSeparadosPorVirgula;
	}
	
	@Override
	protected String doInBackground(String... stringVazia) 
	{
		String url_pegarusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuariopornome.php";//android nao aceita localhost, tem de ser seu IP
		String url_inserirpartidatreinamentonolog = "http://server.karutakanji.pairg.dimap.ufrn.br/app/inserirpartidatreinamentonolog.php";//android nao aceita localhost, tem de ser seu IP
		String url_pegaridpartidatreinamentocriadapelousuario = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegaridpartidatreinamentocriadapelousuario.php";
		String url_inserirpartidatreinamentonologpalavraserradas = "http://server.karutakanji.pairg.dimap.ufrn.br/app/inserirpartidatreinamentonologpalavraserradas.php";
		
		//String url_select = "http://192.168.0.110/amit/inserirpartidanolog.php";//android nao aceita localhost, tem de ser seu IP
		
		try
		{
			//primeiro temos de pegar o id do usu�rio no banco
			HttpClient httpClient0 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs0 = new ArrayList<NameValuePair>();
            HttpPost httpPost0 = new HttpPost(url_pegarusuariopornome);
            nameValuePairs0.add(new BasicNameValuePair("nome_usuario", nome_usuario));
            
            httpPost0.setEntity(new UrlEncodedFormEntity(nameValuePairs0,"UTF-8"));
            HttpResponse httpResponse0 = httpClient0.execute(httpPost0); 
            HttpEntity httpEntity0 = httpResponse0.getEntity();
            InputStream inputStream0;
            inputStream0 = httpEntity0.getContent();
            
            
            int idUsuarioNoBd = this.extrairIdDoUsuarioNoBd(inputStream0);
			String id_usuario = String.valueOf(idUsuarioNoBd);
            
			//agora vamos criar a partida sem as palavras
			HttpClient httpClient1 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
            HttpPost httpPost1 = new HttpPost(url_inserirpartidatreinamentonolog);
            nameValuePairs1.add(new BasicNameValuePair("id_usuario", id_usuario));
            nameValuePairs1.add(new BasicNameValuePair("data_partida", this.data_partida));
            nameValuePairs1.add(new BasicNameValuePair("hora_partida", this.hora_partida));
            nameValuePairs1.add(new BasicNameValuePair("ate_que_nivel_chegou", this.ate_que_nivel_chegou));
            
            httpPost1.setEntity(new UrlEncodedFormEntity(nameValuePairs1,"UTF-8"));
            HttpResponse response1 = httpClient1.execute(httpPost1); 
            HttpEntity entity1 = response1.getEntity();
            String mensagemEntity1 = EntityUtils.toString(entity1);
            mensagemEntity1 = mensagemEntity1 + "";
            
            //agora vamos pegar o id da sala que acabamos de criar
            HttpClient httpClient2 = new DefaultHttpClient();

            ArrayList<NameValuePair> nameValuePairs2 = new ArrayList<NameValuePair>();
            HttpPost httpPost2 = new HttpPost(url_pegaridpartidatreinamentocriadapelousuario);
            nameValuePairs2.add(new BasicNameValuePair("id_usuario", id_usuario));
            
            httpPost2.setEntity(new UrlEncodedFormEntity(nameValuePairs2,"UTF-8"));
            HttpResponse httpResponse2 = httpClient2.execute(httpPost2); 
            HttpEntity httpEntity = httpResponse2.getEntity();

            InputStream inputStream;
            inputStream = httpEntity.getContent();
            this.obterIdSalaDoResultadoDoBd(inputStream);
            
            
            //agora que obtemos o id da partida, podemos finalmente terminar de criar a partida colocando os kanjis no BD tb
            HttpClient httpClient3 = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs3 = new ArrayList<NameValuePair>();
            HttpPost httpPost3 = new HttpPost(url_inserirpartidatreinamentonologpalavraserradas);
            nameValuePairs3.add(new BasicNameValuePair("ids_kanjis", this.idsKanjisSeparadosPorVirgula));
            nameValuePairs3.add(new BasicNameValuePair("ids_categorias_kanjis", this.idsCategoriasKanjisSeparadosPorVirgula));
            nameValuePairs3.add(new BasicNameValuePair("se_acertou_ou_errou", this.seAcertouKanjisSeparadosPorVirgula));
            nameValuePairs3.add(new BasicNameValuePair("id_partidas_treinamento", Integer.toString(this.id_partida)));
            
            httpPost3.setEntity(new UrlEncodedFormEntity(nameValuePairs3,"UTF-8"));
            HttpResponse httpResponse3 = httpClient3.execute(httpPost3); 
            HttpEntity entidade = httpResponse3.getEntity();
            String echoado = EntityUtils.toString(entidade);
            echoado = echoado + "";
		}
		catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		 
		return "";
	}
	
	@Override
	protected void onPostExecute(String s) 
	{
		
	}
	
	private void obterIdSalaDoResultadoDoBd(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            int id_partida_obtido = jObject.getInt("id_partidas_treinamento");
            this.id_partida = id_partida_obtido;

        } catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw e;
        }
	}
	
	private int extrairIdDoUsuarioNoBd(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            int id_usuario = jObject.getInt("id_usuario");
            return id_usuario;

        } catch (Exception e) 
        {
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw e;
        }
	}
	
	
}
