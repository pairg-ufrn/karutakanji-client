package bancodedados;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.karutakanji.MainActivity;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

public class InserirUsuarioNoBDTask extends AsyncTask<String, String, String> 
{
	MainActivity activityInicial;
	private boolean nomeUsuarioJaExiste;
	private boolean emailUsuarioJaExiste;
	
	public InserirUsuarioNoBDTask(MainActivity activity)
	{
		this.activityInicial = activity;
	}
	
	@Override
	protected String doInBackground(String... nomeUsuarioEEmailESenha) 
	{
		String nome_usuario = nomeUsuarioEEmailESenha[0];
		String email_usuario = nomeUsuarioEEmailESenha[1];
		String senha_usuario = nomeUsuarioEEmailESenha[2];
		
		String url_pegarusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuariopornome.php";//android nao aceita localhost, tem de ser seu IP
		String url_inserirusuarionobd = "http://server.karutakanji.pairg.dimap.ufrn.br/app/inserirusuarionobd.php";//android nao aceita localhost, tem de ser seu IP
		String url_inserirusuarionorankingprimeiravez = "http://server.karutakanji.pairg.dimap.ufrn.br/app/inserirusuarionorankingprimeiravez.php";
		String url_pegarusuarioporemail = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuarioporemail.php";//android nao aceita localhost, tem de ser seu IP
		
		
		try
		{
			
			//primeiro, veremos se nao ja existe um usuario com esse nome e email
			this.nomeUsuarioJaExiste = false;
			this.emailUsuarioJaExiste = false;
			
			HttpClient httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            HttpPost httpPost = new HttpPost(url_pegarusuariopornome);
            nameValuePairs.add(new BasicNameValuePair("nome_usuario", nome_usuario));
            
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost); 
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream;
            inputStream = httpEntity.getContent();
            this.nomeUsuarioJaExiste = this.nomeUsuarioExisteNoBd(inputStream);
            
            if(this.nomeUsuarioJaExiste == true)
            {
            	//faz nada
            }
            else
            {
            	//e o email do usuarios era que existe?
            	
            	HttpClient httpClient1_1 = new DefaultHttpClient();
    			
    			ArrayList<NameValuePair> nameValuePairs1_1 = new ArrayList<NameValuePair>();
                HttpPost httpPost1_1 = new HttpPost(url_pegarusuarioporemail);
                nameValuePairs1_1.add(new BasicNameValuePair("email_usuario", email_usuario));
                
                httpPost1_1.setEntity(new UrlEncodedFormEntity(nameValuePairs1_1,"UTF-8"));
                HttpResponse httpResponse1_1 = httpClient1_1.execute(httpPost1_1); 
                HttpEntity httpEntity1_1 = httpResponse1_1.getEntity();
                InputStream inputStream1_1;
                inputStream1_1 = httpEntity1_1.getContent();
                this.emailUsuarioJaExiste = this.emailUsuarioExisteNoBd(inputStream1_1);
            	
            	if(this.emailUsuarioJaExiste == true)
            	{
            		//faz nada
            	}
            	else
            	{
            		//nome e email do usuario nao existem no bd
            		//agora vamos inserir novo usuario no banco
        			HttpClient httpClient2 = new DefaultHttpClient();
        			
        			ArrayList<NameValuePair> nameValuePairs2 = new ArrayList<NameValuePair>();
                    HttpPost httpPost2 = new HttpPost(url_inserirusuarionobd);
                    nameValuePairs2.add(new BasicNameValuePair("nome_usuario", nome_usuario));
                    nameValuePairs2.add(new BasicNameValuePair("email_usuario", email_usuario));
                    nameValuePairs2.add(new BasicNameValuePair("senha_usuario", senha_usuario));
                    
                    httpPost2.setEntity(new UrlEncodedFormEntity(nameValuePairs2,"UTF-8"));
                    HttpResponse httpResponse2 = httpClient2.execute(httpPost2); 
                    
                    nomeUsuarioJaExiste = false;
                    
                    //COISA NOVA: FALTA INSERIR O USUARIO NO RANKING DO JOGO PELA PRIMEIRA VEZ
                    //PRIMEIRO TEMOS DE SABER O ID DELE
                    
                  //primeiro vamos pegar o id do usuario com base no seu nome
        			HttpClient httpClient3 = new DefaultHttpClient();
        			
        			ArrayList<NameValuePair> nameValuePairs3 = new ArrayList<NameValuePair>();
                    HttpPost httpPost3 = new HttpPost(url_pegarusuariopornome);
                    nameValuePairs3.add(new BasicNameValuePair("nome_usuario", nome_usuario));
                    
                    httpPost3.setEntity(new UrlEncodedFormEntity(nameValuePairs3,"UTF-8"));
                    HttpResponse httpResponse3 = httpClient3.execute(httpPost3); 
                    HttpEntity httpEntity3 = httpResponse3.getEntity();
                    InputStream inputStream3;
                    inputStream3 = httpEntity3.getContent();	
                    String id_usuario = this.extrairIdDoUsuarioQueQueremosAchar(inputStream3);
                    
                    HttpClient httpClient4 = new DefaultHttpClient();
        			
                    ArrayList<NameValuePair> nameValuePairs4 = new ArrayList<NameValuePair>();
                    HttpPost httpPost4 = new HttpPost(url_inserirusuarionorankingprimeiravez);
                    nameValuePairs4.add(new BasicNameValuePair("id_usuario", id_usuario));
                    
                    httpPost4.setEntity(new UrlEncodedFormEntity(nameValuePairs4,"UTF-8"));
                    httpClient4.execute(httpPost4); 
                    
            	}
                
            }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String s) 
	{
		if(this.nomeUsuarioJaExiste == false && this.emailUsuarioJaExiste == false)
		{
			this.activityInicial.terminarFazerCadastro();
		}
		else if(this.nomeUsuarioJaExiste == true)
		{
			activityInicial.mostrarMensagemNomeDeUsuarioJaExiste();
		}
		else if(this.emailUsuarioJaExiste == true)
		{
			activityInicial.mostrarMensagemEmailDeUsuarioJaExiste();
		}
		
	}
	
	private boolean nomeUsuarioExisteNoBd(InputStream inputStream)
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_usuario = jObject.getString("id_usuario");
            return true;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            return false;
        }
	}
	
	private boolean emailUsuarioExisteNoBd(InputStream inputStream)
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_usuario = jObject.getString("id_usuario");
            return true;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            return false;
        }
	}
	
	private String extrairIdDoUsuarioQueQueremosAchar(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String id_usuario = jObject.getString("id_usuario");
           
            return id_usuario;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}
}
