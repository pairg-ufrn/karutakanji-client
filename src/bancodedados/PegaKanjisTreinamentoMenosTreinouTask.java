package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karutakanji.ModoTreinamento;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

/*essa classe irah pegar no BD todos os kanjis que o usuario menos treinou no modo treinamento e nas categorias que o usuario quis treinar*/
public class PegaKanjisTreinamentoMenosTreinouTask extends AsyncTask<String, String, String>
{
	private InputStream inputStream;
	private String result;
	private ModoTreinamento telaModoTreinamento;
	
	public PegaKanjisTreinamentoMenosTreinouTask(ModoTreinamento telaModoTreinamento)
	{
		this.telaModoTreinamento = telaModoTreinamento;
	}
	
	/*entrada: "andrews","Cotidiano,Lugar..."*/
	@Override
	protected String doInBackground(String... nome_usuario_e_categorias_separadas_por_virgula_entrada) 
	{
		String url_select = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarkanjismenostreinados.php";//android nao aceita localhost, tem de ser seu IP
		//String url_select = "http://192.168.0.110/amit/pegarlogjogadorjson.php";//android nao aceita localhost, tem de ser seu IP
	    
		
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

	        try {
	            // Set up HTTP post

	            // HttpClient is more then less deprecated. Need to change to URLConnection
	            HttpClient httpClient = new DefaultHttpClient();

	            HttpPost httpPost = new HttpPost(url_select);
	            
	            String nome_usuario = nome_usuario_e_categorias_separadas_por_virgula_entrada[0];
	            String categorias_escolhidas_separadas_por_virgula = nome_usuario_e_categorias_separadas_por_virgula_entrada[1];
	            param.add(new BasicNameValuePair("nome_usuario", nome_usuario));
	            param.add(new BasicNameValuePair("categoriasselecionadas", categorias_escolhidas_separadas_por_virgula));
	            
	            httpPost.setEntity(new UrlEncodedFormEntity(param));
	            HttpResponse httpResponse = httpClient.execute(httpPost);
	            HttpEntity httpEntity = httpResponse.getEntity();
	            /*String mensagemEntity1 = EntityUtils.toString(httpEntity);
	            mensagemEntity1 = mensagemEntity1 + "";*/
	           

	            // Read content & Log
	            inputStream = httpEntity.getContent();
	        } catch (UnsupportedEncodingException e1) {
	            Log.e("UnsupportedEncodingException", e1.toString());
	            e1.printStackTrace();
	        } catch (ClientProtocolException e2) {
	            Log.e("ClientProtocolException", e2.toString());
	            e2.printStackTrace();
	        } catch (IllegalStateException e3) {
	            Log.e("IllegalStateException", e3.toString());
	            e3.printStackTrace();
	        } catch (IOException e4) {
	            Log.e("IOException", e4.toString());
	            e4.printStackTrace();
	        }
	        // Convert response to string using String Builder
	        try {
	            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
	            StringBuilder sBuilder = new StringBuilder();

	            String line = null;
	            while ((line = bReader.readLine()) != null) {
	            	if(line.startsWith("<meta") == false)//pula linha de metadados
	            	{
	            		 sBuilder.append(line + "\n");
	            	}
	               
	            }

	            inputStream.close();
	            String resultadoComMetadados = sBuilder.toString();
	            //tirar os metadados
	            String [] stringSeparadoColchete = resultadoComMetadados.split("\\[");
	            result = "[" + stringSeparadoColchete[1];
	            result = result + "";

	        } catch (Exception e) {
	            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
	        }
	       
	        
			return null;
	   } 
	
	@Override
	protected void onPostExecute(String s) 
	{
		        //parse JSON data
		        try {
		            JSONArray jArray = new JSONArray(result);
		            LinkedList<KanjiTreinar> kanjisQueUsuarioMenosTreinouEmOrdemDoMenosTreinadoParaOMais = 
		            															new LinkedList<KanjiTreinar>();
		            for(int i=0; i < jArray.length(); i++) {

		                JSONObject jObject = jArray.getJSONObject(i);
		                
		                String nomeUmKanjiQueUsuarioErrou = jObject.getString("kanji");
		                String categoriaUmKanjiErrou = "";
		                Resources res = telaModoTreinamento.getResources();
		                Locale myLocale = res.getConfiguration().locale;
		                if(myLocale != null)
		        		{
		        			String language = myLocale.getLanguage();
		        			if(myLocale.getLanguage().compareTo("en") == 0)
		        			{
		        				//pegar traducao e categorias em ingles!
		        				categoriaUmKanjiErrou = jObject.getString("nome_categoria_ingles");
		        			}
		        			else if(myLocale.getLanguage().compareTo("jp") == 0)
		        			{
		        				//pegar traducao e categorias em japones!
		        				categoriaUmKanjiErrou = jObject.getString("nome_categoria_japones");
		        			}
		        			else if(myLocale.getLanguage().compareTo("pt") == 0)
		        			{
		        				//br
		        				categoriaUmKanjiErrou = jObject.getString("nome_categoria");
		        			}
		        			else
		        			{
		        				categoriaUmKanjiErrou = jObject.getString("nome_categoria_ingles");
		        			}
		        		}
		                else
		                {
		                	//se teve algum erro, vamos pegar em ingles mesmo
		                	categoriaUmKanjiErrou = jObject.getString("nome_categoria_ingles");
		                }
		                
		            	ArmazenaKanjisPorCategoria conheceTodosOsKanjisDoJogo = 
		            							ArmazenaKanjisPorCategoria.pegarInstancia();
		            	
		            	SingletonArmazenaCategoriasDoJogo conheceCategoriasDoJogo =
    							SingletonArmazenaCategoriasDoJogo.getInstance();
		            	
		            	int id_categoria_kanji_errou = conheceCategoriasDoJogo.pegarIdDaCategoria(categoriaUmKanjiErrou);
		            	
		            	KanjiTreinar umKanjiQueUsuarioErrou = 
		            			conheceTodosOsKanjisDoJogo.acharKanji(id_categoria_kanji_errou, nomeUmKanjiQueUsuarioErrou);
		            	kanjisQueUsuarioMenosTreinouEmOrdemDoMenosTreinadoParaOMais.add(umKanjiQueUsuarioErrou);
		            	
		            } // End Loop
		            
		            this.telaModoTreinamento.terminouDeEncontrarTodosOskanjisUsuarioMenosTreinouNoTreinamento(kanjisQueUsuarioMenosTreinouEmOrdemDoMenosTreinadoParaOMais);
		           
		        } catch (JSONException e) {
		        	String erro = e.toString();
		        	erro = erro + "";
		            //Log.e("JSONException", "Error: " + e.toString());
		        }
		        
	}

	
}
