package bancodedados;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import br.ufrn.dimap.pairg.karutakanji.android.R;

import com.karutakanji.ModoCompeticao;

import android.os.AsyncTask;
import android.util.Log;

/*essa task pegarah quantas salas estao abertas no modo competicao com base no dan do usuario. O nome serve para nao contarmos as salas abertas pelo proprio usuario*/
public class PegaQuantasSalasAbertasCompeticaoComBaseNoDanTask extends AsyncTask<String, String, String>
{
	private ModoCompeticao telaCompeticao;
	private int quantasSalasAbertas;
	
	public PegaQuantasSalasAbertasCompeticaoComBaseNoDanTask (ModoCompeticao telaCompeticao)
	{
		this.telaCompeticao = telaCompeticao;
	}
	
	@Override
	protected String doInBackground(String... danUsuarioENome) 
	{
		
		String dan_usuario = danUsuarioENome[0];
		String nome_usuario = danUsuarioENome[1];
		
		String url_pegarquantassalasabertascompeticaocombasenodan = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarquantassalasabertascompeticaocombasenodan.php";//android nao aceita localhost, tem de ser seu IP
		
		try
		{
			//veremos se nao ja existe um usuario com esse nome
			HttpClient httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            HttpPost httpPost = new HttpPost(url_pegarquantassalasabertascompeticaocombasenodan);
            
            //antes de incluir o dan do usuario no bd, devemos transformar de volta no que ele era antes de obtermos. Ou seja: Ranking_1,Ranking_2...
            String Ranking_1 = telaCompeticao.getResources().getString(R.string.Ranking_1);
            String Ranking_2 = telaCompeticao.getResources().getString(R.string.Ranking_2);
            String Ranking_3 = telaCompeticao.getResources().getString(R.string.Ranking_3);
            String Ranking_4 = telaCompeticao.getResources().getString(R.string.Ranking_4);
            String Ranking_5 = telaCompeticao.getResources().getString(R.string.Ranking_5);
            String Ranking_6 = telaCompeticao.getResources().getString(R.string.Ranking_6);
            String Ranking_7 = telaCompeticao.getResources().getString(R.string.Ranking_7);
            String Ranking_8 = telaCompeticao.getResources().getString(R.string.Ranking_8);
            String Ranking_9 = telaCompeticao.getResources().getString(R.string.Ranking_9);
            String Ranking_10 = telaCompeticao.getResources().getString(R.string.Ranking_10);
            String Ranking_11 = telaCompeticao.getResources().getString(R.string.Ranking_11);
            String Ranking_12 = telaCompeticao.getResources().getString(R.string.Ranking_12);
            
            if(dan_usuario.compareTo(Ranking_1) == 0)
            {
            	dan_usuario = "Ranking_1";
            }
            else if(dan_usuario.compareTo(Ranking_2) == 0)
            {
            	dan_usuario = "Ranking_2";
            }
            else if(dan_usuario.compareTo(Ranking_3) == 0)
            {
            	dan_usuario = "Ranking_3";
            }
            else if(dan_usuario.compareTo(Ranking_4) == 0)
            {
            	dan_usuario = "Ranking_4";
            }
            else if(dan_usuario.compareTo(Ranking_5) == 0)
            {
            	dan_usuario = "Ranking_5";
            }
            else if(dan_usuario.compareTo(Ranking_6) == 0)
            {
            	dan_usuario = "Ranking_6";
            }
            else if(dan_usuario.compareTo(Ranking_7) == 0)
            {
            	dan_usuario = "Ranking_7";
            }
            else if(dan_usuario.compareTo(Ranking_8) == 0)
            {
            	dan_usuario = "Ranking_8";
            }
            else if(dan_usuario.compareTo(Ranking_9) == 0)
            {
            	dan_usuario = "Ranking_9";
            }
            else if(dan_usuario.compareTo(Ranking_10) == 0)
            {
            	dan_usuario = "Ranking_10";
            }
            else if(dan_usuario.compareTo(Ranking_11) == 0)
            {
            	dan_usuario = "Ranking_11";
            }
            else
            {
            	dan_usuario = "Ranking_12";
            }
            
            nameValuePairs.add(new BasicNameValuePair("dan_usuario", dan_usuario));
            nameValuePairs.add(new BasicNameValuePair("nome_usuario", nome_usuario));
            
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost); 
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream;
            inputStream = httpEntity.getContent();
            
            
            this.extrairTudoDoRankingDoUsuario(inputStream);
            	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String s) 
	{
		//faltou alertar a tela do competicao
		telaCompeticao.mostrarQuantasSalasAbertasComEsseDan(this.quantasSalasAbertas);
	}
	
	
	private void extrairTudoDoRankingDoUsuario(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            this.quantasSalasAbertas = Integer.valueOf(jObject.getString("count(sala_aberta)"));
        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}


}
