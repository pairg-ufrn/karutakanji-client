package bancodedados;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import br.ufrn.dimap.pairg.karutakanji.android.R;

import com.karutakanji.MainActivity;
import com.karutakanji.ModoCompeticao;

import android.os.AsyncTask;
import android.util.Log;

/*essa task pegarah tudo do ranking do usuario com base no nome: pontuacao_total, vitorias_competicao,derrotas_competicao e dan_do_usuario*/
public class PegaRankingDoUsuarioTask extends AsyncTask<String, String, String>
{
	private ModoCompeticao telaCompeticao;
	private int pontuacao_total;
	private int vitorias_competicao;
	private int derrotas_competicao;
	private String dan_do_usuario;
	
	public PegaRankingDoUsuarioTask (ModoCompeticao activity)
	{
		this.telaCompeticao = activity;
	}
	
	@Override
	protected String doInBackground(String... nomeUsuario) 
	{
		
		String nome_usuario = nomeUsuario[0];
		
		String url_pegarrankingusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarrankingusuariopornome.php";//android nao aceita localhost, tem de ser seu IP
		
		try
		{
			//veremos se nao ja existe um usuario com esse nome
			HttpClient httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            HttpPost httpPost = new HttpPost(url_pegarrankingusuariopornome);
            nameValuePairs.add(new BasicNameValuePair("nome_usuario", nome_usuario));
            
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost); 
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream;
            inputStream = httpEntity.getContent();
            
            
            this.extrairTudoDoRankingDoUsuario(inputStream);
            	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String s) 
	{
		SingletonArmazenaRankingUsuario conheceRankingUsuario = 
								SingletonArmazenaRankingUsuario.getInstance();
		conheceRankingUsuario.setDan_do_usuario(dan_do_usuario);
		conheceRankingUsuario.setDerrotas_competicao(derrotas_competicao);
		conheceRankingUsuario.setPontuacao_total(pontuacao_total);
		conheceRankingUsuario.setVitorias_competicao(vitorias_competicao);
		
		//faltou alertar a tela do competicao
		telaCompeticao.terminouDePegarRankingDoUsuario();
	}
	
	
	private void extrairTudoDoRankingDoUsuario(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            this.pontuacao_total = Integer.valueOf(jObject.getString("pontuacao_total"));
            this.vitorias_competicao = Integer.valueOf(jObject.getString("vitorias_competicao"));
            this.derrotas_competicao = Integer.valueOf(jObject.getString("derrotas_competicao"));
            
            //no BD, os dans estao assim: ranking_1,ranking_2 ateh o 12(seria o dan 10). Vamos traduzir para o normal
            String danUsuarioEmFormatoDoBD = jObject.getString("dan_do_usuario");
            
            if(danUsuarioEmFormatoDoBD.compareTo("Ranking_1") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_1);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_2") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_2);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_3") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_3);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_4") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_4);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_5") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_5);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_6") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_6);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_7") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_7);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_8") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_8);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_9") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_9);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_10") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_10);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_11") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_11);
            }
            else if(danUsuarioEmFormatoDoBD.compareTo("Ranking_12") == 0)
            {
            	this.dan_do_usuario = telaCompeticao.getResources().getString(R.string.Ranking_12);
            }

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}


}
