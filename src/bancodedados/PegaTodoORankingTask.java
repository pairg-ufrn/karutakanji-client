package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karutakanji.RankingCompeticao;

import android.os.AsyncTask;
import android.util.Log;

public class PegaTodoORankingTask extends AsyncTask<String, String, Void> 
{
	private RankingCompeticao telaRankingCompeticao;
	private InputStream inputStream;
	private String result;
	
	public PegaTodoORankingTask(RankingCompeticao telaRankingCompeticao)
	{
		this.telaRankingCompeticao = telaRankingCompeticao;
	}
	
	@Override
	protected Void doInBackground(String... nada) 
	{
		String url_select = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegartodooranking.php";//android nao aceita localhost, tem de ser seu IP
		//String url_select = "http://192.168.0.110/amit/pegarlogjogadorjson.php";//android nao aceita localhost, tem de ser seu IP
	       
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

	        try {
	            // Set up HTTP post

	            // HttpClient is more then less deprecated. Need to change to URLConnection
	            HttpClient httpClient = new DefaultHttpClient();

	            HttpPost httpPost = new HttpPost(url_select);
	            
	            httpPost.setEntity(new UrlEncodedFormEntity(param));
	            HttpResponse httpResponse = httpClient.execute(httpPost);
	            HttpEntity httpEntity = httpResponse.getEntity();

	            // Read content & Log
	            inputStream = httpEntity.getContent();
	        } catch (UnsupportedEncodingException e1) {
	            Log.e("UnsupportedEncodingException", e1.toString());
	            e1.printStackTrace();
	        } catch (ClientProtocolException e2) {
	            Log.e("ClientProtocolException", e2.toString());
	            e2.printStackTrace();
	        } catch (IllegalStateException e3) {
	            Log.e("IllegalStateException", e3.toString());
	            e3.printStackTrace();
	        } catch (IOException e4) {
	            Log.e("IOException", e4.toString());
	            e4.printStackTrace();
	        }
	        // Convert response to string using String Builder
	        try {
	            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
	            StringBuilder sBuilder = new StringBuilder();

	            String line = null;
	            while ((line = bReader.readLine()) != null) {
	            	if(line.startsWith("<meta") == false)//pula linha de metadados
	            	{
	            		 sBuilder.append(line + "\n");
	            	}
	               
	            }

	            inputStream.close();
	            result = sBuilder.toString();

	        } catch (Exception e) {
	            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
	        }
	       
	        
			return null;
	   } 
		
	protected void onPostExecute(Void v) 
	{
		ArrayList<DadosRankingCompeticao> dadosRanking = new ArrayList<DadosRankingCompeticao>();
		        //parse JSON data
		        try {
		            JSONArray jArray = new JSONArray(result);    
		            for(int i=0; i < jArray.length(); i++) {

		                JSONObject jObject = jArray.getJSONObject(i);
		                
		                String nome_usuario = jObject.getString("nome_usuario");
		            	int pontuacao_total = Integer.valueOf(jObject.getString("pontuacao_total"));
		            	int vitorias_competicao = Integer.valueOf(jObject.getString("vitorias_competicao"));
		            	int derrotas_competicao = Integer.valueOf(jObject.getString("derrotas_competicao"));
		            	
		            	DadosRankingCompeticao umdadosRanking = new DadosRankingCompeticao();
		            	umdadosRanking.setDerrotas_competicao(derrotas_competicao);
		            	umdadosRanking.setNome_usuario(nome_usuario);
		            	umdadosRanking.setPontuacao_total(pontuacao_total);
		            	umdadosRanking.setPosicao_no_ranking(i + 1);
		            	umdadosRanking.setVitorias_competicao(vitorias_competicao);
		            	
		            	dadosRanking.add(umdadosRanking);
		            	
		      

		            } // End Loop
		            
		            //vamos ordenar esses
		            
		            this.telaRankingCompeticao.atualizarListViewComRankingUsuarios(dadosRanking);
		           
		        } catch (JSONException e) {
		            Log.e("JSONException", "Error: " + e.toString());
		        }
		        
	}
	
	
}
