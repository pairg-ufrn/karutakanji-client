package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karutakanji.DadosPartidasAnteriores;

import android.os.AsyncTask;
import android.util.Log;

public class PegarDadosUltimasPartidasTask extends AsyncTask<String, String, Void>  
{
	private InputStream inputStream;
	private String result;
	private DadosPartidasAnteriores telaDadosPartidasAnteriores;
	private LinkedList<KanjiTreinar> palavrasAcertadas;
	private LinkedList<KanjiTreinar> palavrasErradas;
	private LinkedList<KanjiTreinar> palavrasTreinadas;
	
	public PegarDadosUltimasPartidasTask(DadosPartidasAnteriores telaDadosPartidasAnteriores)
	{
		this.telaDadosPartidasAnteriores = telaDadosPartidasAnteriores;
	}
	
	@Override
	protected Void doInBackground(String... nomeJogador) 
	{
		String url_select = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarlogjogadorjson.php";//android nao aceita localhost, tem de ser seu IP
		//String url_select = "http://192.168.0.110/amit/pegarlogjogadorjson.php";//android nao aceita localhost, tem de ser seu IP
	       
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

	        try {
	            // Set up HTTP post

	            // HttpClient is more then less deprecated. Need to change to URLConnection
	            HttpClient httpClient = new DefaultHttpClient();

	            HttpPost httpPost = new HttpPost(url_select);
	            
	            String nomejogador = nomeJogador[0];
	            param.add(new BasicNameValuePair("nome_usuario", nomejogador));
	            
	            httpPost.setEntity(new UrlEncodedFormEntity(param));
	            HttpResponse httpResponse = httpClient.execute(httpPost);
	            HttpEntity httpEntity = httpResponse.getEntity();

	            // Read content & Log
	            inputStream = httpEntity.getContent();
	        } catch (UnsupportedEncodingException e1) {
	            Log.e("UnsupportedEncodingException", e1.toString());
	            e1.printStackTrace();
	        } catch (ClientProtocolException e2) {
	            Log.e("ClientProtocolException", e2.toString());
	            e2.printStackTrace();
	        } catch (IllegalStateException e3) {
	            Log.e("IllegalStateException", e3.toString());
	            e3.printStackTrace();
	        } catch (IOException e4) {
	            Log.e("IOException", e4.toString());
	            e4.printStackTrace();
	        }
	        // Convert response to string using String Builder
	        try {
	            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
	            StringBuilder sBuilder = new StringBuilder();

	            String line = null;
	            while ((line = bReader.readLine()) != null) {
	            	if(line.startsWith("<meta") == false)//pula linha de metadados
	            	{
	            		 sBuilder.append(line + "\n");
	            	}
	               
	            }

	            inputStream.close();
	            result = sBuilder.toString();

	        } catch (Exception e) {
	            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
	        }
	       
	        
			return null;
	   } 
		
	protected void onPostExecute(Void v) 
	{
		LinkedList<DadosPartidaParaOLog> dadosPartidasParaLog = new LinkedList<DadosPartidaParaOLog>();
		        //parse JSON data
		        try {
		            JSONArray jArray = new JSONArray(result);    
		            for(int i=0; i < jArray.length(); i++) {

		                JSONObject jObject = jArray.getJSONObject(i);
		                
		                String nome_usuario = jObject.getString("usuario_da_partida");
		            	String data = jObject.getString("data");
		            	int pontuacao = Integer.valueOf(jObject.getString("pontuacao"));
		            	String nome_adversario = jObject.getString("nome_adversario");
		            	String voceGanhouOuPerdeu = jObject.getString("voceganhououperdeu");
		            	String categorias_da_partida_separadas_por_virgula = jObject.getString("categorias_da_partida"); //categorias treinadas na partida
		            	String palavras_separadas_por_virgula = jObject.getString("palavras"); //palavras treinadas na partida
		            	String categorias_das_palavras_separadas_por_virgula = jObject.getString("categorias_das_palavras"); //categorias das palavras treinadas na partida
		            	String treinadaerradaouacertadas_das_palavras_separadas_por_virgula = jObject.getString("treinadaerradaouacertadas_das_palavras"); //se foi treinada, errada ou acertada das palavras treinadas na partida
		            	
		            	DadosPartidaParaOLog dadosLog = new DadosPartidaParaOLog();
		            	dadosLog.setnome_adversario(nome_adversario);
		            	dadosLog.setnome_usuario(nome_usuario);
		            	dadosLog.setPontuacao(pontuacao);
		            	dadosLog.setVoceGanhouOuPerdeu(voceGanhouOuPerdeu);
		            	dadosLog.setData(data);
		            	
		            	//tenho de pegar os ids das categorias e separar tudo por virgula
		            	String ids_categorias_separadas_por_virgula = "";
		            	SingletonArmazenaCategoriasDoJogo conheceIdsCategorias = 
		            							SingletonArmazenaCategoriasDoJogo.getInstance();
		            	String[] array_categorias = categorias_da_partida_separadas_por_virgula.split(",");
		            	
		            	for(int j = 0; j < array_categorias.length; j++)
		            	{
		            		String nome_uma_categoria = array_categorias[j];
		            		String id_categoria = String.valueOf(conheceIdsCategorias.pegarIdDaCategoria(nome_uma_categoria));
		            		ids_categorias_separadas_por_virgula = ids_categorias_separadas_por_virgula + id_categoria;
		            		
		            		if(j < array_categorias.length - 1)
		            		{
		            			ids_categorias_separadas_por_virgula = ids_categorias_separadas_por_virgula + ",";
		            		}
		            	}
		            	
		            	dadosLog.setid_categorias_tudo_junto_separado_por_virgula(ids_categorias_separadas_por_virgula);
		            	
		            	//agora vamos formar as palavras acertadas, erradas e jogadas com base no que temos
		            	this.extrairPalavrasAcertadasErradasETreinadas(palavras_separadas_por_virgula,
		            			categorias_das_palavras_separadas_por_virgula,
		            				treinadaerradaouacertadas_das_palavras_separadas_por_virgula);

		            	dadosLog.setPalavrasAcertadas(palavrasAcertadas);
		            	dadosLog.setPalavrasErradas(palavrasErradas);
		            	dadosLog.setPalavrasJogadas(palavrasTreinadas);
		            	
		            	dadosPartidasParaLog.add(dadosLog);

		            } // End Loop
		            
		            this.telaDadosPartidasAnteriores.atualizarListViewComAsUltimasPartidas(dadosPartidasParaLog);
		           
		        } catch (JSONException e) {
		            Log.e("JSONException", "Error: " + e.toString());
		        }
		        
	}
	
	private void extrairPalavrasAcertadasErradasETreinadas(String palavras_separadas_por_virgula,
			String categorias_das_palavras_separadas_por_virgula, String treinadaerradaouacertadas_das_palavras_separadas_por_virgula)
	{
		this.palavrasAcertadas = new LinkedList<KanjiTreinar>();
		this.palavrasErradas = new LinkedList<KanjiTreinar>();
		this.palavrasTreinadas = new LinkedList<KanjiTreinar>();
		String[] arrayPalavras = palavras_separadas_por_virgula.split(",");
		String[] arrayCategoriasPalavras = categorias_das_palavras_separadas_por_virgula.split(",");
		String[] arrayTreinadasErradasEAcertadas = treinadaerradaouacertadas_das_palavras_separadas_por_virgula.split(",");
		
		ArmazenaKanjisPorCategoria achaKanjisPorNomeECategoria = ArmazenaKanjisPorCategoria.pegarInstancia();
		for(int i = 0; i < arrayPalavras.length; i++)
		{
			String umaPalavra = arrayPalavras[i];
			String umaCategoria = arrayCategoriasPalavras[i];
			String umaErradaAcertadaOuTreinada = arrayTreinadasErradasEAcertadas[i];
			
			SingletonArmazenaCategoriasDoJogo conheceCategoriasDoJogo =
					SingletonArmazenaCategoriasDoJogo.getInstance();
        	
        	int id_categoria = conheceCategoriasDoJogo.pegarIdDaCategoria(umaCategoria);
			KanjiTreinar umKanjiTreinar = achaKanjisPorNomeECategoria.acharKanji(id_categoria, umaPalavra);
			
			if(umaErradaAcertadaOuTreinada.compareTo("treinada") == 0)
			{
				this.palavrasTreinadas.add(umKanjiTreinar);
			}
			else if(umaErradaAcertadaOuTreinada.compareTo("errada") == 0)
			{
				this.palavrasErradas.add(umKanjiTreinar);
			}
			else
			{
				this.palavrasAcertadas.add(umKanjiTreinar);
			}
		}
		
	}

}
