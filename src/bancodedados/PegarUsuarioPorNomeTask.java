package bancodedados;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.karutakanji.MainActivity;

public class PegarUsuarioPorNomeTask extends AsyncTask<String, String, String>
{
	MainActivity activityInicial;
	private boolean usuarioExiste;
	
	public PegarUsuarioPorNomeTask (MainActivity activity)
	{
		this.activityInicial = activity;
	}
	
	@Override
	protected String doInBackground(String... nomeUsuarioEEmail) 
	{
		
		String nome_usuario = nomeUsuarioEEmail[0];
		String email_usuario = nomeUsuarioEEmail[1];
		
		String url_pegarusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuariopornome.php";//android nao aceita localhost, tem de ser seu IP
		
		try
		{
			//veremos se nao ja existe um usuario com esse nome
			HttpClient httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            HttpPost httpPost = new HttpPost(url_pegarusuariopornome);
            nameValuePairs.add(new BasicNameValuePair("nome_usuario", nome_usuario));
            
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost); 
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream;
            inputStream = httpEntity.getContent();
            
            
            	//Serah que existe mesmo um usuario desses? E serah que o email estah correto?
            	String emailUsuarioNoBd = this.extrairEmailDoUsuarioQueQueremosAchar(inputStream);
            	
            	if((emailUsuarioNoBd.length() == 0 && email_usuario.length() == 0) || emailUsuarioNoBd.compareTo(email_usuario) == 0)
            	{
            		this.usuarioExiste = true;
            	}
            	else
            	{
            		this.usuarioExiste = false;
            	}
		}
		catch(Exception e)
		{
			this.usuarioExiste = false;
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String s) 
	{
		if(this.usuarioExiste == false)
		{
			this.activityInicial.mostrarMensagemLoginNaoExiste();
		}
		else
		{
			this.activityInicial.terminarFazerLogin();
		}
		
	}
	
	
	private String extrairEmailDoUsuarioQueQueremosAchar(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String email_usuario = jObject.getString("email_usuario");
           
            return email_usuario;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}

}
