package bancodedados;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.karutakanji.MainActivity;

public class RealizarLoginUsuarioTask extends AsyncTask<String, String, String>
{
	MainActivity activityInicial;
	private boolean usuarioExiste;
	private String nome_usuario; //vou verificar se o usuario existe com base em email e senha e de quebra vou pega-lo!
	
	public RealizarLoginUsuarioTask (MainActivity activity)
	{
		this.activityInicial = activity;
	}
	
	@Override
	protected String doInBackground(String... emailESenha) 
	{
		String email_usuario = emailESenha[0];
		String senha_usuario = emailESenha[1];
		
		String url_pegarusuariopornome = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarusuarioporemailesenha.php";//android nao aceita localhost, tem de ser seu IP
		
		try
		{
			//veremos se nao ja existe um usuario com esse nome
			HttpClient httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            HttpPost httpPost = new HttpPost(url_pegarusuariopornome);
            nameValuePairs.add(new BasicNameValuePair("email_usuario", email_usuario));
            nameValuePairs.add(new BasicNameValuePair("senha_usuario", senha_usuario));
            
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse httpResponse = httpClient.execute(httpPost); 
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream;
            inputStream = httpEntity.getContent();
            
            
            	//Serah que existe mesmo um usuario desses? E serah que o email e senha estao correto?
            	LinkedList<String> emailESenhaNoBd = this.extrairEmailESenhaDoUsuarioQueQueremosAchar(inputStream);
            	String emailNoBd = emailESenhaNoBd.get(0);
            	String senhaNoBd = emailESenhaNoBd.get(1);
            	
            	if(emailNoBd != null && senhaNoBd != null && emailNoBd.length() > 0 && emailNoBd.compareTo(email_usuario) == 0 && senhaNoBd.length() > 0 && senhaNoBd.compareTo(senha_usuario) == 0)
            	{
            		this.usuarioExiste = true;
            	}
            	else
            	{
            		this.usuarioExiste = false;
            	}
		}
		catch(Exception e)
		{
			this.usuarioExiste = false;
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String s) 
	{
		if(this.usuarioExiste == false)
		{
			this.activityInicial.mostrarMensagemLoginNaoExiste();
		}
		else
		{
			this.activityInicial.terminarFazerLogin(this.nome_usuario);
		}
		
	}
	
	
	private LinkedList<String> extrairEmailESenhaDoUsuarioQueQueremosAchar(InputStream inputStream) throws Exception
	{
		try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            String result = sBuilder.toString();
            
            JSONArray jArray = new JSONArray(result);  
            JSONObject jObject = jArray.getJSONObject(0);
            
            String email_usuario = jObject.getString("email_usuario");
            String senha_usuario = jObject.getString("senha_usuario");
            this.nome_usuario = jObject.getString("nome_usuario");
           
            LinkedList<String> emailESenhaUsuario = new LinkedList<String>();
            emailESenhaUsuario.add(email_usuario);
            emailESenhaUsuario.add(senha_usuario);
            
            return emailESenhaUsuario;

        } 
		catch (Exception e) 
        {
        	String eString = e.toString();
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            throw new Exception();
        }
	}

}
