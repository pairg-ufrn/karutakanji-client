package bancodedados;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/*singleton que armazena as categorias usadas no jogo relacionadas aos seus nomes*/
public class SingletonArmazenaCategoriasDoJogo 
{
	private HashMap<Integer,Categoria> categoriasESeusIds;
	private static SingletonArmazenaCategoriasDoJogo instancia;
	
	private SingletonArmazenaCategoriasDoJogo()
	{
		categoriasESeusIds = new HashMap<Integer,Categoria>();
	}
	
	public static SingletonArmazenaCategoriasDoJogo getInstance()
	{
		if(instancia == null)
		{
			instancia = new SingletonArmazenaCategoriasDoJogo();
		}
		
		return instancia;
	}

	public HashMap<Integer, Categoria> getCategoriasESeusIds() {
		return categoriasESeusIds;
	}

	public void setCategoriasESeusNomes(
			HashMap<Integer, Categoria> categoriasESeusIds) {
		this.categoriasESeusIds = categoriasESeusIds;
	}
	
	public void armazenarNovaCategoria(int id_categoria, Categoria categoria)
	{
		if(this.categoriasESeusIds.containsKey(id_categoria) == false)
		{
			this.categoriasESeusIds.put(id_categoria, categoria);
		}
	}
	
	public void esvaziarListaCategorias()
	{
		this.categoriasESeusIds.clear();
	}
	
	public int pegarIdDaCategoria(String nomeCategoria)
	{
		Iterator<Integer> iteradorIdsCategorias = this.categoriasESeusIds.keySet().iterator();
		while(iteradorIdsCategorias.hasNext() == true)
		{
			int id_categoria = iteradorIdsCategorias.next();
			Categoria uma_categoria = this.categoriasESeusIds.get(id_categoria);
			String nome_uma_categoria = uma_categoria.getNome();
			if(nomeCategoria.compareTo(nome_uma_categoria) == 0)
			{
				return id_categoria;
			}
		}
		
		return -1;
	}
	
	public String pegarCategoriaPorId(String idCategoriaEmString)
	{
		int idCategoria = Integer.valueOf(idCategoriaEmString);
		Categoria categoria = this.categoriasESeusIds.get(idCategoria);
		
		if(categoria != null)
		{
			return categoria.getNome();
		}
		else
		{
			return null;
		}
	}
	
	public LinkedList<String> getCategoriasArmazenadas()
	{
		LinkedList<String> nomesCategorias = new LinkedList<String>();
		Iterator<Integer> iteradorIdsCategorias = this.categoriasESeusIds.keySet().iterator();
		while(iteradorIdsCategorias.hasNext() == true)
		{
			Categoria categoria = this.categoriasESeusIds.get(iteradorIdsCategorias.next());
			nomesCategorias.add(categoria.getNome());
		}
		
		return nomesCategorias;
	}
	
}
