package bancodedados;

/*desde quando o usuario chega na tela inicial comum do jogo, esse cara ja estarah preenchido e deve ser alterado quando os valores mudarem*/
public class SingletonArmazenaRankingUsuario 
{
	private int pontuacao_total;
	private int vitorias_competicao;
	private int derrotas_competicao;
	private String dan_do_usuario;
	private static SingletonArmazenaRankingUsuario instancia;
	
	private SingletonArmazenaRankingUsuario()
	{
		
	}
	
	public static SingletonArmazenaRankingUsuario getInstance()
	{
		if(instancia == null)
		{
			instancia = new SingletonArmazenaRankingUsuario();
		}
		
		return instancia;
	}

	public int getPontuacao_total() {
		return pontuacao_total;
	}

	public void setPontuacao_total(int pontuacao_total) {
		this.pontuacao_total = pontuacao_total;
	}

	public int getVitorias_competicao() {
		return vitorias_competicao;
	}

	public void setVitorias_competicao(int vitorias_competicao) {
		this.vitorias_competicao = vitorias_competicao;
	}

	public int getDerrotas_competicao() {
		return derrotas_competicao;
	}

	public void setDerrotas_competicao(int derrotas_competicao) {
		this.derrotas_competicao = derrotas_competicao;
	}

	public String getDan_do_usuario() {
		return dan_do_usuario;
	}

	public void setDan_do_usuario(String dan_do_usuario) {
		this.dan_do_usuario = dan_do_usuario;
	}
	
	

}
