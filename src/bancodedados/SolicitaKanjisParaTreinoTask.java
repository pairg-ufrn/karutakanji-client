package bancodedados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.jar.JarOutputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karutakanji.ActivityMultiplayerQueEsperaAtePegarOsKanjis;


import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Essa classe vai solicitar a um servidor php por listas de kanjis para treino no jogo
 * 
 *
 */
public class SolicitaKanjisParaTreinoTask extends AsyncTask<String, String, Void> {
	
	private InputStream inputStream = null;
	private String result = ""; 
	private ProgressDialog loadingDaTelaEmEspera;//eh o dialog da tela em espera pelo resultado do web service
	private ActivityQueEsperaAtePegarOsKanjis activityQueEsperaAtePegarOsKanjis;
	
	public SolicitaKanjisParaTreinoTask(ProgressDialog loadingDaTela, ActivityQueEsperaAtePegarOsKanjis activityQueEsperaAteRequestTerminar)
	{
		this.loadingDaTelaEmEspera = loadingDaTela;
		this.activityQueEsperaAtePegarOsKanjis = activityQueEsperaAteRequestTerminar;
	}
	
	@Override
    protected Void doInBackground(String... params) {
		//LCC: http://10.9.99.239/amit/pegarjlptjson.php
		//sala de aula: http://10.5.26.127/amit/pegarjlptjson.php

       //String url_select = "http://app.karutakanji.pairg.dimap.ufrn.br/pegarjlptjson.php";//android nao aceita localhost, tem de ser seu IP
		String url_select = "http://server.karutakanji.pairg.dimap.ufrn.br/app/pegarjlptjsonnew.php";
		//String url_select = "http://192.168.0.110/amit/pegarjlptjson.php";
       
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

        try {
            // Set up HTTP post

            // HttpClient is more then less deprecated. Need to change to URLConnection
            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url_select);
            httpPost.setEntity(new UrlEncodedFormEntity(param));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            // Read content & Log
            inputStream = httpEntity.getContent();
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
        // Convert response to string using String Builder
        try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
            	if(line.startsWith("<meta") == false)//pula linha de metadados
            	{
            		 sBuilder.append(line + "\n");
            	}
               
            }

            inputStream.close();
            result = sBuilder.toString();

        } catch (Exception e) {
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
            this.activityQueEsperaAtePegarOsKanjis.procedimentoConexaoFalhou();
        }
        
        /*if(this.activityQueEsperaAtePegarOsKanjis instanceof ActivityMultiplayerQueEsperaAtePegarOsKanjis)
        {
        	//se trata da activity do multiplayer
        	ActivityMultiplayerQueEsperaAtePegarOsKanjis activityMultiplayer = (ActivityMultiplayerQueEsperaAtePegarOsKanjis) this.activityQueEsperaAtePegarOsKanjis;
        	if(activityMultiplayer.jogadorEhHost() == true)
        	{
        		 while(activityMultiplayer.oGuestTerminouDeCarregarListaDeCategorias() == false)
        	        {
        	        	//espera...
        	        	try {
        					Thread.sleep(1000);
        				} catch (InterruptedException e) {
        					// TODO Auto-generated catch block
        					e.printStackTrace();
        				}
        	        }
        	}
        }*/
       
        
		return null;
    } // protected Void doInBackground(String... params)
	
	 protected void onPostExecute(Void v) {
	        //parse JSON data
	        try {
	            JSONArray jArray = new JSONArray(result); 
	            ArmazenaKanjisPorCategoria.pegarInstancia().esvaziarTodoOHashmap(); //se uma nova partida comeca, novos kanjis serao colocados nesse hashmap
	            SingletonArmazenaCategoriasDoJogo.getInstance().esvaziarListaCategorias();
	            
	            for(int i=0; i < jArray.length(); i++) {

	                JSONObject jObject = jArray.getJSONObject(i);
	                
	                String jlptAssociado = jObject.getString("jlpt");
	                String kanji = jObject.getString("kanji");
	                String hiraganaDoKanji = jObject.getString("hiragana");
	                
	                String categoriaAssociada = "";
	                String traducao = "";
	                
	                //qual a traducao do kanji que devemos pegar? em ingles ou br? e as categorias?
	                Resources res = activityQueEsperaAtePegarOsKanjis.getResources();
	                Locale myLocale = res.getConfiguration().locale;
	                if(myLocale != null)
	        		{
	        			String language = myLocale.getLanguage();
	        			if(myLocale.getLanguage().compareTo("en") == 0)
	        			{
	        				//pegar traducao e categorias em ingles!
	        				categoriaAssociada = jObject.getString("nome_categoria_ingles");
	    	                traducao = jObject.getString("traducao_ingles");
	        			}
	        			else if(myLocale.getLanguage().compareTo("jp") == 0)
	        			{
	        				//pegar traducao e categorias em japones!
	        				categoriaAssociada = jObject.getString("nome_categoria_japones");
	    	                traducao = hiraganaDoKanji;
	        			}
	        			else if(myLocale.getLanguage().compareTo("pt") == 0)
	        			{
	        				//br
	        				categoriaAssociada = jObject.getString("nome_categoria");
	    	                traducao = jObject.getString("traducao");
	        			}
	        			else
	        			{
	        				categoriaAssociada = jObject.getString("nome_categoria_ingles");
	    	                traducao = jObject.getString("traducao_ingles");
	        			}
	        		}
	                else
	                {
	                	//se teve algum erro, vamos pegar em ingles mesmo
	                	categoriaAssociada = jObject.getString("nome_categoria_ingles");
    	                traducao = jObject.getString("traducao_ingles");
	                }
	                
	                String dificuldadeDoKanji = jObject.getString("dificuldade");
	                
	                //NOVOS PARA PASSAR P PHILLIP
	                String id_do_kanji = jObject.getString("id");
	                int id_categoria = jObject.getInt("id_categoria");
	                String descricao_categoria = jObject.getString("id");
	                
	                Categoria categoriaNovaArmazenar = new Categoria(id_categoria,categoriaAssociada,descricao_categoria);
	                SingletonArmazenaCategoriasDoJogo.getInstance().armazenarNovaCategoria(id_categoria, categoriaNovaArmazenar);
	                //FIM DOS NOVOS
	                
	                int dificuldadeDoKanjiEmNumero; 
	                try
	                {
	                	dificuldadeDoKanjiEmNumero = Integer.valueOf(dificuldadeDoKanji);
	                }
	                catch(NumberFormatException exc)
	                {
	                	dificuldadeDoKanjiEmNumero = 1;
	                }
	                
	                KanjiTreinar novoKanjiTreinar = new KanjiTreinar(jlptAssociado, categoriaAssociada, kanji, 
	                		traducao, hiraganaDoKanji, dificuldadeDoKanjiEmNumero,id_do_kanji);
	                //vamos s� ver se o kanji tem uma lista de possiveis ciladas...
	                /*@SuppressWarnings("unchecked")
					Iterator<String> nomesColunasDoJObject = jObject.keys();
	                while(nomesColunasDoJObject.hasNext())
	                {
	                	if(nomesColunasDoJObject.next().compareTo("possiveis ciladas") == 0)
	                	{
	                		String possiveisCiladas = jObject.getString("possiveis ciladas");
	                		String [] ciladasEmArray = possiveisCiladas.split(";");
	                		LinkedList<String> possiveisCiladasLinkedList = new LinkedList<String>();
	                		for(int j = 0; j < ciladasEmArray.length; j++)
	                		{
	                			possiveisCiladasLinkedList.add(ciladasEmArray[j]);
	                		}
	                		novoKanjiTreinar.setPossiveisCiladasKanji(possiveisCiladasLinkedList);
	                	}
	                	
	                }*/
	                
	                //e, por fim, armazenar esses kanjis na lista de ArmazenaKanjisPorCategoria
	                ArmazenaKanjisPorCategoria.pegarInstancia().adicionarKanjiACategoria(id_categoria, novoKanjiTreinar);
	                
	                

	            } // End Loop
	           
	        } catch (JSONException e) {
	            Log.e("JSONException", "Error: " + e.toString());
	        }
	        catch(Exception e)
	        {
	        	Log.e("JSONException", "Error: " + e.toString());
	        	Exception p = e;
	        	p = null;
	        }
	        ArmazenaKanjisPorCategoria armazenadorKanjis = ArmazenaKanjisPorCategoria.pegarInstancia();
	        
	        this.activityQueEsperaAtePegarOsKanjis.procedimentoAposCarregarKanjis();
	        
	        if(this.loadingDaTelaEmEspera != null)
	        {
	        	//esse loading eh null quando a activity estiver chamando essa task apenas para carregar kanjis
	        	//sem ter nenhum loading para espera
	        	this.loadingDaTelaEmEspera.dismiss();
	        }
	        
	    } // protected void onPostExecute(Void v)
} //class MyAsyncTask extends AsyncTask<String, String, Void>

