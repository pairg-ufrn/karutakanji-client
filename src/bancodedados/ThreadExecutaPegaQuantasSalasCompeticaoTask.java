package bancodedados;

import android.app.ProgressDialog;

import com.karutakanji.ModoCasual;
import com.karutakanji.ModoCompeticao;

/*essa thread vai ficar chamando o buscasalasmodocasualtask varias vezes*/
public class ThreadExecutaPegaQuantasSalasCompeticaoTask extends Thread
{
	private ModoCompeticao telaModoCompeticao;
	private String dan_usuario;
	private String nome_usuario;
	
	public ThreadExecutaPegaQuantasSalasCompeticaoTask(ModoCompeticao telaCompeticao, String dan_usuario, String nome_usuario)
	{
		this.telaModoCompeticao = telaCompeticao;
		this.dan_usuario = dan_usuario;
		this.nome_usuario = nome_usuario;
	}
	
	@Override
	public void run()
	{
		while(true)
		{
			try 
			{
				PegaQuantasSalasAbertasCompeticaoComBaseNoDanTask buscaSalasTask = new PegaQuantasSalasAbertasCompeticaoComBaseNoDanTask(telaModoCompeticao);
				buscaSalasTask.execute(dan_usuario,nome_usuario);
				Thread.sleep(3000);
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				//e.printStackTrace();
				return;
			}
		}
	}
	
	


}
