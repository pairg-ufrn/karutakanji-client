package com.karutakanji;


import android.content.Context;
import android.content.SharedPreferences;

/*classe que armazena todas as configuracoes do jogo como mostrar regras no treinamento ou dicas*/
public class ArmazenaConfiguracoesDoJogo 
{
	private static ArmazenaConfiguracoesDoJogo instancia;
	
	private ArmazenaConfiguracoesDoJogo()
	{
		
	}
	
	public static ArmazenaConfiguracoesDoJogo getInstance()
	{
		if(instancia == null)
		{
			instancia = new ArmazenaConfiguracoesDoJogo();
		}
		
		return instancia;
	}
	
	public boolean getMostrarRegrasDoTreinamento(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvas = contextoAplicacao.getSharedPreferences("mostrar_regras_do_treinamento", Context.MODE_PRIVATE);
		boolean mostrarRegrasDoTreinamento = configuracoesSalvas.getBoolean("mostrar_regras_do_treinamento", true);
		return mostrarRegrasDoTreinamento;
	}
	
	public void alterarMostrarRegrasDoTreinamento(Context contextoAplicacao, boolean novoValor) 
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("mostrar_regras_do_treinamento", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		editorConfig.putBoolean("mostrar_regras_do_treinamento", novoValor);
		editorConfig.commit();
	}
	
	public boolean getMostrarDicasDoTreinamento(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvas = contextoAplicacao.getSharedPreferences("mostrar_dicas_do_treinamento", Context.MODE_PRIVATE);
		boolean mostrarDicasDoTreinamento = configuracoesSalvas.getBoolean("mostrar_dicas_do_treinamento", true);
		return mostrarDicasDoTreinamento;
	}
	
	public void alterarMostrarDicasDoTreinamento(Context contextoAplicacao, boolean novoValor) 
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("mostrar_dicas_do_treinamento", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		editorConfig.putBoolean("mostrar_dicas_do_treinamento", novoValor);
		editorConfig.commit();
	}
	
	/*as descricoes dos itens do modo casual devem ser mostradas novamente?*/
	public void salvarMostrarDescricaoItensCasual(Context contextoAplicacao, boolean mostrarDescricaoItensCasual)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("mostrar_descricao_itens_casual", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		
		editorConfig.putBoolean("mostrar_descricao_itens_casual", mostrarDescricaoItensCasual);
		editorConfig.commit();
	}
	
	public boolean carregarMostrarDescricaoItensCasual(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("mostrar_descricao_itens_casual", Context.MODE_PRIVATE);
		boolean mostrar_descricao_itens_casual = configuracoesSalvar.getBoolean("mostrar_descricao_itens_casual", true);
		return mostrar_descricao_itens_casual;
	}
	
	/*as descricoes dos itens do modo casual devem ser mostradas novamente?*/
	public void salvarMostrarDescricaoItensCompeticao(Context contextoAplicacao, boolean mostrarDescricaoItensCompeticao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("mostrar_descricao_itens_competicao", Context.MODE_PRIVATE);
		SharedPreferences.Editor editorConfig = configuracoesSalvar.edit();
		
		editorConfig.putBoolean("mostrar_descricao_itens_competicao", mostrarDescricaoItensCompeticao);
		editorConfig.commit();
	}
	
	public boolean carregarMostrarDescricaoItensCompeticao(Context contextoAplicacao)
	{
		SharedPreferences configuracoesSalvar = contextoAplicacao.getSharedPreferences("mostrar_descricao_itens_competicao", Context.MODE_PRIVATE);
		boolean mostrar_descricao_itens_competicao = configuracoesSalvar.getBoolean("mostrar_descricao_itens_competicao", true);
		return mostrar_descricao_itens_competicao;
	}
}
