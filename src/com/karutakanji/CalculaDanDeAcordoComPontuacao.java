package com.karutakanji;

import android.app.Activity;
import br.ufrn.dimap.pairg.karutakanji.android.R;

public class CalculaDanDeAcordoComPontuacao 
{
	private static int pontuacaoMinimaIntermediario = 10000;
	private static int pontuacaoMinimaDan1 = 20000;
	private static int pontuacaoMinimaDan2 = 30000;
	private static int pontuacaoMinimaDan3 = 40000;
	private static int pontuacaoMinimaDan4 = 50000;
	private static int pontuacaoMinimaDan5 = 60000;
	private static int pontuacaoMinimaDan6 = 70000;
	private static int pontuacaoMinimaDan7 = 80000;
	private static int pontuacaoMinimaDan8 = 900000;
	private static int pontuacaoMinimaDan9 = 100000;
	private static int pontuacaoMinimaDan10 = 110000;
	
	public static String calcularDanDeAcordoComPontuacao(int pontuacao, Activity activity)
	{
		if(pontuacao < pontuacaoMinimaIntermediario)
		{
			String iniciante = activity.getResources().getString(R.string.iniciante);
			return iniciante;
		}
		else if(pontuacao < pontuacaoMinimaDan1)
		{
			String intermediario = activity.getResources().getString(R.string.intermediario);
			return intermediario;
		}
		else if(pontuacao < pontuacaoMinimaDan2)
		{
			return "Dan 1";
		}
		else if(pontuacao < pontuacaoMinimaDan3)
		{
			return "Dan 2";
		}
		else if(pontuacao < pontuacaoMinimaDan4)
		{
			return "Dan 3";
		}
		else if(pontuacao < pontuacaoMinimaDan5)
		{
			return "Dan 4";
		}
		else if(pontuacao < pontuacaoMinimaDan6)
		{
			return "Dan 5";
		}
		else if(pontuacao < pontuacaoMinimaDan7)
		{
			return "Dan 6";
		}
		else if(pontuacao < pontuacaoMinimaDan8)
		{
			return "Dan 7";
		}
		else if(pontuacao < pontuacaoMinimaDan9)
		{
			return "Dan 8";
		}
		else if(pontuacao < pontuacaoMinimaDan10)
		{
			return "Dan 9";
		}
		else
		{
			return "Dan 10";
		}
	}
	
	public static int quantosPontosFaltamParaAlcançarPróximoDan(int pontuacaoAtual, Activity activity)
	{
		String danUsuario = calcularDanDeAcordoComPontuacao(pontuacaoAtual,activity);
		int pontuacaoProximoDan = 0;
		
		String iniciante = activity.getResources().getString(R.string.iniciante);
		String intermediario = activity.getResources().getString(R.string.intermediario);
		if(danUsuario.compareTo(iniciante) == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaIntermediario;
		}
		else if(danUsuario.compareTo(intermediario) == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan1;
		}
		else if(danUsuario.compareTo("Dan 1") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan2;
		}
		else if(danUsuario.compareTo("Dan 2") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan3;
		}
		else if(danUsuario.compareTo("Dan 3") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan4;
		}
		else if(danUsuario.compareTo("Dan 4") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan5;
		}
		else if(danUsuario.compareTo("Dan 5") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan6;
		}
		else if(danUsuario.compareTo("Dan 6") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan7;
		}
		else if(danUsuario.compareTo("Dan 7") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan8;
		}
		else if(danUsuario.compareTo("Dan 8") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan9;
		}
		else if(danUsuario.compareTo("Dan 9") == 0)
		{
			pontuacaoProximoDan = pontuacaoMinimaDan10;
		}
		else
		{
			pontuacaoProximoDan = 0; //nao ha proximo dan
		}
		
		if(pontuacaoProximoDan == 0)
		{
			return pontuacaoProximoDan;
		}
		else
		{
			return pontuacaoProximoDan - pontuacaoAtual;
		}
	}   

}
