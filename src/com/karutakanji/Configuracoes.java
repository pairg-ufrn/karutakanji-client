package com.karutakanji;

import java.util.Locale;

import bancodedados.ArmazenaTudoParaJogoOffline;
import bancodedados.ChecaVersaoAtualDoSistemaTask;
import br.ufrn.dimap.pairg.karutakanji.android.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.view.ViewDebug.FlagToString;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Configuracoes extends ActivityDoJogoComSom
{
	private boolean mostrarRegrasTreinamento;
	private boolean mostrarDicasTreinamento;
	private boolean manter_logado;
	private boolean auto_preencher_login;
	private boolean mostrar_descricao_itens_casual;
	private boolean mostrar_descricao_itens_competicao;
	private String idioma; //tem 3 possiveis valores: portugues, ingles ou espanhol
	private Locale myLocale;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		//coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
		super.getGameHelper().setMaxAutoSignInAttempts(0);
		//fim das coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
				
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracoes);
		
		
		//parte sobre as configuracoes treinamento
		ArmazenaConfiguracoesDoJogo armazenaConfiguracoes = 
							ArmazenaConfiguracoesDoJogo.getInstance();
		this.mostrarRegrasTreinamento = 
				armazenaConfiguracoes.getMostrarRegrasDoTreinamento(this);
		this.mostrarDicasTreinamento =
				armazenaConfiguracoes.getMostrarDicasDoTreinamento(this);
		this.mostrar_descricao_itens_casual =
				armazenaConfiguracoes.carregarMostrarDescricaoItensCasual(this);
		this.mostrar_descricao_itens_competicao =
				armazenaConfiguracoes.carregarMostrarDescricaoItensCompeticao(this);
		
		Button botaoCheckbox = (Button) findViewById(R.id.checkbox_mostrar_regras_treinamento);
		Button botaoCheckbox2 = (Button) findViewById(R.id.checkbox_mostrar_dicas_treinamento);
		Button botaoCheckboxCasual = (Button) findViewById(R.id.checkbox_mostrar_descricao_dos_itens_casual);
		Button botaoCheckbox4 = (Button) findViewById(R.id.checkbox_mostrar_descricao_itens_competicao);
		
		if(this.mostrarRegrasTreinamento == true)
		{
			botaoCheckbox.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			botaoCheckbox.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		
		if(this.mostrarDicasTreinamento == true)
		{
			botaoCheckbox2.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			botaoCheckbox2.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		
		if(this.mostrar_descricao_itens_casual == true)
		{
			botaoCheckboxCasual.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			botaoCheckboxCasual.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		
		if(this.mostrar_descricao_itens_competicao == true)
		{
			botaoCheckbox4.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			botaoCheckbox4.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		
		//parte sobre as configuracoes de login
		this.auto_preencher_login = ArmazenaTudoParaJogoOffline.getInstance().getLembrarDeMim(this);
		this.manter_logado = ArmazenaTudoParaJogoOffline.getInstance().getManterLogado(this);
		
		Button botaoCheckboxManterLogado = (Button) findViewById(R.id.checkbox_manter_logado_configuracoes);
		Button botaoCheckboxAutoPreencherLogin = (Button) findViewById(R.id.checkbox_auto_preencher_login_configuracoes);
		
		if(this.manter_logado == true)
		{
			botaoCheckboxManterLogado.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			botaoCheckboxManterLogado.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		
		if(this.auto_preencher_login == true)
		{
			botaoCheckboxAutoPreencherLogin.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			botaoCheckboxAutoPreencherLogin.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		
		//PARTE SOBRE AS CONFIGURACOES IDIOMA - MUDAR QUANDO PERMITIR MAIS IDIOMAS
		this.tornarMudancaDeIdiomaImpossivel();
		
		this.mudarFonteDosTextos();
		this.fazerOpcoesDeIdiomasAindaNaoImplementadasFicaremTransaprentes();
		this.alterarVersaoAtualDoSistema();
		
		RadioButton radioPortugues = (RadioButton) findViewById(R.id.radioButtonPortugues);
		RadioButton radioIngles = (RadioButton) findViewById(R.id.radioButtonIngles);
		RadioButton radioEspanhol = (RadioButton) findViewById(R.id.radioButtonEspanhol);
		RadioButton radioJapones = (RadioButton) findViewById(R.id.radioButtonJapones);
		Resources res = getResources();
        this.myLocale = res.getConfiguration().locale;
		if(this.myLocale != null)
		{
			String language = myLocale.getLanguage();
			if(this.myLocale.getLanguage().compareTo("en") == 0)
		    {
		    	radioIngles.setChecked(true);
				radioPortugues.setChecked(false);
				radioEspanhol.setChecked(false);
				radioJapones.setChecked(false);
				this.idioma = "ingles";
		    }
		    else if(this.myLocale.getLanguage().compareTo("es") == 0)
		    {
		    	radioIngles.setChecked(false);
				radioPortugues.setChecked(false);
				radioEspanhol.setChecked(true);
				radioJapones.setChecked(false);
				
				this.idioma = "espanhol";
		    }
		    else if(this.myLocale.getLanguage().compareTo("jp") == 0)
		    {
		    	radioIngles.setChecked(false);
				radioPortugues.setChecked(false);
				radioEspanhol.setChecked(false);
				radioJapones.setChecked(true);
				
				this.idioma = "japones";
		    }
		    else if(this.myLocale.getLanguage().compareTo("pt") == 0)
		    {
		    	radioIngles.setChecked(false);
				radioPortugues.setChecked(true);
				radioEspanhol.setChecked(false);
				radioJapones.setChecked(false);
				
				this.idioma = "portugues";
		    }
		    else // ingles
		    {
		    	radioPortugues.setChecked(false);
				radioIngles.setChecked(true);
				radioEspanhol.setChecked(false);
				radioJapones.setChecked(false);
				
				this.idioma = "ingles";
		    }
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.configuracoes, menu);
		return true;
	}

	
	public void salvarConfiguracoes(View v)
	{
		//armazenar o valor das configuracoes do jogo - treinamento
		ArmazenaConfiguracoesDoJogo armazenaConfiguracoes = 
				ArmazenaConfiguracoesDoJogo.getInstance();
		armazenaConfiguracoes.alterarMostrarRegrasDoTreinamento(this, this.mostrarRegrasTreinamento);
		armazenaConfiguracoes.alterarMostrarDicasDoTreinamento(this, this.mostrarDicasTreinamento);
		armazenaConfiguracoes.salvarMostrarDescricaoItensCasual(this, this.mostrar_descricao_itens_casual);
		armazenaConfiguracoes.salvarMostrarDescricaoItensCompeticao(this, this.mostrar_descricao_itens_competicao);
		
		//armazenar o valor das configuracoes do jogo - login
		ArmazenaTudoParaJogoOffline conheceConfiguracoesLogin =
								ArmazenaTudoParaJogoOffline.getInstance();
		conheceConfiguracoesLogin.salvarLembrarDeMim(this, this.auto_preencher_login);
		conheceConfiguracoesLogin.salvarManterLogado(this, manter_logado);
		
		
		SingletonDeveMostrarTelaLoginNaMainActivity.getInstance().setDeveMostrarTelaLogin(false);
		
		Intent voltaAoMenuPrincipal =
				new Intent(this, MainActivity.class);
		voltaAoMenuPrincipal.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(voltaAoMenuPrincipal);
	}
	
	public void usarConfiguracoesPadrao(View v)
	{
		//armazenar valor padrao das configuracoes do jogo - treinamento
		ArmazenaConfiguracoesDoJogo armazenaConfiguracoes = 
				ArmazenaConfiguracoesDoJogo.getInstance();
		armazenaConfiguracoes.alterarMostrarRegrasDoTreinamento(this, true);
		armazenaConfiguracoes.alterarMostrarDicasDoTreinamento(this, true);
		
		mostrarRegrasTreinamento = true;
		mostrarDicasTreinamento = true;
		
		Button botaoCheckbox = (Button) findViewById(R.id.checkbox_mostrar_regras_treinamento);
		botaoCheckbox.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		
		Button botaoCheckbox2 = (Button) findViewById(R.id.checkbox_mostrar_dicas_treinamento);
		botaoCheckbox2.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		
		//armazenar valor padrao das configuracoes do jogo - login
		
		this.auto_preencher_login = false;
		this.manter_logado = false;
		ArmazenaTudoParaJogoOffline conheceConfiguracoesLogin =
				ArmazenaTudoParaJogoOffline.getInstance();
		conheceConfiguracoesLogin.salvarLembrarDeMim(this, this.auto_preencher_login);
		conheceConfiguracoesLogin.salvarManterLogado(this, manter_logado);

		if(this.manter_logado == true)
		{
			SingletonDeveMostrarTelaLoginNaMainActivity.getInstance().setDeveMostrarTelaLogin(false);
		}
		else
		{
			SingletonDeveMostrarTelaLoginNaMainActivity.getInstance().setDeveMostrarTelaLogin(true);
		}
		
		Button botaoCheckbox3 = (Button) findViewById(R.id.checkbox_manter_logado_configuracoes);
		botaoCheckbox3.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		
		Button botaoCheckbox4 = (Button) findViewById(R.id.checkbox_auto_preencher_login_configuracoes);
		botaoCheckbox4.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		
		this.idioma = "ingles";
		RadioButton radioIngles = (RadioButton) findViewById(R.id.radioButtonIngles);
		radioIngles.setSelected(true);
	}
	
	public void mudarValorMostrarRegrasTreinamento(View v)
	{
		if(this.mostrarRegrasTreinamento == true)
		{
			this.mostrarRegrasTreinamento = false;
		}
		else
		{
			this.mostrarRegrasTreinamento = true;
		}
		
		Button botaoCheckbox = (Button) findViewById(R.id.checkbox_mostrar_regras_treinamento);
		
		if(this.mostrarRegrasTreinamento == true)
		{
			botaoCheckbox.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			botaoCheckbox.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
	}
	
	public void mudarValorMostrarDicasTreinamento(View v)
	{
		if(this.mostrarDicasTreinamento == true)
		{
			this.mostrarDicasTreinamento = false;
		}
		else
		{
			this.mostrarDicasTreinamento = true;
		}
		
		Button botaoCheckbox = (Button) findViewById(R.id.checkbox_mostrar_dicas_treinamento);
		
		if(this.mostrarDicasTreinamento == true)
		{
			botaoCheckbox.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			botaoCheckbox.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
	}
	
	private void fazerOpcoesDeIdiomasAindaNaoImplementadasFicaremTransaprentes()
	{
		//FIZ A COR FICAR CINZA NO LAYOUT DOS RADIOBUTTONS DA TELA!!!
	}
	private void mudarFonteDosTextos()
	{
		String fontPath2 = "fonts/Wonton.ttf";
	    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
	    TextView texto_mostrar_regras_treinamento = (TextView) findViewById(R.id.texto_mostrar_regras_treinamento);
	    TextView texto_mostrar_dicas_treinamento = (TextView) findViewById(R.id.texto_mostrar_dicas_treinamento);
	    TextView titulo_configuracoes = (TextView) findViewById(R.id.titulo_configuracoes);
	    TextView label_idioma = (TextView) findViewById(R.id.label_idioma);
	    RadioButton radioButtonPortugues = (RadioButton) findViewById(R.id.radioButtonPortugues);
	    RadioButton radioButtonIngles = (RadioButton) findViewById(R.id.radioButtonIngles);
	    RadioButton radioButtonEspanhol = (RadioButton) findViewById(R.id.radioButtonEspanhol);
	    
	    TextView label_login = (TextView) findViewById(R.id.label_login);
	    TextView texto_manter_logado_configuracoes = (TextView) findViewById(R.id.texto_manter_logado_configuracoes);
	    TextView texto_auto_preencher_login_configuracoes = (TextView) findViewById(R.id.texto_auto_preencher_login_configuracoes);
	    TextView label_treinamento = (TextView) findViewById(R.id.label_treinamento);
	    
	    TextView label_casual = (TextView) findViewById(R.id.label_casual);
	    TextView label_competicao = (TextView) findViewById(R.id.label_competicao);
	    TextView texto_mostrar_itens_competicao = (TextView) findViewById(R.id.texto_mostrar_itens_competicao);
	    TextView texto_mostrar_itens_casual = (TextView) findViewById(R.id.texto_mostrar_itens_casual);
	    
	    texto_mostrar_regras_treinamento.setTypeface(tf2);
	    texto_mostrar_dicas_treinamento.setTypeface(tf2);
	    titulo_configuracoes.setTypeface(tf2);
	    label_idioma.setTypeface(tf2);
	    radioButtonPortugues.setTypeface(tf2);
	    radioButtonEspanhol.setTypeface(tf2);
	    radioButtonIngles.setTypeface(tf2);
	    label_login.setTypeface(tf2);
	    texto_manter_logado_configuracoes.setTypeface(tf2);
	    texto_auto_preencher_login_configuracoes.setTypeface(tf2);
	    label_treinamento.setTypeface(tf2);
	    
	    label_casual.setTypeface(tf2);
	    label_competicao.setTypeface(tf2);
	    texto_mostrar_itens_competicao.setTypeface(tf2);
	    texto_mostrar_itens_casual.setTypeface(tf2);
	    
	    TextView versao_atual_do_sistema = (TextView) findViewById(R.id.versao_atual_do_sistema);
	    versao_atual_do_sistema.setTypeface(tf2);
	    
	}

	private void alterarVersaoAtualDoSistema()
	{
		TextView versao_atual_do_sistema = (TextView) findViewById(R.id.versao_atual_do_sistema);
		
		ChecaVersaoAtualDoSistemaTask checaVersao = new ChecaVersaoAtualDoSistemaTask();
	    String versaoDoSistemaAtual = checaVersao.getVersaoDoSistema();
	    String versao_atual_do_sistema_string = getResources().getString(R.string.versao_atual_do_sistema);
	    versao_atual_do_sistema.setText(versao_atual_do_sistema_string + " " + versaoDoSistemaAtual);
	}
	
	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	public void onRadioButtonClicked(View view) 
	 {
		    // O radioButton esta marcado?
		    boolean checked = ((RadioButton) view).isChecked();
		    
		    // Check which radio button was clicked
		    switch(view.getId()) {
		        case R.id.radioButtonPortugues:
		            if (checked)
		                this.idioma = "portugues";
		            	setLocale("pt");
		            break;
		        case R.id.radioButtonEspanhol:
		            if (checked)
		            	this.idioma = "espanhol";
		            break;
		        case R.id.radioButtonIngles:
		            if (checked)
		            	this.idioma = "ingles";
		            	setLocale("en");
		            break;
		        case R.id.radioButtonJapones:
		            if (checked)
		            	this.idioma = "japones";
		            	setLocale("jp");
		            break;
		    }
		    
	}
	
	private void tornarMudancaDeIdiomaImpossivel()
	{
		this.idioma = "ingles";
		RadioButton radioButtonEspanhol = (RadioButton) findViewById(R.id.radioButtonEspanhol);
		
		radioButtonEspanhol.setClickable(false);
		
	}
	
	public void mudarvalormanterlogado(View v)
	{
		Button botaoCheckbox3 = (Button) findViewById(R.id.checkbox_manter_logado_configuracoes);
		
		if(this.manter_logado == true)
		{
			this.manter_logado = false;
			botaoCheckbox3.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		else
		{
			this.manter_logado = true;
			botaoCheckbox3.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
	}
	
	public void mudarvalorautopreencherlogin(View v)
	{
		Button botaoCheckbox4 = (Button) findViewById(R.id.checkbox_auto_preencher_login_configuracoes);
		
		if(this.auto_preencher_login == true)
		{
			this.auto_preencher_login = false;
			botaoCheckbox4.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		else
		{
			this.auto_preencher_login = true;
			botaoCheckbox4.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
	}
	
	public void mudarValorMostrarDescricaoItensCasual(View v)
	{
		Button botaoCheckbox3 = (Button) findViewById(R.id.checkbox_mostrar_descricao_dos_itens_casual);
		
		if(this.mostrar_descricao_itens_casual == true)
		{
			this.mostrar_descricao_itens_casual = false;
			botaoCheckbox3.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		else
		{
			this.mostrar_descricao_itens_casual = true;
			botaoCheckbox3.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
	}
	
	public void mudarValorMostrarDescricaoItensCompeticao(View v)
	{
		Button botaoCheckbox = (Button) findViewById(R.id.checkbox_mostrar_descricao_itens_competicao);
		
		if(this.mostrar_descricao_itens_competicao == true)
		{
			this.mostrar_descricao_itens_competicao = false;
			botaoCheckbox.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		else
		{
			this.mostrar_descricao_itens_competicao = true;
			botaoCheckbox.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
	}
	
	
	public void irparatelalogin(View v)
	{
		getGameHelper().signOut();
		
		SingletonDeveMostrarTelaLoginNaMainActivity.getInstance().setDeveMostrarTelaLogin(true);
		this.manter_logado = false;
		ArmazenaTudoParaJogoOffline.getInstance().salvarManterLogado(this, false);
		
		Intent voltaAoMenuPrincipal =
				new Intent(this, MainActivity.class);
		voltaAoMenuPrincipal.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(voltaAoMenuPrincipal);
	}
	
	public void setLocale(String lang) {
	 	//Toast.makeText(getApplicationContext(), "mudando texto para" + lang, Toast.LENGTH_SHORT).show();
        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, Configuracoes.class);
        startActivity(refresh);
        finish();
        
        ArmazenaTudoParaJogoOffline salvaConfiguracoes = ArmazenaTudoParaJogoOffline.getInstance();
        salvaConfiguracoes.salvarLinguaDoJogo(this);
    }

}
