package com.karutakanji;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;


import bancodedados.ActivityQueEsperaAtePegarOsKanjis;
import bancodedados.AdapterListViewDasUltimasPartidasCompeticao;
import bancodedados.ArmazenaTudoParaJogoOffline;
import bancodedados.DadosPartidaParaOLog;
import bancodedados.PegarDadosUltimasPartidasCompeticaoTask;
import bancodedados.PegarDadosUltimasPartidasTask;
import bancodedados.SingletonArmazenaCategoriasDoJogo;
import bancodedados.SingletonArmazenaRankingUsuario;
import bancodedados.SolicitaKanjisParaTreinoTask;
import br.ufrn.dimap.pairg.karutakanji.android.R;

import android.os.Bundle;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class DadosPartidasAnteriores extends ActivityDoJogoComSom implements ActivityQueEsperaAtePegarOsKanjis, OnScrollListener
{
	LinkedList<DadosPartidaParaOLog> partidasAnteriores;
	private ProgressDialog loadingKanjisDoBd;
	private ListView listView; //listview com as ultimas partidas
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dados_partidas_anteriores);
		
		//precisarei pegar a lista de kanjis mais porque preciso saber as traducoes dos kanjis na hora de mostrar detalhes de uma partida
		this.loadingKanjisDoBd = ProgressDialog.show(DadosPartidasAnteriores.this, getResources().getString(R.string.carregando_dados_das_partidas), getResources().getString(R.string.por_favor_aguarde));
		SolicitaKanjisParaTreinoTask pegarKanjisTreino = new SolicitaKanjisParaTreinoTask(this.loadingKanjisDoBd, this);
		pegarKanjisTreino.execute("");
		
		mudarFonteTextosDaTela();
		
		ListView listaDasUltimasPartidas = (ListView) findViewById(R.id.listaDasUltimasPartidas);
		listaDasUltimasPartidas.setOnScrollListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dados_partidas_anteriores, menu);
		return true;
	}
	

	@Override
	public void procedimentoAposCarregarKanjis() 
	{
		PegarDadosUltimasPartidasCompeticaoTask taskPegaUltimasPartidas = new PegarDadosUltimasPartidasCompeticaoTask(this);
		LinkedList<String> nomeEEmailJogador = 
				ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(this);
		
		taskPegaUltimasPartidas.execute(nomeEEmailJogador.get(0));
		
	}
	
	public void atualizarListViewComAsUltimasPartidas(LinkedList<DadosPartidaParaOLog> dadosPartidasAnteriores)
	{
		this.partidasAnteriores = dadosPartidasAnteriores;
		this.enfileirarPartidasAnteriores(); //vou enfileirar do mais recente para o menos
		
		try
		{
			// Defined Array values to show in ListView
			int tamanhoValues = this.partidasAnteriores.size();
			
			ArrayList<String> values = new ArrayList<String>();
			
	        for(int i = 0; i < tamanhoValues; i++)
	        {
	        	String labelAdversario = getResources().getString(R.string.label_adversario);
	        	//String labelCategorias = getResources().getString(R.string.categorias);
	        	//String labelDataPartida = getResources().getString(R.string.data_da_partida);
	        	String labelDataPartida = getResources().getString(R.string.em_dois_pontos);
	        	/*String ids_categorias = 
	        			this.partidasAnteriores.get(i).getid_categorias_tudo_junto_separado_por_virgula();
	        	//precisamos pegar os nomes dessas categorias
	        	String categoriasComVirgulas = "";
	        	String[] idsCategoriasArray = ids_categorias.split(",");
	        	SingletonArmazenaCategoriasDoJogo conheceCategoriasDoJogo 
	        									= SingletonArmazenaCategoriasDoJogo.getInstance();
	        	for(int j = 0; j < idsCategoriasArray.length; j++)
	        	{
	        		String umIdCategoria = idsCategoriasArray[j];
	        		String umaCategoria = conheceCategoriasDoJogo.pegarCategoriaPorId(umIdCategoria);
	        		categoriasComVirgulas = categoriasComVirgulas + umaCategoria;
	        		
	        		if(j < idsCategoriasArray.length - 1)
	        		{
	        			categoriasComVirgulas = categoriasComVirgulas + ",";
	        		}
	        	}
	        	String ultimoCaractereDasCategorias = String.valueOf(categoriasComVirgulas.charAt(categoriasComVirgulas.length() - 1));
	        	if(ultimoCaractereDasCategorias.compareTo(",") == 0)
	        	{
	        		categoriasComVirgulas = categoriasComVirgulas.substring(0,categoriasComVirgulas.length() - 1); //vamos tirar o ultimo caractere da string
	        	}*/
	        	
	        	String como_aparece_nome_adversario = this.partidasAnteriores.get(i).getnome_adversario();
	        	if(this.partidasAnteriores.get(i).getnome_adversario().length() >= 20)
	        	{
	        		como_aparece_nome_adversario = como_aparece_nome_adversario.substring(0,20);
	        	}
	        	String oQueApareceraComoItemNaLista =
	        			labelAdversario + " " + como_aparece_nome_adversario + "\n" +
	        			labelDataPartida + " " + this.partidasAnteriores.get(i).getData();
	        	values.add(oQueApareceraComoItemNaLista);
	        }
	        

	        // Define a new Adapter
	        // First parameter - Context
	        // Second parameter - Layout for the row
	        // Third parameter - ID of the TextView to which the data is written
	        // Forth - the Array of data
	        	
	        
	        AdapterListViewDasUltimasPartidasCompeticao adapter = new AdapterListViewDasUltimasPartidasCompeticao
	        (this,R.layout.listitem,values);


	        // Assign adapter to ListView
	        this.listView = (ListView) findViewById(R.id.listaDasUltimasPartidas);
	        listView.setAdapter(adapter);  
	        
			
			listView.setOnItemClickListener(new OnItemClickListener() 
	        {

	              @Override
	              public void onItemClick(AdapterView<?> parent, View view,
	                 int position, long id) {
	                
	               // ListView Clicked item index
	               int itemPosition     = position;
	               
	               SingletonDadosDeUmaPartidaASerMostrada armazenaDadosUmaPartidaASerMostrada =
	            		   						SingletonDadosDeUmaPartidaASerMostrada.getInstance();
	               armazenaDadosUmaPartidaASerMostrada.setDadosUmaPartida(partidasAnteriores.get(itemPosition));
	               
	               Intent criaMostrarDadosUmaPartida =
	   					new Intent(DadosPartidasAnteriores.this, MostrarDadosUmaPartida.class);
	   				startActivity(criaMostrarDadosUmaPartida);
	               
	               // ListView Clicked item value
	               //String  itemValue    = (String) listView.getItemAtPosition(position);
	              }
	        }); 
			
			this.atualizarVitoriasModoMultiplayer();
			this.atualizarDerrotasModoMultiplayer();
			        
		}
		catch(Exception e)
	    {
	    	Writer writer = new StringWriter();
	    	PrintWriter printWriter = new PrintWriter(writer);
	    	e.printStackTrace(printWriter);
	    	String s = writer.toString();
	    	Context context = getApplicationContext();
	        Toast t = Toast.makeText(context, s, Toast.LENGTH_LONG);
	        t.show();
	    }
		
		
	}
	
	/*vou enfileirar as ultimas partidas*/
	private void enfileirarPartidasAnteriores()
	{
		if(this.partidasAnteriores.size() > 1)
		{
			LinkedList<DadosPartidaParaOLog> partidasAnterioresEnfileiradas = new LinkedList<DadosPartidaParaOLog>();
			LinkedList<DadosPartidaParaOLog> copiaPartidasAnteriores = new LinkedList<DadosPartidaParaOLog>();
			
			for(int i = 0; i < this.partidasAnteriores.size(); i++)
			{
				DadosPartidaParaOLog umDadoPartida = this.partidasAnteriores.get(i);
				copiaPartidasAnteriores.add(umDadoPartida);
			}
			
			while(copiaPartidasAnteriores.size() > 0)
			{
				DadosPartidaParaOLog dadoMaisRecente = copiaPartidasAnteriores.getFirst();
				
				for(int j = 1; j < copiaPartidasAnteriores.size(); j++)
				{
					DadosPartidaParaOLog umDado = copiaPartidasAnteriores.get(j);
					try 
					{
						Date dataUmDado = new SimpleDateFormat("dd-MMM-yyyy").parse(umDado.getData());
						Date dataDadoMaisRecente = new SimpleDateFormat("dd-MMM-yyyy").parse(dadoMaisRecente.getData());
						if(dataDadoMaisRecente.before(dataUmDado) == true)
						{
							//achamos uma data mais recente ainda
							dadoMaisRecente = umDado;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				partidasAnterioresEnfileiradas.add(dadoMaisRecente);
				copiaPartidasAnteriores.remove(dadoMaisRecente);
			}
			
			//agora que ja temos a lista enfileirada...
			this.partidasAnteriores.clear();
			
			for(int k = 0; k < partidasAnterioresEnfileiradas.size(); k++)
			{
				this.partidasAnteriores.add(partidasAnterioresEnfileiradas.get(k));
			}
		}
	}
	
	private void atualizarVitoriasModoMultiplayer()
	{
		int quantasVitorias = SingletonArmazenaRankingUsuario.getInstance().getVitorias_competicao();
		
		TextView textViewVitorias = (TextView) findViewById(R.id.vitoriasmodomultiplayer);
		textViewVitorias.setText(String.valueOf(quantasVitorias));
	}
	
	private void atualizarDerrotasModoMultiplayer()
	{
		int quantasDerrotas = SingletonArmazenaRankingUsuario.getInstance().getDerrotas_competicao();
		
		TextView textViewDerrotas = (TextView) findViewById(R.id.derrotasmodomultiplayer);
		textViewDerrotas.setText(String.valueOf(quantasDerrotas));
	}

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void procedimentoConexaoFalhou() {
		// TODO Auto-generated method stub
		
	}
	
	private void mudarFonteTextosDaTela()
	{
		String fontPath2 = "fonts/Wonton.ttf";
	    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
	    TextView textoTituloCompeticao = (TextView) findViewById(R.id.textoTituloCompeticao);
	    textoTituloCompeticao.setTypeface(tf2);
	    
	    TextView dados_das_partidas_anteriores = (TextView) findViewById(R.id.dados_das_partidas_anteriores);
	    dados_das_partidas_anteriores.setTypeface(tf2);
	    
	    TextView vitoriasmodomultiplayer = (TextView) findViewById(R.id.vitoriasmodomultiplayer);
	    vitoriasmodomultiplayer.setTypeface(tf2);
	    TextView derrotasmodomultiplayer = (TextView) findViewById(R.id.derrotasmodomultiplayer);
	    derrotasmodomultiplayer.setTypeface(tf2);
	    
	    TextView ultimas_partidas = (TextView) findViewById(R.id.ultimas_partidas);
	    ultimas_partidas.setTypeface(tf2);
	    
	}
	
	@Override
	public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) 
	{
		// TODO Auto-generated method stub
		int firstVisibleItem = arg1;
		int totalItemCount = arg3;
		
		ListView listaDasUltimasPartidas = (ListView) findViewById(R.id.listaDasUltimasPartidas);
		int indiceObjetoDeBaixo = listaDasUltimasPartidas.getLastVisiblePosition();
		
		final int lastItem = firstVisibleItem + arg2;
		
		ImageView setas_partidas_anteriores = (ImageView) findViewById(R.id.setas_partidas_anteriores);
		
		if(firstVisibleItem == 0)
		{
			setas_partidas_anteriores.setImageResource(R.drawable.seta_baixo_listviews);
		}
		else if(lastItem == totalItemCount)
		{
			//a seta para baixo nao aparece
			setas_partidas_anteriores.setImageResource(R.drawable.seta_cima_listviews);
		}
		else if(totalItemCount <= 4)
		{
			//as setas nao precisam aparecer
			setas_partidas_anteriores.setVisibility(View.GONE);
		}
		else
		{
			//ambas as setas aparecem
			setas_partidas_anteriores.setImageResource(R.drawable.seta_cima_e_baixo_listviews);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}
	

}
