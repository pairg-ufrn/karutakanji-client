package com.karutakanji;

import br.ufrn.dimap.pairg.karutakanji.android.R;
import br.ufrn.dimap.pairg.karutakanji.android.R.layout;
import br.ufrn.dimap.pairg.karutakanji.android.R.menu;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class EscolherFormaDeJogoTreinamento extends ActivityDoJogoComSom
{
	private String treinarErradasOuMenosJogadas; //string so tem dois valores: "erradas" ou "menos jogadas" ou "nenhum"

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		//coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
		super.getGameHelper().setMaxAutoSignInAttempts(0);
		//fim das coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_escolher_forma_de_jogo_treinamento);
		this.treinarErradasOuMenosJogadas = "nenhum";
		this.mudarFonteTodosOsTextosDaTela();
		
		TextView textViewTextoInformaFaltaConexao = (TextView) findViewById(R.id.textoInformaLimitacaoFaltaDeConexao);
		if(temConexaoComInternet() == false)
		{
			//modo offline. Temos de proibir o usuario de clicar em qualquer um dos radiobuttons. Ele soh pdoe treinar aleatoriamente,
			//nada de palavras mais erradas ou menos treinadas
			
			RadioButton radio1 = (RadioButton) findViewById(R.id.radio_palavras_erradas);
			radio1.setClickable(false);
			
			RadioButton radio2 = (RadioButton) findViewById(R.id.radio_palavras_menos_treinadas);
			radio2.setClickable(false);
			
			RadioButton radio3 = (RadioButton) findViewById(R.id.radio_palavras_aleatoriamente);
			radio3.setClickable(false);
			
			String textoSemConexao = getResources().getString(R.string.texto_informa_limitacao_falta_de_conexao);
			textViewTextoInformaFaltaConexao.setText(textoSemConexao);
			
		}
		else
		{
			RadioButton radio1 = (RadioButton) findViewById(R.id.radio_palavras_erradas);
			radio1.setClickable(true);
			
			RadioButton radio2 = (RadioButton) findViewById(R.id.radio_palavras_menos_treinadas);
			radio2.setClickable(true);
			
			RadioButton radio3 = (RadioButton) findViewById(R.id.radio_palavras_aleatoriamente);
			radio3.setClickable(true);
			
			textViewTextoInformaFaltaConexao.setText("");
			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		super.onCreateOptionsMenu(menu);
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.escolher_forma_de_jogo_treinamento,
				menu);
		return true;
	}
	
	public void botaoOkEscolherFormaDeJogoTreinamentoClicado(View v)
	{
		if(treinarErradasOuMenosJogadas.compareTo("erradas") == 0)
			{
				SingletonConfiguracoesModoTreinamento.getInstance().setTreinarPalavrasErradas(true);
				SingletonConfiguracoesModoTreinamento.getInstance().setTreinarPalavrasMenosTreinadas(false);
			}
			else if(treinarErradasOuMenosJogadas.compareTo("nenhum") == 0)
			{
				SingletonConfiguracoesModoTreinamento.getInstance().setTreinarPalavrasErradas(false);
				SingletonConfiguracoesModoTreinamento.getInstance().setTreinarPalavrasMenosTreinadas(false);
			}
			else
			{
				//menos treinadas
				SingletonConfiguracoesModoTreinamento.getInstance().setTreinarPalavrasErradas(false);
				SingletonConfiguracoesModoTreinamento.getInstance().setTreinarPalavrasMenosTreinadas(true);
			}
		
		mudarMusicaDeFundo(R.raw.japan_in_spring);
		Intent criaTelaModoTreinamento =
					new Intent(EscolherFormaDeJogoTreinamento.this, ModoTreinamento.class);
		startActivity(criaTelaModoTreinamento);
		
	}
	
	public void onRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.radio_palavras_erradas:
	            if (checked)
	            	this.treinarErradasOuMenosJogadas = "erradas";
	            break;
	        case R.id.radio_palavras_menos_treinadas:
	            if (checked)
	            	this.treinarErradasOuMenosJogadas = "menos jogadas";
	            break;
	        case R.id.radio_palavras_aleatoriamente:
	            if (checked)
	            	this.treinarErradasOuMenosJogadas = "nenhum";
	            break;
	    }
	}

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	private void mudarFonteTodosOsTextosDaTela()
	{
		String fontPath = "fonts/Wonton.ttf";
	    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
	    TextView textooo = (TextView) findViewById(R.id.textoooooooo);
	    textooo.setTypeface(tf);
	    
	    RadioButton radioPalavrasErradas = (RadioButton) findViewById(R.id.radio_palavras_erradas);
	    RadioButton radioPalavrasAleatoriamente = (RadioButton) findViewById(R.id.radio_palavras_aleatoriamente);
	    RadioButton radioPalavrasMenosTreinadas = (RadioButton) findViewById(R.id.radio_palavras_menos_treinadas);
	    radioPalavrasErradas.setTypeface(tf);
	    radioPalavrasAleatoriamente.setTypeface(tf);
	    radioPalavrasMenosTreinadas.setTypeface(tf);
	    
	    TextView textoTituloEscolhaFormaDeJogarModoTreinamento = (TextView) findViewById(R.id.textoTituloEscolhaFormaDeJogarModoTreinamento);
	    textoTituloEscolhaFormaDeJogarModoTreinamento.setTypeface(tf);
	    
	    TextView textViewTextoInformaFaltaConexao = (TextView) findViewById(R.id.textoInformaLimitacaoFaltaDeConexao);
	    textViewTextoInformaFaltaConexao.setTypeface(tf);
	}
	
	private boolean temConexaoComInternet() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
