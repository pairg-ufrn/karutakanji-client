package com.karutakanji;

import bancodedados.TaskAcharUsuarioPorEMailRecuperarSenha;
import br.ufrn.dimap.pairg.karutakanji.android.R;
import br.ufrn.dimap.pairg.karutakanji.android.R.layout;
import br.ufrn.dimap.pairg.karutakanji.android.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EsqueceuSenha extends ActivityDoJogoComSom {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		//coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
		super.getGameHelper().setMaxAutoSignInAttempts(0);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_esqueceu_senha);
		
		this.mudarFonteDosTextosDaTela();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.esqueceu_senha, menu);
		return true;
	}

	@Override
	public void onSignInFailed() 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() 
	{
		// TODO Auto-generated method stub
		
	}
	
	
	public void enviarEmailPerdeuSenha(View v)
	{
		ProgressDialog loadingPegarSenha = ProgressDialog.show(EsqueceuSenha.this, getResources().getString(R.string.obtendoSenha), getResources().getString(R.string.por_favor_aguarde));
        TaskAcharUsuarioPorEMailRecuperarSenha pegaSenha = new TaskAcharUsuarioPorEMailRecuperarSenha(loadingPegarSenha, this);
        EditText textFieldEmailUsuarioEsqueceuSenha = (EditText) findViewById(R.id.textFieldEmailUsuarioEsqueceuSenha);
        String emailUsuario = textFieldEmailUsuarioEsqueceuSenha.getText().toString();
        pegaSenha.execute(emailUsuario);
        
      //FAZER DESAPARECER TECLADO VIRTUAL
		InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE); 
		View view = getCurrentFocus();
		if(view != null)
		{
			inputManager.hideSoftInputFromWindow(view.getWindowToken(),
	                   InputMethodManager.HIDE_NOT_ALWAYS);
		}
		//FIM DO FAZER DESAPARECER TECLADO VIRTUAL
        
	}
	
	/*funcao chamada no final da task TaskAcharUsuarioPorEMailRecuperarSenha*/
	public void enviarEMailSenhaPerdida(String email, String senha, String nome_usuario)
	{
		//GMailSender sender = new GMailSender("fabioandrewsrochamarques@gmail.com", "Doremirocks4");
		
		try 
		{
			TaskEnviaEMail task = new TaskEnviaEMail();
			
			String senha_do_karuta_kanji = getResources().getString(R.string.senha_do_karuta_kanji);
			String assuntoDoEmail = senha_do_karuta_kanji;
			
			String suaSenhaEh = getResources().getString(R.string.suaSenhaEh);
			String corpoDoEmail = nome_usuario + ", " + suaSenhaEh + " " + senha;
			String quemEnviouOEmail = "fabioandrewsrochamarques@gmail.com";
			String emailDestinatario = email;
			
			task.execute(assuntoDoEmail, corpoDoEmail, quemEnviouOEmail, emailDestinatario);
			String email_enviado = getResources().getString(R.string.email_enviado);
			Toast t = Toast.makeText(this, email_enviado, Toast.LENGTH_LONG);
		    t.show();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast t = Toast.makeText(this, "erro", Toast.LENGTH_LONG);
		    t.show();
		} 
	}
	
	public void voltarParaTelaInicial(View v)
	{
		Intent i = new Intent(getApplicationContext(), MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
	}
	
	public void naoAchouSenhaParaEsteEMail()
	{
		String nao_ha_usuarios = getResources().getString(R.string.nao_ha_usuarios);
		Toast t = Toast.makeText(this, nao_ha_usuarios, Toast.LENGTH_LONG);
	    t.show();
	}
	
	private void mudarFonteDosTextosDaTela()
	{
		String fontPath = "fonts/Wonton.ttf";
	    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
	    
		TextView informeUmEmail = (TextView) findViewById(R.id.informeUmEmail);
		TextView emailUsuario = (TextView) findViewById(R.id.emailUsuario);
		
		informeUmEmail.setTypeface(tf);
		emailUsuario.setTypeface(tf);
		
	}

}
