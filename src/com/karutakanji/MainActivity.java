package com.karutakanji;



import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;

import lojinha.ConcreteDAOAcessaDinheiroDoJogador;
import lojinha.DAOAcessaDinheiroDoJogador;

import bancodedados.ArmazenaTudoParaJogoOffline;
import bancodedados.ChecaVersaoAtualDoSistemaTask;
import bancodedados.DadosPartidaParaOLog;
import bancodedados.EnviarDadosDaPartidaParaLogTask;
import bancodedados.InserirUsuarioNoBDTask;
import bancodedados.KanjiTreinar;
import bancodedados.PegaRankingDoUsuarioTask;
import bancodedados.RealizarLoginUsuarioTask;
import bancodedados.SingletonArmazenaRankingUsuario;
import bancodedados.SolicitaKanjisParaTreinoTask;
import br.ufrn.dimap.pairg.karutakanji.android.R;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.PorterDuff;

public class MainActivity extends ActivityDoJogoComSom implements View.OnClickListener
{
	private ChecaVersaoAtualDoSistemaTask checaVersaoAtual;
	final static int[] SCREENS = {R.id.telaatualizeojogo, R.id.telainicialnormal,R.id.telaloginusuario,R.id.telacadastrousuario};
	private boolean haConexaoComInternet;
	private ProgressDialog loadingFazendoLogin;
	private boolean manterLogado; //manter usuario logado ate fechar a activity?
	private boolean lembrarDeMim; //auto-preencher os textfields com os valores?
	private Handler handlerParaMascoteAndar;
	private Runnable runnableThreadParaMascoteAndar; 
	private boolean runnableThreadmascoteAndaDeveriaMorrer;
	private int quantasVezesABonecaAndouEmUmaDirecao; //quanto alcancar um valor especifico, ela deveria ir p a outra direcao
	private boolean mascoteAndandoParaADireita;
	private float posicaoXDaMascoteAoIniciarAplicativo; //devemos fazer a mascote voltar a posicao inicia dela antes de faze-la andar
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
        
		//coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
		super.getGameHelper().setMaxAutoSignInAttempts(0);
		//fim das coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
				
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.carregarESePrecisarAlterarLinguaDoJogo(); //serah que a lingua atual do jogo deveria ser essa msm ou nao?
		
		//vamos pegar a posicao inicial da mascote!
		ImageView mascote = (ImageView) findViewById(R.id.mascotefull);
		this.posicaoXDaMascoteAoIniciarAplicativo = mascote.getX();
		
		//switchToScreen(R.id.telainicialnormal);
		
		//tornar o link esqueceu a senha clicavel e sublinhado
		TextView textViewEsqueceuSenha = (TextView) findViewById(R.id.esqueceu_senha);
		String esqueceu_a_senha = getResources().getString(R.string.esqueceu_a_senha);
		
		SpannableString content = new SpannableString(esqueceu_a_senha);
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		textViewEsqueceuSenha.setText(content);
		textViewEsqueceuSenha.setOnTouchListener(new OnTouchListener(){
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				Intent i = new Intent(getApplicationContext(), EsqueceuSenha.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            startActivity(i);
				// TODO Auto-generated method stub
				return false;
			}
	    });
		
		
		ImageView icone_configuracoes = (ImageView) findViewById(R.id.icone_configuracoes);
		icone_configuracoes.setOnClickListener(this);
		
		ArmazenaTudoParaJogoOffline armazenaTudoParaOffline = 
				ArmazenaTudoParaJogoOffline.getInstance();
		armazenaTudoParaOffline.aumentarQuantasVezesAAplicacaoFoiReiniciada(this);
		
		this.haConexaoComInternet = this.temConexaoComInternet();
		if(haConexaoComInternet == true)
		{
			
			this.checaVersaoAtual = new ChecaVersaoAtualDoSistemaTask(this);
			this.checaVersaoAtual.execute("");
			
			this.lembrarDeMim = armazenaTudoParaOffline.getLembrarDeMim(this);

			this.manterLogado = armazenaTudoParaOffline.getManterLogado(this);
			boolean deveMostrarTelaLoginSegundoSingleton = 
					SingletonDeveMostrarTelaLoginNaMainActivity.getInstance().getDeveMostrarTelaLogin();
			
			this.mudarCheckBoxesDessaTelaDeAcordoComBooleanos();
			
			LinkedList<String> nomeUsuarioEEmail = 
					armazenaTudoParaOffline.getLoginUsuarioLocalmente(this);
			
			if((this.manterLogado == false && deveMostrarTelaLoginSegundoSingleton == true)|| nomeUsuarioEEmail.get(0).length() == 0)
			{
				//fazer login do usuário!!!
				switchToScreen(R.id.telaloginusuario);
				
				getGameHelper().signOut();
				
				Button botaoCadastrar = (Button) findViewById(R.id.botao_cadastrar);
				botaoCadastrar.setClickable(true);
				Button botaoLogin= (Button) findViewById(R.id.botao_login);
				botaoLogin.setClickable(true);
				
				if(nomeUsuarioEEmail.get(0).length() > 0 && lembrarDeMim == true)
				{
					//ja tinha um login antes? Vamos preencher as combo box
					EditText editTextSenhaUsuario = (EditText) findViewById(R.id.textFieldSenhaUsuario);
					editTextSenhaUsuario.setText(nomeUsuarioEEmail.get(2));
					
					EditText editTextEmailUsuario = (EditText) findViewById(R.id.textFieldEmailUsuario);
					editTextEmailUsuario.setText(nomeUsuarioEEmail.get(1));
				}
				
				this.mudarFonteTextosTelaLogin();
			}
			else
			{
				//this.irAoModoTreinamento(null); PARA QUANDO O JOGO DEVERIA IR DIRETO AO MODO TREINAMENTO
				switchToScreen(R.id.telainicialnormal);
				this.mudarFonteTextosTelaInicialNormal();
				this.mudarTextoVersaoAtual();
				fazerMascoteAndarDeUmLadoParaOutro();
			}
		}
		else
		{
			//nao ha conexao com internet. Apenas valores armazenados localmente serao usados
			//this.irAoModoTreinamento(null);
			switchToScreen(R.id.telainicialnormal);
			
			this.executarProcedimentoErroConexao();
			this.mudarFonteTextosTelaInicialNormal();
			
			fazerMascoteAndarDeUmLadoParaOutro();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		ArmazenaTudoParaJogoOffline armazenaTudoParaOffline = 
				ArmazenaTudoParaJogoOffline.getInstance();
		this.haConexaoComInternet = this.temConexaoComInternet();
		if(haConexaoComInternet == true)
		{
			this.checaVersaoAtual = new ChecaVersaoAtualDoSistemaTask(this);
			this.checaVersaoAtual.execute("");
			
			this.lembrarDeMim = armazenaTudoParaOffline.getLembrarDeMim(this);

			this.manterLogado = armazenaTudoParaOffline.getManterLogado(this);
			boolean deveMostrarTelaLoginSegundoSingleton = 
					SingletonDeveMostrarTelaLoginNaMainActivity.getInstance().getDeveMostrarTelaLogin();
			
			this.mudarCheckBoxesDessaTelaDeAcordoComBooleanos();
			
			LinkedList<String> nomeUsuarioEEmail = 
					armazenaTudoParaOffline.getLoginUsuarioLocalmente(this);
			
			if((this.manterLogado == false && deveMostrarTelaLoginSegundoSingleton == true)|| nomeUsuarioEEmail.get(0).length() == 0)
			{
				//fazer login do usuário!!!
				switchToScreen(R.id.telaloginusuario);
				
				getGameHelper().signOut();
				
				Button botaoCadastrar = (Button) findViewById(R.id.botao_cadastrar);
				botaoCadastrar.setClickable(true);
				Button botaoLogin= (Button) findViewById(R.id.botao_login);
				botaoLogin.setClickable(true);
				
				if(nomeUsuarioEEmail.get(0).length() > 0 && lembrarDeMim == true)
				{
					//ja tinha um login antes? Vamos preencher as combo box
					EditText editTextSenhaUsuario = (EditText) findViewById(R.id.textFieldSenhaUsuario);
					editTextSenhaUsuario.setText(nomeUsuarioEEmail.get(2));
					
					EditText editTextEmailUsuario = (EditText) findViewById(R.id.textFieldEmailUsuario);
					editTextEmailUsuario.setText(nomeUsuarioEEmail.get(1));
				}
				
				this.mudarFonteTextosTelaLogin();
			}
			else
			{
				//this.irAoModoTreinamento(null); PARA QUANDO O JOGO DEVERIA IR DIRETO AO MODO TREINAMENTO
				switchToScreen(R.id.telainicialnormal);
				this.mudarFonteTextosTelaInicialNormal();
				this.mudarTextoVersaoAtual();
				fazerMascoteAndarDeUmLadoParaOutro();
			}
		}
		else
		{
			//nao ha conexao com internet. Apenas valores armazenados localmente serao usados
			//this.irAoModoTreinamento(null);
			switchToScreen(R.id.telainicialnormal);
			
			this.executarProcedimentoErroConexao();
			this.mudarFonteTextosTelaInicialNormal();
			
			fazerMascoteAndarDeUmLadoParaOutro();
		}
	}
	@Override
	public void onRestart()
	{
		super.onRestart();
	}
	
	public void irAoModoCasual(View view)
	{
		if(this.haConexaoComInternet == true && this.deviceHasGoogleAccount() == true)
		{
			try
			{
				Intent criaTelaModoCasual =
						new Intent(MainActivity.this, ModoCasual.class);
				startActivity(criaTelaModoCasual);
			}
			catch(Exception e)
			{
				Writer writer = new StringWriter();
		    	PrintWriter printWriter = new PrintWriter(writer);
		    	e.printStackTrace(printWriter);
		    	String s = writer.toString();
		    	Context context = getApplicationContext();
		        Toast t = Toast.makeText(context, s, Toast.LENGTH_LONG);
		        t.show();
			}
		}
		else if(this.deviceHasGoogleAccount() == false)
		{
			String s = getResources().getString(R.string.texto_informa_precisa_de_google_account);
			Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
	        t.show();
		}
		else
		{
			//nao pode ir para o casual
		}
	}
	
	
	
	public void irAoModoTreinamento(View view)
	{
		try
		{
			ArmazenaConfiguracoesDoJogo conheceValorMostrarRegrasTreinamento = 
												ArmazenaConfiguracoesDoJogo.getInstance();
			boolean deveMostrarRegrasTreinamento = 
					conheceValorMostrarRegrasTreinamento.getMostrarRegrasDoTreinamento(this);
			if(deveMostrarRegrasTreinamento == false)
			{
				Intent criaTelaModoTreinamento =
						new Intent(MainActivity.this, EscolherCategoriasModoTreinamento.class);
				startActivity(criaTelaModoTreinamento);
			}
			else
			{
				//as regras do modo treinamento deveriam ser mostradas
				Intent criaTelaModoTreinamento =
						new Intent(MainActivity.this, MostrarRegrasModoTreinamento.class);
				startActivity(criaTelaModoTreinamento);
			}
		}
		catch(Exception e)
		{
			Writer writer = new StringWriter();
	    	PrintWriter printWriter = new PrintWriter(writer);
	    	e.printStackTrace(printWriter);
	    	String s = writer.toString();
	    	Context context = getApplicationContext();
	        Toast t = Toast.makeText(context, s, Toast.LENGTH_LONG);
	        t.show();
		}
		
	}
	
	public void irAoModoCompeticao(View v)
	{
		if(this.haConexaoComInternet == true && this.deviceHasGoogleAccount() == true)
		{
			Intent criaTelaModoCompeticao =
					new Intent(MainActivity.this, ModoCompeticao.class);
			startActivity(criaTelaModoCompeticao);
		}
		else if(this.deviceHasGoogleAccount() == false)
		{
			String s = getResources().getString(R.string.texto_informa_precisa_de_google_account);
			Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
	        t.show();
		}
		else
		{
			//nao pode ir para o competicao
		}
	}
	
	 public void fazerToast(String mensagem)
	 {
		 Toast t = Toast.makeText(this, mensagem, Toast.LENGTH_LONG);
		  t.show();
	 }
	 
	 public void irADadosPartidasAnteriores(View v)
	 {
		 if(this.haConexaoComInternet)
		 {
			 Intent criaTelaDadosAnteriores =
						new Intent(MainActivity.this, DadosPartidasAnteriores.class);
				startActivity(criaTelaDadosAnteriores);
		 }
	 }
	 
	 public void irALojinha(View v)
	 {
		 String lojinha_versoes_futuras = getResources().getString(R.string.lojinha_vira_em_versoes_futuras);
		 this.mostrarErro(lojinha_versoes_futuras);
		 /*Intent criaLojinha =
					new Intent(MainActivity.this, LojinhaMaceteKanjiActivity.class);
			startActivity(criaLojinha);*/
	 }
	 
	 public void adicionarDinheirinho(View v)
	 {
			
			DAOAcessaDinheiroDoJogador acessaDinheiroDoJogador = ConcreteDAOAcessaDinheiroDoJogador.getInstance();
			acessaDinheiroDoJogador.adicionarCredito(1500, this);
			int creditoAtual = acessaDinheiroDoJogador.getCreditoQuePossui(this);
			
	 }
	 
	 public void irParaConfiguracoes(View view)
	 {
		 Intent irParaConfiguracoes =
					new Intent(MainActivity.this, Configuracoes.class);
			startActivity(irParaConfiguracoes);
	 }
	 
	 @Override
		protected void onPause()
		{
		 	this.runnableThreadmascoteAndaDeveriaMorrer = true;
			
			//TocadorMusicaBackground.getInstance().pausarTocadorMusica();
			if(this.isFinishing())
			{
				//Toast.makeText(MainActivity.this, "is finishing will stop service", Toast.LENGTH_SHORT).show();
				Intent iniciaMusicaFundo = new Intent(MainActivity.this, BackgroundSoundService.class);
				stopService(iniciaMusicaFundo);
			}
			super.onPause();
			
		}

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	void switchToScreen(int screenId) {
		// make the requested screen visible; hide all others.
		for (int id : SCREENS) {
		    findViewById(id).setVisibility(screenId == id ? View.VISIBLE : View.GONE);
		}
	}
	
	public void mudarParaTelaAtualizeOJogo(String versaoMaisAtual)
	{
		switchToScreen(R.id.telaatualizeojogo);
		
		String stringMensagemAtualizeJogo = getResources().getString(R.string.mensagem_por_favor_atualize_o_jogo);
		stringMensagemAtualizeJogo = stringMensagemAtualizeJogo + versaoMaisAtual;
		
		TextView textViewAtualize = (TextView) findViewById(R.id.mensagemAtualizeOJogo);
		textViewAtualize.setText(stringMensagemAtualizeJogo);
	}
	
	public void mostrarErro(String erro)
	{
		Toast t = Toast.makeText(this, erro, Toast.LENGTH_LONG);
	    t.show();
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
	    	case R.id.karuta1_imageview:
	    		ImageView imageViewCarta = (ImageView) findViewById(R.id.karuta1_imageview);
	    		imageViewCarta.setColorFilter(Color.rgb(123, 123, 123), android.graphics.PorterDuff.Mode.MULTIPLY);
	    	break;
	    	case R.id.checkbox_lembrar_de_mim_login:
	    		if(this.lembrarDeMim == true)
	    		{
	    			this.lembrarDeMim = false;
	    		}
	    		else
	    		{
	    			this.lembrarDeMim = true;
	    		}
	    		this.mudarCheckBoxesDessaTelaDeAcordoComBooleanos();
	    	break;
	    	case R.id.checkbox_manter_logado_tela_login:
	    		if(this.manterLogado == true)
	    		{
	    			this.manterLogado = false;
	    		}
	    		else
	    		{
	    			this.manterLogado = true;
	    		}
	    		this.mudarCheckBoxesDessaTelaDeAcordoComBooleanos();
	    	break;
	    	case R.id.icone_configuracoes:
	    		this.irParaConfiguracoes(null);
	    	break;
	    	
		}
	}
	
	private boolean temConexaoComInternet() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	public void executarProcedimentoErroConexao()
	{
		String versaoMaisRecente = 
				ArmazenaTudoParaJogoOffline.getInstance().getVersaoMaisRecenteDoSistemaLocalmente(this);
		ChecaVersaoAtualDoSistemaTask checaVersaoAtualDoSistema = new ChecaVersaoAtualDoSistemaTask(this);
		String versaoDoAndroidDoUsuario = 
			checaVersaoAtualDoSistema.getVersaoDoSistema();
		
		boolean conseguiuCarregarListaDePalavrasOffline = 
				ArmazenaTudoParaJogoOffline.getInstance().carregarListasDePalavrasSalvasAnteriormente(this);
		
		if(versaoMaisRecente.length() == 0 || conseguiuCarregarListaDePalavrasOffline == false)
		{
			//caso da primeira vez que o usuario inicie o jogo ele nao tenha conexao com internet, ele nao deve poder fazer nada pq nem a versao mais atual do sistema foi obtida uma unica vez
			this.mudarParaTelaAtualizeOJogo("");
			
			TextView textViewFalhaConexao = (TextView) findViewById(R.id.mensagemAtualizeOJogo);
			String mensagemFaltaConexao = getResources().getString(R.string.erro_falta_conexao_primeira_vez);
			textViewFalhaConexao.setText(mensagemFaltaConexao);
		}
		else if(versaoMaisRecente.compareTo(versaoDoAndroidDoUsuario) != 0)
		{
			this.mudarParaTelaAtualizeOJogo(versaoMaisRecente);
		}
		else
		{
			TextView textViewFalhaConexao = (TextView) findViewById(R.id.textAvisaAoUsuarioFaltaDeConexao);
			String mensagemFaltaConexao = getResources().getString(R.string.erro_falta_conexao);
			textViewFalhaConexao.setText(mensagemFaltaConexao);
		}
	}
	
	/*durante a criação de usuário, o nome que o usuario informou pode ja existir*/
	public void mostrarMensagemNomeDeUsuarioJaExiste()
	{
		String s = getResources().getString(R.string.nome_usuario_ja_existe);
		Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
        t.show();
        
        this.loadingFazendoLogin.dismiss();
	}
	
	/*durante a criação de usuário, o email que o usuario informou pode ja existir*/
	public void mostrarMensagemEmailDeUsuarioJaExiste()
	{
		String s = getResources().getString(R.string.email_ja_existe);
		Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
        t.show();
        
        this.loadingFazendoLogin.dismiss();
	}
	
	/*durante o login de usuário, o usuario pode passar um login q nao existe*/
	public void mostrarMensagemLoginNaoExiste()
	{
		String s = getResources().getString(R.string.login_nao_existe);
		Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
        t.show();
        
        this.loadingFazendoLogin.dismiss();
	}
	
	private void mostrarMensagemInformeUmEmailDeUsuario()
	{
		String s = getResources().getString(R.string.informe_um_email_de_usuario);
		Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
        t.show();
	}
	private void mostrarMensagemInformeUmaSenhaDeUsuario()
	{
		String s = getResources().getString(R.string.informe_uma_senha_de_usuario);
		Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
        t.show();
	}
	private void mostrarMensagemInformeUmNomeDeUsuario()
	{
		String s = getResources().getString(R.string.informe_um_nome_de_usuario);
		Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
        t.show();
	}
	
	
	
	public void fazercadastro(View v)
	{
		//FAZER DESAPARECER TECLADO VIRTUAL
		InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE); 
		View view = getCurrentFocus();
		if(view != null)
		{
			inputManager.hideSoftInputFromWindow(view.getWindowToken(),
	                   InputMethodManager.HIDE_NOT_ALWAYS);
		}
		//FIM DO FAZER DESAPARECER TECLADO VIRTUAL
		
		EditText textFieldNomeUsuario = (EditText)findViewById(R.id.textFieldnomeUsuarioCadastro);
		EditText textFieldEmailUsuario = (EditText)findViewById(R.id.textFieldEmailUsuarioCadastro);
		EditText textFieldSenhaUsuario = (EditText)findViewById(R.id.textFieldSenhaUsuarioCadastro);
		
		String nome_usuario = textFieldNomeUsuario.getText().toString();
		String senha_usuario = textFieldSenhaUsuario.getText().toString();
		String email_usuario = textFieldEmailUsuario.getText().toString();
		
		if(nome_usuario.length() > 0 && senha_usuario.length() > 0 && email_usuario.length() > 0)
		{
			
			InserirUsuarioNoBDTask insereUsuarioNoBd = new InserirUsuarioNoBDTask(this);
			insereUsuarioNoBd.execute(nome_usuario,email_usuario,senha_usuario);
			
			this.loadingFazendoLogin = ProgressDialog.show(this, getResources().getString(R.string.fazendo_cadastro), getResources().getString(R.string.por_favor_aguarde));
		}
		else if(nome_usuario.length() == 0)
		{
			this.mostrarMensagemInformeUmNomeDeUsuario();
		}
		else if(email_usuario.length() == 0)
		{
			this.mostrarMensagemInformeUmEmailDeUsuario();
		}
		else if(senha_usuario.length() == 0)
		{
			this.mostrarMensagemInformeUmaSenhaDeUsuario();
		}
	}
	
	public void terminarFazerCadastro()
	{
		this.loadingFazendoLogin.dismiss();
		
		EditText textFieldNomeUsuario = (EditText)findViewById(R.id.textFieldnomeUsuarioCadastro);
		EditText textFieldEmailUsuario = (EditText)findViewById(R.id.textFieldEmailUsuarioCadastro);
		EditText textFieldSenhaUsuario = (EditText)findViewById(R.id.textFieldSenhaUsuarioCadastro);
		
		String nome_usuario = textFieldNomeUsuario.getText().toString();
		String email_usuario = textFieldEmailUsuario.getText().toString();
		String senha_usuario = textFieldSenhaUsuario.getText().toString();
		
		ArmazenaTudoParaJogoOffline armazenaLoginUsuario = 
						ArmazenaTudoParaJogoOffline.getInstance();
		armazenaLoginUsuario.salvarLoginUsuarioParaUsoFuturo(getApplicationContext(), nome_usuario, email_usuario,senha_usuario);
		
		
		switchToScreen(R.id.telaloginusuario);
		getGameHelper().signOut();
		
		this.mudarFonteTextosTelaLogin();
		
		
		EditText textFieldEmailUsuarioLogin = (EditText)findViewById(R.id.textFieldEmailUsuario);
		EditText textFieldSenhaUsuarioLogin = (EditText)findViewById(R.id.textFieldSenhaUsuario);
		textFieldEmailUsuarioLogin.setText(email_usuario);
		textFieldSenhaUsuarioLogin.setText(senha_usuario);
		
		//this.irAoModoTreinamento(null); PARA QUANDO O JOGO DEVERIA TER SOH O MODO TREINAMENTO
		
		String s = getResources().getString(R.string.cadastro_efetuado_com_sucesso);
		Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
        t.show();
        
        fazerMascoteAndarDeUmLadoParaOutro();
        TextView textViewFalhaConexao = (TextView) findViewById(R.id.textAvisaAoUsuarioFaltaDeConexao);
		textViewFalhaConexao.setText("");
	}
	
	public void fazerlogin(View v)
	{
		getGameHelper().signOut();
		
		//FAZER DESAPARECER TECLADO VIRTUAL
				InputMethodManager inputManager = (InputMethodManager)
		                getSystemService(Context.INPUT_METHOD_SERVICE); 
				View view = getCurrentFocus();
				if(view != null)
				{
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
			                   InputMethodManager.HIDE_NOT_ALWAYS);
				}
				//FIM DO FAZER DESAPARECER TECLADO VIRTUAL
		
		EditText textFieldSenhaUsuario = (EditText)findViewById(R.id.textFieldSenhaUsuario);
		EditText textFieldEmailUsuario = (EditText)findViewById(R.id.textFieldEmailUsuario);
		
		String senha_usuario = textFieldSenhaUsuario.getText().toString();
		String email_usuario = textFieldEmailUsuario.getText().toString();
		
		if(email_usuario.length() > 0)
		{
			
			RealizarLoginUsuarioTask realizaLogin = new RealizarLoginUsuarioTask(this);
			realizaLogin.execute(email_usuario,senha_usuario);
			
			this.loadingFazendoLogin = ProgressDialog.show(this, getResources().getString(R.string.fazendo_login), getResources().getString(R.string.por_favor_aguarde));
		}
		else
		{
			this.mostrarMensagemInformeUmEmailDeUsuario();
		}
	}
	public void terminarFazerLogin(String nome_usuario)
	{
		this.loadingFazendoLogin.dismiss();
		EditText textFieldSenhaUsuario = (EditText)findViewById(R.id.textFieldSenhaUsuario);
		EditText textFieldEmailUsuario = (EditText)findViewById(R.id.textFieldEmailUsuario);
		
		String senha_usuario = textFieldSenhaUsuario.getText().toString();
		String email_usuario = textFieldEmailUsuario.getText().toString();
		
		ArmazenaTudoParaJogoOffline armazenaLoginUsuario = 
						ArmazenaTudoParaJogoOffline.getInstance();
		armazenaLoginUsuario.salvarLoginUsuarioParaUsoFuturo(getApplicationContext(), nome_usuario, email_usuario, senha_usuario);
		armazenaLoginUsuario.salvarLembrarDeMim(this,this.lembrarDeMim);
		armazenaLoginUsuario.salvarManterLogado(this,this.manterLogado);
		
		SingletonDeveMostrarTelaLoginNaMainActivity.getInstance().setDeveMostrarTelaLogin(false); //o user jah fez login, nao tem pra que mostrar de novo
		
		
		switchToScreen(R.id.telainicialnormal);
		
		this.mudarFonteTextosTelaInicialNormal();
		
		this.mudarTextoVersaoAtual();
		
		String s = getResources().getString(R.string.bem_vindo);
		s = s + " " + nome_usuario;
		Toast t = Toast.makeText(this, s, Toast.LENGTH_LONG);
        t.show();
        
        fazerMascoteAndarDeUmLadoParaOutro();
		//this.irAoModoTreinamento(null); PARA QUANDO O JOGO DEVERIA TER SOH O MODO TREINAMENTO
        TextView textViewFalhaConexao = (TextView) findViewById(R.id.textAvisaAoUsuarioFaltaDeConexao);
		textViewFalhaConexao.setText("");
	}
	
	
	
	private boolean deviceHasGoogleAccount(){
        AccountManager accMan = AccountManager.get(this);
        Account[] accArray = accMan.getAccountsByType("com.google");
        return accArray.length >= 1 ? true : false;
	}
	
	private void mudarFonteTextosTelaLogin()
	{
		String fontPath = "fonts/Wonton.ttf";
	    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
	    
		TextView descricaoLogin = (TextView) findViewById(R.id.descricaoLogin);
		TextView senhaUsuario = (TextView) findViewById(R.id.senhaUsuario);
		TextView emailUsuario = (TextView) findViewById(R.id.emailUsuario);
		
		TextView texto_manter_logado = (TextView) findViewById(R.id.texto_manter_logado);
		texto_manter_logado.setTypeface(tf);
		TextView texto_lembrar_de_mim = (TextView) findViewById(R.id.texto_lembrar_de_mim);
		texto_lembrar_de_mim.setTypeface(tf);
		
		descricaoLogin.setTypeface(tf);
		senhaUsuario.setTypeface(tf);
		emailUsuario.setTypeface(tf);
		
		TextView esqueceu_senha = (TextView) findViewById(R.id.esqueceu_senha);
		esqueceu_senha.setTypeface(tf);
		
	}
	
	private void mudarFonteTextosTelaInicialNormal()
	{
		String fontPath = "fonts/Wonton.ttf";
	    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
	    
		TextView textVersaoAtual = (TextView) findViewById(R.id.textVersaoAtual);
		textVersaoAtual.setTypeface(tf);
		TextView textAvisaAoUsuarioFaltaDeConexao = (TextView) findViewById(R.id.textAvisaAoUsuarioFaltaDeConexao);
		textAvisaAoUsuarioFaltaDeConexao.setTypeface(tf);
		
		Button button1 = (Button) findViewById(R.id.botaoCasual);
		Button button2 = (Button) findViewById(R.id.botaoTreinamento);
		Button botaoCompeticao = (Button) findViewById(R.id.botaoCompeticao);
		//Button button6 = (Button) findViewById(R.id.botaoConfiguracoes);
		
		button1.setTypeface(tf);
		button2.setTypeface(tf);
		//button6.setTypeface(tf);
		botaoCompeticao.setTypeface(tf);
		
	}
	
	private void mudarFonteTextosTelaCadastro()
	{
		String fontPath = "fonts/Wonton.ttf";
	    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
	    
		TextView textoCadastro = (TextView) findViewById(R.id.textoCadastro);
		textoCadastro.setTypeface(tf);
		
		TextView nomeUsuario = (TextView) findViewById(R.id.nomeUsuario);
		nomeUsuario.setTypeface(tf);
		TextView emailUsuario = (TextView) findViewById(R.id.emailUsuarioCadastro);
		emailUsuario.setTypeface(tf);
		TextView senhaUsuario = (TextView) findViewById(R.id.senhaUsuarioCadastro);
		senhaUsuario.setTypeface(tf);
		
	}
	
	private void mudarTextoVersaoAtual()
	{
		//String versaoDoSistema = this.checaVersaoAtual.getVersaoDoSistema();
		//String versao =  getResources().getString(R.string.versao);
		TextView textVersaoAtual = (TextView) findViewById(R.id.textVersaoAtual);
		//textVersaoAtual.setText(versao + " " + versaoDoSistema);
		textVersaoAtual.setText(""); //melhor manter a versao atual nas configuracoes
	}
	
	public void irparatelacadastro(View v)
	{
		switchToScreen(R.id.telacadastrousuario);
		this.mudarFonteTextosTelaCadastro();
		
	}
	
	public void voltarparateladelogin(View v)
	{
		switchToScreen(R.id.telaloginusuario);
		getGameHelper().signOut();
	}
	
	
	private void mudarCheckBoxesDessaTelaDeAcordoComBooleanos()
	{
		ImageView imageViewCheckbox = (ImageView) findViewById(R.id.checkbox_manter_logado_tela_login);
		imageViewCheckbox.setClickable(true);
		imageViewCheckbox.setOnClickListener(this);
		
		ImageView imageViewCheckbox2 = (ImageView) findViewById(R.id.checkbox_lembrar_de_mim_login);
		imageViewCheckbox2.setClickable(true);
		imageViewCheckbox2.setOnClickListener(this);
		
		if(this.manterLogado == true)
		{
			imageViewCheckbox.setImageResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			imageViewCheckbox.setImageResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		
		if(this.lembrarDeMim == true)
		{
			imageViewCheckbox2.setImageResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		else
		{
			imageViewCheckbox2.setImageResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
	}
	
	private void fazerMascoteAndarDeUmLadoParaOutro()
	{
		this.runnableThreadmascoteAndaDeveriaMorrer = false;
		this.quantasVezesABonecaAndouEmUmaDirecao = 0;
		this.mascoteAndandoParaADireita = true;
		
		//vamos primeiro fazer ela voltar a posicao inicial
		ImageView mascote = (ImageView) findViewById(R.id.mascotefull);
     	mascote.setX(this.posicaoXDaMascoteAoIniciarAplicativo);
     	
     	//e ao drawable inicial
     	mascote.setImageResource(R.drawable.mascote_full_direita);
     	
		
		 this.handlerParaMascoteAndar = new Handler();
		 this.runnableThreadParaMascoteAndar = new Runnable() {
	         @Override
	         public void run() 
	         {
	             
	            	 MainActivity.this.runOnUiThread(new Runnable() 
	 		        {
	 		            @Override
	 		            public void run() 
	 		            {
	 		            	if(mascoteAndandoParaADireita == true)
	 		            	{
	 		            		ImageView mascote = (ImageView) findViewById(R.id.mascotefull);
		 		            	mascote.setX(mascote.getX() + (float) 5.5);
		 		            	quantasVezesABonecaAndouEmUmaDirecao = quantasVezesABonecaAndouEmUmaDirecao + 1;
		 		            	
		 		            	if(quantasVezesABonecaAndouEmUmaDirecao > 30)
		 		            	{
		 		            		//mudar a direcao!
		 		            		mascote.setImageResource(R.drawable.mascote_full_esquerda);
		 		            		quantasVezesABonecaAndouEmUmaDirecao = 0;
		 		            		mascoteAndandoParaADireita = false;
		 		            	}
	 		            	}
	 		            	else
	 		            	{
	 		            		ImageView mascote = (ImageView) findViewById(R.id.mascotefull);
		 		            	mascote.setX(mascote.getX() - (float) 5.5);
		 		            	quantasVezesABonecaAndouEmUmaDirecao = quantasVezesABonecaAndouEmUmaDirecao + 1;
	 		            	
		 		            	if(quantasVezesABonecaAndouEmUmaDirecao > 30)
		 		            	{
		 		            		//mudar a direcao!
		 		            		mascote.setImageResource(R.drawable.mascote_full_direita);
		 		            		quantasVezesABonecaAndouEmUmaDirecao = 0;
		 		            		mascoteAndandoParaADireita = true;
		 		            	}
	 		            	
	 		            	}
	 		            	
	 		            }
	 		        });
	             
	             if(runnableThreadmascoteAndaDeveriaMorrer == false)
	             {
	            	 handlerParaMascoteAndar.postDelayed(this, 100);  
	             }
	             
	         }
	     };
	     
	     handlerParaMascoteAndar.postDelayed(runnableThreadParaMascoteAndar, 100);
	}
	
	private void carregarESePrecisarAlterarLinguaDoJogo()
	{
		ArmazenaTudoParaJogoOffline conheceConfiguracoesSalvas = ArmazenaTudoParaJogoOffline.getInstance();
		String linguaDoJogoSalvaAnteriormente = 
				conheceConfiguracoesSalvas.carregarLinguaDoJogo(this);
		
		Resources res = getResources();
        Locale myLocale = res.getConfiguration().locale;
		if(myLocale != null)
		{
			String linguaDoJogoAtual = myLocale.getLanguage();
			if(linguaDoJogoSalvaAnteriormente.length() > 0 && linguaDoJogoAtual.compareTo(linguaDoJogoSalvaAnteriormente) != 0)
			{
				//vamos ter de mudar a lingua atual do jogo
				//OBS: Caso nenhuma lingua havia sido salva anteriormente, nao faremos nada pq a lingua serah a do proprio tablet, nao da aplicacao
				this.setLocale(linguaDoJogoSalvaAnteriormente);
			}
		}
	}
	
	private void setLocale(String lang) {
	 	//Toast.makeText(getApplicationContext(), "mudando texto para" + lang, Toast.LENGTH_SHORT).show();
        Locale locale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);
        finish();
        
    }
}
