package com.karutakanji;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver.PendingResult;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import bancodedados.ActivityQueEsperaAtePegarOsKanjis;
import bancodedados.AdapterListViewChatCasual;
import bancodedados.AdapterListViewSalasCriadas;
import bancodedados.ArmazenaKanjisPorCategoria;
import bancodedados.ArmazenaTudoParaJogoOffline;
import bancodedados.BuscaSalasModoCasualComArgumentoTask;
import bancodedados.BuscaSalasModoCasualTask;
import bancodedados.CategoriaDeKanjiParaListviewSelecionavel;
import bancodedados.CriarSalaModoCasualTask;
import bancodedados.AdapterListViewIconeETexto;
import bancodedados.DadosPartidaParaOLog;
import bancodedados.FechaSalaCasualCriadaPeloUsuarioTask;
import bancodedados.EnviarDadosDaPartidaParaLogTask;
import bancodedados.KanjiTreinar;
import bancodedados.MyCustomAdapter;
import bancodedados.MyCustomAdapterSemCheckBox;
import bancodedados.MySpinnerAdapter;
import bancodedados.PegaNomesDeTodasAsCategoriasTask;
import bancodedados.PegaRankingDoUsuarioParaCriarSalaCasualTask;
import bancodedados.SalaModoCasual;
import bancodedados.SingletonArmazenaCategoriasDoJogo;
import bancodedados.SingletonArmazenaRankingUsuario;
import bancodedados.SolicitaKanjisParaTreinoTask;
import bancodedados.SpinnerSelecionaMesmoQuandoVoltaAoMesmoItem;
import bancodedados.ThreadExecutaBuscaSalasCasualTask;
import br.ufrn.dimap.pairg.karutakanji.android.R;


import com.easyandroidanimations.library.BounceAnimation;
import com.easyandroidanimations.library.ShakeAnimation;
import com.github.lzyzsd.circleprogress.CircleProgress;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.multiplayer.realtime.*;
//import com.google.android.gms.games.GamesClient;
import com.google.android.gms.games.multiplayer.realtime.*;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.example.games.basegameutils.BaseGameActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import lojinha.ConcreteDAOAcessaDinheiroDoJogador;
import lojinha.DAOAcessaDinheiroDoJogador;
import lojinha.TransformaPontosEmCredito;

public class ModoCasual extends ActivityDoJogoComSom implements View.OnClickListener, RealTimeMessageReceivedListener,RoomStatusUpdateListener, RoomUpdateListener, OnInvitationReceivedListener, ActivityQueEsperaAtePegarOsKanjis,ActivityMultiplayerQueEsperaAtePegarOsKanjis, OnItemSelectedListener,OnScrollListener 
{

/*
* API INTEGRATION SECTION. This section contains the code that integrates
* the game with the Google Play game services API.
*/

// Debug tag
final static boolean ENABLE_DEBUG = true;
final static String TAG = "ButtonClicker2000";

// Request codes for the UIs that we show with startActivityForResult:
final static int RC_SELECT_PLAYERS = 10000;
final static int RC_INVITATION_INBOX = 10001;
final static int RC_WAITING_ROOM = 10002;

// Room ID where the currently active game is taking place; null if we're
// not playing.
String mRoomId = null;
Room room;

private String nomeUsuario; //nome do usuario que eh obtido assim que o usuario faz login

// Are we playing in multiplayer mode?
boolean mMultiplayer = false;

// The participants in the currently active game
ArrayList<Participant> mParticipants = null;

// My participant ID in the currently active game
String mMyId = null;

// If non-null, this is the id of the invitation we received via the
// invitation listener
String mIncomingInvitationId = null;

// Message buffer for sending messages
byte[] mMsgBuf; //1 e 2 byte eu nao uso, mas o 3 diz se o adversario acertou ou nao clicou na carta1('N','A')



private int quantasRodadasHaverao; //possiveis valores: 1,2,3 e 99(para infinito)
private int rodadaAtual;

private int suaPontuacao; //sua pontuacao no modo multiplayer
private TimerTask timerTaskDecrementaTempoRestante;
private boolean tempoEstahParado; //quando o jogador usa o item que para o tempo, ele para
private int qualImagemEstaSendoUsadaNaAnimacaoDoTempo; //int que vai de 0 ate 17 que serve para criar a animacao do tempo correndo

private LinkedList<KanjiTreinar> kanjisDasCartasNaTela; //os kanjis das cartas que aparecem na tela

private KanjiTreinar kanjiDaDica; //kanji da dica atual que mostra para ambos os jogadores
private LinkedList<KanjiTreinar> kanjisDasCartasNaTelaQueJaSeTornaramDicas; // quais kanjis ja viraram dicas? Essa lista deve se esvaziar no comeco de cada rodada   
private LinkedList<KanjiTreinar> kanjisQuePodemVirarCartas; //no comeco do jogo, eh igual a todos os kanjis das categorias escolhidas pelo usuario, mas com o passar das rodadas, vai-se tirando kanjis dessa lista 

private int pontuacaoDoAdversario; //precisaremos saber a pontuacao do adversario porque na hora de recebermos um item aleatorio, saberemos se devemos receber item do perdedor ou de quem estah ganhando 
private int quantasCartasJaSairamDoJogo; //de X em X cartas que saem, saberemos quando dar um item aleatorio p cada jogador
private LinkedList<String> itensAtuais;
private LinkedList<String> itensDoPerdedor; //essa lista e a de baixo sao preenchidas no comeco do jogo multiplayer
private LinkedList<String> itensDoGanhador;

private ImageView cartaASerTirada; //na hora que o item do trovao for usado, precisaremos armazenar algo nesse atributo so p passar p outra funcao
private boolean usouTrovaoTiraCarta; //booleano que diz se o usuario usou o item trovaotiracarta
private boolean usouReviveCarta; //booleano que diz se o usuario usou o item revivecarta
private boolean usou2x; //booleano que diz se o usuario usou o item 2x
private boolean usouNaoEspereMais; //booleano que diz se o usuario usou o item nao espere mais

private int qualCartaEstaProibida; //quando o user usa o naoesperemais e erra uma carta, ela fica proibida. Mas assim alguem acerta uma carta e a dica muda, a carta deveria voltar ao normal. Esse numero vai de 1 ate 8
								   //quando esse int for 0, nao ha cartas proibidas na tela
private LinkedList<Integer> quaisCartasEstaoDouradas; //quando o usuario usa o cartasdouradas, algumas cartas ficam douradas. Mas assim que alguem acerta uma carta e a dica muda, as cartas deveriam voltar ao normal. Os numeros das cartas vao de 1 a 8

public boolean guestTerminouDeCarregarListaDeCategorias; //booleano para resolver problema de um dos jogadores nao estar recebendo a lista de categorias
private Handler mHandler = new Handler(); //handler para o chat do final do jogo
private ArrayList<String> mensagensChat; //arraylist com as mensagens do chat do fim de jogo
private ArrayList<String> posicoesBaloesMensagensChat; //para cada mensagem do chat, eu verei se o balao deve ficar para a direita ou esquerda

private LinkedList<KanjiTreinar> palavrasAcertadas;//toda palavra/kanji que o usuario acertar entra nessa lista(vai para o log)  
private LinkedList<KanjiTreinar> palavrasErradas;//toda palavra/kanji que o usuario errar entra nessa lista(vai para o log)
private LinkedList<KanjiTreinar> palavrasJogadas; //toda palavra/kanji que sair como dica serah armazenada (vai para o log)

private LinkedList<Integer> posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartas; 
//o unico jogador que estava removendo os kanjis da lista kanjisquepodemvirarcartas era quem escolhia
//os 8 kanjis, mas o outro jogador deve ter essa linkedlist atualizada tb! Soh que ela eh enorme!
//Por isso, assim que um turno passar e um jogador passar os 8 kanjis novos para o outro,
//ele tb deve enviar quais kanjis dessa linkedlist sairam!

private String nomeAdversario;

private ProgressDialog loadingComecoDaPartida; //loading que comeca desde quando sao decididas as categorias do jogo ateh o primeiro kanji da dica ser escolhido
private ProgressDialog loadingSalasModoCasual;
private ProgressDialog loadingEsperarAlguemEntrarSalaModoCasual;

private boolean jogoAcabou;
private boolean jaDeixouASala; //booleano que diz se o usuario ja saiu da sala mutliplayer(usado p nao invocar leaveroom() mais de uma vez)

private boolean naCriacaoDeSalaModoCasual; //diz se o usuario esta na criacao de salas do modo casual
private boolean noPopupQueBuscaSalasPorCategoria; //diz se o usuario esta no popup que busca salas por categoria

private boolean criouUmaSala; //diz se o usuario eh um criador de sala ou nao. Ele sera quem escolheu as categorias nesse caso
private SalaModoCasual salaAtual; //sala em que o usuario entrou ou criou

private LinkedList<SalaModoCasual> salasAbertas; //as salas abertas do lobby do modo casual vao ficar aqui. Isso serve para que a task BuscaSalasModoCasual consiga alertar quando uma mudanca acontece usando um get() desse atributo
ThreadExecutaBuscaSalasCasualTask threadFicaBuscandoSalasModoCasual; //thread que fica de x em x segundos atualizando a listagem de salas no modo casual

private AlertDialog popupPesquisarSalaPorDuracao; //quando o usuario quiser pesquisar salas por duracao da partida, esse popup aparece
private ProgressDialog loadingEsperaRevanche; //progressdialog usado para o usuario que espera o outro criar uma sala para revanche
private boolean salaCriadaEhParaRevanche; //a sala criada para era para uma partida de revanche ou nao? se sim, o criador dele enviar imediatamente o id da sala para o outro jogador
private boolean revancheEstahAcontecendo;
private Runnable runnableThreadDiminuiTempoNoJogo; //precisamos armazenar esse carinha como variavel pq precisaremos mata-lo assim que o jogo terminar
private Handler handlerParaThreadDiminuiTempoDoJogo; //mesmo do acima
private boolean runnableThreadDiminuiTempoNoJogoDeveriaMorrer;

private ListView listViewChatFimDeJogo;
private Dialog popupChatFimDeJogo;

private boolean duranteAcertoDeCarta; //no jogo, existe a situa��o em que um dos jogadores pode acertar uma carta ao mesmo tempo que o outro. Para isso, farei o seguinte: esse booleano
//fica true assim que o jogador comeca o processo de acertar carta(aumentar ponto, alertar ao adversario...) durante esse periodo, qualquer mensagem que ele receber do adversario para tirar uma carta sera desconsiderada. Dessa forma, ambos os jogadores ganham o ponto
private String qualCartaAcertada;
private int dificuldadeCartaAcertada;

private boolean mostrar_descricao_itens_casual; //deve comecar pela tela que mostra os itens do modo casual ou nao?

private int progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta; //sao os circleprogress que uso para determinar o tempo q o usuario ainda espera ao errar uma carta
private int quantosSegundosSemReceberItem; //se 15 em 15 segundos, essa variavel volta ao 0 e o usuario recebe um item

//This array lists all the individual screens our game has.
final static int[] SCREENS = {
 R.id.screen_game, R.id.screen_sign_in,R.id.tela_lobby_modo_casual, R.id.tela_espere_carregar_categorias_casual,R.id.tela_escolha_categoria_criar_nova_sala,R.id.tela_escolha_duracao_criar_nova_sala,
 R.id.screen_wait,R.id.decidindoQuemEscolheACategoria, R.id.tela_jogo_multiplayer,R.id.tela_fim_de_jogo,R.id.popup_escolha_categorias,R.id.popup_escolha_dan,R.id.popup_escolha_duracao,R.id.popup_escolha_nome_usuario, R.id.mostrar_itens_casual};

@Override
public void onCreate(Bundle savedInstanceState) 
{
	super.onCreate(savedInstanceState);
	enableDebugLog(ENABLE_DEBUG, TAG);
	setContentView(R.layout.activity_modo_casual);
	// set up a click listener for everything we care about
	for (int id : CLICKABLES) 
	{
		findViewById(id).setOnClickListener(this);
	}
	
	findViewById(R.id.botao_menu_principal).setOnClickListener(this);
	this.jaDeixouASala = false;
	this.salaCriadaEhParaRevanche = false;
	this.revancheEstahAcontecendo = false;
	this.noPopupQueBuscaSalasPorCategoria = false;
	
	//vou inserir movimento na lista assim que um usuario clicar nas setas cima e baixo da lista de ver salas abertas 
	final ListView lista_salas_abertas = (ListView) findViewById(R.id.lista_salas_abertas);
	ImageView seta_cima_lobby_casual = (ImageView) findViewById(R.id.seta_cima_lobby_casual);
    ImageView seta_baixo_lobby_casual = (ImageView) findViewById(R.id.seta_baixo_lobby_casual);
	
    seta_cima_lobby_casual.setOnTouchListener(new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent e) {
			// TODO Auto-generated method stub
			lista_salas_abertas.smoothScrollBy(-40, 20);
			return true;
			
		}
	});
	
    seta_baixo_lobby_casual.setOnTouchListener(new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent e) {
			// TODO Auto-generated method stub
			lista_salas_abertas.smoothScrollBy(40, 20);
			return true;
			
		}
	});
    
	//TIREI A PARTE QUE ADICIONA O CABE�ALHO DA LISTA!!!
	//View header = getLayoutInflater().inflate(R.layout.header_listview_salas_abertas_casual, null);
	//lista_salas_abertas.addHeaderView(header);
}

/**
* Called by the base class (BaseGameActivity) when sign-in has failed. For
* example, because the user hasn't authenticated yet. We react to this by
* showing the sign-in button.
*/
@Override
public void onSignInFailed() {
Log.d(TAG, "Sign-in failed.");
switchToScreen(R.id.screen_sign_in);
}

/**
* Called by the base class (BaseGameActivity) when sign-in succeeded. We
* react by going to our main screen.
*/
@Override
public void onSignInSucceeded() {
Log.d(TAG, "Sign-in succeeded.");

// register listener so we are notified if we receive an invitation to play
// while we are in the game
Games.Invitations.registerInvitationListener(getApiClient(), this);

// if we received an invite via notification, accept it; otherwise, go to main screen
if (getInvitationId() != null) {
    acceptInviteToRoom(getInvitationId());
    return;
}

//salvarei o nome do usuario para adicionar o log dele no banco de dados
AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
Account[] list = manager.getAccounts();

/*for(Account account: list)
{
    if(account.type.equalsIgnoreCase("com.google"))
    {
        this.emailUsuario = account.name;
        break;
    }
}*/
LinkedList<String> nomeEEmailUsuario = ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(this);
this.nomeUsuario = nomeEEmailUsuario.get(0);

//que tal agora que temos o nome do usuario nos fecharmos todas as salas que estiverem abertas desse usuario? Nao deveria ter nenhum problema mesmo que ele n tivesse salas abertas, certo?
//eh soh p ter garantia que o usuario conseguirah criar salas normalmente
FechaSalaCasualCriadaPeloUsuarioTask deletarSalaCasual = new FechaSalaCasualCriadaPeloUsuarioTask(this);
deletarSalaCasual.execute(nomeUsuario);

//devemos mostrar a tela de descricao dos itens ou nao
this.mostrar_descricao_itens_casual = ArmazenaConfiguracoesDoJogo.getInstance().carregarMostrarDescricaoItensCasual(this);
if(this.mostrar_descricao_itens_casual == false)
{
	switchToMainScreen();
}
else
{
	switchToScreen(R.id.mostrar_itens_casual);
	Button checkbox_aparecer_novamente_descricao_itens_casual = (Button) findViewById(R.id.checkbox_aparecer_novamente_descricao_itens_casual);
	if(this.mostrar_descricao_itens_casual == true)
	{
		checkbox_aparecer_novamente_descricao_itens_casual.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
	}
	else
	{
		checkbox_aparecer_novamente_descricao_itens_casual.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
	}
	mudarFonteTextoMostrarDescricaoItensCasual();
}

//sera que o usuario quer se manter logado na conta da google ou nao?
	/*boolean manterLogadoContaGoogle = 
			ArmazenaTudoParaJogoOffline.getInstance().getManterLogadoNaContaDaGoogle(getApplicationContext());
	
	if(manterLogadoContaGoogle == false)
	{
		final String GAMEHELPER_SHARED_PREFS = "GAMEHELPER_SHARED_PREFS";
	    final String KEY_SIGN_IN_CANCELLATIONS = "KEY_SIGN_IN_CANCELLATIONS";
	    final String KEY_AUTO_SIGN_IN = "KEY_AUTO_SIGN_IN";
	    
		SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences(GAMEHELPER_SHARED_PREFS, Context.MODE_PRIVATE).edit();
        editor.putBoolean(KEY_AUTO_SIGN_IN, false);
        editor.commit();
       
        super.getGameHelper().getApiClient().disconnect();
		 //Games.signOut(super.getGameHelper().getApiClient().);
	}
	else
	{
		
	}*/

}

public void irAoLobbyModoCasual(View v)
{
	ArmazenaConfiguracoesDoJogo.getInstance().salvarMostrarDescricaoItensCasual(this, mostrar_descricao_itens_casual);
	switchToMainScreen();
}

public void alterar_mostrar_descricao_itens_casual(View v)
{
	Button checkbox_aparecer_novamente_descricao_itens_casual = (Button) findViewById(R.id.checkbox_aparecer_novamente_descricao_itens_casual);
	if(this.mostrar_descricao_itens_casual == true)
	{
		this.mostrar_descricao_itens_casual = false;
		checkbox_aparecer_novamente_descricao_itens_casual.setBackgroundResource(R.drawable.checkbox_desmarcada_regras_treinamento);
	}
	else
	{
		this.mostrar_descricao_itens_casual = true;
		checkbox_aparecer_novamente_descricao_itens_casual.setBackgroundResource(R.drawable.checkbox_marcada_regras_treinamento);
	}
}


@Override
public void onClick(View v) {
Intent intent;

switch (v.getId()) {
    case R.id.button_sign_in:
        // user wants to sign in
        if (!verifyPlaceholderIdsReplaced()) {
            showAlert("Error: sample not set up correctly. Please see README.");
            return;
        }
        beginUserInitiatedSignIn();
        break;
    /*case R.id.button_sign_out:
        // user wants to sign out
        signOut();
        switchToScreen(R.id.screen_sign_in);
        break;
    case R.id.button_invite_players:
        // show list of invitable players
        intent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(getApiClient(), 1, 3);
        switchToScreen(R.id.screen_wait);
        startActivityForResult(intent, RC_SELECT_PLAYERS);
        break;
    case R.id.button_see_invitations:
        // show list of pending invitations
        intent = Games.Invitations.getInvitationInboxIntent(getApiClient());
        switchToScreen(R.id.screen_wait);
        startActivityForResult(intent, RC_INVITATION_INBOX);
        break;*/
    case R.id.button_accept_popup_invitation:
        // user wants to accept the invitation shown on the invitation popup
        // (the one we got through the OnInvitationReceivedListener).
        acceptInviteToRoom(mIncomingInvitationId);
        mIncomingInvitationId = null;
        break;
    /*case R.id.button_quick_game:
        // user wants to play against a random opponent right now
        startQuickGame(2);
        break;*/
    /*case R.id.button_modo_casual:
    	switchToScreen(R.id.tela_lobby_modo_casual);
    	this.mostrarLobbyModoCasual();
    	break;*/
    case R.id.karuta1_imageview:
    	if(this.usouTrovaoTiraCarta == false && this.usouReviveCarta == false) //no caso do trovao e do revive, ele clica numa carta p elimina-la
    	{
    		TextView textViewKaruta1 = (TextView) findViewById(R.id.texto_karuta1);
        	String textoKaruta1 = this.kanjisDasCartasNaTela.get(0).getKanji();
        	String textoKanjiDaDica = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta1.compareTo(textoKanjiDaDica) == 0)
        	{
        		//usuario acertou o kanji. Temos de aumentar seus pontos e informar ao adversario que a dica mudou e que nao pode mais clicar nessa carta
        		this.mandarMensagemAoAdversarioEuAcerteiCarta("karuta1");
        		
        	}
        	else
        	{
        		//errou, mas sera q ele usou o item naoesperemais?
        		
        		if(this.usouNaoEspereMais == false)
        		{
        			//devemos executar o som que indica que ele errou uma carta
        			if(((TextView)findViewById(R.id.texto_karuta1)).getText().length() > 0)
        			{
        				super.reproduzirSfx("errou_carta");
                		
                		this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(0)); //colocar no log que ele errou este kanji
                		this.realizarProcedimentoUsuarioErrouCarta();
        			}
            		
        		}
        		else
        		{
        			this.proibirCartaNaPosicao(1);
        		}
        	}
    	}
    	else if(this.usouReviveCarta == true)
    	{
    		this.mandarMensagemMultiplayer("item revivercarta numeroCarta=1");
    		this.realizarProcedimentoReviverCarta(1,false);
    		this.usouReviveCarta = false; //nao esquecer que ele perde o item
    		ImageView imageViewItem = (ImageView)findViewById(R.id.item9);
    		imageViewItem.setImageResource(R.drawable.nenhumitem);
    		
    	}
    	else if(this.usouTrovaoTiraCarta == true)
    	{
    		KanjiTreinar kanjiCartaQueEleClicou = this.kanjisDasCartasNaTela.get(0);
    		ImageView imagemItem = (ImageView) findViewById(R.id.item8);
   		 	imagemItem.setImageResource(R.drawable.nenhumitem);
   		 	
    		if((this.kanjiDaDica.getKanji().compareTo(kanjiCartaQueEleClicou.getKanji()) == 0)
    			&& (this.kanjiDaDica.getCategoriaAssociada().compareTo(kanjiCartaQueEleClicou.getCategoriaAssociada()) == 0))
    		{
    			//usuario quer tirar do jogo a carta com a dica. Nao pode
    			String mensagemErro = getResources().getString(R.string.mensagem_trovao_tira_carta_tira_dica);
    			Toast t = Toast.makeText(this, mensagemErro, Toast.LENGTH_LONG);
    		    t.show();
    		    fazerMascoteFicarZangadaPorUmTempo();
    		    usouTrovaoTiraCarta = false; //nao esquecer que ele perde o item, entao o booleano eh falso
    		}
    		else
    		{
        		this.lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(0);
    		}
    	}
    	
    	break;
    case R.id.karuta2_imageview:
    	if(this.usouTrovaoTiraCarta == false && this.usouReviveCarta == false) //no caso do trovao e do revive, ele clica numa carta p elimina-la
    	{
    		TextView textViewKaruta2 = (TextView) findViewById(R.id.texto_karuta2);
        	String textoKaruta2 = this.kanjisDasCartasNaTela.get(1).getKanji();
        	String textoKanjiDaDica2 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta2.compareTo(textoKanjiDaDica2) == 0)
        	{
        		//usuario acertou o kanji. Temos de aumentar seus pontos e informar ao adversario que a dica mudou e que nao pode mais clicar nessa carta
        		this.mandarMensagemAoAdversarioEuAcerteiCarta("karuta2");
        	}
        	else
        	{
        		//errou, mas sera q ele usou o item naoesperemais?
        		
        		if(this.usouNaoEspereMais == false)
        		{
        			//devemos executar o som que indica que ele errou uma carta
        			if(((TextView)findViewById(R.id.texto_karuta2)).getText().length() > 0)
        			{
        				super.reproduzirSfx("errou_carta");
                		
                		this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(1)); //colocar no log que ele errou este kanji
                		this.realizarProcedimentoUsuarioErrouCarta();
        			}
        		}
        		else
        		{
        			this.proibirCartaNaPosicao(2);
        		}
        	}
    	}
    	else if(this.usouReviveCarta == true)
    	{
    		this.mandarMensagemMultiplayer("item revivercarta numeroCarta=2");
    		this.realizarProcedimentoReviverCarta(2,false);
    		this.usouReviveCarta = false; //nao esquecer que ele perde o item
    		ImageView imageViewItem = (ImageView)findViewById(R.id.item9);
    		imageViewItem.setImageResource(R.drawable.nenhumitem);
    	}
    	else if(this.usouTrovaoTiraCarta == true)
    	{
    		KanjiTreinar kanjiCartaQueEleClicou = this.kanjisDasCartasNaTela.get(1);
    		ImageView imagemItem = (ImageView) findViewById(R.id.item8);
   		 	imagemItem.setImageResource(R.drawable.nenhumitem);
   		 	
    		if((this.kanjiDaDica.getKanji().compareTo(kanjiCartaQueEleClicou.getKanji()) == 0)
    			&& (this.kanjiDaDica.getCategoriaAssociada().compareTo(kanjiCartaQueEleClicou.getCategoriaAssociada()) == 0))
    		{
    			//usuario quer tirar do jogo a carta com a dica. Nao pode
    			String mensagemErro = getResources().getString(R.string.mensagem_trovao_tira_carta_tira_dica);
    			Toast t = Toast.makeText(this, mensagemErro, Toast.LENGTH_LONG);
    		    t.show();
    		    fazerMascoteFicarZangadaPorUmTempo();
    		    
    		    usouTrovaoTiraCarta = false; //nao esquecer que ele perde o item, entao o booleano eh falso
    		}
    		else
    		{
        		this.lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(1);
    		}
    	}
    	break;
    case R.id.karuta3_imageview:
    	if(this.usouTrovaoTiraCarta == false && this.usouReviveCarta == false) //no caso do trovao e do revive, ele clica numa carta p elimina-la
    	{
    		TextView textViewKaruta3 = (TextView) findViewById(R.id.texto_karuta3);
        	String textoKaruta3 = this.kanjisDasCartasNaTela.get(2).getKanji();
        	String textoKanjiDaDica3 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta3.compareTo(textoKanjiDaDica3) == 0)
        	{
        		//usuario acertou o kanji. Temos de aumentar seus pontos e informar ao adversario que a dica mudou e que nao pode mais clicar nessa carta
        		this.mandarMensagemAoAdversarioEuAcerteiCarta("karuta3");
        	}
        	else
        	{
        		//errou, mas sera q ele usou o item naoesperemais?
        		
        		if(this.usouNaoEspereMais == false)
        		{
        			//devemos executar o som que indica que ele errou uma carta
        			if(((TextView)findViewById(R.id.texto_karuta3)).getText().length() > 0)
        			{
        				super.reproduzirSfx("errou_carta");
                		
                		this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(2)); //colocar no log que ele errou este kanji
                		this.realizarProcedimentoUsuarioErrouCarta();
        			}
        		}
        		else
        		{
        			this.proibirCartaNaPosicao(3);
        		}
        	}
    	}
    	else if(this.usouReviveCarta == true)
    	{
    		this.mandarMensagemMultiplayer("item revivercarta numeroCarta=3");
    		this.realizarProcedimentoReviverCarta(3,false);
    		this.usouReviveCarta = false; //nao esquecer que ele perde o item
    		ImageView imageViewItem = (ImageView)findViewById(R.id.item9);
    		imageViewItem.setImageResource(R.drawable.nenhumitem);
    	}
    	else if(this.usouTrovaoTiraCarta == true)
    	{
    		KanjiTreinar kanjiCartaQueEleClicou = this.kanjisDasCartasNaTela.get(2);
    		ImageView imagemItem = (ImageView) findViewById(R.id.item8);
   		 	imagemItem.setImageResource(R.drawable.nenhumitem);
   		 	
    		if((this.kanjiDaDica.getKanji().compareTo(kanjiCartaQueEleClicou.getKanji()) == 0)
    			&& (this.kanjiDaDica.getCategoriaAssociada().compareTo(kanjiCartaQueEleClicou.getCategoriaAssociada()) == 0))
    		{
    			//usuario quer tirar do jogo a carta com a dica. Nao pode
    			String mensagemErro = getResources().getString(R.string.mensagem_trovao_tira_carta_tira_dica);
    			Toast t = Toast.makeText(this, mensagemErro, Toast.LENGTH_LONG);
    		    t.show();
    		    fazerMascoteFicarZangadaPorUmTempo();
    		    usouTrovaoTiraCarta = false; //nao esquecer que ele perde o item, entao o booleano eh falso
    		}
    		else
    		{
        		this.lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(2);
    		}
    	}
    	break;
    case R.id.karuta4_imageview:
    	if(this.usouTrovaoTiraCarta == false && this.usouReviveCarta == false) //no caso do trovao e do revive, ele clica numa carta p elimina-la
    	{
    		TextView textViewKaruta4 = (TextView) findViewById(R.id.texto_karuta4);
        	String textoKaruta4 = this.kanjisDasCartasNaTela.get(3).getKanji();
        	String textoKanjiDaDica4 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta4.compareTo(textoKanjiDaDica4) == 0)
        	{
        		//usuario acertou o kanji. Temos de aumentar seus pontos e informar ao adversario que a dica mudou e que nao pode mais clicar nessa carta
        		this.mandarMensagemAoAdversarioEuAcerteiCarta("karuta4");
        	}
        	else
        	{
        		//errou, mas sera q ele usou o item naoesperemais?
        		
        		if(this.usouNaoEspereMais == false)
        		{
        			//devemos executar o som que indica que ele errou uma carta
        			if(((TextView)findViewById(R.id.texto_karuta4)).getText().length() > 0)
        			{
        				super.reproduzirSfx("errou_carta");
                		
                		this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(3)); //colocar no log que ele errou este kanji
                		this.realizarProcedimentoUsuarioErrouCarta();
        			}
        		}
        		else
        		{
        			this.proibirCartaNaPosicao(4);
        		}
        	}
    	}
    	else if(this.usouReviveCarta == true)
    	{
    		this.mandarMensagemMultiplayer("item revivercarta numeroCarta=4");
    		this.realizarProcedimentoReviverCarta(4,false);
    		this.usouReviveCarta = false; //nao esquecer que ele perde o item
    		ImageView imageViewItem = (ImageView)findViewById(R.id.item9);
    		imageViewItem.setImageResource(R.drawable.nenhumitem);
    	}
    	else if(this.usouTrovaoTiraCarta == true)
    	{
    		KanjiTreinar kanjiCartaQueEleClicou = this.kanjisDasCartasNaTela.get(3);
    		ImageView imagemItem = (ImageView) findViewById(R.id.item8);
   		 	imagemItem.setImageResource(R.drawable.nenhumitem);
   		 	
    		if((this.kanjiDaDica.getKanji().compareTo(kanjiCartaQueEleClicou.getKanji()) == 0)
    			&& (this.kanjiDaDica.getCategoriaAssociada().compareTo(kanjiCartaQueEleClicou.getCategoriaAssociada()) == 0))
    		{
    			//usuario quer tirar do jogo a carta com a dica. Nao pode
    			String mensagemErro = getResources().getString(R.string.mensagem_trovao_tira_carta_tira_dica);
    			Toast t = Toast.makeText(this, mensagemErro, Toast.LENGTH_LONG);
    		    t.show();
    		    fazerMascoteFicarZangadaPorUmTempo();
    		    usouTrovaoTiraCarta = false; //nao esquecer que ele perde o item, entao o booleano eh falso
    		}
    		else
    		{
        		this.lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(3);
    		}
    	}
    	break;
    case R.id.karuta5_imageview:
    	if(this.usouTrovaoTiraCarta == false && this.usouReviveCarta == false) //no caso do trovao e do revive, ele clica numa carta p elimina-la
    	{
    		TextView textViewKaruta5 = (TextView) findViewById(R.id.texto_karuta5);
        	String textoKaruta5 = this.kanjisDasCartasNaTela.get(4).getKanji();
        	String textoKanjiDaDica5 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta5.compareTo(textoKanjiDaDica5) == 0)
        	{
        		//usuario acertou o kanji. Temos de aumentar seus pontos e informar ao adversario que a dica mudou e que nao pode mais clicar nessa carta
        		this.mandarMensagemAoAdversarioEuAcerteiCarta("karuta5");
        	}
        	else
        	{
        		//errou, mas sera q ele usou o item naoesperemais?
        		
        		if(this.usouNaoEspereMais == false)
        		{
        			//devemos executar o som que indica que ele errou uma carta
        			if(((TextView)findViewById(R.id.texto_karuta5)).getText().length() > 0)
        			{
        				super.reproduzirSfx("errou_carta");
                		
                		this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(4)); //colocar no log que ele errou este kanji
                		this.realizarProcedimentoUsuarioErrouCarta();
        			}
        		}
        		else
        		{
        			this.proibirCartaNaPosicao(5);
        		}
        	}
    	}
    	else if(this.usouReviveCarta == true)
    	{
    		this.mandarMensagemMultiplayer("item revivercarta numeroCarta=5");
    		this.realizarProcedimentoReviverCarta(5,false);
    		this.usouReviveCarta = false; //nao esquecer que ele perde o item
    		ImageView imageViewItem = (ImageView)findViewById(R.id.item9);
    		imageViewItem.setImageResource(R.drawable.nenhumitem);
    	}
    	else if(this.usouTrovaoTiraCarta == true)
    	{
    		KanjiTreinar kanjiCartaQueEleClicou = this.kanjisDasCartasNaTela.get(4);
    		ImageView imagemItem = (ImageView) findViewById(R.id.item8);
   		 	imagemItem.setImageResource(R.drawable.nenhumitem);
   		 	
    		if((this.kanjiDaDica.getKanji().compareTo(kanjiCartaQueEleClicou.getKanji()) == 0)
    			&& (this.kanjiDaDica.getCategoriaAssociada().compareTo(kanjiCartaQueEleClicou.getCategoriaAssociada()) == 0))
    		{
    			//usuario quer tirar do jogo a carta com a dica. Nao pode
    			String mensagemErro = getResources().getString(R.string.mensagem_trovao_tira_carta_tira_dica);
    			Toast t = Toast.makeText(this, mensagemErro, Toast.LENGTH_LONG);
    		    t.show();
    		    fazerMascoteFicarZangadaPorUmTempo();
    		    usouTrovaoTiraCarta = false; //nao esquecer que ele perde o item, entao o booleano eh falso
    		}
    		else
    		{
        		this.lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(4);
    		}
    	}
    	break;
    case R.id.karuta6_imageview:
    	if(this.usouTrovaoTiraCarta == false && this.usouReviveCarta == false) //no caso do trovao e do revive, ele clica numa carta p elimina-la
    	{
    		TextView textViewKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
        	String textoKaruta6 = this.kanjisDasCartasNaTela.get(5).getKanji();
        	String textoKanjiDaDica6 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta6.compareTo(textoKanjiDaDica6) == 0)
        	{
        		//usuario acertou o kanji. Temos de aumentar seus pontos e informar ao adversario que a dica mudou e que nao pode mais clicar nessa carta
        		this.mandarMensagemAoAdversarioEuAcerteiCarta("karuta6");
        	}
        	else
        	{
        		//errou, mas sera q ele usou o item naoesperemais?
        		
        		if(this.usouNaoEspereMais == false)
        		{
        			//devemos executar o som que indica que ele errou uma carta
        			if(((TextView)findViewById(R.id.texto_karuta6)).getText().length() > 0)
        			{
        				super.reproduzirSfx("errou_carta");
                		
                		this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(5)); //colocar no log que ele errou este kanji
                		this.realizarProcedimentoUsuarioErrouCarta();
        			}
        		}
        		else
        		{
        			this.proibirCartaNaPosicao(6);
        		}
        	}
    	}
    	else if(this.usouReviveCarta == true)
    	{
    		this.mandarMensagemMultiplayer("item revivercarta numeroCarta=6");
    		this.realizarProcedimentoReviverCarta(6,false);
    		this.usouReviveCarta = false; //nao esquecer que ele perde o item
    		ImageView imageViewItem = (ImageView)findViewById(R.id.item9);
    		imageViewItem.setImageResource(R.drawable.nenhumitem);
    	}
    	else if(this.usouTrovaoTiraCarta == true)
    	{
    		KanjiTreinar kanjiCartaQueEleClicou = this.kanjisDasCartasNaTela.get(5);
    		ImageView imagemItem = (ImageView) findViewById(R.id.item8);
   		 	imagemItem.setImageResource(R.drawable.nenhumitem);
   		 	
    		if((this.kanjiDaDica.getKanji().compareTo(kanjiCartaQueEleClicou.getKanji()) == 0)
    			&& (this.kanjiDaDica.getCategoriaAssociada().compareTo(kanjiCartaQueEleClicou.getCategoriaAssociada()) == 0))
    		{
    			//usuario quer tirar do jogo a carta com a dica. Nao pode
    			String mensagemErro = getResources().getString(R.string.mensagem_trovao_tira_carta_tira_dica);
    			Toast t = Toast.makeText(this, mensagemErro, Toast.LENGTH_LONG);
    		    t.show();
    		    fazerMascoteFicarZangadaPorUmTempo();
    		    usouTrovaoTiraCarta = false; //nao esquecer que ele perde o item, entao o booleano eh falso
    		}
    		else
    		{
        		this.lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(5);
    		}
    	}
    	break;
    case R.id.karuta7_imageview:
    	if(this.usouTrovaoTiraCarta == false && this.usouReviveCarta == false) //no caso do trovao e do revive, ele clica numa carta p elimina-la
    	{
    		TextView textViewKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
        	String textoKaruta7 = this.kanjisDasCartasNaTela.get(6).getKanji();
        	String textoKanjiDaDica7 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta7.compareTo(textoKanjiDaDica7) == 0)
        	{
        		//usuario acertou o kanji. Temos de aumentar seus pontos e informar ao adversario que a dica mudou e que nao pode mais clicar nessa carta
        		this.mandarMensagemAoAdversarioEuAcerteiCarta("karuta7");
        	}
        	else
        	{
        		//errou, mas sera q ele usou o item naoesperemais?
        		
        		if(this.usouNaoEspereMais == false)
        		{
        			//devemos executar o som que indica que ele errou uma carta
        			if(((TextView)findViewById(R.id.texto_karuta7)).getText().length() > 0)
        			{
        				super.reproduzirSfx("errou_carta");
                		
                		this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(6)); //colocar no log que ele errou este kanji
                		this.realizarProcedimentoUsuarioErrouCarta();
        			}
        		}
        		else
        		{
        			this.proibirCartaNaPosicao(7);
        		}
        	}
    	}
    	else if(this.usouReviveCarta == true)
    	{
    		this.mandarMensagemMultiplayer("item revivercarta numeroCarta=7");
    		this.realizarProcedimentoReviverCarta(7,false);
    		this.usouReviveCarta = false; //nao esquecer que ele perde o item
    		ImageView imageViewItem = (ImageView)findViewById(R.id.item9);
    		imageViewItem.setImageResource(R.drawable.nenhumitem);
    	}
    	else if(this.usouTrovaoTiraCarta == true)
    	{
    		KanjiTreinar kanjiCartaQueEleClicou = this.kanjisDasCartasNaTela.get(6);
    		ImageView imagemItem = (ImageView) findViewById(R.id.item8);
   		 	imagemItem.setImageResource(R.drawable.nenhumitem);
   		 	
    		if((this.kanjiDaDica.getKanji().compareTo(kanjiCartaQueEleClicou.getKanji()) == 0)
    			&& (this.kanjiDaDica.getCategoriaAssociada().compareTo(kanjiCartaQueEleClicou.getCategoriaAssociada()) == 0))
    		{
    			//usuario quer tirar do jogo a carta com a dica. Nao pode
    			String mensagemErro = getResources().getString(R.string.mensagem_trovao_tira_carta_tira_dica);
    			Toast t = Toast.makeText(this, mensagemErro, Toast.LENGTH_LONG);
    		    t.show();
    		    fazerMascoteFicarZangadaPorUmTempo();
    		    usouTrovaoTiraCarta = false; //nao esquecer que ele perde o item, entao o booleano eh falso
    		}
    		else
    		{
        		this.lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(6);
    		}
    	}
    	break;
    case R.id.karuta8_imageview:
    	if(this.usouTrovaoTiraCarta == false && this.usouReviveCarta == false) //no caso do trovao e do revive, ele clica numa carta p elimina-la
    	{
    		TextView textViewKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
        	String textoKaruta8 = this.kanjisDasCartasNaTela.get(7).getKanji();
        	String textoKanjiDaDica8 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta8.compareTo(textoKanjiDaDica8) == 0)
        	{
        		//usuario acertou o kanji. Temos de aumentar seus pontos e informar ao adversario que a dica mudou e que nao pode mais clicar nessa carta
        		this.mandarMensagemAoAdversarioEuAcerteiCarta("karuta8");
        	}
        	else
        	{
        		//errou, mas sera q ele usou o item naoesperemais?
        		
        		if(this.usouNaoEspereMais == false)
        		{
        			//devemos executar o som que indica que ele errou uma carta
        			if(((TextView)findViewById(R.id.texto_karuta8)).getText().length() > 0)
        			{
        				super.reproduzirSfx("errou_carta");
                		
                		this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(7)); //colocar no log que ele errou este kanji
                		this.realizarProcedimentoUsuarioErrouCarta();
        			}
        		}
        		else
        		{
        			this.proibirCartaNaPosicao(8);
        		}
        	}
    	}
    	else if(this.usouReviveCarta == true)
    	{
    		this.mandarMensagemMultiplayer("item revivercarta numeroCarta=8");
    		this.realizarProcedimentoReviverCarta(8,false);
    		this.usouReviveCarta = false; //nao esquecer que ele perde o item
    		ImageView imageViewItem = (ImageView)findViewById(R.id.item9);
    		imageViewItem.setImageResource(R.drawable.nenhumitem);
    	}
    	else if(this.usouTrovaoTiraCarta == true)
    	{
    		KanjiTreinar kanjiCartaQueEleClicou = this.kanjisDasCartasNaTela.get(7);
    		ImageView imagemItem = (ImageView) findViewById(R.id.item8);
   		 	imagemItem.setImageResource(R.drawable.nenhumitem);
   		 	
    		if((this.kanjiDaDica.getKanji().compareTo(kanjiCartaQueEleClicou.getKanji()) == 0)
    			&& (this.kanjiDaDica.getCategoriaAssociada().compareTo(kanjiCartaQueEleClicou.getCategoriaAssociada()) == 0))
    		{
    			//usuario quer tirar do jogo a carta com a dica. Nao pode
    			String mensagemErro = getResources().getString(R.string.mensagem_trovao_tira_carta_tira_dica);
    			Toast t = Toast.makeText(this, mensagemErro, Toast.LENGTH_LONG);
    		    t.show();
    		    fazerMascoteFicarZangadaPorUmTempo();
    		    usouTrovaoTiraCarta = false; //nao esquecer que ele perde o item, entao o booleano eh falso
    		}
    		else
    		{
        		this.lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(7);
    		}
    	}
    	break;
    case R.id.item1:
    	this.usarItemAtual("trovaotiracartaaleatoria");
    	break;
    case R.id.item2:
    	this.usarItemAtual("parartempo");
    	break;
    case R.id.item3:
    	this.usarItemAtual("misturarcartas");
    	break;
    case R.id.item4:
    	this.usarItemAtual("mudardica");
    	break;
    case R.id.item5:
    	this.usarItemAtual("doisx");
    	break;
    case R.id.item6:
    	this.usarItemAtual("naoesperemais");
    	break;
    case R.id.item7:
    	this.usarItemAtual("cartasdouradas");
    	break;
    case R.id.item8:
    	this.usarItemAtual("trovaotiracarta");
    	break;
    case R.id.item9:
    	this.usarItemAtual("revivecarta");
    	break;
    case R.id.botao_menu_principal:
    	this.voltarAoMenuInicial(null);
    	break;
}
}

private void mandarMensagemAoAdversarioEuAcerteiCarta(String qualCarta)
{
	this.duranteAcertoDeCarta = true;
	this.qualCartaAcertada = qualCarta;
	this.dificuldadeCartaAcertada = kanjiDaDica.getDificuldadeDoKanji();
	mandarMensagemMultiplayer("eu acertei uma carta");
	
	Animation animacaoTransparente = AnimationUtils.loadAnimation(this, R.anim.anim_transparente_botao);
	
	ImageView imageViewCartaPiscada = null;
	
	if(qualCarta.compareTo("karuta1") == 0)
	{
		imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta1_imageview);
	}
	else if(qualCarta.compareTo("karuta2") == 0)
	{
		imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta2_imageview);
	}
	else if(qualCarta.compareTo("karuta3") == 0)
	{
		imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta3_imageview);
	}
	else if(qualCarta.compareTo("karuta4") == 0)
	{
		imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta4_imageview);
	}
	else if(qualCarta.compareTo("karuta5") == 0)
	{
		imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta5_imageview);
	}
	else if(qualCarta.compareTo("karuta6") == 0)
	{
		imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta6_imageview);
	}
	else if(qualCarta.compareTo("karuta7") == 0)
	{
		imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta7_imageview);
	}
	else
	{
		imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta8_imageview);
	}
	
	imageViewCartaPiscada.startAnimation(animacaoTransparente);
}

private void realizarProcedimentoAposUsuarioAcertarCarta(String qualCarta)
{
	if(qualCarta.compareTo("karuta1") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavel("karuta1");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		//tb devemos executar o som que indica que ele acertou uma carta
		super.reproduzirSfx("acertou_carta");
		this.fazerMascoteFicarFelizPorUmTempo();
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta1 = (ImageView) findViewById(R.id.karuta1_imageview);
		imageViewKaruta1.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta1); //mudei a figura da carta
		findViewById(R.id.karuta1).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta1 = (TextView) findViewById(R.id.texto_karuta1);
		textViewKaruta1.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 0);
		
		this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta2") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavel("karuta2");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		//tb devemos executar o som que indica que ele acertou uma carta
		super.reproduzirSfx("acertou_carta");
		fazerMascoteFicarFelizPorUmTempo();
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta2 = (ImageView) findViewById(R.id.karuta2_imageview);
		imageViewKaruta2.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta2); //mudei a figura da carta
		findViewById(R.id.karuta2).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta2 = (TextView) findViewById(R.id.texto_karuta2);
		textViewKaruta2.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 1);
		
		this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta3") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavel("karuta3");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		//tb devemos executar o som que indica que ele acertou uma carta
		super.reproduzirSfx("acertou_carta");
		fazerMascoteFicarFelizPorUmTempo();
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log     
		ImageView imageViewKaruta3 = (ImageView) findViewById(R.id.karuta3_imageview);
		imageViewKaruta3.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta3); //mudei a figura da carta
		findViewById(R.id.karuta3).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta3 = (TextView) findViewById(R.id.texto_karuta3);
		textViewKaruta3.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 2);
		
		this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta4") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavel("karuta4");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		//tb devemos executar o som que indica que ele acertou uma carta
		super.reproduzirSfx("acertou_carta");
		fazerMascoteFicarFelizPorUmTempo();
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta4 = (ImageView) findViewById(R.id.karuta4_imageview);
		imageViewKaruta4.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta4); //mudei a figura da carta
		findViewById(R.id.karuta4).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta4 = (TextView) findViewById(R.id.texto_karuta4);
		textViewKaruta4.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 3);
		
		this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta5") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavel("karuta5");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		//tb devemos executar o som que indica que ele acertou uma carta
		super.reproduzirSfx("acertou_carta");
		fazerMascoteFicarFelizPorUmTempo();
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
		imageViewKaruta5.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta5); //mudei a figura da carta
		findViewById(R.id.karuta5).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta5 = (TextView) findViewById(R.id.texto_karuta5);
		textViewKaruta5.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 4);
		
		this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta6") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavel("karuta6");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		//tb devemos executar o som que indica que ele acertou uma carta
		super.reproduzirSfx("acertou_carta");
		fazerMascoteFicarFelizPorUmTempo();
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
		imageViewKaruta6.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta6); //mudei a figura da carta
		findViewById(R.id.karuta6).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
		textViewKaruta6.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 5);
		
		this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta7") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavel("karuta7");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		//tb devemos executar o som que indica que ele acertou uma carta
		super.reproduzirSfx("acertou_carta");
		fazerMascoteFicarFelizPorUmTempo();
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		imageViewKaruta7.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta7); //mudei a figura da carta
		findViewById(R.id.karuta7).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
		textViewKaruta7.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 6);
		
		this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta8") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavel("karuta8");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		//tb devemos executar o som que indica que ele acertou uma carta
		super.reproduzirSfx("acertou_carta");
		fazerMascoteFicarFelizPorUmTempo();
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		imageViewKaruta8.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta8); //mudei a figura da carta
		findViewById(R.id.karuta8).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
		textViewKaruta8.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 7);
		
		this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
}

/*no caso dos dois usuarios acertarem a carta ao mesmo tempo, ambos ganham os pontos*/
private void realizarProcedimentoAposUsuarioAcertarCartaEAdversarioTbAcertou(String qualCarta)
{
	if(qualCarta.compareTo("karuta1") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou("karuta1");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta1 = (ImageView) findViewById(R.id.karuta1_imageview);
		imageViewKaruta1.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta1); //mudei a figura da carta
		findViewById(R.id.karuta1).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta1 = (TextView) findViewById(R.id.texto_karuta1);
		textViewKaruta1.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 0);
		
		
		if(this.nomeUsuario.compareTo(nomeAdversario) > 0)
		{
			//	QUEM GERA O KANJI DA DICA NOVO EH QUEM VEM PRIMEIRO NO ALFABETO, JA QUE OS DOIS ACERTARAM AO MESMO TEMPO
			this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		}
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta2") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou("karuta2");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		
		
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta2 = (ImageView) findViewById(R.id.karuta2_imageview);
		imageViewKaruta2.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta2); //mudei a figura da carta
		findViewById(R.id.karuta2).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta2 = (TextView) findViewById(R.id.texto_karuta2);
		textViewKaruta2.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 1);
		
		if(this.nomeUsuario.compareTo(nomeAdversario) > 0)
		{
			//	QUEM GERA O KANJI DA DICA NOVO EH QUEM VEM PRIMEIRO NO ALFABETO, JA QUE OS DOIS ACERTARAM AO MESMO TEMPO
			this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		}
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta3") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou("karuta3");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		
		
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log     
		ImageView imageViewKaruta3 = (ImageView) findViewById(R.id.karuta3_imageview);
		imageViewKaruta3.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta3); //mudei a figura da carta
		findViewById(R.id.karuta3).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta3 = (TextView) findViewById(R.id.texto_karuta3);
		textViewKaruta3.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 2);
		
		if(this.nomeUsuario.compareTo(nomeAdversario) > 0)
		{
			//	QUEM GERA O KANJI DA DICA NOVO EH QUEM VEM PRIMEIRO NO ALFABETO, JA QUE OS DOIS ACERTARAM AO MESMO TEMPO
			this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		}
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta4") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou("karuta4");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		
		
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta4 = (ImageView) findViewById(R.id.karuta4_imageview);
		imageViewKaruta4.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta4); //mudei a figura da carta
		findViewById(R.id.karuta4).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta4 = (TextView) findViewById(R.id.texto_karuta4);
		textViewKaruta4.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 3);
		
		if(this.nomeUsuario.compareTo(nomeAdversario) > 0)
		{
			//	QUEM GERA O KANJI DA DICA NOVO EH QUEM VEM PRIMEIRO NO ALFABETO, JA QUE OS DOIS ACERTARAM AO MESMO TEMPO
			this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		}
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta5") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou("karuta5");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		
		
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
		imageViewKaruta5.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta5); //mudei a figura da carta
		findViewById(R.id.karuta5).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta5 = (TextView) findViewById(R.id.texto_karuta5);
		textViewKaruta5.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 4);
		
		if(this.nomeUsuario.compareTo(nomeAdversario) > 0)
		{
			//	QUEM GERA O KANJI DA DICA NOVO EH QUEM VEM PRIMEIRO NO ALFABETO, JA QUE OS DOIS ACERTARAM AO MESMO TEMPO
			this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		}
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta6") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou("karuta6");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		
		
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
		imageViewKaruta6.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta6); //mudei a figura da carta
		findViewById(R.id.karuta6).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
		textViewKaruta6.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 5);
		
		if(this.nomeUsuario.compareTo(nomeAdversario) > 0)
		{
			//	QUEM GERA O KANJI DA DICA NOVO EH QUEM VEM PRIMEIRO NO ALFABETO, JA QUE OS DOIS ACERTARAM AO MESMO TEMPO
			this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		}
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta7") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou("karuta7");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		
		
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		imageViewKaruta7.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta7); //mudei a figura da carta
		findViewById(R.id.karuta7).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
		textViewKaruta7.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 6);
		
		if(this.nomeUsuario.compareTo(nomeAdversario) > 0)
		{
			//	QUEM GERA O KANJI DA DICA NOVO EH QUEM VEM PRIMEIRO NO ALFABETO, JA QUE OS DOIS ACERTARAM AO MESMO TEMPO
			this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		}
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
	else if(qualCarta.compareTo("karuta8") == 0)
	{

		this.alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou("karuta8");
		this.aumentarPontuacaoComBaseNaDificuldadeDoKanji();
		
		
		
		//se existir alguma carta proibida na tela, ela deve desaparecer(item naoesperemais)
		this.fazerCartaProibidaVoltarAoNormal();
		//se existir alguma carta dourada na tela, ela deve voltar ao normal(item cartasdouradas)
		this.fazerCartasDouradasVoltaremAoNormal();
		
		this.palavrasAcertadas.add(this.kanjiDaDica); //ele acertou mais uma palavra para o log
		ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		imageViewKaruta8.clearAnimation();
		this.fazerImageViewFicarEscuro(imageViewKaruta8); //mudei a figura da carta
		findViewById(R.id.karuta8).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
		TextView textViewKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
		textViewKaruta8.setText("");
		
		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
		fazerTextoVersoDaCartaAparecer(this.kanjiDaDica, 7);
		
		if(this.nomeUsuario.compareTo(nomeAdversario) > 0)
		{
			//	QUEM GERA O KANJI DA DICA NOVO EH QUEM VEM PRIMEIRO NO ALFABETO, JA QUE OS DOIS ACERTARAM AO MESMO TEMPO
			this.gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo();
		}
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
		this.duranteAcertoDeCarta = false;
		this.qualCartaAcertada = "";
	}
}


/*iremos alterar a pontuacao do usuario com base na dificuldade do kanji que ele acabou de acertar.
  A sua pontuacao cresce aqui, mas a do adversario cresce somente se ele acertar alguma carta e vc recebeu
  uma mensagem dizendo que a carta X nao estah mais clicavel*/
private void aumentarPontuacaoComBaseNaDificuldadeDoKanji()
{
	if(this.kanjiDaDica != null)
	{
		int dificuldade = this.kanjiDaDica.getDificuldadeDoKanji();
		if(this.usou2x == false)
		{
			if(dificuldade == 1)
			{
				this.suaPontuacao = this.suaPontuacao + 1;
				//this.realizarAnimacaoAumentaPontuacao(1,false);
				
			}
			else if(dificuldade == 2)
			{
				this.suaPontuacao = this.suaPontuacao + 2;
				//this.realizarAnimacaoAumentaPontuacao(2,false);
			}
			else
			{
				this.suaPontuacao = this.suaPontuacao + 3;
				//this.realizarAnimacaoAumentaPontuacao(3,false);
			}
		}
		else
		{
			//usuario usou o item 2x. As animacoes sao diferentes
			if(dificuldade == 1)
			{
				this.suaPontuacao = this.suaPontuacao + 2;
				//this.realizarAnimacaoAumentaPontuacao(1,true);
			}
			else if(dificuldade == 2)
			{
				this.suaPontuacao = this.suaPontuacao + 4;
				//this.realizarAnimacaoAumentaPontuacao(2,true);
			}
			else
			{
				this.suaPontuacao = this.suaPontuacao + 6;
				//this.realizarAnimacaoAumentaPontuacao(3,true);
			}
			
		}
		
		TextView textoPontuacao = (TextView) findViewById(R.id.nome_e_pontuacao_jogador_esquerda);
		String palavraPontuacao = getResources().getString(R.string.pontuacao);
		String nomeJogador = "";
		
		if(this.nomeUsuario.length() > 12)
		{
			nomeJogador = nomeUsuario.substring(0, 8) + "...";
		}
		else
		{
			nomeJogador = this.nomeUsuario;
		}
		
		
		if(suaPontuacao < 10)
		{
			textoPontuacao.setText(nomeJogador + ":" + "0" + String.valueOf(suaPontuacao));
		}
		else
		{
			textoPontuacao.setText(nomeJogador + ":" + String.valueOf(suaPontuacao));
		}
		
		if(this.usou2x == true)
		 {
			 this.usou2x = false;
			 findViewById(R.id.doisxpequeno).setVisibility(View.INVISIBLE);
				//AINDA FALTA MUDAR O BOOLEANO, MAS ALTERAMOS ELE QUANDO O ADVERSARIO SABE Q O USUARIO ACERTOU COM MAIS PONTOS
		 }
	}
}

private void realizarAnimacaoAumentaPontuacao(int dificuldadeDoKanji, boolean eh2x)
{
	/*final AnimationDrawable animacaoAumentaPontuacao = new AnimationDrawable(); 
	int idImagemAnimacaoAumentaPontos1 = 0;
	int idImagemAnimacaoAumentaPontos2 = 0;
	int idImagemAnimacaoAumentaPontos3 = 0;
	int idImagemAnimacaoAumentaPontos4 = 0;
	int idImagemAnimacaoAumentaPontos5 = 0;
	int idImagemAnimacaoAumentaPontos6 = 0;
	int idImagemAnimacaoAumentaPontos7 = 0;
	int idImagemAnimacaoAumentaPontos8 = 0;
	int idImagemAnimacaoAumentaPontos9 = 0;
	int idImagemAnimacaoAumentaPontos10 = 0;
	int idImagemAnimacaoAumentaPontos11 = 0;
	int idImagemAnimacaoAumentaPontos12 = 0;
	int idImagemAnimacaoAumentaPontos13 = 0;
	int idImagemAnimacaoAumentaPontos14 = 0;
	int idImagemAnimacaoAumentaPontos15 = 0;
	int idImagemAnimacaoAumentaPontos16 = 0;
	int idImagemAnimacaoAumentaPontos17 = 0;
	
	
	if(dificuldadeDoKanji == 1 && eh2x == false)
	{
		idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("animacao10_1", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("animacao10_2", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("animacao10_3", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("animacao10_4", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("animacao10_5", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("animacao10_6", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("animacao10_7", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("animacao10_8", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("animacao10_9", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("animacao10_10", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("animacao10_11", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("animacao10_12", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("animacao10_13", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("animacao10_14", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("animacao10_15", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("animacao10_16", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("animacao10_17", "drawable", getPackageName());
	}
	else if(dificuldadeDoKanji == 2 && eh2x == false)
	{
		idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("animacao20_1", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("animacao20_2", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("animacao20_3", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("animacao20_4", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("animacao20_5", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("animacao20_6", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("animacao20_7", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("animacao20_8", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("animacao20_9", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("animacao20_10", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("animacao20_11", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("animacao20_12", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("animacao20_13", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("animacao20_14", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("animacao20_15", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("animacao20_16", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("animacao20_17", "drawable", getPackageName());
	}
	else if(dificuldadeDoKanji == 3 && eh2x == false)
	{
		idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("animacao30_1", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("animacao30_2", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("animacao30_3", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("animacao30_4", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("animacao30_5", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("animacao30_6", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("animacao30_7", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("animacao30_8", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("animacao30_9", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("animacao30_10", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("animacao30_11", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("animacao30_12", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("animacao30_13", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("animacao30_14", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("animacao30_15", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("animacao30_16", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("animacao30_17", "drawable", getPackageName());
	}
	else if(dificuldadeDoKanji == 1 && eh2x == true)
	{
		idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("doisxanimacao20_1", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("doisxanimacao20_2", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("doisxanimacao20_3", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("doisxanimacao20_4", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("doisxanimacao20_5", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("doisxanimacao20_6", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("doisxanimacao20_7", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("doisxanimacao20_8", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("doisxanimacao20_9", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("doisxanimacao20_10", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("doisxanimacao20_11", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("doisxanimacao20_12", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("doisxanimacao20_13", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("doisxanimacao20_14", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("doisxanimacao20_15", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("doisxanimacao20_16", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("doisxanimacao20_17", "drawable", getPackageName());
	}
	else if(dificuldadeDoKanji == 2 && eh2x == true)
	{
		idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("doisxanimacao40_1", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("doisxanimacao40_2", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("doisxanimacao40_3", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("doisxanimacao40_4", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("doisxanimacao40_5", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("doisxanimacao40_6", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("doisxanimacao40_7", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("doisxanimacao40_8", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("doisxanimacao40_9", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("doisxanimacao40_10", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("doisxanimacao40_11", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("doisxanimacao40_12", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("doisxanimacao40_13", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("doisxanimacao40_14", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("doisxanimacao40_15", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("doisxanimacao40_16", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("doisxanimacao40_17", "drawable", getPackageName());
	}
	else if(dificuldadeDoKanji == 3 && eh2x == true)
	{
		idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("doisxanimacao60_1", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("doisxanimacao60_2", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("doisxanimacao60_3", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("doisxanimacao60_4", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("doisxanimacao60_5", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("doisxanimacao60_6", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("doisxanimacao60_7", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("doisxanimacao60_8", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("doisxanimacao60_9", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("doisxanimacao60_10", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("doisxanimacao60_11", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("doisxanimacao60_12", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("doisxanimacao60_13", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("doisxanimacao60_14", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("doisxanimacao60_15", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("doisxanimacao60_16", "drawable", getPackageName());
		idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("doisxanimacao60_17", "drawable", getPackageName());
	} 
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos1), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos2), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos3), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos4), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos5), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos6), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos7), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos8), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos9), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos10), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos11), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos12), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos13), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos14), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos15), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos16), 50);
	 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos17), 50);
	 animacaoAumentaPontuacao.setOneShot(true);
	 
	 ImageView imageViewMaisPontos = (ImageView) findViewById(R.id.maispontos);
	 imageViewMaisPontos.setImageDrawable(animacaoAumentaPontuacao);
	 
	 imageViewMaisPontos.post(new Runnable() {
		@Override
		public void run() {
			animacaoAumentaPontuacao.start();
		}
	 	});
	 
	 if(usou2x == true)
	 {
		 super.reproduzirSfx("dois_x");
	 }
	 
	 new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        TelaInicialMultiplayer.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	TextView textoPontuacao = (TextView) findViewById(R.id.pontuacao);
		            	
		            	if(suaPontuacao < 100)
		            	{
		            		textoPontuacao.setText("0" + String.valueOf(suaPontuacao));
		            	}
		            	else
		            	{
		            		textoPontuacao.setText(String.valueOf(suaPontuacao));
		            	}
		            }
		        });
		    }
		}, 900);*/
}

void startQuickGame(int variant) 
{
// quick-start a game with 1 randomly selected opponent
	
	//ambos os jogadores ja sairam do lobby das salas do modo casual, entao pode matar a thread que atualizava as salas
	if(threadFicaBuscandoSalasModoCasual != null && threadFicaBuscandoSalasModoCasual.isAlive() == true)
	{
		threadFicaBuscandoSalasModoCasual.interrupt();
	}
	
	try
	{
		final int MIN_OPPONENTS = 1, MAX_OPPONENTS = 1;
		Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(MIN_OPPONENTS,
		        MAX_OPPONENTS, 0);
		RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
		rtmConfigBuilder.setMessageReceivedListener(this);
		rtmConfigBuilder.setRoomStatusUpdateListener(this);
		rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);

		int variante = variant;
		rtmConfigBuilder.setVariant(variante); //somente dois usuarios com o mesmo variante podem jogar juntos no automatch. Usaremos o nivel do usuario como esse variante

		switchToScreen(R.id.screen_wait);
		keepScreenOn();
		resetGameVars();
		Games.RealTimeMultiplayer.create(getApiClient(), rtmConfigBuilder.build());
	}
	catch(Exception e)
	{
		String excecao = e.getMessage();
		excecao = excecao + "";
	}
}

@Override
public void onActivityResult(int requestCode, int responseCode,
    Intent intent) {
super.onActivityResult(requestCode, responseCode, intent);

switch (requestCode) {
    case RC_SELECT_PLAYERS:
        // we got the result from the "select players" UI -- ready to create the room
        handleSelectPlayersResult(responseCode, intent);
        break;
    case RC_INVITATION_INBOX:
        // we got the result from the "select invitation" UI (invitation inbox). We're
        // ready to accept the selected invitation:
        handleInvitationInboxResult(responseCode, intent);
        break;
    case RC_WAITING_ROOM:
        // we got the result from the "waiting room" UI.
        if (responseCode == Activity.RESULT_OK) {
            // ready to start playing
            Log.d(TAG, "Starting game (waiting room returned OK).");
            startGame(true);
        } else if (responseCode == GamesActivityResultCodes.RESULT_LEFT_ROOM) {
            // player indicated that they want to leave the room
            leaveRoom();
        } else if (responseCode == Activity.RESULT_CANCELED) {
            // Dialog was cancelled (user pressed back key, for instance). In our game,
            // this means leaving the room too. In more elaborate games, this could mean
            // something else (like minimizing the waiting room UI).
            leaveRoom();
        }
        break;
}
}

// Handle the result of the "Select players UI" we launched when the user clicked the
// "Invite friends" button. We react by creating a room with those players.
private void handleSelectPlayersResult(int response, Intent data) {
if (response != Activity.RESULT_OK) {
    Log.w(TAG, "*** select players UI cancelled, " + response);
    switchToMainScreen();
    return;
}

Log.d(TAG, "Select players UI succeeded.");

// get the invitee list
final ArrayList<String> invitees = data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);
Log.d(TAG, "Invitee count: " + invitees.size());

// get the automatch criteria
Bundle autoMatchCriteria = null;
int minAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
int maxAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);
if (minAutoMatchPlayers > 0 || maxAutoMatchPlayers > 0) {
    autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
            minAutoMatchPlayers, maxAutoMatchPlayers, 0);
    Log.d(TAG, "Automatch criteria: " + autoMatchCriteria);
}

// create the room
Log.d(TAG, "Creating room...");
RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
rtmConfigBuilder.addPlayersToInvite(invitees);
rtmConfigBuilder.setMessageReceivedListener(this);
rtmConfigBuilder.setRoomStatusUpdateListener(this);
if (autoMatchCriteria != null) {
    rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
}
switchToScreen(R.id.screen_wait);
keepScreenOn();
resetGameVars();
Games.RealTimeMultiplayer.create(getApiClient(), rtmConfigBuilder.build());
Log.d(TAG, "Room created, waiting for it to be ready...");
}

// Handle the result of the invitation inbox UI, where the player can pick an invitation
// to accept. We react by accepting the selected invitation, if any.
private void handleInvitationInboxResult(int response, Intent data) {
if (response != Activity.RESULT_OK) {
    Log.w(TAG, "*** invitation inbox UI cancelled, " + response);
    switchToMainScreen();
    return;
}

Log.d(TAG, "Invitation inbox UI succeeded.");
Invitation inv = data.getExtras().getParcelable(Multiplayer.EXTRA_INVITATION);

// accept invitation
acceptInviteToRoom(inv.getInvitationId());
}

// Accept the given invitation.
void acceptInviteToRoom(String invId) {
// accept the invitation
Log.d(TAG, "Accepting invitation: " + invId);
RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
roomConfigBuilder.setInvitationIdToAccept(invId)
        .setMessageReceivedListener(this)
        .setRoomStatusUpdateListener(this);
switchToScreen(R.id.screen_wait);
keepScreenOn();
resetGameVars();
Games.RealTimeMultiplayer.join(getApiClient(), roomConfigBuilder.build());
}

// Activity is going to the background. We have to leave the current room.
@Override
public void onStop() 
{
	Log.d(TAG, "**** got onStop");
// if we're in a room, leave it.
leaveRoomDoOnStop();

// stop trying to keep the screen on
stopKeepingScreenOn();

//switchToScreen(R.id.screen_wait);
super.onStop();
}

@Override
public void onPause()
{
	super.onPause();
	
	if(threadFicaBuscandoSalasModoCasual != null && threadFicaBuscandoSalasModoCasual.isAlive() == true)
	{
		threadFicaBuscandoSalasModoCasual.interrupt();
	}
}

// Activity just got to the foreground. We switch to the wait screen because we will now
// go through the sign-in flow (remember that, yes, every time the Activity comes back to the
// foreground we go through the sign-in flow -- but if the user is already authenticated,
// this flow simply succeeds and is imperceptible).
@Override
public void onStart() {
super.onStart();
switchToScreen(R.id.screen_wait);
}

// Handle back key to make sure we cleanly leave a game if we are in the middle of one
@Override
public boolean onKeyDown(int keyCode, KeyEvent e) {
if (keyCode == KeyEvent.KEYCODE_BACK && mCurScreen == R.id.screen_game) 
{
    leaveRoom();
    return true;
}
else if (keyCode == KeyEvent.KEYCODE_HOME && mCurScreen == R.id.screen_game) 
{
    leaveRoom();
    return true;
}
return super.onKeyDown(keyCode, e);
}

// Leave the room.
void leaveRoom() 
{
	try
	{
		if(this.jaDeixouASala == false)
		{
			//caso ainda exista alguma sala no bd com o email do usuario, melhr deletar
			FechaSalaCasualCriadaPeloUsuarioTask deletarSalaCasual = new FechaSalaCasualCriadaPeloUsuarioTask(this);
			deletarSalaCasual.execute(nomeUsuario);
			
			this.jaDeixouASala = true;
			this.criouUmaSala = false;
			this.noPopupQueBuscaSalasPorCategoria = false;
			//Log.d(TAG, "Leaving room.");
			tempoRestante = 0;
			stopKeepingScreenOn();
			if (mRoomId != null && this.salaCriadaEhParaRevanche == false) {
			    Games.RealTimeMultiplayer.leave(getApiClient(), this, mRoomId);
			    mRoomId = null;
			    Intent irMenuInicial =
						new Intent(ModoCasual.this, MainActivity.class);
			    irMenuInicial.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);	
			    startActivity(irMenuInicial);
			}else if(this.salaCriadaEhParaRevanche == false){
				 Intent irMenuInicial =
							new Intent(ModoCasual.this, MainActivity.class);
				irMenuInicial.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);	
				 startActivity(irMenuInicial);
			}
			else
			{
				if(mRoomId == null)
				{
					Games.RealTimeMultiplayer.leave(getApiClient(), this, mRoomId);
				    mRoomId = null;
				}
			}
		}
	}
	catch(Exception e)
	{
		String s = e.getMessage();
		s = s + "";
	}
}

void leaveRoomDoOnStop() 
{
	if(this.jaDeixouASala == false)
	{
		//caso ainda exista alguma sala no bd com o email do usuario, melhr deletar
		if(this.criouUmaSala == true)
		{
			FechaSalaCasualCriadaPeloUsuarioTask deletarSalaCasual = new FechaSalaCasualCriadaPeloUsuarioTask(this);
			deletarSalaCasual.execute(nomeUsuario);
		}
		
		this.jaDeixouASala = true;
		this.criouUmaSala = false;
		//Log.d(TAG, "Leaving room.");
		tempoRestante = 0;
		stopKeepingScreenOn();
		if (mRoomId != null) {
		    Games.RealTimeMultiplayer.leave(getApiClient(), this, mRoomId);
		    mRoomId = null;
		} else {
		}
	}
}

// Show the waiting room UI to track the progress of other players as they enter the
// room and get connected.
void showWaitingRoom(Room room) {
// minimum number of players required for our game
// For simplicity, we require everyone to join the game before we start it
// (this is signaled by Integer.MAX_VALUE).
//final int MIN_PLAYERS = Integer.MAX_VALUE;
final int MIN_PLAYERS = 2;
Intent i = Games.RealTimeMultiplayer.getWaitingRoomIntent(getApiClient(), room, MIN_PLAYERS);

// show waiting room UI
startActivityForResult(i, RC_WAITING_ROOM);
}

// Called when we get an invitation to play a game. We react by showing that to the user.
@Override
public void onInvitationReceived(Invitation invitation) {
// We got an invitation to play a game! So, store it in
// mIncomingInvitationId
// and show the popup on the screen.
mIncomingInvitationId = invitation.getInvitationId();
((TextView) findViewById(R.id.incoming_invitation_text)).setText(
        invitation.getInviter().getDisplayName() + " " +
                getString(R.string.is_inviting_you));
switchToScreen(mCurScreen); // This will show the invitation popup
}

@Override
public void onInvitationRemoved(String invitationId) {
if (mIncomingInvitationId.equals(invitationId)) {
    mIncomingInvitationId = null;
    switchToScreen(mCurScreen); // This will hide the invitation popup
}
}

/*
* CALLBACKS SECTION. This section shows how we implement the several games
* API callbacks.
*/

// Called when we are connected to the room. We're not ready to play yet! (maybe not everybody
// is connected yet).
@Override
public void onConnectedToRoom(Room room) {
Log.d(TAG, "onConnectedToRoom.");

// get room ID, participants and my ID:
mRoomId = room.getRoomId();
this.room = room;
mParticipants = room.getParticipants();
mMyId = room.getParticipantId(Games.Players.getCurrentPlayerId(getApiClient()));

// print out the list of participants (for debug purposes)
Log.d(TAG, "Room ID: " + mRoomId);
Log.d(TAG, "My ID " + mMyId);
Log.d(TAG, "<< CONNECTED TO ROOM>>");
}

// Called when we've successfully left the room (this happens a result of voluntarily leaving
// via a call to leaveRoom(). If we get disconnected, we get onDisconnectedFromRoom()).
@Override
public void onLeftRoom(int statusCode, String roomId) {
// we have left the room; return to main screen.
//Log.d(TAG, "onLeftRoom, code " + statusCode);
//switchToMainScreen();
}

// Called when we get disconnected from the room. We return to the main screen.
@Override
public void onDisconnectedFromRoom(Room room) 
{
	mRoomId = null;
	if(this.revancheEstahAcontecendo == false)
	{
		showGameError();
		this.voltarAoMenuInicial(null);
	}
}

// Show error message about game being cancelled and return to main screen.
void showGameError() {
	Toast t = Toast.makeText(this, getString(R.string.game_problem), Toast.LENGTH_LONG);
    t.show();
//showAlert(getString(R.string.game_problem));
}

// Called when room has been created
@Override
public void onRoomCreated(int statusCode, Room room) {
Log.d(TAG, "onRoomCreated(" + statusCode + ", " + room + ")");
if (statusCode != GamesStatusCodes.STATUS_OK) {
    Log.e(TAG, "*** Error: onRoomCreated, status " + statusCode);
    showGameError();
    return;
}

// show the waiting room UI
showWaitingRoom(room);
}

// Called when room is fully connected.
@Override
public void onRoomConnected(int statusCode, Room room) {
Log.d(TAG, "onRoomConnected(" + statusCode + ", " + room + ")");
if (statusCode != GamesStatusCodes.STATUS_OK) {
    Log.e(TAG, "*** Error: onRoomConnected, status " + statusCode);
    showGameError();
    return;
}
updateRoom(room);
}

@Override
public void onJoinedRoom(int statusCode, Room room) {
Log.d(TAG, "onJoinedRoom(" + statusCode + ", " + room + ")");
if (statusCode != GamesStatusCodes.STATUS_OK) {
    Log.e(TAG, "*** Error: onRoomConnected, status " + statusCode);
    showGameError();
    return;
}

// show the waiting room UI
showWaitingRoom(room);
}

// We treat most of the room update callbacks in the same way: we update our list of
// participants and update the display. In a real game we would also have to check if that
// change requires some action like removing the corresponding player avatar from the screen,
// etc.
@Override
public void onPeerDeclined(Room room, List<String> arg1) {
updateRoom(room);
}

@Override
public void onPeerInvitedToRoom(Room room, List<String> arg1) {
updateRoom(room);
}

@Override
public void onP2PDisconnected(String participant) {
}

@Override
public void onP2PConnected(String participant) {
}

@Override
public void onPeerJoined(Room room, List<String> arg1) {
updateRoom(room);
}

@Override
public void onPeerLeft(Room room, List<String> peersWhoLeft) {
updateRoom(room);
}

@Override
public void onRoomAutoMatching(Room room) {
updateRoom(room);
}

@Override
public void onRoomConnecting(Room room) {
updateRoom(room);
}

@Override
public void onPeersConnected(Room room, List<String> peers) {
updateRoom(room);
}

@Override
public void onPeersDisconnected(Room room, List<String> peers) {
updateRoom(room);
}

void updateRoom(Room room) {
if (room != null) {
    mParticipants = room.getParticipants();
}
if (mParticipants != null) {
}
}

/*
* GAME LOGIC SECTION. Methods that implement the game's rules.
*/

// Current state of the game:
int tempoRestante = -1; // how long until the game ends (seconds)
final static int GAME_DURATION = 90; // game duration, seconds.
int mScore = 0; // user's current score

// Reset game variables in preparation for a new game.
void resetGameVars() {
tempoRestante = GAME_DURATION;
mScore = 0;
}

// Start the gameplay phase of the game.
synchronized void startGame(boolean multiplayer) 
{
	mMultiplayer = multiplayer;
	
	this.enviarSeuNomeParaOAdversario();
	
	switchToScreen(R.id.tela_jogo_multiplayer);
	
	if(this.criouUmaSala == false)
	{
		//quem nao criou a sala ainda nao tem o numero de rodadas ou as categorias
		String infinitasRodadas = getResources().getString(R.string.infinitas_rodadas_sem_mais_nada);
		String rodadas = this.salaAtual.getQuantasRodadas();
		
		try
		{
			this.quantasRodadasHaverao = Integer.valueOf(rodadas);
		}
		catch(Exception e)
		{
			this.quantasRodadasHaverao = 99;
		}
		
		
		//o cara que nao criou a sala ira mandar o outro escolher os kanjis para treino 
		//somente apos a task abaixo terminar seu servico na funcao procedimentoaposcarregarkanjis()
		//implementada pela classe ModoCasual
		if(revancheEstahAcontecendo == true)
		{
			//se eh revanche, n precisa pegar os kanjis de novo!
			String mensagemParaODono = "pode comecar a escolher os kanjis";
			this.mandarMensagemMultiplayer(mensagemParaODono);
			this.pegarTodosOsKanjisQuePodemVirarCartas(); //ele tb deve atualizar a lista com todos os kanjis q podem virar cartas. senao quando um turno passar, o jogador q n escolhe as categorias n tem como gerar 8 kanjis
		}
		else
		{
			SolicitaKanjisParaTreinoTask pegaKanjisDoBD = new SolicitaKanjisParaTreinoTask(null, this);
			pegaKanjisDoBD.execute("");
		}
	}
	else
	{
		//eh preciso tirar do banco de dados a sala que estava aberta. Ela agora fechou
		FechaSalaCasualCriadaPeloUsuarioTask fechaSalaAberta = new FechaSalaCasualCriadaPeloUsuarioTask(this);
		fechaSalaAberta.execute(this.nomeUsuario);
	}
	
	
	//como eh o comeco da primeira partida do jogo, vamos fazer o usuario ver uma tela de espera pelo menos ate o kanji da dica ser escolhido
	this.comecarEsperaDoUsuarioParaComecoDaPartida();
	
	//a funcao comecarJogoMultiplayer que inicia a UI do jogo no modo multiplayer so serah iniciada quando os usuarios trocarem emails
}





/*
* UI SECTION. Methods that implement the game's UI.
*/

// This array lists everything that's clickable, so we can install click
// event handlers.
final static int[] CLICKABLES = {
    R.id.button_accept_popup_invitation, R.id.button_sign_in
};

int mCurScreen = -1;

void switchToScreen(int screenId) {
// make the requested screen visible; hide all others.
for (int id : SCREENS) {
    findViewById(id).setVisibility(screenId == id ? View.VISIBLE : View.GONE);
}
mCurScreen = screenId;

// should we show the invitation popup?
boolean showInvPopup;
if (mIncomingInvitationId == null) {
    // no invitation, so no popup
    showInvPopup = false;
} else if (mMultiplayer) {
    // if in multiplayer, only show invitation on main screen
    showInvPopup = (mCurScreen == R.id.tela_lobby_modo_casual);
} else {
    // single-player: show on main screen and gameplay screen
    showInvPopup = (mCurScreen == R.id.tela_lobby_modo_casual || mCurScreen == R.id.screen_game);
}
findViewById(R.id.invitation_popup).setVisibility(showInvPopup ? View.VISIBLE : View.GONE);
}

void switchToMainScreen() {
switchToScreen(isSignedIn() ? R.id.tela_lobby_modo_casual : R.id.screen_sign_in);

SpinnerSelecionaMesmoQuandoVoltaAoMesmoItem spinner = (SpinnerSelecionaMesmoQuandoVoltaAoMesmoItem) findViewById(R.id.spinnerPesquisarSalasModoCasual);
//Create an ArrayAdapter using the string array and a default spinner layout
ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
     R.array.spinner_pesquisar_por_modo_casual, android.R.layout.simple_spinner_item);
//Specify the layout to use when the list of choices appears
adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Apply the adapter to the spinner
spinner.setAdapter(adapter);
spinner.setOnItemSelectedListener(this);

this.mostrarLobbyModoCasual();

}




/*
* MISC SECTION. Miscellaneous methods.
*/

/**
* Checks that the developer (that's you!) read the instructions. IMPORTANT:
* a method like this SHOULD NOT EXIST in your production app! It merely
* exists here to check that anyone running THIS PARTICULAR SAMPLE did what
* they were supposed to in order for the sample to work.
*/
boolean verifyPlaceholderIdsReplaced() {
final boolean CHECK_PKGNAME = true; // set to false to disable check
                                    // (not recommended!)

// Did the developer forget to change the package name?
if (CHECK_PKGNAME && getPackageName().startsWith("com.google.example.")) {
    Log.e(TAG, "*** Sample setup problem: " +
        "package name cannot be com.google.example.*. Use your own " +
        "package name.");
    return false;
}

// Did the developer forget to replace a placeholder ID?
int res_ids[] = new int[] {
        R.string.app_id
};
for (int i : res_ids) {
    if (getString(i).equalsIgnoreCase("ReplaceMe")) {
        Log.e(TAG, "*** Sample setup problem: You must replace all " +
            "placeholder IDs in the ids.xml file by your project's IDs.");
        return false;
    }
}
return true;
}

// Sets the flag to keep this screen on. It's recommended to do that during
// the
// handshake when setting up a game, because if the screen turns off, the
// game will be
// cancelled.
void keepScreenOn() {
getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
}

// Clears the flag that keeps the screen on.
void stopKeepingScreenOn() {
getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
}

/*
* COMMUNICATIONS SECTION. Methods that implement the game's network
* protocol.
*/

// Called when we receive a real-time message from the network.
// Messages in our game are made up of 2 bytes: the first one is 'F' or 'U'
// indicating
// whether it's a final or interim score. The second byte is the score.
// There is also the
// 'S' message, which indicates that the game should start.
@Override
public synchronized void onRealTimeMessageReceived(RealTimeMessage rtm) 
{
	fazerCartasFicaremNaoClicaveis();
	
	byte[] buf = rtm.getMessageData();
	String sender = rtm.getSenderParticipantId();

	String mensagem = "";
	try {
		mensagem = new String(buf, "UTF-8");
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	/*Toast t = Toast.makeText(this, "mensagem recebida:" + mensagem, Toast.LENGTH_LONG);
    t.show();*/
    
	/*if(revancheEstahAcontecendo == true)
	{
		mensagem = mensagem + "";
		
		if(mensagem.contains("nome") == false)
		{
			String comecando_revanche = getResources().getString(R.string.comecando_revanche);
			Toast t = Toast.makeText(getApplicationContext(), comecando_revanche, Toast.LENGTH_LONG);
			//t.show();
		}
	}*/
	if(this.revancheEstahAcontecendo == true && (mensagem.contains("pode comecar a escolher os kanjis") || mensagem.contains("nome=") || (mensagem.contains("kanjis=") && mensagem.contains("item misturarcartas kanjis=") == false)))
	{
		String comecando_revanche = getResources().getString(R.string.comecando_revanche);
		Toast t = Toast.makeText(getApplicationContext(), comecando_revanche, Toast.LENGTH_LONG);
		t.show();
	}
	
	if(mensagem.contains("eu acertei uma carta") == true)
	{
		if(this.duranteAcertoDeCarta == false)
		{
			mandarMensagemMultiplayer("pode acertar carta");
		}
		else
		{
			super.reproduzirSfx("acertou mas adversario que ganhou pontos");
			fazerMascoteFicarFelizPorUmTempo();
			
			realizarProcedimentoAposUsuarioAcertarCartaEAdversarioTbAcertou(qualCartaAcertada);
				
			ImageView imageViewCartaPiscada = null;
				
				String qualCarta = this.qualCartaAcertada;
				
				if(qualCarta.compareTo("karuta1") == 0)
				{
					imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta1_imageview);
				}
				else if(qualCarta.compareTo("karuta2") == 0)
				{
					imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta2_imageview);
				}
				else if(qualCarta.compareTo("karuta3") == 0)
				{
					imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta3_imageview);
				}
				else if(qualCarta.compareTo("karuta4") == 0)
				{
					imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta4_imageview);
				}
				else if(qualCarta.compareTo("karuta5") == 0)
				{
					imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta5_imageview);
				}
				else if(qualCarta.compareTo("karuta6") == 0)
				{
					imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta6_imageview);
				}
				else if(qualCarta.compareTo("karuta7") == 0)
				{
					imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta7_imageview);
				}
				else
				{
					imageViewCartaPiscada = (ImageView) findViewById(R.id.karuta8_imageview);
				}
				
				if(imageViewCartaPiscada != null)
				{
					imageViewCartaPiscada.clearAnimation();
				}
				
		}
	}
	else if(mensagem.contains("pode acertar carta") == true)
	{
		//Toast t = Toast.makeText(getApplicationContext(), "pode acertar carta recebido", Toast.LENGTH_LONG);
		//t.show();
		this.realizarProcedimentoAposUsuarioAcertarCarta(this.qualCartaAcertada);
	}
	else if(mensagem.contains("naoClicavel=") == true && jogoAcabou == false && duranteAcertoDeCarta == false)
	{
		//alguem acertou uma carta e por isso essa carta nao deveria ser mais clicavel p ambos os jogadores
		//tb eh avisado se o adversario usou o 2x, ou seja, ganhou o dobro dos pontos ou nao
		//ex: naoClicavel=karuta1;usou2x=false
		String[] cartaNaoClicavelEUsou2x = mensagem.split(";");		
		String karutaNaoClicavel = cartaNaoClicavelEUsou2x[0].replace("naoClicavel=", "");
		if(karutaNaoClicavel.compareTo("karuta1") == 0)
		{
			ImageView imageViewKaruta1 = (ImageView) findViewById(R.id.karuta1_imageview);
			if(imageViewKaruta1 != null)
			{
				imageViewKaruta1.setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
				this.fazerImageViewFicarEscuro(imageViewKaruta1); //mudei a figura da carta
	    		TextView textViewKaruta1 = (TextView) findViewById(R.id.texto_karuta1);
	    		textViewKaruta1.setText("");
	    		
	    		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
	    		KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(0);
	    		fazerTextoVersoDaCartaAparecer(kanji, 0);
			}
		}
		else if(karutaNaoClicavel.compareTo("karuta2") == 0)
		{
			ImageView imageViewKaruta2 = (ImageView) findViewById(R.id.karuta2_imageview);
			
			if(imageViewKaruta2 != null)
			{
				imageViewKaruta2.setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
			
				this.fazerImageViewFicarEscuro(imageViewKaruta2); //mudei a figura da carta
				TextView textViewKaruta2 = (TextView) findViewById(R.id.texto_karuta2);
				textViewKaruta2.setText("");
				
				//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
				KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(1);
	    		fazerTextoVersoDaCartaAparecer(kanji, 1);
			}
		}
		else if(karutaNaoClicavel.compareTo("karuta3") == 0)
		{
			ImageView imageViewKaruta3 = (ImageView) findViewById(R.id.karuta3_imageview);
			
			if(imageViewKaruta3 != null)
			{
				imageViewKaruta3.setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
				this.fazerImageViewFicarEscuro(imageViewKaruta3); //mudei a figura da carta
	    		TextView textViewKaruta3 = (TextView) findViewById(R.id.texto_karuta3);
	    		textViewKaruta3.setText("");
	    		
	    		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
	    		KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(2);
	    		fazerTextoVersoDaCartaAparecer(kanji, 2);
			}
		}
		else if(karutaNaoClicavel.compareTo("karuta4") == 0)
		{
			ImageView imageViewKaruta4 = (ImageView) findViewById(R.id.karuta4_imageview);
			if(imageViewKaruta4 != null)
			{
				imageViewKaruta4.setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
				this.fazerImageViewFicarEscuro(imageViewKaruta4); //mudei a figura da carta
	    		TextView textViewKaruta4 = (TextView) findViewById(R.id.texto_karuta4);
	    		textViewKaruta4.setText("");
	    		
	    		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
	    		KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(3);
	    		fazerTextoVersoDaCartaAparecer(kanji, 3);
			}
		}
		else if(karutaNaoClicavel.compareTo("karuta5") == 0)
		{
			ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			
			if(imageViewKaruta5 != null)
			{
				imageViewKaruta5.setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
				this.fazerImageViewFicarEscuro(imageViewKaruta5); //mudei a figura da carta
	    		TextView textViewKaruta5 = (TextView) findViewById(R.id.texto_karuta5);
	    		textViewKaruta5.setText("");
	    		
	    		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
	    		KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(4);
	    		fazerTextoVersoDaCartaAparecer(kanji, 4);
			}
		}
		else if(karutaNaoClicavel.compareTo("karuta6") == 0)
		{
			ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			if(imageViewKaruta6 != null)
			{
				imageViewKaruta6.setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
				this.fazerImageViewFicarEscuro(imageViewKaruta6); //mudei a figura da carta
	    		TextView textViewKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
	    		textViewKaruta6.setText("");
	    		
	    		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
	    		KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(5);
	    		fazerTextoVersoDaCartaAparecer(kanji, 5);
			}
		}
		else if(karutaNaoClicavel.compareTo("karuta7") == 0)
		{
			ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			
			if(imageViewKaruta7 != null)
			{
				imageViewKaruta7.setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
				this.fazerImageViewFicarEscuro(imageViewKaruta7); //mudei a figura da carta
	    		TextView textViewKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
	    		textViewKaruta7.setText("");
	    		
	    		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
	    		KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(6);
	    		fazerTextoVersoDaCartaAparecer(kanji, 6);
			}
		}
		else if(karutaNaoClicavel.compareTo("karuta8") == 0)
		{
			ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			if(imageViewKaruta8 != null)
			{
				findViewById(R.id.karuta8).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
				this.fazerImageViewFicarEscuro(imageViewKaruta8); //mudei a figura da carta
	    		TextView textViewKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
	    		textViewKaruta8.setText("");
	    		
	    		//quando um usuario acerta uma carta, seu verso deve ficar com a traducao em hiragana dela e seu kanji
	    		KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(7);
	    		fazerTextoVersoDaCartaAparecer(kanji, 7);
			}
		}
		
		//a cada X cartas que ja se foram do jogo, um item eh aleatoriamente gerado para cada um dos jogadores		
		//se o adversario usou o item 2x, ele deve ganhar o dobro dos pontos
		String usou2xEmString = cartaNaoClicavelEUsou2x[1].replace("usou2x=", "");
		boolean usou2x = Boolean.valueOf(usou2xEmString);
		String dificuldadeEmString = cartaNaoClicavelEUsou2x[2].replace("dificuldade=", "");
		int dificuldade = Integer.valueOf(dificuldadeEmString);
		
		if(dificuldade == 1)
		{
			if(usou2x == false)
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 1;
			}
			else
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 2;
			}
		}
		else if(dificuldade == 2)
		{
			if(usou2x == false)
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 2;
			}
			else
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 4;
			}
		}
		else
		{
			if(usou2x == false)
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 3;
			}
			else
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 6;
			}
		}
		
		TextView textoPontuacaoAdversario = (TextView) findViewById(R.id.nome_e_pontuacao_jogador_direita);
		String palavraPontuacao = getResources().getString(R.string.pontuacao);
		String nomeAdversarioTexto= "";
		
		if(this.nomeAdversario.length() > 12)
		{
			nomeAdversarioTexto = nomeAdversario.substring(0, 8) + "...";
		}
		else
		{
			nomeAdversarioTexto = this.nomeAdversario;
		}
		
		
		if(pontuacaoDoAdversario < 10)
		{
			textoPontuacaoAdversario.setText(nomeAdversarioTexto + ":" + "0" + String.valueOf(pontuacaoDoAdversario));
		}
		else
		{
			textoPontuacaoAdversario.setText(nomeAdversarioTexto + ":" + String.valueOf(pontuacaoDoAdversario));
		}
		
		
		this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
		//this.verificarSeJogadorRecebeUmItemAleatorio();
	}
	else if(mensagem.contains("naoClicavelVoceTbAcertou=") == true && jogoAcabou == false && duranteAcertoDeCarta == false)
	{
		//no caso de dois jogadores clicando ao mesmo tempo na carta, nao precisamos aumentar a quantidade de cartas que sairam do jogo, apenas mexer com a pontuacao do adversario
		String[] cartaNaoClicavelEUsou2xEDificuldade = mensagem.split(";");
		String usou2xEmString = cartaNaoClicavelEUsou2xEDificuldade[1].replace("usou2x=", "");
		boolean usou2x = Boolean.valueOf(usou2xEmString);
		String dificuldadeEmString = cartaNaoClicavelEUsou2xEDificuldade[2].replace("dificuldade=", "");
		int dificuldade = Integer.valueOf(dificuldadeEmString);
		
		if(dificuldade == 1)
		{
			if(usou2x == false)
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 1;
			}
			else
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 2;
			}
		}
		else if(dificuldade == 2)
		{
			if(usou2x == false)
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 2;
			}
			else
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 4;
			}
		}
		else
		{
			if(usou2x == false)
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 3;
			}
			else
			{
				this.pontuacaoDoAdversario = this.pontuacaoDoAdversario + 6;
			}
		}
		
		TextView textoPontuacaoAdversario = (TextView) findViewById(R.id.nome_e_pontuacao_jogador_direita);
		String palavraPontuacao = getResources().getString(R.string.pontuacao);
		String nomeAdversarioTexto= "";
		
		if(this.nomeAdversario.length() > 12)
		{
			nomeAdversarioTexto = nomeAdversario.substring(0, 8) + "...";
		}
		else
		{
			nomeAdversarioTexto = this.nomeAdversario;
		}
		
		
		if(pontuacaoDoAdversario < 10)
		{
			textoPontuacaoAdversario.setText(nomeAdversarioTexto + ":" + "0" + String.valueOf(pontuacaoDoAdversario));
		}
		else
		{
			textoPontuacaoAdversario.setText(nomeAdversarioTexto + ":" + String.valueOf(pontuacaoDoAdversario));
		}
	}
	else if(mensagem.contains("mandar dados da partida para singleton") == true)
	{
		//o adversario pediu para o jogador armazenar os dados da partida no singleton(categorias escolhidas e quantas rodadas)
		
		//como eh o comeco da primeira partida do jogo, vamos fazer o usuario ver uma tela de espera pelo menos ate o kanji da dica ser escolhido
		this.comecarEsperaDoUsuarioParaComecoDaPartida();
		
		SingletonGuardaDadosDaPartida.getInstance().setQuantasRodadasHaverao(this.quantasRodadasHaverao);
		
		ArrayList<CategoriaDeKanjiParaListviewSelecionavel> categoriaDeKanjiList = this.dataAdapter.getCategoriaDeKanjiList();
		
		 ArmazenaKanjisPorCategoria conheceKanjisECategorias = ArmazenaKanjisPorCategoria.pegarInstancia();
		 SingletonArmazenaCategoriasDoJogo conheceCategorias = SingletonArmazenaCategoriasDoJogo.getInstance();	
		 for(int i = 0; i < categoriaDeKanjiList.size(); i++)
			{
				CategoriaDeKanjiParaListviewSelecionavel umaCategoria = categoriaDeKanjiList.get(i);
				if(umaCategoria.isSelected() == true)
				{
					String nomeCategoria = umaCategoria.getName();
					int posicaoParenteses = nomeCategoria.indexOf("(");
					String nomeCategoriaSemParenteses = nomeCategoria.substring(0, posicaoParenteses);
					int id_categoria = conheceCategorias.pegarIdDaCategoria(nomeCategoriaSemParenteses);
					LinkedList<KanjiTreinar> kanjisDaCategoria = 
							conheceKanjisECategorias.getListaKanjisTreinar(id_categoria);
					SingletonGuardaDadosDaPartida.getInstance().adicionarNovaCategoriaESeusKanjis(nomeCategoriaSemParenteses, kanjisDaCategoria);
					
				}
			}	
			
		//vamos mudar a tela
		switchToScreen(R.id.tela_jogo_multiplayer);
		this.comecarJogoMultiplayer();
		
		//falta avisar ao jogador que escolhe as categorias p sortear os kanjis
		String mensagemParaODono = "pode comecar a escolher os kanjis";
		
		this.mandarMensagemMultiplayer(mensagemParaODono);
		
		this.pegarTodosOsKanjisQuePodemVirarCartas(); //ele tb deve atualizar a lista com todos os kanjis q podem virar cartas. senao quando um turno passar, o jogador q n escolhe as categorias n tem como gerar 8 kanjis
		
		this.salaCriadaEhParaRevanche = false; //acabou de comecar uma partida de verdade? entao nao eh mais revanche, senao os usuarios nao poderiam voltar ao menu inicial
	}
	else if(mensagem.contains("pode comecar a escolher os kanjis") == true)
	{
		//mensagem enviada do jogador que nao escolhe a categoria para o que escolhe. Eh para que o jogador que escolhe a categoria tambem escolha os kanjis
		//essa mensagem so ocorre uma vez que eh no comeco da primeira partida do jogo
		
		this.pegarTodosOsKanjisQuePodemVirarCartas();
		this.escolher8KanjisParaARodada();
	}
	else if(mensagem.contains("kanjis=") == true && mensagem.contains("item misturarcartas kanjis=") == false && jogoAcabou == false)
	{
		//mensagem de quem escolhe categorias p/ dizer ao adversario quais os kanjis que estao na tela dele
		//tb eh enviado as posicoes dos ultimos kanjis de kanjisquepodemvirarcartas que precisam ser removidas
		//essas duas infos sao separadas por um @
		//ex: kanjis=au|cotidiano;me|corpo...@1;2;...
		//No final desse if, eh gerado um kanji da dica tb.
		String[] kanjisEPosicoesQuePodemVirarCartasRemovidas = mensagem.split("@");
		String kanjis =kanjisEPosicoesQuePodemVirarCartasRemovidas[0].replace("kanjis=", "");
		String[] kanjisSeparadosPorPontoEVirgula = kanjis.split(";");
		
		
		//tenho de achar cada KanjiTreinar com base na mensagem mandada. Felizmente ja tenho categoria e texto do kanji
		
		if(this.kanjisDasCartasNaTela == null)
		{
			this.kanjisDasCartasNaTela = new LinkedList<KanjiTreinar>();
		}
		if(this.kanjisDasCartasNaTelaQueJaSeTornaramDicas == null)
		{
			this.kanjisDasCartasNaTelaQueJaSeTornaramDicas = new LinkedList<KanjiTreinar>();
		}
		
		this.kanjisDasCartasNaTela.clear(); //se foi de uma rodada para outra, eh bom limpar essa lista
		this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.clear(); //essa tb
		
		for(int i = 0; i < kanjisSeparadosPorPontoEVirgula.length; i++)
		{
			String umKanjiECategoria = kanjisSeparadosPorPontoEVirgula[i];
			String[] kanjiECategoria = umKanjiECategoria.split("\\|");
			
			if(kanjiECategoria.length > 1)
			{
				//tem um kanji e uma categoria p serem obtidos
				String kanji = kanjiECategoria[0];
				String categoria = kanjiECategoria[1];
				
				int id_categoria = Integer.valueOf(categoria);
				KanjiTreinar umKanjiTreinar = 
						ArmazenaKanjisPorCategoria.pegarInstancia().acharKanji(id_categoria, kanji);
				
				this.kanjisDasCartasNaTela.add(umKanjiTreinar);
			}
		}
		
		this.mudarCartasNaTela(kanjisSeparadosPorPontoEVirgula);
		//serah que foram geradas 8 cartas na tela ou menos?
		
		
		tornarORestoDasCartasNaTelaVazias();
		
		//o jogador que nao escolheu a categoria e nem os 8 kanjis da tela eh o unico que pode criar a dica do kanji
		this.gerarKanjiDaDica();
		
		//apos gerar o kanji da dica e visto que as 8 cartas ja estao na tela, pode-se dispensar o laoding inicial que diz p o usuario que o jogo estah sendo iniciado
		if(this.rodadaAtual == 1)
		{
			this.loadingComecoDaPartida.dismiss();
		}
		
		String posicoesKanjisPodemVirarCartasASeremRemovidas = kanjisEPosicoesQuePodemVirarCartasRemovidas[1];
		String[] posicoesArray = posicoesKanjisPodemVirarCartasASeremRemovidas.split(";");
		
		for(int j = 0; j < posicoesArray.length; j++)
		{
			int umaPosicao = Integer.valueOf(posicoesArray[j]);
			this.kanjisQuePodemVirarCartas.remove(umaPosicao);
		}
		
	}
	else if(mensagem.contains("kanjiDaDica=") == true && mensagem.contains("item") == false)
	{
		//alguem acertou algum kanji(ou eh o comeco de tudo) e eh necessairo mudar a dica do kanji
		//e nao eh o item p mudar a dica atual
		//formato: kanjiDaDica=asa|1
		
		//se existir alguma carta dourada na tela(item cartasdouradas), ela deve voltar ao normal. No entanto,
		//se ela for a carta que acabou de deixar de ser a dica, ela vira verso. Por isso, vamos fazer as cartas
		//douradas voltarem ao normal ANTES de mudar kanjidadica
		fazerCartasDouradasVoltaremAoNormal();
				
		String kanjiECategoria = mensagem.replace("kanjiDaDica=", "");
		String[] kanjiECategoriaArray = kanjiECategoria.split("\\|");
		String kanji = kanjiECategoriaArray[0];
		String categoria = kanjiECategoriaArray[1];
		
		int id_categoria = Integer.valueOf(categoria);
		
		this.kanjiDaDica = ArmazenaKanjisPorCategoria.pegarInstancia().acharKanji(id_categoria, kanji);
		this.palavrasJogadas.add(kanjiDaDica);
		this.alterarTextoDicaComBaseNoKanjiDaDica();
		this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.add(this.kanjiDaDica);
		
		//caso essa seja a primeira rodada e ja foi gerado um kanji da dica inicial, pode-se dispensar o loading que diz que o jogo estah sendo iniciado 
		if(this.rodadaAtual == 1 && this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size() == 1 && loadingComecoDaPartida.isShowing() == true)
		{
			this.loadingComecoDaPartida.dismiss();
		}
		
		//assim que a dica muda, se existir alguma carta proibida na tela, ela desaparece(item naoesperemais)
		fazerCartaProibidaVoltarAoNormal();
	}
	else if(mensagem.contains("rodadaMudou") == true)
	{
		this.rodadaAtual = this.rodadaAtual + 1;
		
		String textoRodada = getResources().getString(R.string.rodada_sem_dois_pontos);
		TextView textViewRodada = (TextView) findViewById(R.id.rodada);
		textViewRodada.setText(textoRodada + " " + String.valueOf(this.rodadaAtual));
		
		this.tornarTodasAsCartasNaTelaClicaveisEVaziasNovamente();
	}
	else if(mensagem.contains("gerarMais8Cartas") == true)
	{
		/*somente quem escolhe as categorias recebe essa mensagem q significa que ele deve gerar novas 8 
		 *cartas p a rodada. A rodada acabou de mudar*/
		this.escolher8KanjisParaARodada();
	}
	else if(mensagem.contains("item trovaotiracartaaleatoria indiceCartaRemovida=") == true)
	{
		//o adversario lancou um trovaotiracartaaleatoria e o trovao deve cair em quem recebeu esta mensagem tb
		fazerPlaquinhaAdversarioBalancarUmPouco();
		String indiceCartaRemovidaEmString = mensagem.replace("item trovaotiracartaaleatoria indiceCartaRemovida=", "");
		int indiceCartaRemovida = Integer.valueOf(indiceCartaRemovidaEmString);
		this.lancarTrovaoNaTelaNaCartaDeIndice(indiceCartaRemovida,false);
	}
	else if(mensagem.contains("item trovaotiracarta indiceCartaRemovida=") == true)
	{
		//o adversario lancou um trovaotiracarta e o trovao deve cair em quem recebeu esta mensagem tb
		fazerPlaquinhaAdversarioBalancarUmPouco();
		String indiceCartaRemovidaEmString = mensagem.replace("item trovaotiracarta indiceCartaRemovida=", "");
		int indiceCartaRemovida = Integer.valueOf(indiceCartaRemovidaEmString);
		this.lancarTrovaoNaTelaNaCartaDeIndice(indiceCartaRemovida,false);
	}
	else if(mensagem.contains("item parartempo") == true)
	{
		fazerPlaquinhaAdversarioBalancarUmPouco();
		this.realizarProcedimentoPararTempo(false);
	}
	else if(mensagem.contains("item misturarcartas kanjis=") == true)
	{
		/*os kanjis chegam assim: kanjis=au|cotidiano;kau|cotidiano...*/
		fazerPlaquinhaAdversarioBalancarUmPouco();
		String mensagemKanjis = mensagem.replace("item misturarcartas kanjis=", "");
		String[] kanjisECategorias = mensagemKanjis.split(";");
		
		LinkedList<String> textoKanjisNovos = new LinkedList<String>();
		LinkedList<String> categoriasKanjisNovos = new LinkedList<String>();
		
		for(int i = 0; i < kanjisECategorias.length; i++)
		{
			String umKanjiECategoria = kanjisECategorias[i];
			String[] kanjiECategoriaArray = umKanjiECategoria.split("\\|");
			textoKanjisNovos.add(kanjiECategoriaArray[0]);
			
			String nome_categoria = SingletonArmazenaCategoriasDoJogo.getInstance().pegarCategoriaPorId(kanjiECategoriaArray[1]);
			categoriasKanjisNovos.add(nome_categoria);
			
		}
		
		this.misturarCartasRecebeuCartasOutroUsuario(textoKanjisNovos, categoriasKanjisNovos);
	}
	else if(mensagem.contains("item mudardica kanjiDaDica=") == true)
	{
		fazerPlaquinhaAdversarioBalancarUmPouco();
		this.tirarKanjiDicaAtualDeCartasQueJaViraramDicasEPalavrasJogadas();
		
		String kanjiECategoria = mensagem.replace("item mudardica kanjiDaDica=", "");
		String[] kanjiECategoriaArray = kanjiECategoria.split("\\|");
		String kanji = kanjiECategoriaArray[0];
		String categoria = kanjiECategoriaArray[1];
		
		
		int id_categoria = Integer.valueOf(categoria);
		this.kanjiDaDica = ArmazenaKanjisPorCategoria.pegarInstancia().acharKanji(id_categoria, kanji);
		this.palavrasJogadas.add(kanjiDaDica);
		this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.add(this.kanjiDaDica);
		this.realizarProcedimentoMudandoDicaAtual(false);
	}
	else if(mensagem.contains("item revivercarta numeroCarta=") == true)
	{
		//adversario reviveu uma carta
		fazerPlaquinhaAdversarioBalancarUmPouco();
		String stringNumeroCartaRevivida = mensagem.replace("item revivercarta numeroCarta=", "");
		int numeroCartaRevivida = Integer.valueOf(stringNumeroCartaRevivida);
		this.realizarProcedimentoReviverCarta(numeroCartaRevivida,true);
	}
	else if(mensagem.contains("termineiDeCarregarListaDeCategoria;") == true)
	{
		//guest manda pro host que jah terminou de carregar lista de categorias
		this.guestTerminouDeCarregarListaDeCategorias = true;
	}
	else if(mensagem.contains("fim de jogo") == true)
	{
		//algum jogador alcancou o fim de jogo e o outro tb deve fazer o mesmo
		switchToScreen(R.id.tela_fim_de_jogo);
		this.jogoAcabou = true;
		comecarFimDeJogo();
	}
	else if(mensagem.contains("oponente falou no chat="))
	{
		String mensagemAdicionarAoChat = mensagem.replaceFirst("oponente falou no chat=", "");
		this.adicionarMensagemNoChat(mensagemAdicionarAoChat, false,true);
	}
	else if(mensagem.contains("nome=") == true)
	{
		this.nomeAdversario = mensagem.replace("nome=", "");
		this.comecarJogoMultiplayer(); //so eh possivel iniciar os componentes da tela do jogo multiplayer se o email do adversario tiver sido enviado
		
		this.salaCriadaEhParaRevanche = false; //acabou de comecar uma partida de verdade? entao nao eh mais revanche, senao os usuarios nao poderiam voltar ao menu inicial
	}
	else if(mensagem.contains("id_sala_revanche=") == true)
	{
		//alguem criou uma sala para revanche e acabou de enviar um id para uma pessoa que estava esperando
		this.loadingEsperaRevanche.dismiss();
		int id_nova_sala = Integer.valueOf(mensagem.replace("id_sala_revanche=", ""));
		this.salaAtual.setId_sala(id_nova_sala);
		this.salaCriadaEhParaRevanche = true;
		this.leaveRoom();
		this.jaDeixouASala = false; //pq agora ele vai entrar em outra sala!!!
		this.entrarNaSala(this.salaAtual);
		revancheEstahAcontecendo = true;
	}
	else if(mensagem.contains("quero jogar uma revanche") == true)
	{
		//alguem quer jogar uma revanche. Tem de ver se o usuario quer tambem ou nao
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		            //Yes button clicked
		        	mandarMensagemMultiplayer("tambem quero jogar revanche");
		        	salaCriadaEhParaRevanche = true;
		        	realizarRevanche();
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            //No button clicked
		        	mandarMensagemMultiplayer("mas eu nao quero jogar revanche");
		        	salaCriadaEhParaRevanche = false;
		            break;
		        }
		    }
		};

		String mensagemAdversarioQuerJogarRevanche = getResources().getString(R.string.mensagem_adversario_quer_revanche);
		String sim = getResources().getString(R.string.sim);
		String nao = getResources().getString(R.string.nao);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemAdversarioQuerJogarRevanche).setPositiveButton(sim, dialogClickListener)
		    .setNegativeButton(nao, dialogClickListener).show();
	}
	else if(mensagem.contains("mas eu nao quero jogar revanche") == true)
	{
		//um usuario que recebeu um convite para jogar revanche nao quis jogar a revanche. agora vc recebe uma mensagem num toast dizendo isso
		this.loadingEsperaRevanche.dismiss();
		this.salaCriadaEhParaRevanche = false;
		String s = getResources().getString(R.string.adversario_nao_quer_revanche);
		Toast t = Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG);
		t.show();
	}
	else if(mensagem.contains("tambem quero jogar revanche") == true)
	{
		//o usuario que mandou um convite para uma revanche acabou de saber que seu adversario tb quer a revanche
		this.loadingEsperaRevanche.dismiss();
		this.salaCriadaEhParaRevanche = true;
		this.realizarRevanche();
	}
	
	fazerCartasFicaremClicaveis();

}

	//se o adversario usou um item, a plaquinha com o nome dele deve balancar por um pouco
	private void fazerPlaquinhaAdversarioBalancarUmPouco()
	{
		RelativeLayout label_nome_e_pontuacao_jogador_direita =
				(RelativeLayout) findViewById(R.id.label_nome_e_pontuacao_jogador_direita);
		new ShakeAnimation(label_nome_e_pontuacao_jogador_direita).animate();
		
		ImageView foto_jogador_direita_competicao = (ImageView) findViewById(R.id.foto_jogador_direita);
		new ShakeAnimation(foto_jogador_direita_competicao).animate();
		
	}
	
	//se o usuario usou um item, a plaquinha com o nome dele deve balancar por um pouco
		private void fazerPlaquinhaUsuarioBalancarUmPouco()
		{
			RelativeLayout label_nome_e_pontuacao_jogador_esquerda =
					(RelativeLayout) findViewById(R.id.label_nome_e_pontuacao_jogador_esquerda);
			new ShakeAnimation(label_nome_e_pontuacao_jogador_esquerda).animate();
			
			ImageView foto_jogador_esquerda_competicao = (ImageView) findViewById(R.id.foto_jogador_esquerda);
			new ShakeAnimation(foto_jogador_esquerda_competicao).animate();
			
		}

private void fazerCartasFicaremNaoClicaveis()
{
	ImageView imageViewKaruta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	ImageView imageViewKaruta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	ImageView imageViewKaruta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	ImageView imageViewKaruta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	
	if(imageViewKaruta1 != null)
	{
		imageViewKaruta1.setOnClickListener(null);
	}
	if(imageViewKaruta2 != null)
	{
		imageViewKaruta2.setOnClickListener(null);
	}
	if(imageViewKaruta3 != null)
	{
		imageViewKaruta3.setOnClickListener(null);
	}
	if(imageViewKaruta4 != null)
	{
		imageViewKaruta4.setOnClickListener(null);
	}
	if(imageViewKaruta5 != null)
	{
		imageViewKaruta5.setOnClickListener(null);
	}
	if(imageViewKaruta6 != null)
	{
		imageViewKaruta6.setOnClickListener(null);
	}
	if(imageViewKaruta7 != null)
	{
		imageViewKaruta7.setOnClickListener(null);
	}
	if(imageViewKaruta8 != null)
	{
		imageViewKaruta8.setOnClickListener(null);
	}
}

private void fazerCartasFicaremClicaveis()
{
	ImageView imageViewKaruta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	ImageView imageViewKaruta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	ImageView imageViewKaruta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	ImageView imageViewKaruta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	
	if(imageViewKaruta1 != null)
	{
		imageViewKaruta1.setOnClickListener(this);
	}
	if(imageViewKaruta2 != null)
	{
		imageViewKaruta2.setOnClickListener(this);
	}
	if(imageViewKaruta3 != null)
	{
		imageViewKaruta3.setOnClickListener(this);
	}
	if(imageViewKaruta4 != null)
	{
		imageViewKaruta4.setOnClickListener(this);
	}
	if(imageViewKaruta5 != null)
	{
		imageViewKaruta5.setOnClickListener(this);
	}
	if(imageViewKaruta6 != null)
	{
		imageViewKaruta6.setOnClickListener(this);
	}
	if(imageViewKaruta7 != null)
	{
		imageViewKaruta7.setOnClickListener(this);
	}
	if(imageViewKaruta8 != null)
	{
		imageViewKaruta8.setOnClickListener(this);
	}
}


/*NOVO DA ACTIVITY REFERENTE A SELECIONAR CATEGORIAS */
private MyCustomAdapter dataAdapter = null;
private ProgressDialog loadingKanjisDoBd;
private static String jlptEnsinarNaFerramenta = "4";

private void solicitarPorKanjisPraTreino() {
	this.loadingKanjisDoBd = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_kanjis_remotamente), getResources().getString(R.string.por_favor_aguarde));
	  SolicitaKanjisParaTreinoTask armazenarMinhasFotos = new SolicitaKanjisParaTreinoTask(this.loadingKanjisDoBd, this);
	  armazenarMinhasFotos.execute("");
	 
}
  
 public void procedimentoAposCarregarKanjis() 
 {
	 
	 if(this.naCriacaoDeSalaModoCasual == true)
	 {
		 //esta na criacao de uma sala? Entao vamos disponibilizar esses kanjis carregados numa listview
		 switchToScreen(R.id.tela_escolha_categoria_criar_nova_sala);
		 
		//Array list of countries
		 ArmazenaTudoParaJogoOffline armazenaPalavrasMemoriaInterna =
					ArmazenaTudoParaJogoOffline.getInstance();
		 
		 String idioma_da_lista_de_palavras = "";
			Resources res = getResources();
	        Locale myLocale = res.getConfiguration().locale;
			if(myLocale != null)
			{
				String language = myLocale.getLanguage();
				if(myLocale.getLanguage().compareTo("en") == 0)
			    {
			    	idioma_da_lista_de_palavras = "ingles";
			    }
				else if(myLocale.getLanguage().compareTo("jp") == 0)
			    {
			    	idioma_da_lista_de_palavras = "japones";
			    }
			    else if(myLocale.getLanguage().compareTo("pt") == 0)// br
			    {
			    	idioma_da_lista_de_palavras = "portugues";
			    }
			    else
			    {
			    	idioma_da_lista_de_palavras = "ingles";
			    }
			}
			else
			{
				idioma_da_lista_de_palavras = "ingles";
			}
		 armazenaPalavrasMemoriaInterna.salvarListaDePalavrasParaUsoFuturo(this,idioma_da_lista_de_palavras);

		 ArrayList<CategoriaDeKanjiParaListviewSelecionavel> listaDeCategorias = new ArrayList<CategoriaDeKanjiParaListviewSelecionavel>();

		 LinkedList<String> categoriasDosKanjis = 
				 SingletonArmazenaCategoriasDoJogo.getInstance().getCategoriasArmazenadas();

		 this.mostrarCategoriasParaOUsuario(categoriasDosKanjis);
		 
		 boolean possoEscolherCategorias = true;
	 }
	 else if(this.noPopupQueBuscaSalasPorCategoria == true)
	 {

		 LinkedList<String> categoriasDosKanjis = 
				 SingletonArmazenaCategoriasDoJogo.getInstance().getCategoriasArmazenadas();
		 
		 this.mostrarPopupPesquisarPorCategorias(categoriasDosKanjis);
	 }
	 else
	 {
		 //AQUI SO OCORRERAH APOS O NAO HOST PEDIR OS KANJIS DO BD NA FUNCAO startgame() e a task terminar de pegar os kanjis do bd
		 String categorias = this.salaAtual.getCategoriasJuntas();
			
			String[] stringSeparada = categorias.split(",");
			
			ArmazenaKanjisPorCategoria conheceKanjisECategorias = ArmazenaKanjisPorCategoria.pegarInstancia();
			SingletonArmazenaCategoriasDoJogo conheceCategorias = SingletonArmazenaCategoriasDoJogo.getInstance();
			for(int i = 0; i < stringSeparada.length; i++)
			{
				String umaCategoria = stringSeparada[i];
				int id_categoria = conheceCategorias.pegarIdDaCategoria(umaCategoria);
				LinkedList<KanjiTreinar> kanjisDaCategoria = 
						conheceKanjisECategorias.getListaKanjisTreinar(id_categoria);
				SingletonGuardaDadosDaPartida.getInstance().adicionarNovaCategoriaESeusKanjis(umaCategoria, kanjisDaCategoria);
			}
			
			//falta avisar ao jogador que escolhe as categorias p sortear os kanjis
			String mensagemParaODono = "pode comecar a escolher os kanjis";
					
			this.mandarMensagemMultiplayer(mensagemParaODono);
					
			this.pegarTodosOsKanjisQuePodemVirarCartas(); //ele tb deve atualizar a lista com todos os kanjis q podem virar cartas. senao quando um turno passar, o jogador q n escolhe as categorias n tem como gerar 8 kanjis
	 }
 }
 
 private void mostrarCategoriasParaOUsuario(final LinkedList<String> categorias)
 {
		int tamanhoLista1 = categorias.size()/2;
		final String[] arrayCategorias = new String[tamanhoLista1];
		final String[] arrayCategorias2 = new String[categorias.size() - tamanhoLista1];
		int iteradorCategorias1 = 0;
		int iteradorCategorias2 = 0;
		
		for(int i = 0; i < categorias.size(); i++)
		{
			String umaCategoria = categorias.get(i);
			if(iteradorCategorias1 < arrayCategorias.length)
			{
				arrayCategorias[iteradorCategorias1] = umaCategoria;
				iteradorCategorias1 = iteradorCategorias1 + 1;
			}
			else
			{
				arrayCategorias2[iteradorCategorias2] = umaCategoria;
				iteradorCategorias2 = iteradorCategorias2 + 1;
			}
		}
		
		Integer[] imageId = new Integer[arrayCategorias.length];
		Integer[] imageId2 = new Integer[arrayCategorias2.length];
		
		for(int j = 0; j < arrayCategorias.length; j++)
		{
			String umaCategoria = arrayCategorias[j];
			int idImagem = RetornaIconeDaCategoriaParaTelasDeEscolha.retornarIdIconeDaCategoria(umaCategoria,this);
			imageId[j] = idImagem;
		}
		for(int k = 0; k < arrayCategorias2.length; k++)
		{
			String umaCategoria = arrayCategorias2[k];
			int idImagem = RetornaIconeDaCategoriaParaTelasDeEscolha.retornarIdIconeDaCategoria(umaCategoria,this);
			imageId2[k] = idImagem;
		}

	    
	    final boolean[] categoriaEstahSelecionada = new boolean[arrayCategorias.length];
	    final boolean[] categoriaEstahSelecionada2 = new boolean[arrayCategorias.length];
		for(int l = 0; l < arrayCategorias.length; l++)
		{
			categoriaEstahSelecionada[l] = false;
		}
		for(int m = 0; m < arrayCategorias2.length; m++)
		{
			categoriaEstahSelecionada2[m] = false;
		}
		
		//devo colocar em baixo do nome da categoria, quantas palavras ela tem
		final String[] arrayCategoriasComQuantasPalavras = new String[arrayCategorias.length];
		final String[] arrayCategoriasComQuantasPalavras2 = new String[arrayCategorias2.length];
		
		SingletonArmazenaCategoriasDoJogo conheceCategorias = SingletonArmazenaCategoriasDoJogo.getInstance();
		for(int a = 0; a < arrayCategorias.length; a++)
		{
			String umaCategoria = arrayCategorias[a];
			int id_categoria = conheceCategorias.pegarIdDaCategoria(umaCategoria);
			int quantasPalavrasTemACategoria = 
					ArmazenaKanjisPorCategoria.pegarInstancia().quantasPalavrasTemACategoria(id_categoria);
			String textoDaCategoria = umaCategoria + " (" + String.valueOf(quantasPalavrasTemACategoria) + ")";
			String categoriaEscritaEmKanji = RetornaNomeCategoriaEscritaEmKanji.retornarNomeCategoriaEscritaEmKanji(umaCategoria,this);
			textoDaCategoria = categoriaEscritaEmKanji + "\n" + textoDaCategoria;
			arrayCategoriasComQuantasPalavras[a] = textoDaCategoria;
		}
		for(int b = 0; b < arrayCategorias2.length; b++)
		{
			String umaCategoria = arrayCategorias2[b];
			int id_categoria = conheceCategorias.pegarIdDaCategoria(umaCategoria);
			int quantasPalavrasTemACategoria = 
					ArmazenaKanjisPorCategoria.pegarInstancia().quantasPalavrasTemACategoria(id_categoria);
			String textoDaCategoria = umaCategoria + " (" + String.valueOf(quantasPalavrasTemACategoria) + ")";
			String categoriaEscritaEmKanji = RetornaNomeCategoriaEscritaEmKanji.retornarNomeCategoriaEscritaEmKanji(umaCategoria,this);
			textoDaCategoria = categoriaEscritaEmKanji + "\n" + textoDaCategoria;
			arrayCategoriasComQuantasPalavras2[b] = textoDaCategoria;
		}
		
		
		//definindo fontes para os textos dessa tela...
		this.mudarFonteEscolhaAsCategoriasDaSala();
		Typeface typeFaceFonteTextoListViewIconeETexto = this.escolherFonteDoTextoListViewIconeETexto();
		
		AdapterListViewIconeETexto adapter = new AdapterListViewIconeETexto(ModoCasual.this, arrayCategoriasComQuantasPalavras, imageId,typeFaceFonteTextoListViewIconeETexto,true,true,false);
		    adapter.setLayoutUsadoParaTextoEImagem(R.layout.list_item_icone_e_texto_menor);
			ListView list=(ListView) findViewById(R.id.listaCategoriasCriarSala1);
		    
		        list.setAdapter(adapter);
		        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		                @Override
		                public void onItemClick(AdapterView<?> parent, View view,
		                                        int position, long id) 
		                {
		                    if(categoriaEstahSelecionada[position] == false)
		                    {
		                    	categoriaEstahSelecionada[position] = true;
		                    	ImageView imageView = (ImageView) view.findViewById(R.id.img);
		                    	imageView.setAlpha(255);
		                    	TextView textView = (TextView) view.findViewById(R.id.txt);
		                    	int alpha = 255;
		                    	textView.setTextColor(Color.argb(alpha, 0, 0, 0));
		                    }
		                    else
		                    {
		                    	categoriaEstahSelecionada[position] = false;
		                    	ImageView imageView = (ImageView) view.findViewById(R.id.img);
		                    	imageView.setAlpha(100);
		                    	TextView textView = (TextView) view.findViewById(R.id.txt);
		                    	int alpha = 100;
		                    	textView.setTextColor(Color.argb(alpha, 0, 0, 0));
		                    }
		                }
		            });
		        
		        
		        AdapterListViewIconeETexto adapter2 = new AdapterListViewIconeETexto(ModoCasual.this, arrayCategoriasComQuantasPalavras2, imageId2,typeFaceFonteTextoListViewIconeETexto,true,true,false);
		        adapter2.setLayoutUsadoParaTextoEImagem(R.layout.list_item_icone_e_texto_menor);
		        ListView list2=(ListView)findViewById(R.id.listaCategoriasCriarSala2);
			        list2.setAdapter(adapter2);
			        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			                @Override
			                public void onItemClick(AdapterView<?> parent, View view,
			                                        int position, long id) 
			                {
			                    if(categoriaEstahSelecionada2[position] == false)
			                    {
			                    	categoriaEstahSelecionada2[position] = true;
			                    	ImageView imageView = (ImageView) view.findViewById(R.id.img);
			                    	imageView.setAlpha(255);
			                    	TextView textView = (TextView) view.findViewById(R.id.txt);
			                    	int alpha = 255;
			                    	textView.setTextColor(Color.argb(alpha, 0, 0, 0));
			                    }
			                    else
			                    {
			                    	categoriaEstahSelecionada2[position] = false;
			                    	ImageView imageView = (ImageView) view.findViewById(R.id.img);
			                    	imageView.setAlpha(100);
			                    	TextView textView = (TextView) view.findViewById(R.id.txt);
			                    	int alpha = 100;
			                    	textView.setTextColor(Color.argb(alpha, 0, 0, 0));
			                    }
			                }
			            });

	 
		        
		//falta definir a a��o para o bot�o que continua a criacao de sala
		  Button botaoOk = (Button) findViewById(R.id.confirmar_escolha_categorias_sala_casual);
		  botaoOk.setOnClickListener(new Button.OnClickListener() 
		  {
			  public void onClick(View v) 
		      {
			    	String categoriasSeparadasPorVirgula = "";
			    	for(int n = 0; n < categoriaEstahSelecionada.length; n++)
			    	{
			    		if(categoriaEstahSelecionada[n] == true)
			    		{
			    			//o usuario quer procurar com essa categoria
			    			String umaCategoria = arrayCategorias[n];
			    			
			    			categoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula + umaCategoria + ",";
			    			
			    		}
			    	}
			    	for(int o = 0; o < categoriaEstahSelecionada2.length; o++)
			    	{
			    		if(categoriaEstahSelecionada2[o] == true)
			    		{
			    			//o usuario quer procurar com essa categoria
			    			String umaCategoria = arrayCategorias2[o];
			    			
			    			categoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula + umaCategoria + ",";
			    			
			    		}
			    	}
			    	
			    	
			    	if(categoriasSeparadasPorVirgula.length() > 1)
			    	{
			    		categoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula.substring(0,categoriasSeparadasPorVirgula.length()-1);
			    		
			    		String[] arrayCategoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula.split(",");
			    		
			    
			   			SingletonGuardaDadosDaPartida.getInstance().limparCategoriasEKanjis();
			   			 
			   			 ArmazenaKanjisPorCategoria conheceKanjisECategorias = ArmazenaKanjisPorCategoria.pegarInstancia();
			   			 SingletonArmazenaCategoriasDoJogo conheceCategorias = SingletonArmazenaCategoriasDoJogo.getInstance();
			   			 for(int i = 0; i < arrayCategoriasSeparadasPorVirgula.length; i++)
			   				{
			   					String umaCategoria = arrayCategoriasSeparadasPorVirgula[i];
			   					int id_categoria = conheceCategorias.pegarIdDaCategoria(umaCategoria);
			   					LinkedList<KanjiTreinar> kanjisDaCategoria = 
			   								conheceKanjisECategorias.getListaKanjisTreinar(id_categoria);
			   					SingletonGuardaDadosDaPartida.getInstance().adicionarNovaCategoriaESeusKanjis(umaCategoria, kanjisDaCategoria);
			 
			   				}
			   				
			   				//MUDAR PARA A TELA DE ESCOLHA DE DURACAO DO MODO TREINAMENTO
			   				switchToScreen(R.id.tela_escolha_duracao_criar_nova_sala);
			   				mudarFonteEscolhaDuracaoDaSala();
			   			
			   				
			   		}
			    	else
			   		 {
			   			 String mensagem = getResources().getString(R.string.erroEscolherCategorias);
			   			 Toast t = Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_LONG);
			   			 t.show();
			   		 }
			    }
		  });
	}
 
 public synchronized void mandarMensagemMultiplayer(String mensagem)
 {
	 byte[] mensagemEmBytes = mensagem.getBytes();
	 for (Participant p : mParticipants) 
		{
			if (p.getParticipantId().equals(mMyId))
			{
				continue;
			}
		    else
		    {
		    	Games.RealTimeMultiplayer.sendReliableMessage(getApiClient(),null, mensagemEmBytes, mRoomId,
			            p.getParticipantId());
		    	
		    }
		}
 }
 
 public void onRadioButtonClicked(View view) 
 {
	    // O radioButton esta marcado?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.radioButton1:
	            if (checked)
	                this.quantasRodadasHaverao = 1;
	            break;
	        case R.id.radioButton2:
	            if (checked)
	            	this.quantasRodadasHaverao = 2;
	            break;
	        case R.id.radioButton3:
	            if (checked)
	            	this.quantasRodadasHaverao = 3;
	            break;
	        case R.id.radioButton4:
	            if (checked)
	            	this.quantasRodadasHaverao = 99; //infinitas rodadas
	            break;
	    }
	    
	    /*String quantasRodadasHaveraoString = "quantasRodadasHaverao=" + this.quantasRodadasHaverao;
	    this.mandarMensagemMultiplayer(quantasRodadasHaveraoString);*/
}

 private void quemEscolheCategoriasClicouNoBotaoOk()
 {
	 //primeiro iremos armazenar no singleton todas as categorias escolhidas e kanjis delas
	 ArrayList<CategoriaDeKanjiParaListviewSelecionavel> categoriaDeKanjiList = this.dataAdapter.getCategoriaDeKanjiList();
	
	 if(categoriaDeKanjiList.size() == 0)
	 {
		 String mensagem = getResources().getString(R.string.erroEscolherCategorias);
		 Toast t = Toast.makeText(this, mensagem, Toast.LENGTH_LONG);
		 t.show();
	 }
	 else
	 {
		 SingletonGuardaDadosDaPartida.getInstance().limparCategoriasEKanjis();
		 
		 ArmazenaKanjisPorCategoria conheceKanjisECategorias = ArmazenaKanjisPorCategoria.pegarInstancia();
		SingletonArmazenaCategoriasDoJogo conheceCategorias = SingletonArmazenaCategoriasDoJogo.getInstance();	
		 for(int i = 0; i < categoriaDeKanjiList.size(); i++)
			{
				CategoriaDeKanjiParaListviewSelecionavel umaCategoria = categoriaDeKanjiList.get(i);
				if(umaCategoria.isSelected() == true)
				{
					String nomeCategoria = umaCategoria.getName();
					int posicaoParenteses = nomeCategoria.indexOf("(");
					String nomeCategoriaSemParenteses = nomeCategoria.substring(0, posicaoParenteses);
					int id_categoria = conheceCategorias.pegarIdDaCategoria(nomeCategoriaSemParenteses);
					LinkedList<KanjiTreinar> kanjisDaCategoria = 
							conheceKanjisECategorias.getListaKanjisTreinar(id_categoria);
					SingletonGuardaDadosDaPartida.getInstance().adicionarNovaCategoriaESeusKanjis(nomeCategoriaSemParenteses, kanjisDaCategoria);
					
				}
			}
			
		//Agora vamos armazenar quantas rodadas o jogo terah no singleton
		SingletonGuardaDadosDaPartida.getInstance().setQuantasRodadasHaverao(quantasRodadasHaverao);
		
		//Agora falta alertar ao outro jogador que ele precisa mudar tb, mas nao se preocupe que o MyCustomAdapter ja mantem as categorias selecionadas tb e o quantasRodadasHaverao foi atualizado tb
		String stringAlertarJogador = "mandar dados da partida para singleton";
		this.mandarMensagemMultiplayer(stringAlertarJogador);
		 
		 //por fim, vamos mudar a tela
		 switchToScreen(R.id.tela_jogo_multiplayer);
	 }
	 
	//como eh o comeco da primeira partida do jogo, vamos fazer o usuario ver uma tela de espera pelo menos ate o kanji da dica ser escolhido
	this.comecarEsperaDoUsuarioParaComecoDaPartida();
		
 }
 
 private void comecarJogoMultiplayer()
 {
	 mudarFonteDosKanjis();
	 /*findViewById(R.id.pontuacao).setVisibility(View.VISIBLE);
	 findViewById(R.id.rodada).setVisibility(View.VISIBLE);
	 findViewById(R.id.karuta1).setVisibility(View.VISIBLE);
	 findViewById(R.id.karuta2).setVisibility(View.VISIBLE);
	 findViewById(R.id.karuta3).setVisibility(View.VISIBLE);
	 findViewById(R.id.karuta4).setVisibility(View.VISIBLE);
	 findViewById(R.id.karuta5).setVisibility(View.VISIBLE);
	 findViewById(R.id.karuta6).setVisibility(View.VISIBLE);
	 findViewById(R.id.karuta7).setVisibility(View.VISIBLE);
	 findViewById(R.id.karuta8).setVisibility(View.VISIBLE);
	 findViewById(R.id.texto_karuta1).setVisibility(View.VISIBLE);
	 findViewById(R.id.texto_karuta2).setVisibility(View.VISIBLE);
	 findViewById(R.id.texto_karuta3).setVisibility(View.VISIBLE);
	 findViewById(R.id.texto_karuta4).setVisibility(View.VISIBLE);
	 findViewById(R.id.texto_karuta5).setVisibility(View.VISIBLE);
	 findViewById(R.id.texto_karuta6).setVisibility(View.VISIBLE);
	 findViewById(R.id.texto_karuta7).setVisibility(View.VISIBLE);
	 findViewById(R.id.texto_karuta8).setVisibility(View.VISIBLE);
	 
	 findViewById(R.id.item).setVisibility(View.VISIBLE);
	 findViewById(R.id.tempo).setVisibility(View.VISIBLE);
	 findViewById(R.id.label_item).setVisibility(View.VISIBLE);
	 findViewById(R.id.mascote).setVisibility(View.VISIBLE);
	 findViewById(R.id.dica_kanji).setVisibility(View.VISIBLE);*/
	 findViewById(R.id.parartempopequeno).setVisibility(View.INVISIBLE);
	 findViewById(R.id.doisxpequeno).setVisibility(View.INVISIBLE);
	 findViewById(R.id.naoesperemais).setVisibility(View.INVISIBLE);
	 
	 findViewById(R.id.karuta1_imageview).setOnClickListener(this);
	 findViewById(R.id.karuta2_imageview).setOnClickListener(this);
	 findViewById(R.id.karuta3_imageview).setOnClickListener(this);
	 findViewById(R.id.karuta4_imageview).setOnClickListener(this);
	 findViewById(R.id.karuta5_imageview).setOnClickListener(this);
	 findViewById(R.id.karuta6_imageview).setOnClickListener(this);
	 findViewById(R.id.karuta7_imageview).setOnClickListener(this);
	 findViewById(R.id.karuta8_imageview).setOnClickListener(this);
	 
	 
	 findViewById(R.id.item1).setOnClickListener(this);
	 findViewById(R.id.item1).setClickable(false);
	 findViewById(R.id.item2).setOnClickListener(this);
	 findViewById(R.id.item2).setClickable(false);
	 findViewById(R.id.item3).setOnClickListener(this);
	 findViewById(R.id.item3).setClickable(false);
	 findViewById(R.id.item4).setOnClickListener(this);
	 findViewById(R.id.item4).setClickable(false);
	 findViewById(R.id.item4).setOnClickListener(this);
	 findViewById(R.id.item4).setClickable(false);
	 findViewById(R.id.item5).setOnClickListener(this);
	 findViewById(R.id.item5).setClickable(false);
	 findViewById(R.id.item6).setOnClickListener(this);
	 findViewById(R.id.item6).setClickable(false);
	 findViewById(R.id.item7).setOnClickListener(this);
	 findViewById(R.id.item7).setClickable(false);
	 findViewById(R.id.item8).setOnClickListener(this);
	 findViewById(R.id.item8).setClickable(false);
	 findViewById(R.id.item9).setOnClickListener(this);
	 findViewById(R.id.item9).setClickable(false);
	 
	 TextView textoPontuacao = (TextView) findViewById(R.id.nome_e_pontuacao_jogador_esquerda);
		String palavraPontuacao = getResources().getString(R.string.pontuacao);
		String nomeJogador = "";
		
		if(this.nomeUsuario.length() > 12)
		{
			nomeJogador = nomeUsuario.substring(0, 8) + "...";
		}
		else
		{
			nomeJogador = this.nomeUsuario;
		}
		
		textoPontuacao.setText(nomeJogador + ":" + "00");
		
		TextView textoPontuacaoAdversario = (TextView) findViewById(R.id.nome_e_pontuacao_jogador_direita);
		String nomeAdversarioTexto= "";
		
		if(this.nomeAdversario.length() > 12)
		{
			nomeAdversarioTexto = nomeAdversario.substring(0, 8) + "...";
		}
		else
		{
			nomeAdversarioTexto = this.nomeAdversario;
		}
		
		
		
	 textoPontuacaoAdversario.setText(nomeAdversarioTexto + ":" + "00");
		
		
	 this.rodadaAtual = 1;
	 TextView textViewRodada = (TextView) findViewById(R.id.rodada);
	 String labelRodada = getResources().getString(R.string.rodada_sem_dois_pontos);
	 textViewRodada.setText(labelRodada + " " + "1");
	 
	 this.quantasCartasJaSairamDoJogo = 0;
	 this.suaPontuacao = 0;
	 this.pontuacaoDoAdversario = 0;
	 this.palavrasAcertadas = new LinkedList<KanjiTreinar>();
	 this.palavrasErradas = new LinkedList<KanjiTreinar>();
	 this.palavrasJogadas = new LinkedList<KanjiTreinar>();
	 
	 this.itensAtuais = new LinkedList<String>();
	 this.itensDoGanhador = new LinkedList<String>();
	 this.itensDoPerdedor = new LinkedList<String>();
	 itensDoGanhador.add("trovaotiracartaaleatoria"); //os ultimos testados
	 itensDoGanhador.add("parartempo");
	 itensDoGanhador.add("misturarcartas");
	 itensDoPerdedor.add("mudardica");
	 itensDoGanhador.add("doisx");
	 itensDoPerdedor.add("naoesperemais");
	 itensDoPerdedor.add("cartasdouradas"); //os ultimos testados
	 itensDoPerdedor.add("trovaotiracarta"); 
	 itensDoPerdedor.add("revivecarta");
	 
	 this.usouTrovaoTiraCarta = false;
	 this.usouReviveCarta = false;
	 this.usou2x = false;
	 this.usouNaoEspereMais = false;
	 
	 this.qualCartaEstaProibida = 0; //nenhuma carta esta proibida no comeco do jogo
	 this.quaisCartasEstaoDouradas = new LinkedList<Integer>(); //nenhuma carta estah dourada no comeco
	 
	 TextView textoTempo = (TextView) findViewById(R.id.tempo);
	 String stringTempo = getResources().getString(R.string.tempo_restante);
	 textoTempo.setText(stringTempo + "1:30");
	 tempoEstahParado = false;
	 this.jogoAcabou = false;
	 
	 this.qualImagemEstaSendoUsadaNaAnimacaoDoTempo = 0;
	 
	 /*this.timerTaskDecrementaTempoRestante = new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        TelaInicialMultiplayer.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	if(tempoEstahParado == false)
		            	{
		            		passarUmSegundo();
		            	}
		            }
		        });
		    }
	 };
     new Timer().schedule(this.timerTaskDecrementaTempoRestante, 1000);*/
	 
	 this.quantosSegundosSemReceberItem = 0;
	 this.handlerParaThreadDiminuiTempoDoJogo = new Handler();
	 this.runnableThreadDiminuiTempoNoJogoDeveriaMorrer = false;
	 this.runnableThreadDiminuiTempoNoJogo = new Runnable() {
         @Override
         public void run() 
         {
             if (tempoEstahParado == false)
             {
            	 ModoCasual.this.runOnUiThread(new Runnable() 
 		        {
 		            @Override
 		            public void run() 
 		            {
 		            	if(loadingComecoDaPartida  != null  && loadingComecoDaPartida.isShowing() == false)
 		            	{
 		            		//o tempo so passa apos o loading comeco da partida passar
 		            		passarUmSegundo();
 	 		            	if(tempoRestante <= 0)
 	 		            	{
 	 		            		//o jogo deve acabar
 	 		            		
 	 		            		if(jogoAcabou == false)
 	 		            		{
 	 		            			terminarJogoEEnviarMesagemAoAdversario();
 	 		            		}
 	 		            	}
 	 		            	else if(quantosSegundosSemReceberItem >= 15)
 	 		            	{
 	 		            		verificarSeJogadorRecebeUmItemAleatorio();
 	 		            		quantosSegundosSemReceberItem = 0;
 	 		            	}
 		            	}
 		            }
 		        });
             }
             
             if(runnableThreadDiminuiTempoNoJogoDeveriaMorrer == false)
             {
            	 handlerParaThreadDiminuiTempoDoJogo.postDelayed(this, 1000);  
             }
             
         }
     };
     
     handlerParaThreadDiminuiTempoDoJogo.postDelayed(runnableThreadDiminuiTempoNoJogo, 1000);
	 
     /*final Handler h2 = new Handler();
     h2.postDelayed(new Runnable() {
         @Override
         public void run() 
         {
             if (tempoEstahParado == false)
             {
            	 ModoCasual.this.runOnUiThread(new Runnable() 
 		        {
 		            @Override
 		            public void run() 
 		            {
 		            	if(loadingComecoDaPartida.isShowing() == false)
 		            	{
 		            		//o tempo so passa apos o loading comeco da partida passar
 		            		if(jogoAcabou == false)
	 		            	{
 		            			//mudarImagemAnimacaoTempoCorrendo();
	 		            	}
 		            	}
 		            }
 		        });
             }
             
             h2.postDelayed(this, 100); 
             
         }
     }, 100);*/
     
     //falta iniciar a musica de fundo do jogo
     this.mudarMusicaDeFundo(R.raw.japan_in_spring);
     
     this.duranteAcertoDeCarta = false;
     this.qualCartaAcertada = "";
	 
 }
 
 private void passarUmSegundo()
 {
	 this.tempoRestante = this.tempoRestante - 1;
	 this.quantosSegundosSemReceberItem = this.quantosSegundosSemReceberItem + 1;
	 String tempoParaMostrar = ""; //n irei mostrar 90s, irei mostrar 1:30,1:29,0:30...
	 if(tempoRestante < 60)
	 {
		 if(tempoRestante < 10)
		 {
			 tempoParaMostrar = "0:0" + String.valueOf(tempoRestante);
		 }
		 else
		 {
			 tempoParaMostrar = "0:" + String.valueOf(tempoRestante);
		 }
	 }
	 else
	 {
		 int segundosMenosUmMinuto = this.tempoRestante - 60;
		 if(segundosMenosUmMinuto < 10)
		 {
			 tempoParaMostrar = "1:0" + String.valueOf(segundosMenosUmMinuto);
		 }
		 else
		 {
			 tempoParaMostrar = "1:" + String.valueOf(segundosMenosUmMinuto);
		 }
	 }
	 
	 TextView textoTempo = (TextView) findViewById(R.id.tempo);
	 textoTempo.setText(tempoParaMostrar);
 }
 
 private void mudarImagemAnimacaoTempoCorrendo()
 {
	 TextView textoTempo = (TextView) findViewById(R.id.tempo);
	 this.qualImagemEstaSendoUsadaNaAnimacaoDoTempo = this.qualImagemEstaSendoUsadaNaAnimacaoDoTempo + 1;
	 if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo > 17)
	 {
		 qualImagemEstaSendoUsadaNaAnimacaoDoTempo = 0;
	 }
	 
	 if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 0)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo0);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 1)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo1);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 2)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo2);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 3)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo3);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 4)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo4);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 5)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo5);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 6)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo6);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 7)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo7);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 8)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo8);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 9)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo9);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 10)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo10);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 11)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo11);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 12)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo12);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 13)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo13);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 14)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo14);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 15)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo15);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 16)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo16);
	 }
	 else if(qualImagemEstaSendoUsadaNaAnimacaoDoTempo == 17)
	 {
		 textoTempo.setBackgroundResource(R.drawable.tempo17);
	 }
 }
 
 
 /*alem da funcao abaixo mudar cada uma das cartas na tela e decidir quais os 8 kanjis da rodada serao usados,
  * ela tb manda uma mensagem ao jogador que nao eh quem escolhe a categoria avisando quais kanjis entraram.
  * A funcao so eh executada pelo jogador que eh que escolhe as categorias*/
 private synchronized void escolher8KanjisParaARodada()
 {
	 if(this.criouUmaSala == true)
	 {
		 //quem escolhe a categoria quem vai sortear os kanjis

		 if(kanjisDasCartasNaTela == null)
		 {
			 this.kanjisDasCartasNaTela = new LinkedList<KanjiTreinar>();
		 }
		 
		 if(kanjisDasCartasNaTelaQueJaSeTornaramDicas == null)
		 {
			 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas = new LinkedList<KanjiTreinar>();
		 }
		 
		 if(this.posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartas == null)
		 {
			 this.posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartas = new LinkedList<Integer>();
		 }
		 
		 this.kanjisDasCartasNaTela.clear();
		 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.clear();
		 this.posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartas.clear();
		 
		 for(int i = 0; i < 8; i++)
		 {
			 KanjiTreinar kanjiTreinar = this.escolherUmKanjiParaTreinar();
			 
			 if(kanjiTreinar == null)
			 {
				 //acabaram-se os kanjis que posso usar na tela
				 this.tornarORestoDasCartasNaTelaVazias();
				 break;
				 
			 }
			 else
			 { 
				 this.kanjisDasCartasNaTela.add(kanjiTreinar);
				 
				 if(i == 0)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta1);
					 this.colocarTextoVerticalNaCarta(texto, kanjiTreinar.getKanji());
					 findViewById(R.id.karuta1_imageview).setClickable(true);
					 this.colorirBordaDaCartaDeAcordoComCategoria(0);
					 fazerTextoVersoDaCartaDesaparecer(0);
				 }
				 else if(i == 1)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta2);
					 this.colocarTextoVerticalNaCarta(texto, kanjiTreinar.getKanji());
					 findViewById(R.id.karuta2_imageview).setClickable(true);
					 this.colorirBordaDaCartaDeAcordoComCategoria(1);
					 fazerTextoVersoDaCartaDesaparecer(1);
				 }
				 else if(i == 2)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta3);
					 this.colocarTextoVerticalNaCarta(texto, kanjiTreinar.getKanji());
					 findViewById(R.id.karuta3_imageview).setClickable(true);
					 this.colorirBordaDaCartaDeAcordoComCategoria(2);
					 fazerTextoVersoDaCartaDesaparecer(2);
				 }
				 else if(i == 3)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta4);
					 this.colocarTextoVerticalNaCarta(texto, kanjiTreinar.getKanji());
					 findViewById(R.id.karuta4_imageview).setClickable(true);
					 this.colorirBordaDaCartaDeAcordoComCategoria(3);
					 fazerTextoVersoDaCartaDesaparecer(3);
				 }
				 else if(i == 4)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta5);
					 this.colocarTextoVerticalNaCarta(texto, kanjiTreinar.getKanji());
					 findViewById(R.id.karuta5_imageview).setClickable(true);
					 this.colorirBordaDaCartaDeAcordoComCategoria(4);
					 fazerTextoVersoDaCartaDesaparecer(4);
				 }
				 else if(i == 5)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta6);
					 this.colocarTextoVerticalNaCarta(texto, kanjiTreinar.getKanji());
					 findViewById(R.id.karuta6_imageview).setClickable(true);
					 this.colorirBordaDaCartaDeAcordoComCategoria(5);
					 fazerTextoVersoDaCartaDesaparecer(5);
				 }
				 else if(i == 6)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta7);
					 this.colocarTextoVerticalNaCarta(texto, kanjiTreinar.getKanji());
					 findViewById(R.id.karuta7_imageview).setClickable(true);
					 this.colorirBordaDaCartaDeAcordoComCategoria(6);
					 fazerTextoVersoDaCartaDesaparecer(6);
				 }
				 else if(i == 7)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta8);
					 this.colocarTextoVerticalNaCarta(texto, kanjiTreinar.getKanji());
					 findViewById(R.id.karuta8_imageview).setClickable(true);
					 this.colorirBordaDaCartaDeAcordoComCategoria(7);
					 fazerTextoVersoDaCartaDesaparecer(7);
				 } 
				 
			 }
		 }
		 
		 
		 //falta agora avisar ao outro jogador quais as cartas na tela
		 String kanjisString = "kanjis=";
		 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
		 {
			 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(i);
			 if(i < this.kanjisDasCartasNaTela.size() - 1)
			 {
				 String categoriaAssociada = umKanji.getCategoriaAssociada();
				 int id_categoria_associada = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(categoriaAssociada);
				 kanjisString = kanjisString + umKanji.getKanji() + "|" + String.valueOf(id_categoria_associada) + ";";
			 }
			 else
			 {
				 String categoriaAssociada = umKanji.getCategoriaAssociada();
				 int id_categoria_associada = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(categoriaAssociada);
				 kanjisString = kanjisString + umKanji.getKanji() + "|" + String.valueOf(id_categoria_associada);
			 }
		 }
		 
		 //devemos enviar tambem as posicoes dos kanjis que foram removidos em kanjisQuePodemVirarCartas
		 String posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartasEmString = "";
		 
		 for(int j = 0; j < this.posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartas.size(); j++)
		 {
			 int umaPosicao = this.posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartas.get(j);
			 posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartasEmString =
					 posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartasEmString + String.valueOf(umaPosicao) + ";";
		 }
		 
		 String mensagemParaAdversario = kanjisString + "@" + posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartasEmString;
		 this.mandarMensagemMultiplayer(mensagemParaAdversario);
	 }
 }
 
 /*pode ser que em alguma rodada, nao seja possivel criar 8 kanjis p mostrar na tela. O resto dessas 8 cartas erao cartas vazias*/
 private void tornarORestoDasCartasNaTelaVazias()
 {
	 int quantasCartasNaTela = this.kanjisDasCartasNaTela.size();
	 if(quantasCartasNaTela == 1)
	 {
		 TextView textView2 = (TextView) findViewById(R.id.texto_karuta2);
		 textView2.setText("");
		 ImageView carta2 = (ImageView) findViewById(R.id.karuta2_imageview);
		 carta2.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta2);
		 
		 TextView textView3 = (TextView) findViewById(R.id.texto_karuta3);
		 textView3.setText("");
		 ImageView carta3 = (ImageView) findViewById(R.id.karuta3_imageview);
		 carta3.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta3);
		 
		 TextView textView4 = (TextView) findViewById(R.id.texto_karuta4);
		 textView4.setText("");
		 ImageView carta4 = (ImageView) findViewById(R.id.karuta4_imageview);
		 carta4.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta4);
		 
		 TextView textView5 = (TextView) findViewById(R.id.texto_karuta5);
		 textView5.setText("");
		 ImageView carta5 = (ImageView) findViewById(R.id.karuta5_imageview);
		 carta5.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta5);
		 
		 TextView textView6 = (TextView) findViewById(R.id.texto_karuta6);
		 textView6.setText("");
		 ImageView carta6 = (ImageView) findViewById(R.id.karuta6_imageview);
		 carta6.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta6);
		 
		 TextView textView7 = (TextView) findViewById(R.id.texto_karuta7);
		 textView7.setText("");
		 ImageView carta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		 carta7.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta7);
		 
		 TextView textView8 = (TextView) findViewById(R.id.texto_karuta8);
		 textView8.setText("");
		 ImageView carta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		 carta8.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta8);
	 }
	 else if(quantasCartasNaTela == 2)
	 {
		 TextView textView3 = (TextView) findViewById(R.id.texto_karuta3);
		 textView3.setText("");
		 ImageView carta3 = (ImageView) findViewById(R.id.karuta3_imageview);
		 carta3.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta3);
		 
		 TextView textView4 = (TextView) findViewById(R.id.texto_karuta4);
		 textView4.setText("");
		 ImageView carta4 = (ImageView) findViewById(R.id.karuta4_imageview);
		 carta4.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta4);
		 
		 TextView textView5 = (TextView) findViewById(R.id.texto_karuta5);
		 textView5.setText("");
		 ImageView carta5 = (ImageView) findViewById(R.id.karuta5_imageview);
		 carta5.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta5);
		 
		 TextView textView6 = (TextView) findViewById(R.id.texto_karuta6);
		 textView6.setText("");
		 ImageView carta6 = (ImageView) findViewById(R.id.karuta6_imageview);
		 carta6.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta6);
		 
		 TextView textView7 = (TextView) findViewById(R.id.texto_karuta7);
		 textView7.setText("");
		 ImageView carta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		 carta7.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta7);
		 
		 TextView textView8 = (TextView) findViewById(R.id.texto_karuta8);
		 textView8.setText("");
		 ImageView carta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		 carta8.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta8);
	 }
	 else if(quantasCartasNaTela == 3)
	 {
		 
		 TextView textView4 = (TextView) findViewById(R.id.texto_karuta4);
		 textView4.setText("");
		 ImageView carta4 = (ImageView) findViewById(R.id.karuta4_imageview);
		 carta4.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta4);
		 
		 TextView textView5 = (TextView) findViewById(R.id.texto_karuta5);
		 textView5.setText("");
		 ImageView carta5 = (ImageView) findViewById(R.id.karuta5_imageview);
		 carta5.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta5);
		 
		 TextView textView6 = (TextView) findViewById(R.id.texto_karuta6);
		 textView6.setText("");
		 ImageView carta6 = (ImageView) findViewById(R.id.karuta6_imageview);
		 carta6.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta6);
		 
		 TextView textView7 = (TextView) findViewById(R.id.texto_karuta7);
		 textView7.setText("");
		 ImageView carta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		 carta7.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta7);
		 
		 TextView textView8 = (TextView) findViewById(R.id.texto_karuta8);
		 textView8.setText("");
		 ImageView carta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		 carta8.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta8);
	 }
	 else if(quantasCartasNaTela == 4)
	 { 
		 TextView textView5 = (TextView) findViewById(R.id.texto_karuta5);
		 textView5.setText("");
		 ImageView carta5 = (ImageView) findViewById(R.id.karuta5_imageview);
		 carta5.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta5);
		 
		 TextView textView6 = (TextView) findViewById(R.id.texto_karuta6);
		 textView6.setText("");
		 ImageView carta6 = (ImageView) findViewById(R.id.karuta6_imageview);
		 carta6.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta6);
		 
		 TextView textView7 = (TextView) findViewById(R.id.texto_karuta7);
		 textView7.setText("");
		 ImageView carta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		 carta7.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta7);
		 
		 TextView textView8 = (TextView) findViewById(R.id.texto_karuta8);
		 textView8.setText("");
		 ImageView carta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		 carta8.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta8);
	 }
	 else if(quantasCartasNaTela == 5)
	 { 
		 TextView textView6 = (TextView) findViewById(R.id.texto_karuta6);
		 textView6.setText("");
		 ImageView carta6 = (ImageView) findViewById(R.id.karuta6_imageview);
		 carta6.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta6);
		 
		 TextView textView7 = (TextView) findViewById(R.id.texto_karuta7);
		 textView7.setText("");
		 ImageView carta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		 carta7.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta7);
		 
		 TextView textView8 = (TextView) findViewById(R.id.texto_karuta8);
		 textView8.setText("");
		 ImageView carta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		 carta8.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta8);
	 }
	 else if(quantasCartasNaTela == 6)
	 { 
		 TextView textView7 = (TextView) findViewById(R.id.texto_karuta7);
		 textView7.setText("");
		 ImageView carta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		 carta7.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta7);
		 
		 TextView textView8 = (TextView) findViewById(R.id.texto_karuta8);
		 textView8.setText("");
		 ImageView carta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		 carta8.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta8);
	 }
	 else if(quantasCartasNaTela == 7)
	 { 
		 TextView textView8 = (TextView) findViewById(R.id.texto_karuta8);
		 textView8.setText("");
		 ImageView carta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		 carta8.setClickable(false);
		 this.fazerImageViewFicarEscuro(carta8);
	 }
	 
 }
 
 /*esse metodo retorna null caso todos os kanjis de kanjisQuePodemVirarCartas for usado*/
 private KanjiTreinar escolherUmKanjiParaTreinar()
 {
	 if(kanjisQuePodemVirarCartas.size() <= 0)
	 {
			return null;
	 }
	 else
	 {
		 Random geraNumAleatorio = new Random();
		 int posicaoKanjiEscolhido = geraNumAleatorio.nextInt(this.kanjisQuePodemVirarCartas.size());
		 
		 KanjiTreinar kanjiEscolhido = this.kanjisQuePodemVirarCartas.remove(posicaoKanjiEscolhido); 
		 this.posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartas.add(posicaoKanjiEscolhido);
		 
		 return kanjiEscolhido; 
	 }
 }
 
 /*muda todas as cartas da tela com base no atributo kanjisDasCartasNaTela. Entrada possivel: au|cotidiano,me|corpo,...*/
 private void mudarCartasNaTela(String[] kanjis)
 {
	 for(int i = 0; i < kanjis.length; i++)
	 {
		 String umKanjiECategoria = kanjis[i];
		 String[] kanjiECategoriaArray = umKanjiECategoria.split("\\|");
		 if(kanjiECategoriaArray.length > 1)
		 {
			 //tem algum kanji p ser extraido aqui. Pois ele tem o kanji e a categoria
			 String umKanji = kanjiECategoriaArray[0];
			 String umaCategoria = kanjiECategoriaArray[1];
			 
			 boolean cartaDoKanjiDeveSerX = this.cartaDoKanjiDeveSerX(umaCategoria, umKanji);
			 //se o kanji ja tiver virado dica anteriormente e nao eh a dica atual, entao a carta dele ja deveria ter saido do jogo
			 
			 if(i == 0)
			 {
				 if(cartaDoKanjiDeveSerX == true)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta1);
					 texto.setText(""); 
					 ImageView imageViewCarta1 = (ImageView) findViewById(R.id.karuta1_imageview);
					 imageViewCarta1.setClickable(false);
					 this.fazerImageViewFicarEscuro(imageViewCarta1);
					 
					//o verso deve ficar com a traducao em hiragana dela e seu kanji
			    	KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(i);
			    	fazerTextoVersoDaCartaAparecer(kanji, i);
					 
				 }
				 else
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta1);
					 this.colocarTextoVerticalNaCarta(texto, umKanji);
					 ImageView imageViewCarta1 = (ImageView) findViewById(R.id.karuta1_imageview);
					 imageViewCarta1.setClickable(true);
					 imageViewCarta1.setImageResource(R.drawable.karutavazia);
					 this.fazerCartaVoltarACorNormal(imageViewCarta1,i);
					 fazerTextoVersoDaCartaDesaparecer(i);
					 
					 //fazer os duplos textos que aparecem atras da carta ficarem invisiveis
					 RelativeLayout textoDuploKaruta1 = (RelativeLayout) findViewById(R.id.textoDuploKaruta1); //e trocar por 2 textos
				     textoDuploKaruta1.setVisibility(View.GONE);
					 
				 }
			 }
			 else if(i == 1)
			 {
				 if(cartaDoKanjiDeveSerX == true)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta2);
					 texto.setText(""); 
					 ImageView imageViewCarta2 = (ImageView) findViewById(R.id.karuta2_imageview);
					 imageViewCarta2.setClickable(false);
					 this.fazerImageViewFicarEscuro(imageViewCarta2);
					 
					//o verso deve ficar com a traducao em hiragana dela e seu kanji
					 KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(i);
				     fazerTextoVersoDaCartaAparecer(kanji, i);
				 }
				 else
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta2);
					 this.colocarTextoVerticalNaCarta(texto, umKanji);
					 ImageView imageViewCarta2 = (ImageView) findViewById(R.id.karuta2_imageview);
					 imageViewCarta2.setClickable(true);
					 imageViewCarta2.setImageResource(R.drawable.karutavazia);
					 this.fazerCartaVoltarACorNormal(imageViewCarta2,i);
					 fazerTextoVersoDaCartaDesaparecer(i);
					 
					//fazer os duplos textos que aparecem atras da carta ficarem invisiveis
					 RelativeLayout textoDuploKaruta2 = (RelativeLayout) findViewById(R.id.textoDuploKaruta2); //e trocar por 2 textos
				     textoDuploKaruta2.setVisibility(View.GONE);
				 }
			 }
			 else if(i == 2)
			 {
				 if(cartaDoKanjiDeveSerX == true)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta3);
					 texto.setText(""); 
					 ImageView imageViewCarta3 = (ImageView) findViewById(R.id.karuta3_imageview);
					 imageViewCarta3.setClickable(false);
					 this.fazerImageViewFicarEscuro(imageViewCarta3);
					 
					//o verso deve ficar com a traducao em hiragana dela e seu kanji
					 KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(i);
				     fazerTextoVersoDaCartaAparecer(kanji, i);
					 
				 }
				 else
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta3);
					 this.colocarTextoVerticalNaCarta(texto, umKanji);
					 ImageView imageViewCarta3 = (ImageView) findViewById(R.id.karuta3_imageview);
					 imageViewCarta3.setClickable(true);
					 imageViewCarta3.setImageResource(R.drawable.karutavazia);
					 this.fazerCartaVoltarACorNormal(imageViewCarta3,i);
					 fazerTextoVersoDaCartaDesaparecer(i);
					 
					//fazer os duplos textos que aparecem atras da carta ficarem invisiveis
					 RelativeLayout textoDuploKaruta3 = (RelativeLayout) findViewById(R.id.textoDuploKaruta3); //e trocar por 2 textos
				     textoDuploKaruta3.setVisibility(View.GONE);
				 }
			 }
			 else if(i == 3)
			 {
				 if(cartaDoKanjiDeveSerX == true)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta4);
					 texto.setText(""); 
					 ImageView imageViewCarta4 = (ImageView) findViewById(R.id.karuta4_imageview);
					 imageViewCarta4.setClickable(false);
					 this.fazerImageViewFicarEscuro(imageViewCarta4);
					 
					//o verso deve ficar com a traducao em hiragana dela e seu kanji
					 KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(i);
				     fazerTextoVersoDaCartaAparecer(kanji, i);
				 }
				 else
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta4);
					 this.colocarTextoVerticalNaCarta(texto, umKanji);
					 ImageView imageViewCarta4 = (ImageView) findViewById(R.id.karuta4_imageview);
					 imageViewCarta4.setClickable(true);
					 imageViewCarta4.setImageResource(R.drawable.karutavazia);
					 this.fazerCartaVoltarACorNormal(imageViewCarta4,i);
					 fazerTextoVersoDaCartaDesaparecer(i);
					 
					//fazer os duplos textos que aparecem atras da carta ficarem invisiveis
					 RelativeLayout textoDuploKaruta4 = (RelativeLayout) findViewById(R.id.textoDuploKaruta4); //e trocar por 2 textos
				     textoDuploKaruta4.setVisibility(View.GONE);
				 }
			 }
			 else if(i == 4)
			 {
				 if(cartaDoKanjiDeveSerX == true)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta5);
					 texto.setText(""); 
					 ImageView imageViewCarta5 = (ImageView) findViewById(R.id.karuta5_imageview);
					 imageViewCarta5.setClickable(false);
					 this.fazerImageViewFicarEscuro(imageViewCarta5);
					 
					//o verso deve ficar com a traducao em hiragana dela e seu kanji
					 KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(i);
				     fazerTextoVersoDaCartaAparecer(kanji, i);
				 }
				 else
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta5);
					 this.colocarTextoVerticalNaCarta(texto, umKanji);
					 ImageView imageViewCarta5 = (ImageView) findViewById(R.id.karuta5_imageview);
					 imageViewCarta5.setClickable(true);
					 imageViewCarta5.setImageResource(R.drawable.karutavazia);
					 this.fazerCartaVoltarACorNormal(imageViewCarta5,i);
					 fazerTextoVersoDaCartaDesaparecer(i);
					 
					//fazer os duplos textos que aparecem atras da carta ficarem invisiveis
					 RelativeLayout textoDuploKaruta5 = (RelativeLayout) findViewById(R.id.textoDuploKaruta5); //e trocar por 2 textos
				     textoDuploKaruta5.setVisibility(View.GONE);
				 }
			 }
			 else if(i == 5)
			 {
				 if(cartaDoKanjiDeveSerX == true)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta6);
					 texto.setText(""); 
					 ImageView imageViewCarta6 = (ImageView) findViewById(R.id.karuta6_imageview);
					 imageViewCarta6.setClickable(false);
					 this.fazerImageViewFicarEscuro(imageViewCarta6);
					 
					//o verso deve ficar com a traducao em hiragana dela e seu kanji
					 KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(i);
				     fazerTextoVersoDaCartaAparecer(kanji, i);
				 }
				 else
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta6);
					 this.colocarTextoVerticalNaCarta(texto, umKanji);
					 ImageView imageViewCarta6 = (ImageView) findViewById(R.id.karuta6_imageview);
					 imageViewCarta6.setClickable(true);
					 imageViewCarta6.setImageResource(R.drawable.karutavazia);
					 this.fazerCartaVoltarACorNormal(imageViewCarta6,i);
					 fazerTextoVersoDaCartaDesaparecer(i);
					 
					//fazer os duplos textos que aparecem atras da carta ficarem invisiveis
					 RelativeLayout textoDuploKaruta6 = (RelativeLayout) findViewById(R.id.textoDuploKaruta6); //e trocar por 2 textos
				     textoDuploKaruta6.setVisibility(View.GONE);
				 }
			 }
			 else if(i == 6)
			 {
				 if(cartaDoKanjiDeveSerX == true)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta7);
					 texto.setText(""); 
					 ImageView imageViewCarta7 = (ImageView) findViewById(R.id.karuta7_imageview);
					 imageViewCarta7.setClickable(false);
					 this.fazerImageViewFicarEscuro(imageViewCarta7);
					 
					//o verso deve ficar com a traducao em hiragana dela e seu kanji
					 KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(i);
				     fazerTextoVersoDaCartaAparecer(kanji, i);
				 }
				 else
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta7);
					 this.colocarTextoVerticalNaCarta(texto, umKanji);
					 ImageView imageViewCarta7 = (ImageView) findViewById(R.id.karuta7_imageview);
					 imageViewCarta7.setClickable(true);
					 imageViewCarta7.setImageResource(R.drawable.karutavazia);
					 this.fazerCartaVoltarACorNormal(imageViewCarta7,i);
					 fazerTextoVersoDaCartaDesaparecer(i);
					 

					//fazer os duplos textos que aparecem atras da carta ficarem invisiveis
					RelativeLayout textoDuploKaruta7 = (RelativeLayout) findViewById(R.id.textoDuploKaruta7); //e trocar por 2 textos
					textoDuploKaruta7.setVisibility(View.GONE);
				 }
			 }
			 else if(i == 7)
			 {
				 if(cartaDoKanjiDeveSerX == true)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta8);
					 texto.setText(""); 
					 ImageView imageViewCarta8 = (ImageView) findViewById(R.id.karuta8_imageview);
					 imageViewCarta8.setClickable(false);
					 this.fazerImageViewFicarEscuro(imageViewCarta8);
					 
					//o verso deve ficar com a traducao em hiragana dela e seu kanji
					 KanjiTreinar kanji = this.kanjisDasCartasNaTela.get(i);
				     fazerTextoVersoDaCartaAparecer(kanji, i);
				 }
				 else
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta8);
					 this.colocarTextoVerticalNaCarta(texto, umKanji);
					 ImageView imageViewCarta8 = (ImageView) findViewById(R.id.karuta8_imageview);
					 imageViewCarta8.setClickable(true);
					 imageViewCarta8.setImageResource(R.drawable.karutavazia);
					 this.fazerCartaVoltarACorNormal(imageViewCarta8,i);
					 fazerTextoVersoDaCartaDesaparecer(i);
					 
					//fazer os duplos textos que aparecem atras da carta ficarem invisiveis
					 RelativeLayout textoDuploKaruta8 = (RelativeLayout) findViewById(R.id.textoDuploKaruta8); //e trocar por 2 textos
				     textoDuploKaruta8.setVisibility(View.GONE);
				 }
			 }
		 }
	 }
	 
 }
 
 /*caso o kanji dessa categoria e texto nao seja o kanjidaDica e ele ja tenha saido como dica, entao a carta respetiva desse kanji deveria ser uma carta X*/
 private boolean cartaDoKanjiDeveSerX(String categoriaKanji, String textoKanji)
 {
	 if(this.kanjiDaDica == null)
	 {
		 //no primeiro turno, quando nao ha dica nenhuma, nenhum kanji eh X
		 return false;
	 }
	 
	 if((this.kanjiDaDica.getKanji().compareTo(textoKanji) == 0)
			 && (this.kanjiDaDica.getCategoriaAssociada().compareTo(categoriaKanji) == 0))
	 {
		 return false; //eh o kanji da dica
	 }
	 else
	 {
		 for(int i = 0; i < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); i++)
		 {
			 KanjiTreinar umKanjiJaVirouDica = this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(i);
			 
			 if((umKanjiJaVirouDica.getCategoriaAssociada().compareTo(categoriaKanji) == 0) 
					 && (umKanjiJaVirouDica.getKanji().compareTo(textoKanji) == 0))
			 {
				 //o kanji ja virou dica anteriormente e alguem ja o acertou e por isso deve ser um X
				 return true;
			 }
			 
		 }
		 
		 return false; //passamos por todos os kanjis que ja viraram dicas e nenhum deles era esse kanji como parametro. Entao ele ainda estah no jogo
	 }
 }
 
 /*funcao para gerar a dica que vai aparecer para ambos os usuarios e ainda enviar ao outro jogador essa dica*/
 private void gerarKanjiDaDica()
 {
	 LinkedList<KanjiTreinar> kanjisQueAindaNaoViraramDicas = new LinkedList<KanjiTreinar>();
	 
	 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
	 {
		 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(i);
		 
		 boolean kanjiJaVirouDica = false;
		 for(int j = 0; j < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); j++)
		 {
			 KanjiTreinar umKanjiQueVirouDica = this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(j);
			 
			 if(umKanjiQueVirouDica.getKanji().compareTo(umKanji.getKanji()) == 0)
			 {
				 kanjiJaVirouDica = true;
				 break;
			 }
		 }
		 
		 if(kanjiJaVirouDica == false)
		 {
			 kanjisQueAindaNaoViraramDicas.add(umKanji);
		 }
	 }
	 
	 
	 if(kanjisQueAindaNaoViraramDicas.size() > 0)
	 {
		 Random geraNumAleatorio = new Random(); 
		 int indiceKanjiDaDica = geraNumAleatorio.nextInt(kanjisQueAindaNaoViraramDicas.size());
		 
		 KanjiTreinar umKanji = kanjisQueAindaNaoViraramDicas.get(indiceKanjiDaDica);
		 this.kanjiDaDica = umKanji;
		 this.palavrasJogadas.add(kanjiDaDica);
		 
		 int idCategoriaKanjiDaDica = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(this.kanjiDaDica.getCategoriaAssociada());
		 
		 String mensagem = "kanjiDaDica=" + this.kanjiDaDica.getKanji() + "|" + idCategoriaKanjiDaDica;
		 this.mandarMensagemMultiplayer(mensagem);
		 
		 this.alterarTextoDicaComBaseNoKanjiDaDica();
		 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.add(umKanji);
	 }
	 else
	 {
		 //nao conseguimos gerar novo kanji da dica: passar pra proxima rodada ou terminar jogo?
		 
		 if(this.quantasRodadasHaverao != this.rodadaAtual)
		 {
			 realizarProcedimentoPassarParaProximaRodada();
		 }
		 else
		 {
			 this.terminarJogoEEnviarMesagemAoAdversario();
		 }
	 }
 }
 
 private void alterarTextoDicaComBaseNoKanjiDaDica()
 {
	 TextView textoDica = (TextView) findViewById(R.id.dica_kanji);
	 TextView textoTraducao = (TextView) findViewById(R.id.dica_kanji_traducao);
	 String hiraganaDoKanji = this.kanjiDaDica.getHiraganaDoKanji();
	 String traducaoDoKanji = this.kanjiDaDica.getTraducaoEmPortugues();
	 textoDica.setText(hiraganaDoKanji);
	 
	 boolean usuarioUsandoVersaoJaponesa = false; //se o usuario estiver usando a versao japonesa, a traducao n eh necessaria
	 Resources res = this.getResources();
     Locale myLocale = res.getConfiguration().locale;
     if(myLocale != null)
		 {
			String language = myLocale.getLanguage();
			if(myLocale.getLanguage().compareTo("jp") == 0)
			{
				usuarioUsandoVersaoJaponesa = true;
			}
		 }
     
     final boolean usuarioUsandoVersaoJaponesaFinal = usuarioUsandoVersaoJaponesa;
	 if(usuarioUsandoVersaoJaponesaFinal == false)
	 {
		 textoTraducao.setText(traducaoDoKanji);
	 }
	 else
	 {
		 textoTraducao.setText("");
	 }
 }
 
 
 /*assim que o usuario acerta uma carta, ele avisa ao adversairo que aquela carta nao pode ser mais escolhida*/
 private void alertarAoAdversarioQueACartaNaoEhMaisClicavel(String qualCarta)
 {
	 String mensagem = "naoClicavel=" + qualCarta + ";" + "usou2x=" + this.usou2x + ";" + "dificuldade=" + dificuldadeCartaAcertada;; //ex: naoClicavel=karuta1
	 this.mandarMensagemMultiplayer(mensagem);	 
 }
 
 private void alertarAoAdversarioQueACartaNaoEhMaisClicavelVoceTbAcertou(String qualCarta)
 {
	 String mensagem = "naoClicavelVoceTbAcertou=" + qualCarta + ";" + "usou2x=" + this.usou2x + ";" + "dificuldade=" + dificuldadeCartaAcertada; //ex: naoClicavel=karuta1
	 this.mandarMensagemMultiplayer(mensagem);	 
 }
 
 /*funcao chamada assim que alguem acerta algum kanji na tela.*/
 private void gerarKanjiDaDicaOuIniciarNovaRodadaOuTerminarJogo()
 {
	 if(this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size() == this.kanjisDasCartasNaTela.size())
	 {
		 //todos os kanjis da tela ja se tornaram dicas. Eh hora de mudar a rodada ou terminar o jogo
		 if(this.quantasRodadasHaverao == this.rodadaAtual || this.kanjisQuePodemVirarCartas.size() == 0)
		 {
			 //o jogo deve terminar. Ou a quantidade de rodadas foi alcancada ou nao existem mais cartas a serem geradas
			 new Timer().schedule(new TimerTask() 
			 { 
				    @Override
				    public void run() 
				    {
				        //If you want to operate UI modifications, you must run ui stuff on UiThread.
				        ModoCasual.this.runOnUiThread(new Runnable() 
				        {
				            @Override
				            public void run() 
				            {
				            	terminarJogoEEnviarMesagemAoAdversario();
				            }
				        });
				    }
				}, 1000);
		 }
		 else
		 {
			 //deve-se passar para a proxima rodada
			 new Timer().schedule(new TimerTask() 
			 { 
				    @Override
				    public void run() 
				    {
				        //If you want to operate UI modifications, you must run ui stuff on UiThread.
				        ModoCasual.this.runOnUiThread(new Runnable() 
				        {
				            @Override
				            public void run() 
				            {
				            	realizarProcedimentoPassarParaProximaRodada();
				            }
				        });
				    }
				}, 1000);
			 
		 }
	 }
	 else
	 {
		 this.gerarKanjiDaDica();
	 }
 }
 
 private void realizarProcedimentoPassarParaProximaRodada()
 {
	 this.rodadaAtual = this.rodadaAtual + 1;
	 TextView textViewRodada = (TextView) findViewById(R.id.rodada);
	 String textoRodada = getResources().getString(R.string.rodada_sem_dois_pontos);
	 textViewRodada.setText(textoRodada + " " + String.valueOf(this.rodadaAtual));
	 
	 this.tornarTodasAsCartasNaTelaClicaveisEVaziasNovamente();
	 
	 String mensagemRodadaMudou = "rodadaMudou";
	 this.mandarMensagemMultiplayer(mensagemRodadaMudou);
	 
	 if(this.criouUmaSala == true)
	 {
		 //jogador eh quem escolheu as categorias, entao eh ele quem sorteia os 8 kanjis da rodada
		 this.escolher8KanjisParaARodada();
	 }
	 else
	 {
		 //jogador nao eh quem escolheu as categorias, entao ele deve avisar ao adversario que eh necessario criar mais 8 cartas p a jogada
		 String mensagemGerarMais8Cartas = "gerarMais8Cartas";
		 this.mandarMensagemMultiplayer(mensagemGerarMais8Cartas);
	 }
 }
 
 /*as cartas voltam a ser clicaveis e o X no meio da carta desaparece*/
 private void tornarTodasAsCartasNaTelaClicaveisEVaziasNovamente()
 {
	 ImageView imageViewKaruta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	 imageViewKaruta1.setImageResource(R.drawable.karutavazia); //mudei a figura da carta
	 this.fazerCartaVoltarACorNormal(imageViewKaruta1,0);
	 findViewById(R.id.karuta1).setClickable(true);
	 
	 ImageView imageViewKaruta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	 imageViewKaruta2.setImageResource(R.drawable.karutavazia); //mudei a figura da carta
	 this.fazerCartaVoltarACorNormal(imageViewKaruta2,1);
	 findViewById(R.id.karuta2).setClickable(true);
	 
	 ImageView imageViewKaruta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	 imageViewKaruta3.setImageResource(R.drawable.karutavazia); //mudei a figura da carta
	 this.fazerCartaVoltarACorNormal(imageViewKaruta3,2);
	 findViewById(R.id.karuta3).setClickable(true);
	 
	 ImageView imageViewKaruta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	 imageViewKaruta4.setImageResource(R.drawable.karutavazia); //mudei a figura da carta
	 this.fazerCartaVoltarACorNormal(imageViewKaruta4,3);
	 findViewById(R.id.karuta4).setClickable(true);
	 
	 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	 imageViewKaruta5.setImageResource(R.drawable.karutavazia); //mudei a figura da carta
	 this.fazerCartaVoltarACorNormal(imageViewKaruta5,4);
	 findViewById(R.id.karuta5).setClickable(true);
	 
	 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	 imageViewKaruta6.setImageResource(R.drawable.karutavazia); //mudei a figura da carta
	 this.fazerCartaVoltarACorNormal(imageViewKaruta6,5);
	 findViewById(R.id.karuta6).setClickable(true);
	 
	 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	 imageViewKaruta7.setImageResource(R.drawable.karutavazia); //mudei a figura da carta
	 this.fazerCartaVoltarACorNormal(imageViewKaruta7,6);
	 findViewById(R.id.karuta7).setClickable(true);
	 
	 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	 imageViewKaruta8.setImageResource(R.drawable.karutavazia); //mudei a figura da carta
	 this.fazerCartaVoltarACorNormal(imageViewKaruta8,7);
	 findViewById(R.id.karuta8).setClickable(true);
 }
 
 private synchronized void pegarTodosOsKanjisQuePodemVirarCartas()
 {
	 if(this.kanjisQuePodemVirarCartas == null)
	 {
		 this.kanjisQuePodemVirarCartas = new LinkedList<KanjiTreinar>(); 
	 }
	 else
	 {
		 this.kanjisQuePodemVirarCartas.clear();
	 }
	 HashMap<String,LinkedList<KanjiTreinar>> categoriasEscolhidasEKanjisDelas = SingletonGuardaDadosDaPartida.getInstance().getCategoriasEscolhidasEKanjisDelas();
	 
	 Iterator<String> iteradorCategoriasEKanjis = categoriasEscolhidasEKanjisDelas.keySet().iterator();
	 while(iteradorCategoriasEKanjis.hasNext() == true)
	 {
		 String umaCategoria = iteradorCategoriasEKanjis.next();
		 LinkedList<KanjiTreinar> kanjisDaCategoria = categoriasEscolhidasEKanjisDelas.get(umaCategoria);
		 
		 for(int i = 0; i < kanjisDaCategoria.size(); i++)
		 {
			 this.kanjisQuePodemVirarCartas.add(kanjisDaCategoria.get(i));
		 }
	 }
	 this.kanjisQuePodemVirarCartas.size();
 }

 /*se o usuario errou, ele espera 5 segundos sem clicar em nada*/
 private void realizarProcedimentoUsuarioErrouCarta()
 {
	 String mensagemClicouCartaErrada = getResources().getString(R.string.mensagem_clicou_carta_errada);
	 Toast t = Toast.makeText(this, mensagemClicouCartaErrada, Toast.LENGTH_LONG);
	 t.show();
	 fazerMascoteFicarZangadaPorUmTempo();
	 
	 ImageView karuta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	 ImageView karuta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	 ImageView karuta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	 ImageView karuta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	 ImageView karuta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	 ImageView karuta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	 ImageView karuta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	 ImageView karuta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	 
	 karuta1.setClickable(false);
	 karuta2.setClickable(false);
	 karuta3.setClickable(false);
	 karuta4.setClickable(false);
	 karuta5.setClickable(false);
	 karuta6.setClickable(false);
	 karuta7.setClickable(false);
	 karuta8.setClickable(false);
	 
	 karuta1.setAlpha(128);
	 karuta2.setAlpha(128);
	 karuta3.setAlpha(128);
	 karuta4.setAlpha(128);
	 karuta5.setAlpha(128);
	 karuta6.setAlpha(128);
	 karuta7.setAlpha(128);
	 karuta8.setAlpha(128);
	 
	 final CircleProgress circleprogress_karuta1 = (CircleProgress) findViewById(R.id.circleprogress_karuta1);
	 circleprogress_karuta1.setVisibility(View.VISIBLE);
	 circleprogress_karuta1.setMax(100);
	 circleprogress_karuta1.setProgress(0);
	 final CircleProgress circleprogress_karuta2 = (CircleProgress) findViewById(R.id.circleprogress_karuta2);
	 circleprogress_karuta2.setVisibility(View.VISIBLE);
	 circleprogress_karuta2.setMax(100);
	 circleprogress_karuta2.setProgress(0);
	 final CircleProgress circleprogress_karuta3 = (CircleProgress) findViewById(R.id.circleprogress_karuta3);
	 circleprogress_karuta3.setVisibility(View.VISIBLE);
	 circleprogress_karuta3.setMax(100);
	 circleprogress_karuta3.setProgress(0);
	 final CircleProgress circleprogress_karuta4 = (CircleProgress) findViewById(R.id.circleprogress_karuta4);
	 circleprogress_karuta4.setVisibility(View.VISIBLE);
	 circleprogress_karuta4.setMax(100);
	 circleprogress_karuta4.setProgress(0);
	 final CircleProgress circleprogress_karuta5 = (CircleProgress) findViewById(R.id.circleprogress_karuta5);
	 circleprogress_karuta5.setVisibility(View.VISIBLE);
	 circleprogress_karuta5.setMax(100);
	 circleprogress_karuta5.setProgress(0);
	 final CircleProgress circleprogress_karuta6 = (CircleProgress) findViewById(R.id.circleprogress_karuta6);
	 circleprogress_karuta6.setVisibility(View.VISIBLE);
	 circleprogress_karuta6.setMax(100);
	 circleprogress_karuta6.setProgress(0);
	 final CircleProgress circleprogress_karuta7 = (CircleProgress) findViewById(R.id.circleprogress_karuta7);
	 circleprogress_karuta7.setVisibility(View.VISIBLE);
	 circleprogress_karuta7.setMax(100);
	 circleprogress_karuta7.setProgress(0);
	 final CircleProgress circleprogress_karuta8 = (CircleProgress) findViewById(R.id.circleprogress_karuta8);
	 circleprogress_karuta8.setVisibility(View.VISIBLE);
	 circleprogress_karuta8.setMax(100);
	 circleprogress_karuta8.setProgress(0);
	 
	 this.progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta = 0;
	 
	 final Handler handler = new Handler();
	 Runnable runnableThread = new Runnable() {
         @Override
         public void run() 
         {
             
            	ModoCasual.this.runOnUiThread(new Runnable() 
 		        {
 		            @Override
 		            public void run() 
 		            {
 		            		progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta = progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta + 10;
 		            		circleprogress_karuta1.setProgress(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta);
 		            		circleprogress_karuta2.setProgress(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta);
 		            		circleprogress_karuta3.setProgress(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta);
 		            		circleprogress_karuta4.setProgress(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta);
 		            		circleprogress_karuta5.setProgress(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta);
 		            		circleprogress_karuta6.setProgress(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta);
 		            		circleprogress_karuta7.setProgress(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta);
 		            		circleprogress_karuta8.setProgress(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta);
 		            		
 	 		            	if(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta == 100)
 	 		            	{
 	 		            		//acabou a espera!
 	 		            		terminouEsperaUsuarioErrouCarta();
 	 		            	}
 		            }
 		        });
             
            	
            	if(progressoTodosCircleProgressEsperaDoUsuarioAoErrarCarta < 100)
            	{
            		//ainda iremos realizar a tarefa da thread mais uma vez!!!
            		 handler.postDelayed(this, 500); //faca as contas: 5000(5 segundos) -- 100%
						 //				   X                -- 10%
						 // no cruz-credo, isso dah 500. de 500 em 500 milisegundos, eu aumento em 10% a porcentagem de conclusao ateh chegar em 5000, que eh 5 segundos
            	}
            	 								
         	}
         };
     
     handler.postDelayed(runnableThread, 500); //faltou iniciar a thread atraves do handler!!!
	 
	 /*new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		                	terminouEsperaUsuarioErrouCarta();
		            }
		        });
		    }
		}, 5000);*/
 }
 
 private void terminouEsperaUsuarioErrouCarta()
 { 
	 if(this.jogoAcabou == false)
	 {
		 ImageView karuta1 = (ImageView) findViewById(R.id.karuta1_imageview);
		 ImageView karuta2 = (ImageView) findViewById(R.id.karuta2_imageview);
		 ImageView karuta3 = (ImageView) findViewById(R.id.karuta3_imageview);
		 ImageView karuta4 = (ImageView) findViewById(R.id.karuta4_imageview);
		 ImageView karuta5 = (ImageView) findViewById(R.id.karuta5_imageview);
		 ImageView karuta6 = (ImageView) findViewById(R.id.karuta6_imageview);
		 ImageView karuta7 = (ImageView) findViewById(R.id.karuta7_imageview);
		 ImageView karuta8 = (ImageView) findViewById(R.id.karuta8_imageview);
		 
		 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
		 {
			 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(i);
			 
			 if(kanjiNaoSeTornouDicaOuEhADica(umKanji) == false)
			 {
				 if(i == 0)
				 {
					 karuta1.setClickable(false);
				 }
				 else if(i == 1)
				 {
					 karuta2.setClickable(false);
				 }
				 else if(i == 2)
				 {
					 karuta3.setClickable(false);
				 }
				 else if(i == 3)
				 {
					 karuta4.setClickable(false);
				 }
				 else if(i == 4)
				 {
					 karuta5.setClickable(false);
				 }
				 else if(i == 5)
				 {
					 karuta6.setClickable(false);
				 }
				 else if(i == 6)
				 {
					 karuta7.setClickable(false);
				 }
				 else if(i == 7)
				 {
					 karuta8.setClickable(false);
				 }
			 }
			 else
			 {
				 if(i == 0)
				 {
					 karuta1.setClickable(true);
				 }
				 else if(i == 1)
				 {
					 karuta2.setClickable(true);
				 }
				 else if(i == 2)
				 {
					 karuta3.setClickable(true);
				 }
				 else if(i == 3)
				 {
					 karuta4.setClickable(true);
				 }
				 else if(i == 4)
				 {
					 karuta5.setClickable(true);
				 }
				 else if(i == 5)
				 {
					 karuta6.setClickable(true);
				 }
				 else if(i == 6)
				 {
					 karuta7.setClickable(true);
				 }
				 else if(i == 7)
				 {
					 karuta8.setClickable(true);
				 }
			 }
		 }
		 
		 karuta1.setAlpha(255);
		 karuta2.setAlpha(255);
		 karuta3.setAlpha(255);
		 karuta4.setAlpha(255);
		 karuta5.setAlpha(255);
		 karuta6.setAlpha(255);
		 karuta7.setAlpha(255);
		 karuta8.setAlpha(255);
		 
		 CircleProgress circleprogress_karuta1 = (CircleProgress) findViewById(R.id.circleprogress_karuta1);
		 circleprogress_karuta1.setVisibility(View.GONE);
		 CircleProgress circleprogress_karuta2 = (CircleProgress) findViewById(R.id.circleprogress_karuta2);
		 circleprogress_karuta2.setVisibility(View.GONE);
		 CircleProgress circleprogress_karuta3 = (CircleProgress) findViewById(R.id.circleprogress_karuta3);
		 circleprogress_karuta3.setVisibility(View.GONE);
		 CircleProgress circleprogress_karuta4 = (CircleProgress) findViewById(R.id.circleprogress_karuta4);
		 circleprogress_karuta4.setVisibility(View.GONE);
		 CircleProgress circleprogress_karuta5 = (CircleProgress) findViewById(R.id.circleprogress_karuta5);
		 circleprogress_karuta5.setVisibility(View.GONE);
		 CircleProgress circleprogress_karuta6 = (CircleProgress) findViewById(R.id.circleprogress_karuta6);
		 circleprogress_karuta6.setVisibility(View.GONE);
		 CircleProgress circleprogress_karuta7 = (CircleProgress) findViewById(R.id.circleprogress_karuta7);
		 circleprogress_karuta7.setVisibility(View.GONE);
		 CircleProgress circleprogress_karuta8 = (CircleProgress) findViewById(R.id.circleprogress_karuta8);
		 circleprogress_karuta8.setVisibility(View.GONE);
	 }
 }
 
 /*retorna true se o kanji nao se tornou dica ou se ele eh a propria dica*/
 private boolean kanjiNaoSeTornouDicaOuEhADica(KanjiTreinar kanji)
 {
	 String categoriaKanji = kanji.getCategoriaAssociada();
	 String nomeKanji = kanji.getKanji();
	 
	 if((this.kanjiDaDica.getCategoriaAssociada().compareTo(categoriaKanji) == 0) &&
		(this.kanjiDaDica.getKanji().compareTo(nomeKanji) == 0))
	 {
		 //eh o kanji da dica
		 return true;
	 }
	 else
	 {
		 boolean jaVirouDica = false;
		 
		 for(int i = 0; i < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); i++)
		 {
			 KanjiTreinar kanjiJaVirouDica = this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(i);
			 
			 if((kanjiJaVirouDica.getCategoriaAssociada().compareTo(categoriaKanji) == 0) &&
				(kanjiJaVirouDica.getKanji().compareTo(nomeKanji) == 0))
			{
				jaVirouDica = true;
				break;
			}
		 }
		 
		 if(jaVirouDica == false)
		 {
			 return true;
		 }
		 else
		 {
			 return false;
		 }
	 }
 }
 
 private void verificarSeJogadorRecebeUmItemAleatorio()
 {
	 if(this.jogoAcabou == false)
	 {
		 // antes tinha if(this.quantasCartasJaSairamDoJogo % 3 == 0)
		 //{
			 //de 3 em 3 cartas que saem, um item aleatorio eh gerado
			 
				 //o jogador estah sem itens. Vamos dar um p ele
				 String itemAGanhar = "";
				 if(this.pontuacaoDoAdversario > this.suaPontuacao)
				 {
					 //voce estah perdendo.
					 //eh possivel vc receber um item novo ou nao?
					 if(this.itensDoPerdedor.size() == this.itensAtuais.size())
					 {
						 //melhor o usuario n receber nenhum item!!!
					 }
					 else
					 {
						 boolean foiDecididoItemQueJogadorIrahGanhar = false;
						 
						 while(foiDecididoItemQueJogadorIrahGanhar == false)
						 {
							 Random geraNumeroAleatorio = new Random();
							 int indiceSeuItem = geraNumeroAleatorio.nextInt(this.itensDoPerdedor.size());
							 itemAGanhar = this.itensDoPerdedor.get(indiceSeuItem);
							 //serah q esse item o jogador n ja tem?
							 boolean jogadorJaTemEsseItem = false;
							 for(int i = 0; i < this.itensAtuais.size(); i++)
							 {
								 String umItemJogadorJaTem = this.itensAtuais.get(i);
								 if(umItemJogadorJaTem.compareTo(itemAGanhar) == 0)
								 {
									 jogadorJaTemEsseItem = true;
								 }
							 }
							 
							 if(jogadorJaTemEsseItem == false)
							 {
								 foiDecididoItemQueJogadorIrahGanhar = true;
							 }
						 }
					 }
				 }
				 else
				 {
					 //voce estah ganhando ou esta empatado
					//eh possivel vc receber um item novo ou nao?
					 if(this.itensDoGanhador.size() == this.itensAtuais.size())
					 {
						 //melhor o usuario n receber nenhum item!!!
					 }
					 else
					 {
						 boolean foiDecididoItemQueJogadorIrahGanhar = false;
						 
						 while(foiDecididoItemQueJogadorIrahGanhar == false)
						 {
							 Random geraNumeroAleatorio = new Random();
							 int indiceSeuItem = geraNumeroAleatorio.nextInt(this.itensDoGanhador.size());
							 itemAGanhar = this.itensDoGanhador.get(indiceSeuItem);
							 //serah q esse item o jogador n ja tem?
							 boolean jogadorJaTemEsseItem = false;
							 for(int i = 0; i < this.itensAtuais.size(); i++)
							 {
								 String umItemJogadorJaTem = this.itensAtuais.get(i);
								 if(umItemJogadorJaTem.compareTo(itemAGanhar) == 0)
								 {
									 jogadorJaTemEsseItem = true;
								 }
							 }
							 
							 if(jogadorJaTemEsseItem == false)
							 {
								 foiDecididoItemQueJogadorIrahGanhar = true;
							 }
						 }
					 }
				 }
				 
				 if(itemAGanhar.compareTo("") != 0)
				 {
					 //ele pode ganhar mesmo um item
					 this.itensAtuais.add(itemAGanhar);
					 this.mudarImagemItemDeAcordoComOItemAdquirido(itemAGanhar);
				 }
		 //} 
	 }
 }
 
 private void mudarImagemItemDeAcordoComOItemAdquirido(String itemAdquirido)
 { 
	 if(itemAdquirido.compareTo("trovaotiracartaaleatoria") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item1);
		 imagemItem.setImageResource(R.drawable.trovaotiracartaaleatoria);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
	 else if(itemAdquirido.compareTo("parartempo") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item2);
		 imagemItem.setImageResource(R.drawable.parartempo);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
	 else if(itemAdquirido.compareTo("misturarcartas") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item3);
		 imagemItem.setImageResource(R.drawable.misturarcartas);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
	 else if(itemAdquirido.compareTo("mudardica") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item4);
		 imagemItem.setImageResource(R.drawable.mudardica);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
	 else if(itemAdquirido.compareTo("doisx") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item5);
		 imagemItem.setImageResource(R.drawable.doisx);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
	 else if(itemAdquirido.compareTo("naoesperemais") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item6);
		 imagemItem.setImageResource(R.drawable.naoesperemais);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
	 else if(itemAdquirido.compareTo("cartasdouradas") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item7);
		 imagemItem.setImageResource(R.drawable.cartasdouradas);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
	 else if(itemAdquirido.compareTo("trovaotiracarta") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item8);
		 imagemItem.setImageResource(R.drawable.trovaotiracarta);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
	 else if(itemAdquirido.compareTo("revivecarta") == 0)
	 {
		 ImageView imagemItem = (ImageView) findViewById(R.id.item9);
		 imagemItem.setImageResource(R.drawable.revivecarta);
		 imagemItem.setClickable(true); //agora que o usuario possui um item, a imagem eh clicavel
		 new BounceAnimation(imagemItem).animate();
	 }
 }
 
 private void usarItemAtual(String nomeItemUsado)
 {
	 if(this.usouReviveCarta == false && this.usouTrovaoTiraCarta == false)
	 {
		 //o usuario n esta com nenhum item impactante em uso atualmente
		 if(nomeItemUsado.compareTo("trovaotiracartaaleatoria") == 0)
		 {
			 if(this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size() == this.kanjisDasCartasNaTela.size())
			 {
				 //nao existe nenhum kanji que o trovao possa atacar porque soh existe aquele kanji que eh a dica
				 String mensagemTrovaoAcertaNada = getResources().getString(R.string.mensagem_trovao_carta_aleatoria_nao_acerta_nada);
				 Toast t = Toast.makeText(this, mensagemTrovaoAcertaNada , Toast.LENGTH_LONG);
				 t.show();
				 this.fazerMascoteFicarZangadaPorUmTempo();
				 
				 ImageView imagemItem = (ImageView) findViewById(R.id.item1);
				 imagemItem.setImageResource(R.drawable.nenhumitem);
				 imagemItem.setClickable(false);
			 }
			 else
			 {
				 Random geraNumeroAleatorio = new Random();
				 boolean achouUmKanjiParaTirar = false;
				 int posicaoKanjiTirarEmKanjisDasCartasNaTela = -1;
				 
				 while(achouUmKanjiParaTirar == false && posicaoKanjiTirarEmKanjisDasCartasNaTela == -1)
				 {
					 int posicaoCartaAleatoria = geraNumeroAleatorio.nextInt(this.kanjisDasCartasNaTela.size());
					 KanjiTreinar kanjiDessaPosicao = this.kanjisDasCartasNaTela.get(posicaoCartaAleatoria);
					 
					 //peguei um kanji qualquer dos da tela. Mas sera que ele ja tinha virado dica antes?Se sim, nao posso escolhe-lo porque a carta dele ja ter sido tirada do jogo
					 boolean kanjiJaVirouDica = false;
					 
					 for(int i = 0; i < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); i++)
					 {
						 KanjiTreinar umKanjiQueJaVirouDica = this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(i);
						 
						 if((kanjiDessaPosicao.getKanji().compareTo(umKanjiQueJaVirouDica.getKanji()) == 0) &&
						    (kanjiDessaPosicao.getCategoriaAssociada().compareTo(umKanjiQueJaVirouDica.getCategoriaAssociada()) == 0))
						 {
							 kanjiJaVirouDica = true;
							 break;
						 }
					 }
					 
					 if(kanjiJaVirouDica == false)
					 {
						 //achamos um kanji p remover
						 achouUmKanjiParaTirar = true;
						 posicaoKanjiTirarEmKanjisDasCartasNaTela = posicaoCartaAleatoria;
					 }
				 }
				 
				 this.lancarTrovaoNaTelaNaCartaDeIndice(posicaoKanjiTirarEmKanjisDasCartasNaTela,true);
				 
				 //falta avisar ao adversario p lancar um trovao na mesma carta
				 String mensagemLancarTrovao = "item trovaotiracartaaleatoria indiceCartaRemovida=" + posicaoKanjiTirarEmKanjisDasCartasNaTela;
				 this.mandarMensagemMultiplayer(mensagemLancarTrovao);
				 
				 ImageView imagemItem = (ImageView) findViewById(R.id.item1);
				 imagemItem.setImageResource(R.drawable.nenhumitem);
				 imagemItem.setClickable(false);
			 }
			 
		 }
		 else if(nomeItemUsado.compareTo("parartempo") == 0)
		 {
			 this.realizarProcedimentoPararTempo(true);
			 
			 String mensagemUsouItemPararTempo = "item parartempo";
			 this.mandarMensagemMultiplayer(mensagemUsouItemPararTempo);
			 
			 ImageView imagemItem = (ImageView) findViewById(R.id.item2);
			 imagemItem.setImageResource(R.drawable.nenhumitem);
			 imagemItem.setClickable(false);
		 }
		 else if(nomeItemUsado.compareTo("misturarcartas") == 0)
		 {
			 this.misturarCartasEAvisarAoAdversario();
			 ImageView imagemItem = (ImageView) findViewById(R.id.item3);
			 imagemItem.setImageResource(R.drawable.nenhumitem);
			 imagemItem.setClickable(false);
		 }
		 else if(nomeItemUsado.compareTo("mudardica") == 0)
		 {
			 this.usarItemMudarDica();
			 ImageView imagemItem = (ImageView) findViewById(R.id.item4);
			 imagemItem.setImageResource(R.drawable.nenhumitem);
			 imagemItem.setClickable(false);
		 }
		 else if(nomeItemUsado.compareTo("doisx") == 0)
		 {
			 super.reproduzirSfx("dois_x");
			 fazerPlaquinhaUsuarioBalancarUmPouco();
			 this.usou2x = true;
			 findViewById(R.id.doisxpequeno).setVisibility(View.VISIBLE);
			 ImageView imagemItem = (ImageView) findViewById(R.id.item5);
			 imagemItem.setImageResource(R.drawable.nenhumitem);
			 imagemItem.setClickable(false);
		 }
		 else if(nomeItemUsado.compareTo("naoesperemais") == 0)
		 {
			 this.usouNaoEspereMais = true;
			 findViewById(R.id.naoesperemais).setVisibility(View.VISIBLE);
			 ImageView imagemItem = (ImageView) findViewById(R.id.item6);
			 imagemItem.setImageResource(R.drawable.nenhumitem);
			 imagemItem.setClickable(false);
		 }
		 else if(nomeItemUsado.compareTo("cartasdouradas") == 0)
		 {
			 this.realizarProcedimentoFazerCartasFicaremDouradas();
			 super.reproduzirSfx("cartas_douradas");
			 ImageView imagemItem = (ImageView) findViewById(R.id.item7);
			 imagemItem.setImageResource(R.drawable.nenhumitem);
			 imagemItem.setClickable(false);
		 }
		 else if(nomeItemUsado.compareTo("trovaotiracarta") == 0)
		 {
			ImageView item = (ImageView) findViewById(R.id.item8);
			item.setImageResource(R.drawable.trovaotiracarta);
			
			String escolha_uma_carta_para_lancar_trovao = getResources().getString(R.string.escolha_uma_carta_para_lancar_trovao);
			Toast t = Toast.makeText(this, escolha_uma_carta_para_lancar_trovao , Toast.LENGTH_LONG);
			t.show();
			
			this.usouTrovaoTiraCarta = true;
			item.setClickable(false);
		 }
		 else if(nomeItemUsado.compareTo("revivecarta") == 0)
		 {
			 ImageView item = (ImageView) findViewById(R.id.item9);
			 item.setImageResource(R.drawable.revivecarta);
			 
			 String escolha_uma_carta_para_reviver = getResources().getString(R.string.escolha_uma_carta_para_reviver);
			 Toast t = Toast.makeText(this, escolha_uma_carta_para_reviver , Toast.LENGTH_LONG);
			 t.show();
			 this.usouReviveCarta = true;
			 
			 //falta tornar todas as cartas clicaveis novamente p o usuario escolher uma carta p reviver
			 findViewById(R.id.karuta1_imageview).setClickable(true);
			 findViewById(R.id.karuta2_imageview).setClickable(true);
			 findViewById(R.id.karuta3_imageview).setClickable(true);
			 findViewById(R.id.karuta4_imageview).setClickable(true);
			 findViewById(R.id.karuta5_imageview).setClickable(true);
			 findViewById(R.id.karuta6_imageview).setClickable(true);
			 findViewById(R.id.karuta7_imageview).setClickable(true);
			 findViewById(R.id.karuta8_imageview).setClickable(true);
			 
			 item.setClickable(false);
		 }
		 
		 //falta tirar o item da linkedlist
		 for(int i = 0; i < this.itensAtuais.size(); i++)
		 {
			 String umItem = this.itensAtuais.get(i);
			 if(umItem.compareTo(nomeItemUsado) == 0)
			 {
				 itensAtuais.remove(i);
				 break;
			 }
		 }
	 }
	 else
	 {
		 if(this.usouReviveCarta == true)
		 {
			 String antes_disso = getResources().getString(R.string.antes_disso);
			 String escolha_uma_carta_para_reviver = getResources().getString(R.string.escolha_uma_carta_para_reviver);
			 String msgmToast = antes_disso + escolha_uma_carta_para_reviver;
			 Toast.makeText(this, msgmToast, Toast.LENGTH_SHORT).show();
		 }
		 else
		 {
			 String antes_disso = getResources().getString(R.string.antes_disso);
			 String escolha_uma_carta_para_lancar_trovao = getResources().getString(R.string.escolha_uma_carta_para_lancar_trovao);
			 String msgmToast = antes_disso + escolha_uma_carta_para_lancar_trovao;
			 Toast.makeText(this, msgmToast, Toast.LENGTH_SHORT).show();
		 }
	 }
 }
 
 /*lanca um trovao que torna uma das cartas na tela inclicavel. O indice vai de 0 a 7*/
 private void lancarTrovaoNaTelaNaCartaDeIndice(int indiceCartaTrovoada,boolean ehQuemUsouTrovao)
 {
	 TextView textoCartaTirada = null;
	 
	 if(indiceCartaTrovoada == 0)
	 {
		 cartaASerTirada = (ImageView) findViewById(R.id.karuta1_imageview);
		 textoCartaTirada = (TextView) findViewById(R.id.texto_karuta1);
	 }
	 else if(indiceCartaTrovoada == 1)
	 {
		 cartaASerTirada = (ImageView) findViewById(R.id.karuta2_imageview);
		 textoCartaTirada = (TextView) findViewById(R.id.texto_karuta2);
	 }
	 else if(indiceCartaTrovoada == 2)
	 {
		 cartaASerTirada = (ImageView) findViewById(R.id.karuta3_imageview);
		 textoCartaTirada = (TextView) findViewById(R.id.texto_karuta3);
	 }
	 else if(indiceCartaTrovoada == 3)
	 {
		 cartaASerTirada = (ImageView) findViewById(R.id.karuta4_imageview);
		 textoCartaTirada = (TextView) findViewById(R.id.texto_karuta4);
	 }
	 else if(indiceCartaTrovoada == 4)
	 {
		 cartaASerTirada = (ImageView) findViewById(R.id.karuta5_imageview);
		 textoCartaTirada = (TextView) findViewById(R.id.texto_karuta5);
	 }
	 else if(indiceCartaTrovoada == 5)
	 {
		 cartaASerTirada = (ImageView) findViewById(R.id.karuta6_imageview);
		 textoCartaTirada = (TextView) findViewById(R.id.texto_karuta6);
	 }
	 else if(indiceCartaTrovoada == 6)
	 {
		 cartaASerTirada = (ImageView) findViewById(R.id.karuta7_imageview);
		 textoCartaTirada = (TextView) findViewById(R.id.texto_karuta7);
	 }
	 else if(indiceCartaTrovoada == 7)
	 {
		 cartaASerTirada = (ImageView) findViewById(R.id.karuta8_imageview);
		 textoCartaTirada = (TextView) findViewById(R.id.texto_karuta8);
	 }
	 
	 
	 final AnimationDrawable animacaoTrovaoAcertaCarta = new AnimationDrawable();
	 int idImagemKarutaTrovao1 = getResources().getIdentifier("karutatrovao1", "drawable", getPackageName());
	 int idImagemKarutaTrovao2 = getResources().getIdentifier("karutatrovao2", "drawable", getPackageName());
	 int idImagemKarutaTrovao3 = getResources().getIdentifier("karutatrovao3", "drawable", getPackageName());
	 int idImagemKarutaTrovao4 = getResources().getIdentifier("karutatrovao4", "drawable", getPackageName());
	 int idImagemKarutaTrovao5 = getResources().getIdentifier("karutavazia", "drawable", getPackageName());
	 
	 animacaoTrovaoAcertaCarta.addFrame(getResources().getDrawable(idImagemKarutaTrovao1), 200);
	 animacaoTrovaoAcertaCarta.addFrame(getResources().getDrawable(idImagemKarutaTrovao2), 200);
	 animacaoTrovaoAcertaCarta.addFrame(getResources().getDrawable(idImagemKarutaTrovao3), 200);
	 animacaoTrovaoAcertaCarta.addFrame(getResources().getDrawable(idImagemKarutaTrovao4), 200);
	 animacaoTrovaoAcertaCarta.addFrame(getResources().getDrawable(idImagemKarutaTrovao5), 10);
	 
	 animacaoTrovaoAcertaCarta.setOneShot(true);
	 cartaASerTirada.setImageDrawable(animacaoTrovaoAcertaCarta);
	 
	 if(ehQuemUsouTrovao == true)
	 {
		 fazerPlaquinhaUsuarioBalancarUmPouco();
	 }
	 cartaASerTirada.post(new Runnable() {
		@Override
		public void run() {
			animacaoTrovaoAcertaCarta.start();
		}
	}); 
	 
	 super.reproduzirSfx("trovao");
	 
	cartaASerTirada.setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	textoCartaTirada.setText("");
	
	final int indiceCartaTrovoadaFinal = indiceCartaTrovoada;
	new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	fazerImageViewFicarEscuro(cartaASerTirada);
		            	//o verso deve ficar com a traducao em hiragana dela e seu kanji
		            	KanjiTreinar kanji = kanjisDasCartasNaTela.get(indiceCartaTrovoadaFinal);
		            	fazerTextoVersoDaCartaAparecer(kanji, indiceCartaTrovoadaFinal);
		            }
		        });
		    }
		}, 1000);
	
	KanjiTreinar kanjiRemovido = this.kanjisDasCartasNaTela.get(indiceCartaTrovoada);
	this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.add(kanjiRemovido);
	
	this.quantasCartasJaSairamDoJogo = this.quantasCartasJaSairamDoJogo + 1;
	
	this.usouTrovaoTiraCarta = false;
	//this.verificarSeJogadorRecebeUmItemAleatorio();
 }
 
 /*no caso de trovao carta nao aleatoria*/
 private void lancarTrovaoNaTelaNaCartaDeIndiceEAvisarAoAdversario(int indiceCarta)
 {
	 this.lancarTrovaoNaTelaNaCartaDeIndice(indiceCarta,true);
	 
	 //falta avisar ao adversario p lancar um trovao na mesma carta
	 String mensagemLancarTrovao = "item trovaotiracarta indiceCartaRemovida=" + indiceCarta;
	 this.mandarMensagemMultiplayer(mensagemLancarTrovao);
 }
 
 private void realizarProcedimentoPararTempo(boolean ehQuemUsouOItem)
 {
	 TextView textViewTempo = (TextView) findViewById(R.id.tempo);
	 textViewTempo.setTextColor(Color.RED);
	 findViewById(R.id.parartempopequeno).setVisibility(View.VISIBLE);
	 
	 this.tempoEstahParado = true;
	 
	 //as cartas nao vao ficar clicaveis por um tempo assim como quando um jogador erra uma carta
	 ImageView karuta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	 ImageView karuta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	 ImageView karuta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	 ImageView karuta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	 ImageView karuta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	 ImageView karuta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	 ImageView karuta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	 ImageView karuta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	 
	 karuta1.setClickable(false);
	 karuta2.setClickable(false);
	 karuta3.setClickable(false);
	 karuta4.setClickable(false);
	 karuta5.setClickable(false);
	 karuta6.setClickable(false);
	 karuta7.setClickable(false);
	 karuta8.setClickable(false);
	 
	 karuta1.setAlpha(128);
	 karuta2.setAlpha(128);
	 karuta3.setAlpha(128);
	 karuta4.setAlpha(128);
	 karuta5.setAlpha(128);
	 karuta6.setAlpha(128);
	 karuta7.setAlpha(128);
	 karuta8.setAlpha(128);
	 
	 if(ehQuemUsouOItem == true)
	 {
		 fazerPlaquinhaUsuarioBalancarUmPouco(); 
	 }
	 super.reproduzirSfx("parar_tempo");
	 new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	tempoEstahParado = false;
		            	TextView textViewTempo = (TextView) findViewById(R.id.tempo);
		           	 	textViewTempo.setTextColor(Color.BLACK);
		           	 	findViewById(R.id.parartempopequeno).setVisibility(View.INVISIBLE);
		                terminouEsperaUsuarioErrouCarta();
		            }
		        });
		    }
		}, 5000);
 }
 
 
 private void misturarCartasEAvisarAoAdversario()
 {
	 Collections.shuffle(this.kanjisDasCartasNaTela);
	 
	 final String[] kanjisEBarrasComCategoria = new String[this.kanjisDasCartasNaTela.size()]; //so preciso mudar as cartas na tela, mas a funcao recebe somente essa estrutura: au|cotidiano,me|corpo... 
	 
	 String mensagemParaOAdversario = "item misturarcartas kanjis="; //mensagem p alertar ao adversario a nova ordem dos kanjis
	 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
	 {
		 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(i);
		 kanjisEBarrasComCategoria[i] = umKanji.getKanji() + "|" + umKanji.getCategoriaAssociada();
		 
		 //para o adversario, vamos enviar apenas os ids das categorias, nao as categorias em si
		 int idCategoria = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(umKanji.getCategoriaAssociada());
		 
		 mensagemParaOAdversario = mensagemParaOAdversario + umKanji.getKanji() + "|" + String.valueOf(idCategoria);
		 
	     mensagemParaOAdversario = mensagemParaOAdversario + ";"; //tanta faz se no item final tiver um ;, isso foi testado
	 }
	 
	 this.mandarMensagemMultiplayer(mensagemParaOAdversario);
	 
	 TextView textoKaruta1 = (TextView) findViewById(R.id.texto_karuta1);
	 textoKaruta1.setText("");
	 TextView textoKaruta2 = (TextView) findViewById(R.id.texto_karuta2);
	 textoKaruta2.setText("");
	 TextView textoKaruta3 = (TextView) findViewById(R.id.texto_karuta3);
	 textoKaruta3.setText("");
	 TextView textoKaruta4 = (TextView) findViewById(R.id.texto_karuta4);
	 textoKaruta4.setText("");
	 TextView textoKaruta5 = (TextView) findViewById(R.id.texto_karuta5);
	 textoKaruta5.setText("");
	 TextView textoKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
	 textoKaruta6.setText("");
	 TextView textoKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
	 textoKaruta7.setText("");
	 TextView textoKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
	 textoKaruta8.setText("");
	 
	 ImageView imageViewCarta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	 ImageView imageViewCarta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	 ImageView imageViewCarta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	 ImageView imageViewCarta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	 ImageView imageViewCarta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	 ImageView imageViewCarta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	 ImageView imageViewCarta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	 ImageView imageViewCarta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	 
	 
	 imageViewCarta1.setClickable(false);
	 imageViewCarta2.setClickable(false);
	 imageViewCarta3.setClickable(false);
	 imageViewCarta4.setClickable(false);
	 imageViewCarta5.setClickable(false);
	 imageViewCarta6.setClickable(false);
	 imageViewCarta7.setClickable(false);
	 imageViewCarta8.setClickable(false);
	 
	 final AnimationDrawable animacaoPoofCarta = new AnimationDrawable();
	 int idImagemKarutaPoof1 = getResources().getIdentifier("karutapoof1", "drawable", getPackageName());
	 int idImagemKarutaPoof2 = getResources().getIdentifier("karutapoof2", "drawable", getPackageName());
	 int idImagemKarutaPoof3 = getResources().getIdentifier("karutapoof3", "drawable", getPackageName());
	 int idImagemKarutaPoof4 = getResources().getIdentifier("karutapoof4", "drawable", getPackageName());
	 int idImagemKarutaVazia = getResources().getIdentifier("karutavazia", "drawable", getPackageName());
	 
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaPoof1), 200);
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaPoof2), 200);
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaPoof3), 200);
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaPoof4), 200);
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaVazia), 200);
	 
	 animacaoPoofCarta.setOneShot(true);
	 imageViewCarta1.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta2.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta3.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta4.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta5.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta6.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta7.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta8.setImageDrawable(animacaoPoofCarta);
	 
	 fazerPlaquinhaUsuarioBalancarUmPouco();
	 fazerTextoVersoDaCartaDesaparecer(0);
	 fazerTextoVersoDaCartaDesaparecer(1);
	 fazerTextoVersoDaCartaDesaparecer(2);
	 fazerTextoVersoDaCartaDesaparecer(3);
	 fazerTextoVersoDaCartaDesaparecer(4);
	 fazerTextoVersoDaCartaDesaparecer(5);
	 fazerTextoVersoDaCartaDesaparecer(6);
	 fazerTextoVersoDaCartaDesaparecer(7);
	 
	 imageViewCarta1.post(new Runnable() {
		@Override
		public void run() {
			animacaoPoofCarta.start();
		}
	 	});
	 imageViewCarta2.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		});
	 imageViewCarta3.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		});
	 imageViewCarta4.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		}); 
	 imageViewCarta5.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		}); 
	 imageViewCarta6.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		});
	 imageViewCarta7.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		}); 
	 imageViewCarta8.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		}); 
	 
	 
	 new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	mudarCartasNaTela(kanjisEBarrasComCategoria); //so irei realizar essa funcao apos a animacao ter sido realizada em cada uma das cartas
		            	
		            	if(kanjisDasCartasNaTelaQueJaSeTornaramDicas.size() < 8)
		        		{
		            		//ANTES AQUI EM CIMA ESTAVA O KANJISDASCARTASDATELAQUESETORNARAMDICAS. FAZ SENTIDO? ACHO Q NAO
		        			//menos de 8 cartas, por isso algumas devem ficar vazias
		        			tornarORestoDasCartasNaTelaVazias();
		        		}
		            	
		            }
		        });
		    }
		}, 1200);
	 
 }
 
 /*funcao do usuario que recebeu a mensagem p mudar as cartas na tela.
  * As linkedlists tem o mesmo tamanho*/
 private void misturarCartasRecebeuCartasOutroUsuario(LinkedList<String> textoKanjisNovos, LinkedList<String> categoriasKanjisNovos)
 {
	 //primeiro iremos pegar os kanjis com base nas categorias e texto
	 LinkedList<KanjiTreinar> novosKanjis = new LinkedList<KanjiTreinar>();
	 
	 for(int i = 0; i < textoKanjisNovos.size(); i++)
	 {
		 String umTextoKanji = textoKanjisNovos.get(i);
		 String umaCategoria = categoriasKanjisNovos.get(i);
		 
		 for(int j = 0; j < this.kanjisDasCartasNaTela.size(); j++)
		 {
			 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(j);
			 
			 if(umKanji.getKanji().compareTo(umTextoKanji) == 0 && umKanji.getCategoriaAssociada().compareTo(umaCategoria) == 0)
			 {
				 novosKanjis.add(umKanji);
				 break;
			 }
		 }
	 }
	 
	 //agora podemos tornar a lista de kanjis das cartas na tela essa nova lista
	 this.kanjisDasCartasNaTela.clear();
	 
	 for(int k = 0; k < novosKanjis.size(); k++)
	 {
		 KanjiTreinar umKanji = novosKanjis.get(k);
		 this.kanjisDasCartasNaTela.add(umKanji);
	 }
	 
	 
	 //agora podemos comecar a mostrar a mistura das cartas na tela
	 final String[] kanjisEBarrasComCategoria = new String[this.kanjisDasCartasNaTela.size()]; //so preciso mudar as cartas na tela, mas a funcao recebe somente essa estrutura: au|cotidiano,me|corpo... 
	 
	 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
	 {
		 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(i);
		 kanjisEBarrasComCategoria[i] = umKanji.getKanji() + "|" + umKanji.getCategoriaAssociada();
		 
	 }
	 
	 
	 TextView textoKaruta1 = (TextView) findViewById(R.id.texto_karuta1);
	 textoKaruta1.setText("");
	 TextView textoKaruta2 = (TextView) findViewById(R.id.texto_karuta2);
	 textoKaruta2.setText("");
	 TextView textoKaruta3 = (TextView) findViewById(R.id.texto_karuta3);
	 textoKaruta3.setText("");
	 TextView textoKaruta4 = (TextView) findViewById(R.id.texto_karuta4);
	 textoKaruta4.setText("");
	 TextView textoKaruta5 = (TextView) findViewById(R.id.texto_karuta5);
	 textoKaruta5.setText("");
	 TextView textoKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
	 textoKaruta6.setText("");
	 TextView textoKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
	 textoKaruta7.setText("");
	 TextView textoKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
	 textoKaruta8.setText("");
	 
	 ImageView imageViewCarta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	 ImageView imageViewCarta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	 ImageView imageViewCarta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	 ImageView imageViewCarta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	 ImageView imageViewCarta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	 ImageView imageViewCarta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	 ImageView imageViewCarta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	 ImageView imageViewCarta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	 
	 
	 imageViewCarta1.setClickable(false);
	 imageViewCarta2.setClickable(false);
	 imageViewCarta3.setClickable(false);
	 imageViewCarta4.setClickable(false);
	 imageViewCarta5.setClickable(false);
	 imageViewCarta6.setClickable(false);
	 imageViewCarta7.setClickable(false);
	 imageViewCarta8.setClickable(false);
	 
	 final AnimationDrawable animacaoPoofCarta = new AnimationDrawable();
	 int idImagemKarutaPoof1 = getResources().getIdentifier("karutapoof1", "drawable", getPackageName());
	 int idImagemKarutaPoof2 = getResources().getIdentifier("karutapoof2", "drawable", getPackageName());
	 int idImagemKarutaPoof3 = getResources().getIdentifier("karutapoof3", "drawable", getPackageName());
	 int idImagemKarutaPoof4 = getResources().getIdentifier("karutapoof4", "drawable", getPackageName());
	 int idImagemKarutaVazia = getResources().getIdentifier("karutavazia", "drawable", getPackageName());
	 
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaPoof1), 200);
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaPoof2), 200);
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaPoof3), 200);
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaPoof4), 200);
	 animacaoPoofCarta.addFrame(getResources().getDrawable(idImagemKarutaVazia), 200);
	 
	 animacaoPoofCarta.setOneShot(true);
	 imageViewCarta1.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta2.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta3.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta4.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta5.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta6.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta7.setImageDrawable(animacaoPoofCarta);
	 imageViewCarta8.setImageDrawable(animacaoPoofCarta);
	 
	 fazerTextoVersoDaCartaDesaparecer(0);
	 fazerTextoVersoDaCartaDesaparecer(1);
	 fazerTextoVersoDaCartaDesaparecer(2);
	 fazerTextoVersoDaCartaDesaparecer(3);
	 fazerTextoVersoDaCartaDesaparecer(4);
	 fazerTextoVersoDaCartaDesaparecer(5);
	 fazerTextoVersoDaCartaDesaparecer(6);
	 fazerTextoVersoDaCartaDesaparecer(7);
	 
	 imageViewCarta1.post(new Runnable() {
		@Override
		public void run() {
			animacaoPoofCarta.start();
		}
	 	});
	 imageViewCarta2.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		});
	 imageViewCarta3.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		});
	 imageViewCarta4.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		}); 
	 imageViewCarta5.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		}); 
	 imageViewCarta6.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		});
	 imageViewCarta7.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		}); 
	 imageViewCarta8.post(new Runnable() {
			@Override
			public void run() {
				animacaoPoofCarta.start();
			}
		});
	 
	 
	 new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	mudarCartasNaTela(kanjisEBarrasComCategoria); //so irei realizar essa funcao apos a animacao ter sido realizada em cada uma das cartas
		            	if(kanjisDasCartasNaTelaQueJaSeTornaramDicas.size() < 8)
		        		{
		            		//ANTES AQUI EM CIMA ESTAVA O KANJISDASCARTASDATELAQUESETORNARAMDICAS. FAZ SENTIDO? ACHO Q NAO
		        			//menos de 8 cartas, por isso algumas devem ficar vazias
		        			tornarORestoDasCartasNaTelaVazias();
		        		}
		            	
		            }
		        });
		    }
		}, 1200);
 }
 
 /*caso uma nova dica possa ser criada, a dica atual mudarah. */
 private void usarItemMudarDica()
 {
	 if(kanjisDasCartasNaTela.size() == kanjisDasCartasNaTelaQueJaSeTornaramDicas.size())
	 {
		 //sinto muito, mas nao ha dicas diferentes que possam ser geradas. A ultima dica ja esta presente
		 String mensagemErroMudarDica = getResources().getString(R.string.erro_mudar_dica);
		 Toast.makeText(this, mensagemErroMudarDica, Toast.LENGTH_SHORT).show();
		 fazerMascoteFicarZangadaPorUmTempo();
	 }
	 else
	 { 
		 //acharemos uma nova dica que seja diferente da anterior
		 //primeiro, precisamos remover o kanji da dica da linkedlist chamada kanjisDasCartasNaTelaQueJaSeTornaramDicas
		 
		 this.tirarKanjiDicaAtualDeCartasQueJaViraramDicasEPalavrasJogadas();
		 
		 //agora vamos gerar um novo kanji da dica diferente da anterior
		 
		 LinkedList<KanjiTreinar> kanjisQueAindaNaoViraramDicas = new LinkedList<KanjiTreinar>();
		 
		 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
		 {
			 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(i);
			 
			 boolean kanjiJaVirouDica = false;
			 for(int j = 0; j < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); j++)
			 {
				 KanjiTreinar umKanjiQueVirouDica = this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(j);
				 
				 if(umKanjiQueVirouDica.getKanji().compareTo(umKanji.getKanji()) == 0)
				 {
					 kanjiJaVirouDica = true;
					 break;
				 }
			 }
			 
			 if(kanjiJaVirouDica == false 
					 && (this.ehMesmoKanji(umKanji, kanjiDaDica) == false))
			 {
				 //agora tb n queremos que esse kanji seja a antiga dica!!!!
				 kanjisQueAindaNaoViraramDicas.add(umKanji);
			 }
		 }
		 
		 Random geraNumAleatorio = new Random();
		 int tamanhoKanjisQueNaoViraramDicas = kanjisQueAindaNaoViraramDicas.size();
		 
		 int indiceKanjiDaDica = geraNumAleatorio.nextInt(tamanhoKanjisQueNaoViraramDicas);
		 
		 KanjiTreinar umKanji = kanjisQueAindaNaoViraramDicas.get(indiceKanjiDaDica);
		 this.kanjiDaDica = umKanji;
		 this.palavrasJogadas.add(kanjiDaDica);
		 
		 int idKanjiDaDica = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(this.kanjiDaDica.getCategoriaAssociada());
		 String mensagem = "item mudardica kanjiDaDica=" + this.kanjiDaDica.getKanji() + "|" + idKanjiDaDica;
		 this.mandarMensagemMultiplayer(mensagem);
		 
		 //this.alterarTextoDicaComBaseNoKanjiDaDica(); //vamos realizar um poof antes de mudar esse texto
		 
		 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.add(umKanji);
		 
		 this.realizarProcedimentoMudandoDicaAtual(true);
			 
	 }
 }
 
 private boolean ehMesmoKanji(KanjiTreinar k1, KanjiTreinar k2)
 {
	 if((k1.getKanji().compareTo(k2.getKanji()) == 0) && (k1.getCategoriaAssociada().compareTo(k2.getCategoriaAssociada()) == 0))
	 {
		 return true;
	 }
	 else
	 {
		 return false;
	 }
 }
 
 private void tirarKanjiDicaAtualDeCartasQueJaViraramDicasEPalavrasJogadas()
 {
	//primeiro, precisamos remover o kanji da dica da linkedlist chamada kanjisDasCartasNaTelaQueJaSeTornaramDicas
	 for(int g = 0; g < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); g++)
	 {
		 KanjiTreinar umKanjiJaVirouDica = this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(g);
		 
		 if((umKanjiJaVirouDica.getKanji().compareTo(kanjiDaDica.getKanji()) == 0) 
			 && (umKanjiJaVirouDica.getCategoriaAssociada().compareTo(kanjiDaDica.getCategoriaAssociada()) == 0))
		 {
			 //achamos o kanji da dica! Falta remove-lo
			 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.remove(g);
			 break;	 
		 } 
	 }
	 
	 //agora precisamos remove-lo das palavras jogadas tb
	 
	 for(int h = 0; h < this.palavrasJogadas.size(); h++)
	 {
		 KanjiTreinar umKanjiJaJogado = this.palavrasJogadas.get(h);
		 
		 if((umKanjiJaJogado.getKanji().compareTo(kanjiDaDica.getKanji()) == 0) 
			 && (umKanjiJaJogado.getCategoriaAssociada().compareTo(kanjiDaDica.getCategoriaAssociada()) == 0))
		 {
			 //achamos o kanji da dica! Falta remove-lo
			 this.palavrasJogadas.remove(h);
			 break;	 
		 } 
	 }
 }
 
 //a dica atual ficarah com "..." por alguns segundos ate mudar
 private void realizarProcedimentoMudandoDicaAtual(boolean ehQuemUsouItem)
 {
	//primeiro faremos a mascote ficar pensativa...
	 final ImageView imageViewMascote = (ImageView) findViewById(R.id.mascote);
	 imageViewMascote.setImageResource(R.drawable.mascote_pensativa);
	 
	 TextView textoDicaKanji = (TextView) findViewById(R.id.dica_kanji);
	 TextView textoDicaKanjiTraducao = (TextView) findViewById(R.id.dica_kanji_traducao);
	 String mensagemDicaMudando = getResources().getString(R.string.palavra_esta_mudando);
	 textoDicaKanji.setText(mensagemDicaMudando);
	 textoDicaKanjiTraducao.setText("");
	 
	 super.reproduzirSfx("mudar_dica");
	 
	 //durante essa espera, as cartas n devem ser clicaveis
	 ImageView imageViewCarta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	 ImageView imageViewCarta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	 ImageView imageViewCarta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	 ImageView imageViewCarta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	 ImageView imageViewCarta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	 ImageView imageViewCarta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	 ImageView imageViewCarta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	 ImageView imageViewCarta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	 
	 
	 imageViewCarta1.setClickable(false);
	 imageViewCarta2.setClickable(false);
	 imageViewCarta3.setClickable(false);
	 imageViewCarta4.setClickable(false);
	 imageViewCarta5.setClickable(false);
	 imageViewCarta6.setClickable(false);
	 imageViewCarta7.setClickable(false);
	 imageViewCarta8.setClickable(false);
	 
	 final String[] kanjisEBarrasComCategoria = new String[this.kanjisDasCartasNaTela.size()]; //so preciso disso p tornar as cartas clicaveis novamente e somente aquelas que deveriam ser clicaveis
	 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
	 {
		 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(i);
		 kanjisEBarrasComCategoria[i] = umKanji.getKanji() + "|" + umKanji.getCategoriaAssociada();
		 
	 }
	 
	 if(ehQuemUsouItem == true)
	 {
		 fazerPlaquinhaUsuarioBalancarUmPouco(); 
	 }
	 new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	mudarCartasNaTela(kanjisEBarrasComCategoria); //so irei realizar essa funcao apos a animacao ter sido realizada
		            	if(kanjisDasCartasNaTelaQueJaSeTornaramDicas.size() < 8)
		        		{
		        			//menos de 8 cartas, por isso algumas devem ficar vazias
		        			tornarORestoDasCartasNaTelaVazias();
		        		}
		            	alterarTextoDicaComBaseNoKanjiDaDica();
		            	//faremos a mascote ficar normal...
		            	final ImageView imageViewMascote = (ImageView) findViewById(R.id.mascote);
		            	imageViewMascote.setImageResource(R.drawable.mascote);
		            }
		        });
		    }
		}, 3000);
 }
 
 /*o usuario usou o item para reviver uma carta. O numero da carta vai de 1 ate 8*/
 private void realizarProcedimentoReviverCarta(int numeroCarta, boolean ehUsuarioQueRecebeuMensagem)
 {
	 KanjiTreinar kanjiRevivido = this.kanjisDasCartasNaTela.get(numeroCarta - 1);
	 
	 //temos de tirar ele das cartas que ja viraram dicas
	 //mas o usuario pode dar uma de engracadinho e tentar reviver uma carta que ta viva!
	 //o usuario n pode reviver carta ja viva ou a da dicaatual
	 boolean cartaFoiRemovidaDaLinkedListTornaramDicas = false;
	 String textoCartaRevivida = "";
	 for(int i = 0; i < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); i++)
	 {
		 KanjiTreinar umKanjiQueJaVirouDica = this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(i);
		 
		 if(this.ehMesmoKanji(umKanjiQueJaVirouDica, kanjiRevivido) == true &&
				 this.ehMesmoKanji(this.kanjiDaDica, kanjiRevivido) == false)
		 {
			 //achamos o kanji que era p reviver! Ele n pode ser o kanji da dica
			 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.remove(i);
			 textoCartaRevivida = umKanjiQueJaVirouDica.getKanji();
			 cartaFoiRemovidaDaLinkedListTornaramDicas = true;
			 break;
		 }
	 }
	 
	 if(cartaFoiRemovidaDaLinkedListTornaramDicas == false)
	 {
		 //dizer ao usuario que essa carta nao pode ser revivida. O usuario q recebeu a mensagem da carta ser revivida n deve receber esse informativo
		 if(ehUsuarioQueRecebeuMensagem == false)
		 {
			 String mensagemErroReviverCarta = getResources().getString(R.string.erro_reviver_carta);
			 Toast t = Toast.makeText(this, mensagemErroReviverCarta, Toast.LENGTH_LONG);
			 t.show();
			 fazerMascoteFicarZangadaPorUmTempo();
			 /*TextView textViewFalaMascote = (TextView) findViewById(R.id.dica_kanji);
			 String fraseAtualMascote = String.valueOf(textViewFalaMascote.getText());
			 mascoteFalaFrasePor4Segundos(mensagemErroReviverCarta, fraseAtualMascote);*/
		 } 
	 }
	 else
	 {
		 //Falta mudar a figura da carta para nao ser mais a imagem de carta removida
		 ImageView imageViewCartaRevivida;
		 TextView textViewCartaRevivida;
		 int posicaoDaCartaRevivida = 0;
		 if(numeroCarta == 1)
		 {
			 imageViewCartaRevivida = (ImageView)findViewById(R.id.karuta1_imageview);
			 textViewCartaRevivida = (TextView)findViewById(R.id.texto_karuta1);
			 posicaoDaCartaRevivida = 0;
		 }
		 else if(numeroCarta == 2)
		 {
			 imageViewCartaRevivida = (ImageView)findViewById(R.id.karuta2_imageview);
			 textViewCartaRevivida = (TextView)findViewById(R.id.texto_karuta2);
			 posicaoDaCartaRevivida = 1;
		 }
		 else if(numeroCarta == 3)
		 {
			 imageViewCartaRevivida = (ImageView)findViewById(R.id.karuta3_imageview);
			 textViewCartaRevivida = (TextView)findViewById(R.id.texto_karuta3);
			 posicaoDaCartaRevivida = 2;
		 }
		 else if(numeroCarta == 4)
		 {
			 imageViewCartaRevivida = (ImageView)findViewById(R.id.karuta4_imageview);
			 textViewCartaRevivida = (TextView)findViewById(R.id.texto_karuta4);
			 posicaoDaCartaRevivida = 3;
		 }
		 else if(numeroCarta == 5)
		 {
			 imageViewCartaRevivida = (ImageView)findViewById(R.id.karuta5_imageview);
			 textViewCartaRevivida = (TextView)findViewById(R.id.texto_karuta5);
			 posicaoDaCartaRevivida = 4;
		 }
		 else if(numeroCarta == 6)
		 {
			 imageViewCartaRevivida = (ImageView)findViewById(R.id.karuta6_imageview);
			 textViewCartaRevivida = (TextView)findViewById(R.id.texto_karuta6);
			 posicaoDaCartaRevivida = 5;
		 }
		 else if(numeroCarta == 7)
		 {
			 imageViewCartaRevivida = (ImageView)findViewById(R.id.karuta7_imageview);
			 textViewCartaRevivida = (TextView)findViewById(R.id.texto_karuta7);
			 posicaoDaCartaRevivida = 6;
		 }
		 else
		 {
			 imageViewCartaRevivida = (ImageView)findViewById(R.id.karuta8_imageview);
			 textViewCartaRevivida = (TextView)findViewById(R.id.texto_karuta8);
			 posicaoDaCartaRevivida = 7;
		 }
		
		 //falta realizar a animacao de carta sendo revivida
		 this.realizarAnimacaoCartaRevivida(imageViewCartaRevivida, textViewCartaRevivida, textoCartaRevivida,posicaoDaCartaRevivida,ehUsuarioQueRecebeuMensagem);
	 } 
 }
 
 private void realizarAnimacaoCartaRevivida(ImageView imageViewCartaRevivida, TextView textViewCartaRevivida, String textoCartaRevivida, int posicaoDaCarta,boolean ehUsuarioQueRecebeuMensagem)
 {
	 final TextView textViewCartaRevividaFinal = textViewCartaRevivida;
	 final ImageView imageViewCartaRevividaFinal = imageViewCartaRevivida;
	 final String textoCartaRevividaFinal = textoCartaRevivida; 
	 
	 final AnimationDrawable animacaoReviveCarta = new AnimationDrawable();
	 int idImagemKarutaRevive1 = getResources().getIdentifier("karutarevive1", "drawable", getPackageName());
	 int idImagemKarutaRevive2 = getResources().getIdentifier("karutarevive2", "drawable", getPackageName());
	 int idImagemKarutaRevive3 = getResources().getIdentifier("karutarevive3", "drawable", getPackageName());
	 int idImagemKarutaVazia = getResources().getIdentifier("karutavazia", "drawable", getPackageName());
	 
	 animacaoReviveCarta.addFrame(getResources().getDrawable(idImagemKarutaRevive1), 200);
	 animacaoReviveCarta.addFrame(getResources().getDrawable(idImagemKarutaRevive2), 200);
	 animacaoReviveCarta.addFrame(getResources().getDrawable(idImagemKarutaRevive3), 200);
	 animacaoReviveCarta.addFrame(getResources().getDrawable(idImagemKarutaVazia), 200);
	 
	 animacaoReviveCarta.setOneShot(true);
	 imageViewCartaRevivida.setImageDrawable(animacaoReviveCarta);
	 
	 //super.reproduzirSfx("reviver_carta");
	 
	 //parte que faz o verso da carta desaparecer
	 textViewCartaRevividaFinal.setVisibility(View.VISIBLE);
	 fazerTextoVersoDaCartaDesaparecer(posicaoDaCarta);
	 textViewCartaRevividaFinal.setText("");
	 
	 imageViewCartaRevivida.post(new Runnable() {
		@Override
		public void run() {
			animacaoReviveCarta.start();
		}
	 	});
	 
	 super.reproduzirSfx("reviver_carta");
	 if(ehUsuarioQueRecebeuMensagem == false)
	 {
		 //eh o usuario que usou o item
		 fazerPlaquinhaUsuarioBalancarUmPouco();
	 }
	 final int posicaoCartaFinal = posicaoDaCarta;
	 new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		       		  //FALTA TORNAR A CARTA CLICAVEL E AS OUTRAS NAO. posso fazer isso com a funcao abaixo
		       		  terminouEsperaUsuarioErrouCarta();
		       		  fazerCartaVoltarACorNormal(imageViewCartaRevividaFinal,posicaoCartaFinal);
		       		  colocarTextoVerticalNaCarta(textViewCartaRevividaFinal, textoCartaRevividaFinal);
		       		  
		            }
		        });
		    }
		}, 1000);
 }
 
 /*faz carta nessa posicao ficar proibida. Chamada quando o usuario erra uma carta e usou o item naoesperemais Posicao vai de 1 a 8*/
 private void proibirCartaNaPosicao(int posicao)
 {
	 this.palavrasErradas.add(this.kanjisDasCartasNaTela.get(posicao - 1));
	 this.qualCartaEstaProibida = posicao;
	 ImageView imageViewCartaErrada;
	 TextView textViewCartaErrada;
	 
	 if(posicao == 1)
	 {
		 imageViewCartaErrada = (ImageView) findViewById(R.id.karuta1_imageview);
		 textViewCartaErrada = (TextView) findViewById(R.id.texto_karuta1);
	 }
	 else if(posicao == 2)
	 {
		 imageViewCartaErrada = (ImageView) findViewById(R.id.karuta2_imageview);
		 textViewCartaErrada = (TextView) findViewById(R.id.texto_karuta2);
	 }
	 else if(posicao == 3)
	 {
		 imageViewCartaErrada = (ImageView) findViewById(R.id.karuta3_imageview);
		 textViewCartaErrada = (TextView) findViewById(R.id.texto_karuta3);
	 }
	 else if(posicao == 4)
	 {
		 imageViewCartaErrada = (ImageView) findViewById(R.id.karuta4_imageview);
		 textViewCartaErrada = (TextView) findViewById(R.id.texto_karuta4);
	 }
	 else if(posicao == 5)
	 {
		 imageViewCartaErrada = (ImageView) findViewById(R.id.karuta5_imageview);
		 textViewCartaErrada = (TextView) findViewById(R.id.texto_karuta5);
	 }
	 else if(posicao == 6)
	 {
		 imageViewCartaErrada = (ImageView) findViewById(R.id.karuta6_imageview);
		 textViewCartaErrada = (TextView) findViewById(R.id.texto_karuta6);
	 }
	 else if(posicao == 7)
	 {
		 imageViewCartaErrada = (ImageView) findViewById(R.id.karuta7_imageview);
		 textViewCartaErrada = (TextView) findViewById(R.id.texto_karuta7);
	 }
	 else 
	 {
		 imageViewCartaErrada = (ImageView) findViewById(R.id.karuta8_imageview);
		 textViewCartaErrada = (TextView) findViewById(R.id.texto_karuta8);
	 }
	 
	 imageViewCartaErrada.setImageResource(R.drawable.karutavaziaproibida);
	 imageViewCartaErrada.setClickable(false);
	 imageViewCartaErrada.setAlpha(128);
	 textViewCartaErrada.setVisibility(View.INVISIBLE);
	 
	 findViewById(R.id.naoesperemais).setVisibility(View.INVISIBLE);
	 this.usouNaoEspereMais = false;
	 
	 super.reproduzirSfx("nao_espere_mais");
	 fazerPlaquinhaUsuarioBalancarUmPouco();
 }
 
 private void fazerCartaProibidaVoltarAoNormal()
 {
	 if(this.qualCartaEstaProibida != 0)
	 {
		 //existe uma carta proibida
		 ImageView imageViewCartaProibida;
		 TextView textViewCartaProibida;
		 if(this.qualCartaEstaProibida == 1)
		 {
			 imageViewCartaProibida = (ImageView) findViewById(R.id.karuta1_imageview);
			 textViewCartaProibida = (TextView) findViewById(R.id.texto_karuta1);
		 }
		 else if(this.qualCartaEstaProibida == 2)
		 {
			 imageViewCartaProibida = (ImageView) findViewById(R.id.karuta2_imageview);
			 textViewCartaProibida = (TextView) findViewById(R.id.texto_karuta2);
		 }
		 else if(this.qualCartaEstaProibida == 3)
		 {
			 imageViewCartaProibida = (ImageView) findViewById(R.id.karuta3_imageview);
			 textViewCartaProibida = (TextView) findViewById(R.id.texto_karuta3);
		 }
		 else if(this.qualCartaEstaProibida == 4)
		 {
			 imageViewCartaProibida = (ImageView) findViewById(R.id.karuta4_imageview);
			 textViewCartaProibida = (TextView) findViewById(R.id.texto_karuta4);
		 }
		 else if(this.qualCartaEstaProibida == 5)
		 {
			 imageViewCartaProibida = (ImageView) findViewById(R.id.karuta5_imageview);
			 textViewCartaProibida = (TextView) findViewById(R.id.texto_karuta5);
		 }
		 else if(this.qualCartaEstaProibida == 6)
		 {
			 imageViewCartaProibida = (ImageView) findViewById(R.id.karuta6_imageview);
			 textViewCartaProibida = (TextView) findViewById(R.id.texto_karuta6);
		 }
		 else if(this.qualCartaEstaProibida == 7)
		 {
			 imageViewCartaProibida = (ImageView) findViewById(R.id.karuta7_imageview);
			 textViewCartaProibida = (TextView) findViewById(R.id.texto_karuta7);
		 }
		 else
		 {
			 imageViewCartaProibida = (ImageView) findViewById(R.id.karuta8_imageview);
			 textViewCartaProibida = (TextView) findViewById(R.id.texto_karuta8);
		 }
		 
		 imageViewCartaProibida.setClickable(true);
		 imageViewCartaProibida.setAlpha(255);
		 this.colorirBordaDaCartaDeAcordoComCategoria(this.qualCartaEstaProibida - 1);
		 
		 textViewCartaProibida.setVisibility(View.VISIBLE);
		 this.qualCartaEstaProibida = 0;
	 }
 }
 
 private void fazerCartasDouradasVoltaremAoNormal()
 {
	 //quando alguem usa cartas douradas e o adversario eh quem acerta a carta, as douradas viram normais,
	 //mas uma das douradas era a da dica antiga, certo? Ela n deveria virar normal, ela deveria virar o verso.
	 //Por isso, essa funcao eh chamada antes de a carta com a dica atual mudar e se achar a carta da dica, deve fazer ela virar verso
	 if(this.quaisCartasEstaoDouradas != null && this.quaisCartasEstaoDouradas.size() > 0)
	 {
		 for(int i = 0; i < this.quaisCartasEstaoDouradas.size(); i++)
		 {
			 int posicaoUmaCartaDourada = this.quaisCartasEstaoDouradas.get(i);
			 
			 ImageView imageViewCartaVoltaAoNormal;
			 
			 if(posicaoUmaCartaDourada == 1)
			 {
				 imageViewCartaVoltaAoNormal = (ImageView) findViewById(R.id.karuta1_imageview);
			 }
			 else if(posicaoUmaCartaDourada == 2)
			 {
				 imageViewCartaVoltaAoNormal = (ImageView) findViewById(R.id.karuta2_imageview);
			 }
			 else if(posicaoUmaCartaDourada == 3)
			 {
				 imageViewCartaVoltaAoNormal = (ImageView) findViewById(R.id.karuta3_imageview);
			 }
			 else if(posicaoUmaCartaDourada == 4)
			 {
				 imageViewCartaVoltaAoNormal = (ImageView) findViewById(R.id.karuta4_imageview);
			 }
			 else if(posicaoUmaCartaDourada == 5)
			 {
				 imageViewCartaVoltaAoNormal = (ImageView) findViewById(R.id.karuta5_imageview);
			 }
			 else if(posicaoUmaCartaDourada == 6)
			 {
				 imageViewCartaVoltaAoNormal = (ImageView) findViewById(R.id.karuta6_imageview);
			 }
			 else if(posicaoUmaCartaDourada == 7)
			 {
				 imageViewCartaVoltaAoNormal = (ImageView) findViewById(R.id.karuta7_imageview);
			 }
			 else
			 {
				 imageViewCartaVoltaAoNormal = (ImageView) findViewById(R.id.karuta8_imageview);
			 }
			 
			 KanjiTreinar kanjiDaCartaDourada = this.kanjisDasCartasNaTela.get(posicaoUmaCartaDourada - 1);
			 if((this.kanjiDaDica != null) &&
					 (kanjiDaCartaDourada.getKanji().compareTo(this.kanjiDaDica.getKanji()) == 0) && 
					 (kanjiDaCartaDourada.getCategoriaAssociada().compareTo(this.kanjiDaDica.getCategoriaAssociada()) == 0))
			 {
				 //era p a carta ficar verso, nao normal
				 imageViewCartaVoltaAoNormal.setImageResource(R.drawable.karutaversocompeticao);
			 }
			 else
			 {
				 //pode ser que o usuario estivesse com cartas douradas e,de repente, o adversario lancou o trovao. Vamos ver se a carta ainda estah no jogo
				 boolean cartaAindaNoJogo = true;
				 
				 for(int j = 0; j < kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); j++)
				 {
					 KanjiTreinar umKanjiCartaNaTelaJaVirouDica =
							 kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(j);
					 if(umKanjiCartaNaTelaJaVirouDica.getKanji().compareTo(kanjiDaCartaDourada.getKanji()) == 0)
					 {
						 cartaAindaNoJogo = false;
					 }
				 }
				 
				 if(cartaAindaNoJogo == true)
				 {
					 this.colorirBordaDaCartaDeAcordoComCategoria(posicaoUmaCartaDourada - 1); 
				 }
				 else
				 {
					 imageViewCartaVoltaAoNormal.setImageResource(R.drawable.karutaversocompeticao);
				 }
			 }
		 }
		 this.quaisCartasEstaoDouradas.clear();
	 }
 }
 
 //o usuario usou o item das cartas douradas. 4 cartas ou menos ficarao com cor diferente
 private void realizarProcedimentoFazerCartasFicaremDouradas()
 {
	 //primeiro, vamos pegar as posicoes de todas cartas cartas que ainda estao no jogo e a dica
	 //essas posicoes sao referentes a linkedlist kanjisdascartasnatela
	 
	 //antes de tudo, vamos pegar so a carta da dica. Ela com certeza serah dourada
	 LinkedList<KanjiTreinar> kanjisNaoSairamDoJogo = new LinkedList<KanjiTreinar>();
	 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
	 {
		 KanjiTreinar umKanjiCartaNaTela = this.kanjisDasCartasNaTela.get(i);
		 
		 if(this.ehMesmoKanji(umKanjiCartaNaTela,this.kanjiDaDica) == true)
		 {
			 //eh o kanji da dica
			 kanjisNaoSairamDoJogo.add(umKanjiCartaNaTela);
		 }
	 }
	 
	 //agora vamos checar as que nao sao a carta da dica
	 for(int j = 0; j < this.kanjisDasCartasNaTela.size(); j++)
	 {
		 KanjiTreinar umKanjiCartaNaTela = this.kanjisDasCartasNaTela.get(j);
		 if(this.ehMesmoKanji(umKanjiCartaNaTela,this.kanjiDaDica) == true)
		 {
			 //eh o kanji da dica. Ja adicionamos ele. Nao faremos nada
		 }
		 else
		 {
			 boolean cartaJaSaiuDoJogo = false;
			 for(int k = 0; k < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); k++)
			 {
				 KanjiTreinar umKanjiCartaJaVirouDica = 
						 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(k);
				 if(this.ehMesmoKanji(umKanjiCartaJaVirouDica, umKanjiCartaNaTela) == true)
				 {
					 cartaJaSaiuDoJogo = true;
					 break;
				 }
			 }
	         
			 if(cartaJaSaiuDoJogo == false)
			 {
				 kanjisNaoSairamDoJogo.add(umKanjiCartaNaTela);
			 }
		 }
	 }
	 
	 //agora dependendo de quantas posicoes foram encontradas, faremos algumas cartas serem douradas
	 int quantasCartasDouradasDevemSerCriadas = 0;
	 if(kanjisNaoSairamDoJogo.size() == 1)
	 {
		 quantasCartasDouradasDevemSerCriadas = 1;
	 }
	 else if(kanjisNaoSairamDoJogo.size() == 2)
	 {
		 quantasCartasDouradasDevemSerCriadas = 2;
	 }
	 else if(kanjisNaoSairamDoJogo.size() == 3)
	 {
		 quantasCartasDouradasDevemSerCriadas = 3;
	 }
	 else if(kanjisNaoSairamDoJogo.size() >= 4)
	 {
		 quantasCartasDouradasDevemSerCriadas = 4;
	 }
	 else
	 {
		 //nao vai acontecer essa situacao, mas para previnir...
		 quantasCartasDouradasDevemSerCriadas = 0;
	 }
	 
	 //agora vamos pintar logo a carta da dica. Ela eh a primeira dos kanjisNaoSairamDoJogo
	 if(quantasCartasDouradasDevemSerCriadas > 0)
	 {
		 KanjiTreinar kanjiCartaVaiVirarDourada = kanjisNaoSairamDoJogo.remove(0);
		 
		 //agora vamos achar essa carta dourada e pinta-la!
		 for(int kk = 0; kk < this.kanjisDasCartasNaTela.size(); kk++)
		 {
			 KanjiTreinar umKanjiCartaNaTela = this.kanjisDasCartasNaTela.get(kk);
			 
			 if(this.ehMesmoKanji(umKanjiCartaNaTela,kanjiCartaVaiVirarDourada) == true)
			 {
				 //achamos a carta que vai ser dourada
				 ImageView imageViewCartaDourada;
				 if(kk == 0)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta1_imageview);
					 this.quaisCartasEstaoDouradas.add(1);
				 }
				 else if(kk == 1)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta2_imageview);
					 this.quaisCartasEstaoDouradas.add(2);
				 }
				 else if(kk == 2)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta3_imageview);
					 this.quaisCartasEstaoDouradas.add(3);
				 }
				 else if(kk == 3)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta4_imageview);
					 this.quaisCartasEstaoDouradas.add(4);
				 }
				 else if(kk == 4)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta5_imageview);
					 this.quaisCartasEstaoDouradas.add(5);
				 }
				 else if(kk == 5)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta6_imageview);
					 this.quaisCartasEstaoDouradas.add(6);
				 }
				 else if(kk == 6)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta7_imageview);
					 this.quaisCartasEstaoDouradas.add(7);
				 }
				 else
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta8_imageview);
					 this.quaisCartasEstaoDouradas.add(8);
				 }
				 
				 imageViewCartaDourada.setImageResource(R.drawable.karutavaziadourada);
				 break;
			 }
		 }
		 
		 //devemos diminuir q auntidade necessarias de cartas douradas porque ja criamos uma
		 quantasCartasDouradasDevemSerCriadas = quantasCartasDouradasDevemSerCriadas - 1;
		 
	 }
	 
	 //agora vamos pintar as outras cartas se existirem
	 for(int k = 0; k < quantasCartasDouradasDevemSerCriadas; k++)
	 {
		 Random geraNumAleatorio = new Random();
		 int posicaoKanji = geraNumAleatorio.nextInt(kanjisNaoSairamDoJogo.size());
		 KanjiTreinar kanjiCartaVaiVirarDourada = kanjisNaoSairamDoJogo.remove(posicaoKanji);
		 
		 //agora vamos achar essa carta dourada e pinta-la!
		 for(int l = 0; l < this.kanjisDasCartasNaTela.size(); l++)
		 {
			 KanjiTreinar umKanjiCartaNaTela = this.kanjisDasCartasNaTela.get(l);
			 
			 if(this.ehMesmoKanji(umKanjiCartaNaTela,kanjiCartaVaiVirarDourada) == true)
			 {
				 //achamos a carta que vai ser dourada
				 ImageView imageViewCartaDourada;
				 if(l == 0)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta1_imageview);
					 this.quaisCartasEstaoDouradas.add(1);
				 }
				 else if(l == 1)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta2_imageview);
					 this.quaisCartasEstaoDouradas.add(2);
				 }
				 else if(l == 2)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta3_imageview);
					 this.quaisCartasEstaoDouradas.add(3);
				 }
				 else if(l == 3)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta4_imageview);
					 this.quaisCartasEstaoDouradas.add(4);
				 }
				 else if(l == 4)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta5_imageview);
					 this.quaisCartasEstaoDouradas.add(5);
				 }
				 else if(l == 5)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta6_imageview);
					 this.quaisCartasEstaoDouradas.add(6);
				 }
				 else if(l == 6)
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta7_imageview);
					 this.quaisCartasEstaoDouradas.add(7);
				 }
				 else
				 {
					 imageViewCartaDourada = (ImageView) findViewById(R.id.karuta8_imageview);
					 this.quaisCartasEstaoDouradas.add(8);
				 }
				 
				 imageViewCartaDourada.setImageResource(R.drawable.karutavaziadourada);
				 break;
			 }
		 } 
	 }
	 
	 fazerPlaquinhaUsuarioBalancarUmPouco();
 }
 
 /*a mascote fala a frase por 4 segundos e depois volta a fala anterior*/
 private void mascoteFalaFrasePor4Segundos(String fraseFalarPor4Segundos, String fraseAnterior)
 {
	 final TextView textViewFalaMascote = (TextView) findViewById(R.id.dica_kanji);
	 textViewFalaMascote.setText(fraseFalarPor4Segundos);
	 
	 final String fraseAnteriorFinal = fraseAnterior;
	 new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	textViewFalaMascote.setText(fraseAnteriorFinal);
		            }
		        });
		    }
		}, 4000);
 }
 
 @Override
 public boolean jogadorEhHost() {
 	if(this.criouUmaSala == true)
 	{
 		return true;
 	}
 	else
 	{
 		return false;
 	}
 	
 }

 public boolean oGuestTerminouDeCarregarListaDeCategorias() {
 	return guestTerminouDeCarregarListaDeCategorias;
 }
 
 private void terminarJogoEEnviarMesagemAoAdversario()
 {
	 this.jogoAcabou = true;
	 
	 try
	 {
		 this.mandarMensagemMultiplayer("fim de jogo");
		 switchToScreen(R.id.tela_fim_de_jogo);
		 comecarFimDeJogo();
	 }
	 catch(IllegalStateException E)
	 {
		 voltarAoMenuInicial(null);
	 }
 }
 
 private void comecarFimDeJogo()
 {
	 findViewById(R.id.textoPontuacaoAdversario).setVisibility(View.VISIBLE);
	 findViewById(R.id.textoSuaPontuacao).setVisibility(View.VISIBLE);
	 findViewById(R.id.voceganhououperdeu).setVisibility(View.VISIBLE);
	 //findViewById(R.id.botao_revanche).setVisibility(View.VISIBLE);
	 
	 TextView textoPontuacaoAdversario = (TextView) findViewById(R.id.textoPontuacaoAdversario);
	 String pontuacaoDoAdversario = getResources().getString(R.string.pontuacaoDoAdversario);
	 textoPontuacaoAdversario.setText(pontuacaoDoAdversario + String.valueOf(this.pontuacaoDoAdversario));
	 
	 TextView textoSuaPontuacao = (TextView) findViewById(R.id.textoSuaPontuacao);
	 String stringSuaPontuacao = getResources().getString(R.string.suaPontuacao);
	 textoSuaPontuacao.setText(stringSuaPontuacao + String.valueOf(this.suaPontuacao));
	 
	 String voce = getResources().getString(R.string.voce);
	 if(this.suaPontuacao > this.pontuacaoDoAdversario)
	 {
		 String ganhou = getResources().getString(R.string.ganhou);
		 TextView tituloTelaFimDeJogo = (TextView) findViewById(R.id.voceganhououperdeu);
		 tituloTelaFimDeJogo.setText(voce + " " + ganhou + "!");
		 
		 ImageView mascote = (ImageView) findViewById(R.id.mascote_final_casual);
		 mascote.setImageResource(R.drawable.mascote_feliz);
	 }
	 else if(this.suaPontuacao < this.pontuacaoDoAdversario)
	 {
		 String perdeu = getResources().getString(R.string.perdeu);
		 TextView tituloTelaFimDeJogo = (TextView) findViewById(R.id.voceganhououperdeu);
		 tituloTelaFimDeJogo.setText(voce + " " + perdeu + "...");
		 
		 ImageView mascote = (ImageView) findViewById(R.id.mascote_final_casual);
		 mascote.setImageResource(R.drawable.mascote_zangada);
	 }
	 else
	 {
		 String empatou = getResources().getString(R.string.empatou);
		 TextView tituloTelaFimDeJogo = (TextView) findViewById(R.id.voceganhououperdeu);
		 tituloTelaFimDeJogo.setText(empatou + "!");
		 
		 ImageView mascote = (ImageView) findViewById(R.id.mascote_final_casual);
		 mascote.setImageResource(R.drawable.mascote_pensativa);
	 }
	 
	//vamos criar tudo para o popup que mostra o chat do fim de jogo, mas sem mostrar ainda!
	this.mensagensChat = new ArrayList<String>();
	this.posicoesBaloesMensagensChat = new ArrayList<String>();
	this.popupChatFimDeJogo = new Dialog(this);
	
	popupChatFimDeJogo.requestWindowFeature(Window.FEATURE_NO_TITLE);
	
	  // Include dialog.xml file
	popupChatFimDeJogo.setContentView(R.layout.popup_chat_final_modo_casual);
	popupChatFimDeJogo.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	
	this.mudarFonteTextoPopupFimDeJogoCasual(popupChatFimDeJogo);

//criar acao para o botao de mandar mensagem do dialog
	Button sendBtn = (Button) popupChatFimDeJogo.findViewById(R.id.sendMessageChat);
	sendBtn.setOnClickListener(new Button.OnClickListener() 
	{
		public void onClick(View v) 
		{
			mandarMensagemChat(popupChatFimDeJogo);
    }
	});


	//criar acao para botao de fechar dialog
	Button botao_fechar_chat_final_modo_casual = (Button) popupChatFimDeJogo.findViewById(R.id.botao_fechar_chat_final_modo_casual);
	botao_fechar_chat_final_modo_casual.setOnClickListener(new Button.OnClickListener() 
	{
	 	public void onClick(View v) 
	 	{
	 		popupChatFimDeJogo.dismiss();
	 	}
	});
	
	//definir adapter para o listview das mensagens do chat
	this.setListAdapter();
	
	 //VAMOS DEFINIR A ACAO PARA O BOTAO QUE ABRE O CHAT DO FIM DE JOGO
	 Button botaoAbreChat = (Button) findViewById(R.id.botao_abre_chat);
	 botaoAbreChat.setOnClickListener(new Button.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  popupChatFimDeJogo.show();
	      }	  
	  });
	 
	 
	 this.enviarDadosDaPartidaParaOLogDoUsuarioNoBancoDeDados();
	 
	 //falta adicionar o dinheiro que o usuario ganhou na partida
	 
	 /*int creditosAdicionarAoJogador = TransformaPontosEmCredito.converterPontosEmCredito(this.suaPontuacao);
	 DAOAcessaDinheiroDoJogador daoDinheiroJogador = ConcreteDAOAcessaDinheiroDoJogador.getInstance();
	 daoDinheiroJogador.adicionarCredito(creditosAdicionarAoJogador, this);
	 String textoGanhouCreditoNaPartida = getResources().getString(R.string.texto_ganhou) + " " + 
	 											 creditosAdicionarAoJogador + " " + getResources().getString(R.string.moeda_do_jogo) + " " + 
	 											 getResources().getString(R.string.texto_na_partida); 
	 Toast.makeText(this, textoGanhouCreditoNaPartida, Toast.LENGTH_SHORT).show();*/
	 
	 this.mudarMusicaDeFundo(R.raw.tradicional_japanese_ethnic_music_pack);
	 
	 this.fazerVariaveisDoJogoFicaremComValoresNullIniciais(); //caso uma revanche seja requisitada, eh bom que algumas variaveis fiquem com valores null como estavam antes de qualquer partida
	 this.handlerParaThreadDiminuiTempoDoJogo.removeCallbacksAndMessages(this.runnableThreadDiminuiTempoNoJogo);
	 this.runnableThreadDiminuiTempoNoJogoDeveriaMorrer = true;
	 this.mudarFonteTelaFimDeJogo();
 }

 
 
 private void setListAdapter() { //arraylist<string>
     AdapterListViewChatCasual adapter = new AdapterListViewChatCasual(this, R.layout.listitem, this.mensagensChat,this.posicoesBaloesMensagensChat,this);
     ListView listViewChat = (ListView) popupChatFimDeJogo.findViewById(R.id.mensagens_chat);
     listViewChat.setAdapter(adapter);
   }

 /**
  * 
  * @param mensagem
  * @param adicionarNomeDoRemetente precisa complementar a mensagem com o nome do remetente ou nao...
  * @return a mensagem adicionada no chat.
  */
 private String adicionarMensagemNoChat(String mensagem, boolean adicionarNomeDoRemetente,boolean foiMensagemDoAdversario)
 {
 	String mensagemAdicionarNoChat = mensagem;
 	if(adicionarNomeDoRemetente == true)
 	{
 		//append na mensagem o nome do remetente
 		//String emailUsuario = this.emailUsuario.substring(0, 11);
 		mensagemAdicionarNoChat = this.nomeUsuario + ":" + mensagem;
 	}
 	
 	this.mensagensChat.add(mensagemAdicionarNoChat);
 	
 	if(foiMensagemDoAdversario == true)
 	{
 		this.posicoesBaloesMensagensChat.add("direita");
 	}
 	else
 	{
 		this.posicoesBaloesMensagensChat.add("esquerda");
 	}
 	
 	setListAdapter();
 	return mensagemAdicionarNoChat;
 }

 private void avisarAoOponenteQueDigitouMensagem(String mensagemAdicionarNoChat)
 {
 	//mandar mensagem para oponente...
 	this.mandarMensagemMultiplayer("oponente falou no chat=" + mensagemAdicionarNoChat);
 }

 private void enviarSeuNomeParaOAdversario()
 {
	 this.mandarMensagemMultiplayer("nome=" + this.nomeUsuario);
 }
 
 private void enviarDadosDaPartidaParaOLogDoUsuarioNoBancoDeDados()
 {
	 //enviaremos as informacoes da partida num log que escreveremos para o usu�rio e salvaremos num servidor remoto
	 DadosPartidaParaOLog dadosPartida = new DadosPartidaParaOLog();
	 HashMap<String,LinkedList<KanjiTreinar>> categoriasEKanjis = SingletonGuardaDadosDaPartida.getInstance().getCategoriasEscolhidasEKanjisDelas();
	 String idCategoriasEmString = "";
	 Iterator<String> iteradorCategorias = categoriasEKanjis.keySet().iterator();
	 while(iteradorCategorias.hasNext() == true)
	 {
		 String umaCategoria = iteradorCategorias.next();
		 int id_categoria = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(umaCategoria);
		 idCategoriasEmString = idCategoriasEmString + String.valueOf(id_categoria);
		 if(iteradorCategorias.hasNext() == true)
		 {
			 idCategoriasEmString = idCategoriasEmString + ",";
		 }
	 }
	 
	 dadosPartida.setid_categorias_tudo_junto_separado_por_virgula(idCategoriasEmString);
	 
	 Calendar c = Calendar.getInstance();
	 DateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	 String formattedDate = df.format(c.getTime());
	 
	 dadosPartida.setData(formattedDate);
	 LinkedList<String> nomeEEmailUsuario = 
			 ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(this);
	 String nome_usuario = nomeEEmailUsuario.get(0);
	 
	 dadosPartida.setnome_usuario(nome_usuario);
	 
	 dadosPartida.setPalavrasAcertadas(this.palavrasAcertadas);
	 dadosPartida.setPalavrasErradas(this.palavrasErradas);
	 dadosPartida.setPalavrasJogadas(this.palavrasJogadas);
	 dadosPartida.setPontuacao(suaPontuacao);
	 
	 if(this.suaPontuacao > this.pontuacaoDoAdversario)
	 {
		 String ganhou = getResources().getString(R.string.ganhou);
		 dadosPartida.setVoceGanhouOuPerdeu(ganhou);
	 }
	 else if(this.suaPontuacao < this.pontuacaoDoAdversario)
	 {
		 String perdeu = getResources().getString(R.string.perdeu);
		 dadosPartida.setVoceGanhouOuPerdeu(perdeu);
	 }
	 else
	 {
		 String empatou = getResources().getString(R.string.empatou);
		 dadosPartida.setVoceGanhouOuPerdeu(empatou); 
	 }
	 
	 dadosPartida.setnome_adversario(this.nomeAdversario);
	 
	 EnviarDadosDaPartidaParaLogTask armazenaNoLog = new EnviarDadosDaPartidaParaLogTask();
	 armazenaNoLog.execute(dadosPartida);
 }
 
 public void voltarAoMenuInicial(View v)
 {
		try
		{
			/*this.leaveRoom();
			 Intent irMenuInicial =
						new Intent(TelaInicialMultiplayer.this, MainActivity.class);
			irMenuInicial.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);	
			 startActivity(irMenuInicial);*/
			this.leaveRoom();
		}
		catch(Exception e)
		{
			String mensagemerro = e.getMessage();
			mensagemerro = mensagemerro + "";
		}
	
 }
 
 public void mandarMensagemChat(Dialog dialog)
 {
	 EditText textfieldMensagemDigitada = (EditText) dialog.findViewById(R.id.chatEditText);
 	String mensagemDigitada = textfieldMensagemDigitada.getText().toString();
 	
 	if(mensagemDigitada.length() > 0)
 	{
 		textfieldMensagemDigitada.setText("");
 	 	String mensagemAdicionadaAoChat = this.adicionarMensagemNoChat(mensagemDigitada, true, false);
 	 	this.avisarAoOponenteQueDigitouMensagem(mensagemAdicionadaAoChat);
 	}
 	else
 	{
 		String mensagem = getResources().getString(R.string.erro_mandar_mensagem_chat);
		 Toast t = Toast.makeText(this, mensagem, Toast.LENGTH_LONG);
		 t.show();
 	}
 }
 
 /*foi percebido um bug onde no comeco da primeira rodada de qualquer partida o usuario ve todas as cartas iguais e a dica do kanji nao eh a correta.
  * Irei fazer o usuario esperar um pouco antes de comecar a partida p o bug n acontecer. A espera acaba quando ambos os jogadores ja tem a dica do kanji atualizada*/
 private void comecarEsperaDoUsuarioParaComecoDaPartida()
 {
	 this.loadingComecoDaPartida = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.iniciandoJogo), getResources().getString(R.string.por_favor_aguarde));
 }
 
 /*� o que eu uso para escurecer as cartas*/
 private void fazerImageViewFicarEscuro(ImageView imageView)
 {
	 //imageView.setColorFilter(Color.rgb(123, 123, 123), android.graphics.PorterDuff.Mode.MULTIPLY);
	 imageView.setImageResource(R.drawable.karutaverso);
 }
 
 /*o imageview eh o imageview da carta e o qualcarta vai de 0 ate 7 e indica a posicao da carta*/
 private void fazerCartaVoltarACorNormal(ImageView imageView, int qualCarta)
 {
	 //imageView.setColorFilter(null);
	 //imageView.setAlpha(255);
	 this.colorirBordaDaCartaDeAcordoComCategoria(qualCarta);
	 //segunda tentativa caso o acima nao funcionar: imageView.setImageResource(R.drawable.karutavazia); mas tem de colorir carta segundo categoria
 }
 
 /*coloca o texto verticalmente numa carta*/
 private void colocarTextoVerticalNaCarta(TextView textViewUmaCarta, String texto)
 {
	 //cada caractere do texto deve vir seguido de um \n
	 String textoComBarrasN = "";
	 
	 for(int i = 0; i < texto.length(); i++)
	 {
		 String umaLetra = String.valueOf(texto.charAt(i));
		 textoComBarrasN = textoComBarrasN + umaLetra + "\n";
	 }
	 
	 textViewUmaCarta.setText(textoComBarrasN);
 }

@Override
public void procedimentoConexaoFalhou() {
	// TODO Auto-generated method stub
	
}

private void mudarFonteDosKanjis()
{
	String fontPath = "fonts/KaoriGel.ttf";
	 
    // text view label
    TextView txtkaruta1 = (TextView) findViewById(R.id.texto_karuta1);
    TextView txtkaruta2 = (TextView) findViewById(R.id.texto_karuta2);
    TextView txtkaruta3 = (TextView) findViewById(R.id.texto_karuta3);
    TextView txtkaruta4 = (TextView) findViewById(R.id.texto_karuta4);
    TextView txtkaruta5 = (TextView) findViewById(R.id.texto_karuta5);
    TextView txtkaruta6 = (TextView) findViewById(R.id.texto_karuta6);
    TextView txtkaruta7 = (TextView) findViewById(R.id.texto_karuta7);
    TextView txtkaruta8 = (TextView) findViewById(R.id.texto_karuta8);
    
    // Loading Font Face
    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

    // Applying font
    txtkaruta1.setTypeface(tf);
    txtkaruta2.setTypeface(tf);
    txtkaruta3.setTypeface(tf);
    txtkaruta4.setTypeface(tf);
    txtkaruta5.setTypeface(tf);
    txtkaruta6.setTypeface(tf);
    txtkaruta7.setTypeface(tf);
    txtkaruta8.setTypeface(tf);
    
    //tb vou fazer isso para a label itens
    String fontPath2 = "fonts/Wonton.ttf";
    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
    TextView label_itens = (TextView) findViewById(R.id.label_itens);
    label_itens.setTypeface(tf2);
}

/*indiceDaCarta vai de 0 ate 7*/
private void colorirBordaDaCartaDeAcordoComCategoria(int indiceDaCarta)
{
	ImageView karuta1 = (ImageView) findViewById(R.id.karuta1_imageview);
    ImageView karuta2 = (ImageView) findViewById(R.id.karuta2_imageview);
    ImageView karuta3 = (ImageView) findViewById(R.id.karuta3_imageview);
    ImageView karuta4 = (ImageView) findViewById(R.id.karuta4_imageview);
    ImageView karuta5 = (ImageView) findViewById(R.id.karuta5_imageview);
    ImageView karuta6 = (ImageView) findViewById(R.id.karuta6_imageview);
    ImageView karuta7 = (ImageView) findViewById(R.id.karuta7_imageview);
    ImageView karuta8 = (ImageView) findViewById(R.id.karuta8_imageview);
    
    
    if(indiceDaCarta < this.kanjisDasCartasNaTela.size())
    {
    	KanjiTreinar umKanjiCartaNaTela = this.kanjisDasCartasNaTela.get(indiceDaCarta);
    	ImageView cartaASerMudada;
    	
    	if(indiceDaCarta == 0)
    	{
    		cartaASerMudada = karuta1;
    	}
    	else if(indiceDaCarta == 1)
    	{
    		cartaASerMudada = karuta2;
    	}
    	else if(indiceDaCarta == 2)
    	{
    		cartaASerMudada = karuta3;
    	}
    	else if(indiceDaCarta == 3)
    	{
    		cartaASerMudada = karuta4;
    	}
    	else if(indiceDaCarta == 4)
    	{
    		cartaASerMudada = karuta5;
    	}
    	else if(indiceDaCarta == 5)
    	{
    		cartaASerMudada = karuta6;
    	}
    	else if(indiceDaCarta == 6)
    	{
    		cartaASerMudada = karuta7;
    	}
    	else
    	{
    		cartaASerMudada = karuta8;
    	}
    	
    	String categoria = umKanjiCartaNaTela.getCategoriaAssociada();
    	
    	String cotidiano = getResources().getString(R.string.cotidiano);
     	String lugar = getResources().getString(R.string.lugar);
     	String natureza = getResources().getString(R.string.natureza);
     	String verbos = getResources().getString(R.string.verbos);
     	String adjetivos = getResources().getString(R.string.adjetivos);
     	String tempo = getResources().getString(R.string.tempo);
     	String supermercado = getResources().getString(R.string.supermercado);
     	String lazer = getResources().getString(R.string.lazer);
     	String educacao = getResources().getString(R.string.educacao);
     	String trabalho = getResources().getString(R.string.trabalho);
     	String geografia = getResources().getString(R.string.geografia);
     	
     	if(categoria.compareTo(cotidiano) == 0)
     	{
     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaamarelo);
     		cartaASerMudada.setImageResource(R.drawable.karutavaziaverdeescuro);
     	}
     	else if(categoria.compareTo(lugar) == 0)
     	{
     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaazul);
     		cartaASerMudada.setImageResource(R.drawable.karutavaziaazulmaisescuro);
     	}
     	else if(categoria.compareTo(natureza) == 0)
     	{
     		cartaASerMudada.setImageResource(R.drawable.karutavaziaverdeclaro);
     	}
     	else if(categoria.compareTo(verbos) == 0)
     	{
     		//cartaASerMudada.setImageResource(R.drawable.karutavaziacinza);
     		cartaASerMudada.setImageResource(R.drawable.karutavazialaranja);
     	}
     	else if(categoria.compareTo(adjetivos) == 0)
     	{
     		cartaASerMudada.setImageResource(R.drawable.karutavaziaroxoclaro);
     	}
     	else if(categoria.compareTo(tempo) == 0)
     	{
     		cartaASerMudada.setImageResource(R.drawable.karutavaziaroxo);
     	}
     	else if(categoria.compareTo(supermercado) == 0)
     	{
     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaroxo);
     		cartaASerMudada.setImageResource(R.drawable.karutavaziaazulescuro);
     	}
     	else if(categoria.compareTo(lazer) == 0)
     	{
     		cartaASerMudada.setImageResource(R.drawable.karutavaziarosa);
     	}
     	else if(categoria.compareTo(educacao) == 0)
     	{
     		cartaASerMudada.setImageResource(R.drawable.karutavaziaazulclaro);
     	}
     	else if(categoria.compareTo(trabalho) == 0)
     	{
     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaverdeclaro);
     		cartaASerMudada.setImageResource(R.drawable.karutavaziamarrom);
     	}
     	else if(categoria.compareTo(geografia) == 0)
     	{
     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaverdeescuro);
     		cartaASerMudada.setImageResource(R.drawable.karutavaziacinza);
     	}
     	else
     	{
     		//cartaASerMudada.setImageResource(R.drawable.karutavaziavermelho);
     		cartaASerMudada.setImageResource(R.drawable.karutavaziamarromclaro);
     	}
    } 	
}


private void mostrarLobbyModoCasual() 
{
	this.criouUmaSala = false;
	this.naCriacaoDeSalaModoCasual = false;
	
	this.loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
	mudarFonteComponentesLobbyModoCasual();
	
	 SpinnerSelecionaMesmoQuandoVoltaAoMesmoItem spinnerFiltragem = (SpinnerSelecionaMesmoQuandoVoltaAoMesmoItem) findViewById(R.id.spinnerPesquisarSalasModoCasual);
	 String labelFiltroNenhum = getResources().getString(R.string.nada);
	 String labelFiltroCategoria = getResources().getString(R.string.categorias);
	 String labelFiltroRanking = getResources().getString(R.string.dan);
	 String labelFiltroUsername = getResources().getString(R.string.usuario);
	 List<String> filtrosDeSala = new ArrayList<String>();
	 filtrosDeSala.add(labelFiltroNenhum);
	 filtrosDeSala.add(labelFiltroCategoria);
	 filtrosDeSala.add(labelFiltroRanking);
	 filtrosDeSala.add(labelFiltroUsername);
	 
	 SpinnerSelecionaMesmoQuandoVoltaAoMesmoItem spinner = (SpinnerSelecionaMesmoQuandoVoltaAoMesmoItem) findViewById(R.id.spinnerPesquisarSalasModoCasual);
	 
	 SpinnerAdapter adapterDoSpinner = spinner.getAdapter();
	 //if(adapterDoSpinner == null)
	 //{
		//Create an ArrayAdapter using the string array and a default spinner layout
			/*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
			     R.array.spinner_pesquisar_por_modo_casual, android.R.layout.simple_spinner_item);
			//Specify the layout to use when the list of choices appears
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
			//Apply the adapter to the spinner
		 MySpinnerAdapter adapter = new MySpinnerAdapter(getBaseContext(),
			        R.layout.spinner_layout,Arrays.asList(getResources().getStringArray(R.array.spinner_pesquisar_por_modo_casual)));
		 spinner.setAdapter(adapter); 
			spinner.setOnItemSelectedListener(this);
	 //}
	//threadFicaBuscandoSalasModoCasual = new ThreadExecutaBuscaSalasCasualTask(this, loadingSalasModoCasual);
	//threadFicaBuscandoSalasModoCasual.start();
	//BuscaSalasModoCasualTask procurarSalas = new BuscaSalasModoCasualTask(this.loadingSalasModoCasual, this);
	  //procurarSalas.execute("");
		
			ListView listViewSalas = (ListView) findViewById(R.id.lista_salas_abertas);
			listViewSalas.setOnScrollListener(this);
}

public LinkedList<SalaModoCasual> getSalasAbertas()
{	
	return this.salasAbertas;
}

 public void mostrarListaComSalasAposCarregar(LinkedList<SalaModoCasual> salasModoCasual, boolean mostrarAlerta) 
 {
	 if(salaAtual == null && this.salasAbertas != null && mostrarAlerta == true)
	 {
		 //alertar ao usuario que novas salas foram criadas!
		 //so mostrar enquanto a sala atual ainda for vazia
		 
		 Animation animacaoPiscar = AnimationUtils.loadAnimation(this, R.anim.anim_piscar_alerta_salas);
		 final TextView textoAlertaNovasSalas = (TextView) findViewById(R.id.alerta_salas_novas);
		 textoAlertaNovasSalas.setVisibility(View.VISIBLE);
		 textoAlertaNovasSalas.startAnimation(animacaoPiscar); 
		 
		//FALTA CRIAR UMA THREAD P DEPOIS DE 2 SEGUNDOS ELA TORNAR A VIEW INVISIVEL NOVAMENTE
		 new Timer().schedule(new TimerTask() 
		 { 
			    @Override
			    public void run() 
			    {
			        //If you want to operate UI modifications, you must run ui stuff on UiThread.
			        ModoCasual.this.runOnUiThread(new Runnable() 
			        {
			            @Override
			            public void run() 
			            {
			            	textoAlertaNovasSalas.setVisibility(View.INVISIBLE);
			            }
			        });
			    }
			}, 3000);
	 }
	 
	 
	 /*AdapterListViewSalasCriadas adapterSalasAtivas = new AdapterListViewSalasCriadas
				(this, R.layout.item_lista_sala, salasCarregadasModoCasual, this);


		 // Assign adapter to ListView
		 ListView listViewSalas = (ListView) findViewById(R.id.lista_salas_abertas);
		 //Parcelable state = listViewSalas.onSaveInstanceState(); //pegar o estado da listview para quando ela atualizar o user nao voltar p o comeco da lista
		 
		 
		 listViewSalas.setAdapter(adapterSalasAtivas); 
		 
		 
		 //listViewSalas.onRestoreInstanceState(state); //voltar ao estado antes do novo adapter*/
	 
	 
	 ListView listViewSalas = (ListView) findViewById(R.id.lista_salas_abertas);
		int indiceObjetoDeCima = listViewSalas.getFirstVisiblePosition();
		int visiblePercent = getVisiblePercent(listViewSalas.getChildAt(0)); //pega o primeiro item VISIVEL da listview
		if(visiblePercent != 100)
		{
			//se o primeiro item visivel da listview nao estah 100% visivel, o primeiro item visivel deveria ser o proximo
			//antes tava dando bug por exemplo: O item mais acima atualmente visivel era soh 50% visivel.
			indiceObjetoDeCima = indiceObjetoDeCima + 1;
		}
		
		int novoIndiceObjetoDeCima = 0; 
		
		if(this.salasAbertas != null && salasAbertas.size() > 0)
		{ 
			if(indiceObjetoDeCima > salasAbertas.size() - 1)
			{
				indiceObjetoDeCima = indiceObjetoDeCima - 1;
			}
			SalaModoCasual salaVistaPorUltimo = salasAbertas.get(indiceObjetoDeCima);
			//Toast.makeText(getApplicationContext(), "indiceObjetoDeCima=" + indiceObjetoDeCima, Toast.LENGTH_SHORT).show();
			this.salasAbertas.clear();
			for(int i = 0; i < salasModoCasual.size(); i++)
			{
				this.salasAbertas.add(salasModoCasual.get(i));
			}
			
			
			for(int y = 0; y < salasAbertas.size(); y++)
			{
				SalaModoCasual umaSalaAberta = salasAbertas.get(y);
				if(umaSalaAberta.getId_sala() == salaVistaPorUltimo.getId_sala())
				{
					novoIndiceObjetoDeCima = y;
					//Toast.makeText(getApplicationContext(), "novoIndiceObjetoDeCima=" + novoIndiceObjetoDeCima, Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}
		else
		{
			if(this.salasAbertas != null)
			{
				this.salasAbertas.clear();
			}
			else
			{
				this.salasAbertas = new LinkedList<SalaModoCasual>();
			}
			for(int i = 0; i < salasModoCasual.size(); i++)
			{
				this.salasAbertas.add(salasModoCasual.get(i));
			}
		}
		
		
		//comecar adapter para listview
		final ArrayList<SalaModoCasual> salasCarregadasModoCasual = new ArrayList<SalaModoCasual>();
		for(int i = 0; i < salasAbertas.size(); i++)
		 {
			 salasCarregadasModoCasual.add(salasAbertas.get(i));
		 }  
	
		 
		AdapterListViewSalasCriadas adapterSalasAtivas = new AdapterListViewSalasCriadas
				(this, R.layout.item_lista_sala, salasCarregadasModoCasual, this);

		 // Assign adapter to ListView
		 //Parcelable state = listViewSalas.onSaveInstanceState();//pegar estado atual da listView
		 listViewSalas.setAdapter(adapterSalasAtivas); 
		 if(indiceObjetoDeCima != 0)
		 {
			 //se o usuario nao estava no comeco da lista. Caso ele estivesse no comeco, era melhor mostrar a nova sala
			 listViewSalas.setSelection(novoIndiceObjetoDeCima); 
		 }	 
		 
		 /*listViewSalas.setOnItemClickListener(new OnItemClickListener() {
			 public void onItemClick(AdapterView parent, View view,
					 int position, long id) 
			 {
				 salaAtual = salasCarregadasModoCasual.get(position);
				 int id_sala = salaAtual.getId_sala();
				 startQuickGame(id_sala);
				 
				 
			 }
		 });*/
		 
		 //em seguida, setar o conteudo do spinner de filtragem
		 /*Spinner spinnerFiltragem = (Spinner) findViewById(R.id.spinnerPesquisarSalasModoCasual);
		 String labelFiltroNenhum = getResources().getString(R.string.nada);
		 String labelFiltroCategoria = getResources().getString(R.string.categorias);
		 String labelFiltroRanking = getResources().getString(R.string.dan);
		 String labelFiltroUsername = getResources().getString(R.string.email);
		 List<String> filtrosDeSala = new ArrayList<String>();
		 filtrosDeSala.add(labelFiltroNenhum);
		 filtrosDeSala.add(labelFiltroCategoria);
		 filtrosDeSala.add(labelFiltroRanking);
		 filtrosDeSala.add(labelFiltroUsername);
		 
		 Spinner spinner = (Spinner) findViewById(R.id.spinnerPesquisarSalasModoCasual);
		 SpinnerAdapter adapterDoSpinner = spinner.getAdapter();
		 if(adapterDoSpinner == null)
		 {
			//Create an ArrayAdapter using the string array and a default spinner layout
				ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
				     R.array.spinner_pesquisar_por_modo_casual, android.R.layout.simple_spinner_item);
				//Specify the layout to use when the list of choices appears
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				//Apply the adapter to the spinner
				spinner.setAdapter(adapter); 
				spinner.setOnItemSelectedListener(this);
		 }*/
		 
		 //listViewSalas.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_INSET);
		 //listViewSalas.setFastScrollEnabled(true);
		 //listViewSalas.setFastScrollAlwaysVisible(true);
		 
 }
 
 /*quando o usuario clica no iconezinho da porta, ele deve entrar numa sala*/
 public void entrarNaSala(SalaModoCasual salaVaiEntrar)
 {
	 salaAtual = salaVaiEntrar;
	 int id_sala = salaAtual.getId_sala();
	 startQuickGame(id_sala);
 }
 
 
 /*quando o usuario clica no iconezinho "C" em uma das salas, ele deve ver as categorias daquela sala*/
 public void abrirPopupMostrarCategoriasDeUmaSala(String[] categorias)
 {
	 final Dialog dialog = new Dialog(ModoCasual.this);
	 dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
	  // Include dialog.xml file
	 dialog.setContentView(R.layout.popup_categorias_de_uma_sala_casual_da_lista);
	 
	 
	 for(int i = 0; i < categorias.length; i++)
	 {
		 ImageView imageViewQuadradoBase; //quadrado que fica por baixo dos icones das categorias
		 ImageView imageViewQuadradoCategoria;
		 TextView textViewCategoria;
		 if(i == 0)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado1base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado1categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto1categoria);
		 }
		 else if(i == 1)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado2base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado2categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto2categoria);
		 }
		 else if(i == 2)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado3base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado3categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto3categoria);
		 }
		 else if(i == 3)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado4base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado4categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto4categoria);
		 }
		 else if(i == 4)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado5base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado5categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto5categoria);
		 }
		 else if(i == 5)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado6base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado6categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto6categoria);
		 }
		 else if(i == 6)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado7base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado7categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto7categoria);
		 }
		 else if(i == 7)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado8base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado8categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto8categoria);
		 }
		 else if(i == 8)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado9base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado9categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto9categoria);
		 }
		 else if(i == 9)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado10base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado10categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto10categoria);
		 }
		 else if(i == 10)
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado11base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado11categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto11categoria);
		 }
		 else
		 {
			 imageViewQuadradoBase = (ImageView) dialog.findViewById(R.id.quadrado12base);
			 imageViewQuadradoCategoria = (ImageView) dialog.findViewById(R.id.quadrado12categoria);
			 textViewCategoria = (TextView) dialog.findViewById(R.id.texto12categoria);
		 }
		 
		 //vamos tornar o quadrado de fundo e o icone da categoriavisiveis
		 imageViewQuadradoBase.setVisibility(View.VISIBLE);
		 imageViewQuadradoCategoria.setVisibility(View.VISIBLE);
		 textViewCategoria.setVisibility(View.VISIBLE);
		 
		 //agora falta mudar o icone de acordo com a categoria
		 String umaCategoria = categorias[i];
		 int imageResourceIconeCategoria = RetornaIconeDaCategoriaParaTelasDeEscolha.retornarIdIconeDaCategoria(umaCategoria,this); 
		 //funcao acima eh so para pegar o icone da categoria com base no nome dela,tipo R.id.icone_cotidiano
		 
		 imageViewQuadradoCategoria.setImageResource(imageResourceIconeCategoria); 
		 
		 //agora falta mudar o texto e fonte
		 textViewCategoria.setText(umaCategoria);
		 String fontPath = "fonts/Wonton.ttf";
		 Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		 textViewCategoria.setTypeface(tf);
	 }
	 
	 TextView titulo_popup_categorias_de_uma_sala_casual = (TextView) dialog.findViewById(R.id.titulo_popup_categorias_de_uma_sala_casual);
	 String fontPath = "fonts/Wonton.ttf";
	 Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
	 titulo_popup_categorias_de_uma_sala_casual.setTypeface(tf);
	 
	 Button botao_fechar_popup_categorias_uma_sala_casual = (Button) dialog.findViewById(R.id.botao_fechar_popup_categorias_uma_sala_casual);
	 botao_fechar_popup_categorias_uma_sala_casual.setOnClickListener(new Button.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  dialog.dismiss();
	      }	  
	  });
	 
	 dialog.show();
 }
 
 public static int getVisiblePercent(View v) {
     if (v != null && v.isShown()) {
         Rect r = new Rect();
         v.getGlobalVisibleRect(r);
         double sVisible = r.width() * r.height();
         double sTotal = v.getWidth() * v.getHeight();
         return (int) (100 * sVisible / sTotal);
     } else {
         return -1;
     }
 }
 
 public void abrirTelaCriarNovaSala(View v)
 {
	this.naCriacaoDeSalaModoCasual = true;
	switchToScreen(R.id.tela_espere_carregar_categorias_casual);
	this.solicitarPorKanjisPraTreino();
	LinkedList<String> nomeEMailESenhaUsuario = 
				ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(this);
	String nome_usuario = nomeEMailESenhaUsuario.get(0);
	//precisamos do ranking do usuario para criar a nova sala com base no ranking do usuario
	PegaRankingDoUsuarioParaCriarSalaCasualTask pegaRanking = new PegaRankingDoUsuarioParaCriarSalaCasualTask(this);
	pegaRanking.execute(nome_usuario);
	
 }
 

public void quemEscolheCategoriasClicouNoBotaoOkCriacaoDeSala(View v)
{
	//nao eh uma partida para revanche, certo?
	salaCriadaEhParaRevanche = false;
	
	 //primeiro iremos armazenar no singleton todas as categorias escolhidas e kanjis delas
	 Iterator<String> iteradorCategoriasEscolhidas = 
			 SingletonGuardaDadosDaPartida.getInstance().getCategoriasEscolhidasEKanjisDelas().keySet().iterator();
	 String categoriasSeparadasPorVirgula = "";
	 
	 while(iteradorCategoriasEscolhidas.hasNext() == true)
	 {
		 categoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula + iteradorCategoriasEscolhidas.next();
		 
		 if(iteradorCategoriasEscolhidas.hasNext() == true)
		 {
			 categoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula + ",";
		 }
	 }
	
	 if(categoriasSeparadasPorVirgula.compareTo("") == 0)
	 {
		 String mensagem = getResources().getString(R.string.erroEscolherCategorias);
		 Toast t = Toast.makeText(this, mensagem, Toast.LENGTH_LONG);
		 t.show();
	 }
	 else
	 {
		 
		 CriarSalaModoCasualTask armazenaNoBdNovaSala = new CriarSalaModoCasualTask(this);
		 SalaModoCasual novaSala = new SalaModoCasual();
		 
		 
		 String dan = SingletonArmazenaRankingUsuario.getInstance().getDan_do_usuario();
		 novaSala.setDanDoCriador(dan);
		 novaSala.setNomeDoCriador(nomeUsuario);
		 
		 if(this.quantasRodadasHaverao > 3)
		 {
			 //o jogador escolheu infinitas rodadas
			 novaSala.setQuantasRodadas(getResources().getString(R.string.infinitas_rodadas_sem_mais_nada));
		 }
		 else
		 {
			 if(this.quantasRodadasHaverao < 1)
			 {
				 this.quantasRodadasHaverao = 1;
			 }
			 novaSala.setQuantasRodadas(String.valueOf(this.quantasRodadasHaverao)); 
		 }
		 
		 
		 novaSala.setCategoriasJuntas(categoriasSeparadasPorVirgula);
		 
		 armazenaNoBdNovaSala.execute(novaSala);
		 this.salaAtual = novaSala;
		 
		 /*SingletonGuardaDadosDaPartida.getInstance().limparCategoriasEKanjis();
		 
		 ArmazenaKanjisPorCategoria conheceKanjisECategorias = ArmazenaKanjisPorCategoria.pegarInstancia();
			for(int i = 0; i < categoriaDeKanjiList.size(); i++)
			{
				CategoriaDeKanjiParaListviewSelecionavel umaCategoria = categoriaDeKanjiList.get(i);
				if(umaCategoria.isSelected() == true)
				{
					String nomeCategoria = umaCategoria.getName();
					int posicaoParenteses = nomeCategoria.indexOf("(");
					String nomeCategoriaSemParenteses = nomeCategoria.substring(0, posicaoParenteses);
					LinkedList<KanjiTreinar> kanjisDaCategoria = 
							conheceKanjisECategorias.getListaKanjisTreinar(nomeCategoriaSemParenteses);
					SingletonGuardaDadosDaPartida.getInstance().adicionarNovaCategoriaESeusKanjis(nomeCategoriaSemParenteses, kanjisDaCategoria);
					
				}
			}*/
			
		//Agora vamos armazenar quantas rodadas o jogo terah no singleton
		SingletonGuardaDadosDaPartida.getInstance().setQuantasRodadasHaverao(quantasRodadasHaverao);
		
	 }
}


public void iniciarAberturaDeSalaCasualComId(final int id_sala)
{
	
	if(this.salaCriadaEhParaRevanche == false)
	{
		this.salaAtual.setId_sala(id_sala);
		int id_sala_criada = id_sala;
		startQuickGame(id_sala_criada);
		this.criouUmaSala = true;
	}
	else
	{
		this.mandarMensagemMultiplayer("id_sala_revanche=" + id_sala); //tem de avisar ao usuario o novo id!!!
		leaveRoom();
 		jaDeixouASala = false; //pq agora ele vai entrar em outra sala!!!
 		salaAtual.setId_sala(id_sala);
 		int id_sala_criada = id_sala;
 		startQuickGame(id_sala_criada);
 		criouUmaSala = true;
 		revancheEstahAcontecendo = true;
		
		/*Handler handler = new Handler(); 
	    handler.postDelayed(new Runnable() { 
	         public void run() { 
	        	leaveRoom();
	     		jaDeixouASala = false; //pq agora ele vai entrar em outra sala!!!
	     		salaAtual.setId_sala(id_sala);
	     		int id_sala_criada = id_sala;
	     		startQuickGame(id_sala_criada);
	     		criouUmaSala = true;
	     		revancheEstahAcontecendo = true;
	         } 
	    }, 3000);*/
	}
}

public void recarregarSalas(View v)
{
	this.loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
	  BuscaSalasModoCasualTask procurarSalas = new BuscaSalasModoCasualTask(this.loadingSalasModoCasual, this);
	  procurarSalas.execute("");
}

@Override
public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) 
{
	String itemEscolhido = arg0.getItemAtPosition(arg2).toString();
	String categorias = getResources().getString(R.string.categorias);
	String duracao_da_partida = getResources().getString(R.string.duracao_da_partida);
	String dan = getResources().getString(R.string.dan);
	String usuario = getResources().getString(R.string.usuario);
	
	if(itemEscolhido.compareTo(categorias) == 0)
	{
		if(threadFicaBuscandoSalasModoCasual != null && threadFicaBuscandoSalasModoCasual.isAlive() == true)
		{
			threadFicaBuscandoSalasModoCasual.interrupt();
		}
		
		this.noPopupQueBuscaSalasPorCategoria = true;
		this.loadingSalasModoCasual = 
				ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_todas_as_categorias), getResources().getString(R.string.por_favor_aguarde));
		 
		//PegaNomesDeTodasAsCategoriasTask pegaCategorias = new PegaNomesDeTodasAsCategoriasTask(this.loadingSalasModoCasual,this);
		//pegaCategorias.execute("");
		SolicitaKanjisParaTreinoTask solicitaKanjisTreino = new SolicitaKanjisParaTreinoTask(loadingSalasModoCasual, this);
		solicitaKanjisTreino.execute("");
	}
	else if (itemEscolhido.compareTo(duracao_da_partida) == 0)
	{
		if(threadFicaBuscandoSalasModoCasual != null && threadFicaBuscandoSalasModoCasual.isAlive() == true)
		{
			threadFicaBuscandoSalasModoCasual.interrupt();
		}
		this.noPopupQueBuscaSalasPorCategoria = false;
		this.mostrarPopupPesquisarPorDuracao();
	}
	else if (itemEscolhido.compareTo(dan) == 0)
	{
		if(threadFicaBuscandoSalasModoCasual != null && threadFicaBuscandoSalasModoCasual.isAlive() == true)
		{
			threadFicaBuscandoSalasModoCasual.interrupt();
		}
		this.noPopupQueBuscaSalasPorCategoria = false;
		this.mostrarPopupPesquisarPorDan();
	}
	else if (itemEscolhido.compareTo(usuario) == 0)
	{
		if(threadFicaBuscandoSalasModoCasual != null && threadFicaBuscandoSalasModoCasual.isAlive() == true)
		{
			threadFicaBuscandoSalasModoCasual.interrupt();
		}
		this.noPopupQueBuscaSalasPorCategoria = false;
		this.mostrarPopupPesquisarPorUsuario();
	}
	else
	{
		//usuario escolheu nenhum
		this.threadFicaBuscandoSalasModoCasual = new ThreadExecutaBuscaSalasCasualTask(this, this.loadingSalasModoCasual);
		this.threadFicaBuscandoSalasModoCasual.start();
	}
}

private void mostrarPopupPesquisarPorUsuario()
{
	
    // Include dialog.xml file
    switchToScreen(R.id.popup_escolha_nome_usuario);
    
    //mudar fonte do titulo informe um email
    this.mudarFonteTituloETextoEBotaoOkInformeUmEmailPesquisaSalaModoCasual();
    
    final ModoCasual telaModoCasual = this;
    Button botaoOk = (Button) findViewById(R.id.confirmar_pesquisa_email);
    Button botaoCancelar = (Button) findViewById(R.id.cancelar_pesquisa_email);
    botaoOk.setOnClickListener(new Button.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			//FAZER DESAPARECER TECLADO VIRTUAL
				InputMethodManager inputManager = (InputMethodManager)
		                getSystemService(Context.INPUT_METHOD_SERVICE); 
				View view = getCurrentFocus();
				if(view != null)
				{
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
			                   InputMethodManager.HIDE_NOT_ALWAYS);
				}
				//FIM DO FAZER DESAPARECER TECLADO VIRTUAL
				
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  EditText editTextInformeEmailPesquisarSalasModoCasual = (EditText) findViewById(R.id.edit_text_informe_email_pesquisar_salas_modo_casual);
			  String nomeusuario = editTextInformeEmailPesquisarSalasModoCasual.getText().toString();
				loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
				BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
						new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
				taskBuscaSalasModoCasual.execute("nomeusuario",nomeusuario);
	      }	  
	  });
    
    botaoCancelar.setOnClickListener(new Button.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			//FAZER DESAPARECER TECLADO VIRTUAL
				InputMethodManager inputManager = (InputMethodManager)
		                getSystemService(Context.INPUT_METHOD_SERVICE); 
				View view = getCurrentFocus();
				if(view != null)
				{
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
			                   InputMethodManager.HIDE_NOT_ALWAYS);
				}
				//FIM DO FAZER DESAPARECER TECLADO VIRTUAL
				switchToScreen(R.id.tela_lobby_modo_casual);
	      }
	  });
}

private void mostrarPopupPesquisarPorDuracao()
{
	String umaRodada = getResources().getString(R.string.uma_rodada);
	String duasRodadas = getResources().getString(R.string.duas_rodadas);
	String tresRodadas = getResources().getString(R.string.tres_rodadas);
	String infinitasRodadas = getResources().getString(R.string.infinitas_rodadas);
	final String[] arrayDuracao = {umaRodada,duasRodadas,tresRodadas,infinitasRodadas};
	
	Integer[] imageId = new Integer[4];
	imageId[0] = R.drawable.mascote_feliz_pequena;
	imageId[1] = R.drawable.macote_normal_pequena;
	imageId[2] = R.drawable.mascote_assustada_pequena;
	imageId[3] = R.drawable.mascote_zangada_pequena;

	
    // arraylist to keep the selected items
	
	
    // Include dialog.xml file
    switchToScreen(R.id.popup_escolha_duracao);
	
	//definindo fontes para os textos dessa tela...
	this.mudarFonteTituloInformeADuracaoPesquisaSalaModoCasual();
	Typeface typeFaceFonteTextoListViewIconeETexto = this.escolherFonteDoTextoListViewIconeETexto();
	
	final ModoCasual telaModoCasual = this;
	
	AdapterListViewIconeETexto adapter = new AdapterListViewIconeETexto(ModoCasual.this, arrayDuracao, imageId,typeFaceFonteTextoListViewIconeETexto,false,false,true);
	    ListView list=(ListView)findViewById(R.id.listaDuracaoPesquisaSalas);
	    
	        list.setAdapter(adapter);
	        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	                @Override
	                public void onItemClick(AdapterView<?> parent, View view,
	                                        int position, long id) 
	                {
	                	switch(position)
	                    {
	                        case 0:
	                                // Your code when first option seletced
	                        	switchToScreen(R.id.tela_lobby_modo_casual);
	                        	loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
	                    		BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
	                    				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
	                    		taskBuscaSalasModoCasual.execute("duracao",String.valueOf(1));
	                                break;
	                        case 1:
	                                // Your code when 2nd  option seletced
	                        	switchToScreen(R.id.tela_lobby_modo_casual);
	                        	loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
	                    		BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual2 =
	                    				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
	                    		taskBuscaSalasModoCasual2.execute("duracao",String.valueOf(2));
	                                break;   
	                        case 2:
	                               // Your code when 3rd option seletced
	                        	switchToScreen(R.id.tela_lobby_modo_casual);
	                        	loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
	                    		BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual3 =
	                    				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
	                    		taskBuscaSalasModoCasual3.execute("duracao",String.valueOf(3));
	                                break;
	                        case 3:
	                                 // Your code when 4th  option seletced
	                        	switchToScreen(R.id.tela_lobby_modo_casual);
	                        	String infinitasRodadas = getResources().getString(R.string.infinitas_rodadas_sem_mais_nada);
	                        	loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
	                    		BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual4 =
	                    				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
	                    		taskBuscaSalasModoCasual4.execute("duracao",infinitasRodadas);
	                                break;
	                    }
	                }
	            });


	  /*WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	  lp.copyFrom(dialog.getWindow().getAttributes());
	  lp.width = 400;
	  lp.height = 400;*/
	  //dialog.getWindow().setAttributes(lp);
	        
	  Button cancelar_escolha_duracao = (Button) findViewById(R.id.cancelar_escolha_duracao);
	  cancelar_escolha_duracao.setOnClickListener(new Button.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
	      }
	  });
	
}

private void mostrarPopupPesquisarPorDan()
{
    // Include dialog.xml file
    switchToScreen(R.id.popup_escolha_dan);
    
    ImageView inicianteImageView = (ImageView) findViewById(R.id.iniciante);
    ImageView intermediarioImageView = (ImageView) findViewById(R.id.intermediario);
    ImageView dan1ImageView = (ImageView) findViewById(R.id.dan1);
    ImageView dan2ImageView = (ImageView) findViewById(R.id.dan2);
    ImageView dan3ImageView = (ImageView) findViewById(R.id.dan3);
    ImageView dan4ImageView = (ImageView) findViewById(R.id.dan4);
    ImageView dan5ImageView = (ImageView) findViewById(R.id.dan5);
    ImageView dan6ImageView = (ImageView) findViewById(R.id.dan6);
    ImageView dan7ImageView = (ImageView) findViewById(R.id.dan7);
    ImageView dan8ImageView = (ImageView) findViewById(R.id.dan8);
    ImageView dan9ImageView = (ImageView) findViewById(R.id.dan9);
    ImageView dan10ImageView = (ImageView) findViewById(R.id.dan10);
    
    inicianteImageView.setClickable(true);
    intermediarioImageView.setClickable(true);
    dan1ImageView.setClickable(true);
    dan2ImageView.setClickable(true);
    dan3ImageView.setClickable(true);
    dan4ImageView.setClickable(true);
    dan5ImageView.setClickable(true);
    dan6ImageView.setClickable(true);
    dan7ImageView.setClickable(true);
    dan8ImageView.setClickable(true);
    dan9ImageView.setClickable(true);
    dan10ImageView.setClickable(true);
    
    final String iniciante = getResources().getString(R.string.iniciante);
	final String intermediario = getResources().getString(R.string.intermediario);
	final String dan1 = getResources().getString(R.string.dan1);
	final String dan2 = getResources().getString(R.string.dan2);
	final String dan3 = getResources().getString(R.string.dan3);
	final String dan4 = getResources().getString(R.string.dan4);
	final String dan5 = getResources().getString(R.string.dan5);
	final String dan6 = getResources().getString(R.string.dan6);
	final String dan7 = getResources().getString(R.string.dan7);
	final String dan8 = getResources().getString(R.string.dan8);
	final String dan9 = getResources().getString(R.string.dan9);
	final String dan10 = getResources().getString(R.string.dan10);
    
	this.mudarFonteTextosInformeUmDanPesquisaSalaModoCasual();
	
    final ModoCasual telaModoCasual = this;
    
    inicianteImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
      			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
      				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
      		taskBuscaSalasModoCasual.execute("dan",iniciante);
	      }	  
	  });
    intermediarioImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
    			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
    				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
    		taskBuscaSalasModoCasual.execute("dan",intermediario);
	      }	  
	  });
    dan1ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
  			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
  				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
  		taskBuscaSalasModoCasual.execute("dan",dan1);
	      }	  
	  });
    dan2ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan2);
	      }	  
	  });
    dan3ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan3);
	      }	  
	  });
    dan4ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan4);
	      }	  
	  });
    dan5ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan5);
	      }	  
	  });
    dan6ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan6);
	      }	  
	  });
    dan7ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan7);
	      }	  
	  });
    dan8ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan8);
	      }	  
	  });
    dan9ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan9);
	      }	  
	  });
    dan10ImageView.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
			BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual =
				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		taskBuscaSalasModoCasual.execute("dan",dan10);
	      }	  
	  });
    
    
    //faltou o botao cancelar
    Button cancelar_escolha_dan = (Button) findViewById(R.id.cancelar_escolha_dan);
    cancelar_escolha_dan.setOnClickListener(new ImageView.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
	      }	  
	  });
    
}

//o metodo abaixo so eh chamado pela task PegaNomesDeTodasAsCategoriasTask
public void mostrarPopupPesquisarPorCategorias(final LinkedList<String> categorias)
{
	
	int tamanhoLista1 = categorias.size()/2;
	final String[] arrayCategorias = new String[tamanhoLista1];
	final String[] arrayCategorias2 = new String[categorias.size() - tamanhoLista1];
	int iteradorCategorias1 = 0;
	int iteradorCategorias2 = 0;
	
	for(int i = 0; i < categorias.size(); i++)
	{
		String umaCategoria = categorias.get(i);
		if(iteradorCategorias1 < arrayCategorias.length)
		{
			arrayCategorias[iteradorCategorias1] = umaCategoria;
			iteradorCategorias1 = iteradorCategorias1 + 1;
		}
		else
		{
			arrayCategorias2[iteradorCategorias2] = umaCategoria;
			iteradorCategorias2 = iteradorCategorias2 + 1;
		}
	}
	
	Integer[] imageId = new Integer[arrayCategorias.length];
	Integer[] imageId2 = new Integer[arrayCategorias2.length];
	
	for(int j = 0; j < arrayCategorias.length; j++)
	{
		String umaCategoria = arrayCategorias[j];
		int idImagem = RetornaIconeDaCategoriaParaTelasDeEscolha.retornarIdIconeDaCategoria(umaCategoria,this);
		imageId[j] = idImagem;
	}
	for(int k = 0; k < arrayCategorias2.length; k++)
	{
		String umaCategoria = arrayCategorias2[k];
		int idImagem = RetornaIconeDaCategoriaParaTelasDeEscolha.retornarIdIconeDaCategoria(umaCategoria,this);
		imageId2[k] = idImagem;
	}

	
    // arraylist to keep the selected items
   
	//vou tirar a linha que divide o titulo e o conteudo do dialog
	/*int divierId = dialog.getContext().getResources()
            .getIdentifier("android:id/titleDivider", null, null);
	View divider = dialog.findViewById(divierId);
	divider.setBackgroundColor(Color.WHITE);*/
	
	
    // Include dialog.xml file
    switchToScreen(R.id.popup_escolha_categorias);
    
    final boolean[] categoriaEstahSelecionada = new boolean[arrayCategorias.length];
    final boolean[] categoriaEstahSelecionada2 = new boolean[arrayCategorias.length];
	for(int l = 0; l < arrayCategorias.length; l++)
	{
		categoriaEstahSelecionada[l] = false;
	}
	for(int m = 0; m < arrayCategorias2.length; m++)
	{
		categoriaEstahSelecionada2[m] = false;
	}
	
	//devo colocar em baixo do nome da categoria, quantas palavras ela tem
			final String[] arrayCategoriasComQuantasPalavras = new String[arrayCategorias.length];
			final String[] arrayCategoriasComQuantasPalavras2 = new String[arrayCategorias2.length];
			SingletonArmazenaCategoriasDoJogo conheceCategorias = SingletonArmazenaCategoriasDoJogo.getInstance();
			
			for(int a = 0; a < arrayCategorias.length; a++)
			{
				String umaCategoria = arrayCategorias[a];
				int id_categoria = conheceCategorias.pegarIdDaCategoria(umaCategoria);
				int quantasPalavrasTemACategoria = 
						ArmazenaKanjisPorCategoria.pegarInstancia().quantasPalavrasTemACategoria(id_categoria);
				String textoDaCategoria = umaCategoria + " (" + String.valueOf(quantasPalavrasTemACategoria) + ")";
				String categoriaEscritaEmKanji = RetornaNomeCategoriaEscritaEmKanji.retornarNomeCategoriaEscritaEmKanji(umaCategoria,this);
				textoDaCategoria = categoriaEscritaEmKanji + "\n" + textoDaCategoria;
				arrayCategoriasComQuantasPalavras[a] = textoDaCategoria;
			}
			for(int b = 0; b < arrayCategorias2.length; b++)
			{
				String umaCategoria = arrayCategorias2[b];
				int id_categoria = conheceCategorias.pegarIdDaCategoria(umaCategoria);
				int quantasPalavrasTemACategoria = 
						ArmazenaKanjisPorCategoria.pegarInstancia().quantasPalavrasTemACategoria(id_categoria);
				String textoDaCategoria = umaCategoria + " (" + String.valueOf(quantasPalavrasTemACategoria) + ")";
				String categoriaEscritaEmKanji = RetornaNomeCategoriaEscritaEmKanji.retornarNomeCategoriaEscritaEmKanji(umaCategoria,this);
				textoDaCategoria = categoriaEscritaEmKanji + "\n" + textoDaCategoria;
				arrayCategoriasComQuantasPalavras2[b] = textoDaCategoria;
			}
	
	//definindo fontes para os textos dessa tela...
	this.mudarFonteTituloEBotaoOkInformeAsCategoriasPesquisaSalaModoCasual();
	Typeface typeFaceFonteTextoListViewIconeETexto = this.escolherFonteDoTextoListViewIconeETexto();
	AdapterListViewIconeETexto adapter = new AdapterListViewIconeETexto(ModoCasual.this, arrayCategoriasComQuantasPalavras, imageId,typeFaceFonteTextoListViewIconeETexto,true,true,false);
	adapter.setLayoutUsadoParaTextoEImagem(R.layout.list_item_icone_e_texto_menor);   
	ListView list=(ListView)findViewById(R.id.listaCategoriasPesquisaSalas1);
	    
	        list.setAdapter(adapter);
	        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	                @Override
	                public void onItemClick(AdapterView<?> parent, View view,
	                                        int position, long id) 
	                {
	                    if(categoriaEstahSelecionada[position] == false)
	                    {
	                    	categoriaEstahSelecionada[position] = true;
	                    	ImageView imageView = (ImageView) view.findViewById(R.id.img);
	                    	imageView.setAlpha(255);
	                    	TextView textView = (TextView) view.findViewById(R.id.txt);
	                    	int alpha = 255;
	                		textView.setTextColor(Color.argb(alpha, 0, 0, 0));
	                    }
	                    else
	                    {
	                    	categoriaEstahSelecionada[position] = false;
	                    	ImageView imageView = (ImageView) view.findViewById(R.id.img);
	                    	imageView.setAlpha(100);
	                    	TextView textView = (TextView) view.findViewById(R.id.txt);
	                    	int alpha = 100;
	                		textView.setTextColor(Color.argb(alpha, 0, 0, 0));
	                    }
	                }
	            });
	        
	        
	        AdapterListViewIconeETexto adapter2 = new AdapterListViewIconeETexto(ModoCasual.this, arrayCategoriasComQuantasPalavras2, imageId2,typeFaceFonteTextoListViewIconeETexto,true,true,false);
	        adapter2.setLayoutUsadoParaTextoEImagem(R.layout.list_item_icone_e_texto_menor);   
	        ListView list2=(ListView)findViewById(R.id.listaCategoriasPesquisaSalas2);
		        list2.setAdapter(adapter2);
		        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		                @Override
		                public void onItemClick(AdapterView<?> parent, View view,
		                                        int position, long id) 
		                {
		                    if(categoriaEstahSelecionada2[position] == false)
		                    {
		                    	categoriaEstahSelecionada2[position] = true;
		                    	ImageView imageView = (ImageView) view.findViewById(R.id.img);
		                    	imageView.setAlpha(255);
		                    	TextView textView = (TextView) view.findViewById(R.id.txt);
		                    	int alpha = 255;
		                		textView.setTextColor(Color.argb(alpha, 0, 0, 0));
		                    }
		                    else
		                    {
		                    	categoriaEstahSelecionada2[position] = false;
		                    	ImageView imageView = (ImageView) view.findViewById(R.id.img);
		                    	imageView.setAlpha(100);
		                    	TextView textView = (TextView) view.findViewById(R.id.txt);
		                    	int alpha = 100;
		                		textView.setTextColor(Color.argb(alpha, 0, 0, 0));
		                    }
		                }
		            });

    //this.popupPesquisarSalaPorCategoria = builder.create();//AlertDialog dialog; create like this outside onClick
	        
	//falta definir a a��o para o bot�o ok desse popup das categorias
	  Button botaoOk = (Button) findViewById(R.id.confirmar_escolha_categorias);
	  Button botaoCancelar = (Button) findViewById(R.id.cancelar_escolha_categorias);
	  final ModoCasual telaModoCasual = this;
	  botaoOk.setOnClickListener(new Button.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
			  loadingSalasModoCasual = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.carregando_salas), getResources().getString(R.string.por_favor_aguarde));
		    	BuscaSalasModoCasualComArgumentoTask taskBuscaSalasModoCasual12 =
		    				new BuscaSalasModoCasualComArgumentoTask(loadingSalasModoCasual, telaModoCasual);
		    	String categoriasSeparadasPorVirgula = "";
		    	for(int n = 0; n < categoriaEstahSelecionada.length; n++)
		    	{
		    		if(categoriaEstahSelecionada[n] == true)
		    		{
		    			//o usuario quer procurar com essa categoria
		    			String umaCategoria = arrayCategorias[n];
		    			
		    			categoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula + umaCategoria + ",";
		    			
		    		}
		    	}
		    	for(int o = 0; o < categoriaEstahSelecionada2.length; o++)
		    	{
		    		if(categoriaEstahSelecionada2[o] == true)
		    		{
		    			//o usuario quer procurar com essa categoria
		    			String umaCategoria = arrayCategorias2[o];
		    			
		    			categoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula + umaCategoria + ",";
		    			
		    		}
		    	}
		    	
		    	
		    	if(categoriasSeparadasPorVirgula.length() > 1)
		    	{
		    		categoriasSeparadasPorVirgula = categoriasSeparadasPorVirgula.substring(0,categoriasSeparadasPorVirgula.length()-1);
		    	}
		    	taskBuscaSalasModoCasual12.execute("categorias",categoriasSeparadasPorVirgula);    
		    	
		    	//A STRING SCIMA ESTAH NORMAL COM AS CATEGORIAS. POR ALGUM MOTIVO O LISTVIEW NAO ESTAH SENDO ATUALIZADO COM O RESULTADO DA BUSCA
	      }
	  });
	  
	  botaoCancelar.setOnClickListener(new Button.OnClickListener() 
	  {
		  public void onClick(View v) 
	      {
			  switchToScreen(R.id.tela_lobby_modo_casual);
	      }
	  });
}


private Typeface escolherFonteDoTextoListViewIconeETexto()
{
	String fontPath = "fonts/Wonton.ttf";
    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
    return tf;
}

/*o Dialog eh passado para procurar o textview nele*/
private void mudarFonteTituloEBotaoOkInformeAsCategoriasPesquisaSalaModoCasual()
{
	String fontPath = "fonts/Wonton.ttf";
    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
    TextView titulo = (TextView) findViewById(R.id.tituloEscolhaAsCategoriasPesquisarSalasModoCasual);
    titulo.setTypeface(tf);
    
    TextView textoTituloCasualEscolhaCategorias = (TextView) findViewById(R.id.textoTituloCasualEscolhaCategorias);
    textoTituloCasualEscolhaCategorias.setTypeface(tf);
    
}

private void mudarFonteTituloInformeADuracaoPesquisaSalaModoCasual()
{
	String fontPath = "fonts/Wonton.ttf";
    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
    TextView titulo = (TextView) findViewById(R.id.tituloEscolhaDuracaoPesquisaSalasModoCasual);
    titulo.setTypeface(tf);
    
    TextView textoTituloCasualEscolhaDuracao = (TextView) findViewById(R.id.textoTituloCasualEscolhaDuracao);
    textoTituloCasualEscolhaDuracao.setTypeface(tf);
}

private void mudarFonteTituloETextoEBotaoOkInformeUmEmailPesquisaSalaModoCasual()
{
	String fontPath = "fonts/Wonton.ttf";
    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
    TextView titulo = (TextView) findViewById(R.id.tituloEscolhaEmailPesquisaSalasModoCasual);
    titulo.setTypeface(tf);
    
    TextView texto = (TextView) findViewById(R.id.label_email_pesquisa_sala_modo_casual);
    texto.setTypeface(tf);
    
    TextView textoTituloCasualEscolhaNome = (TextView) findViewById(R.id.textoTituloCasualEscolhaNome);
    textoTituloCasualEscolhaNome.setTypeface(tf);
}

private void mudarFonteTextosInformeUmDanPesquisaSalaModoCasual()
{
	String fontPath = "fonts/Wonton.ttf";
    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
    
    TextView textoTituloCasualEscolhaDan = (TextView) findViewById(R.id.textoTituloCasualEscolhaDan);
    textoTituloCasualEscolhaDan.setTypeface(tf);
    
    Button cancelar_escolha_dan = (Button) findViewById(R.id.cancelar_escolha_dan);
    cancelar_escolha_dan.setTypeface(tf);
    
}

private void mudarFonteComponentesLobbyModoCasual()
{
	String fontPath = "fonts/mvboli.ttf";
    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
    TextView labelPesquisarPorSalas = (TextView) findViewById(R.id.labelPesquisarSalas);
    labelPesquisarPorSalas.setTypeface(tf);
    

    /*TextView label_titulo_do_jogador = (TextView) findViewById(R.id.label_titulo_do_jogador);
    label_titulo_do_jogador.setTypeface(tf);
    TextView label_titulo_username = (TextView) findViewById(R.id.label_titulo_username);
    label_titulo_username.setTypeface(tf);
    TextView label_categorias_selecionadas = (TextView) findViewById(R.id.label_categorias_selecionadas);
    label_categorias_selecionadas.setTypeface(tf);*/
    
    
    String fontPath2 = "fonts/Wonton.ttf";
    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
    TextView textoTitulo = (TextView) findViewById(R.id.textoTituloCasual);
    textoTitulo.setTypeface(tf2);
    
    TextView labelPesquisarSalas = (TextView) findViewById(R.id.labelPesquisarSalas);
    labelPesquisarSalas.setTypeface(tf2);
    TextView alerta_salas_novas = (TextView) findViewById(R.id.alerta_salas_novas);
    alerta_salas_novas.setTypeface(tf2);
    
    
    
}

private void mudarFonteEscolhaAsCategoriasDaSala()
{
	String fontPath2 = "fonts/Wonton.ttf";
    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
    TextView textoTitulo = (TextView) findViewById(R.id.escolha_categorias_da_sala);
    textoTitulo.setTypeface(tf2);
}

private void mudarFonteEscolhaDuracaoDaSala()
{
	String fontPath2 = "fonts/Wonton.ttf";
    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
    TextView textoTitulo = (TextView) findViewById(R.id.escolha_duracao_da_sala);
    textoTitulo.setTypeface(tf2);
    
    RadioButton radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
    RadioButton radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
    RadioButton radioButton3 = (RadioButton) findViewById(R.id.radioButton3);
    RadioButton radioButton4 = (RadioButton) findViewById(R.id.radioButton4);
    
    radioButton1.setTypeface(tf2);
    radioButton2.setTypeface(tf2);
    radioButton3.setTypeface(tf2);
    radioButton4.setTypeface(tf2);
}

private void mudarFonteTelaFimDeJogo()
{
	String fontPath2 = "fonts/Wonton.ttf";
    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
    TextView textoSuaPontuacao = (TextView) findViewById(R.id.textoSuaPontuacao);
    textoSuaPontuacao.setTypeface(tf2);
    TextView textoPontuacaoAdversario = (TextView) findViewById(R.id.textoPontuacaoAdversario);
    textoPontuacaoAdversario.setTypeface(tf2);
    TextView voceganhououperdeu = (TextView) findViewById(R.id.voceganhououperdeu);
    voceganhououperdeu.setTypeface(tf2);
    
    TextView titulofimdejogocasual = (TextView) findViewById(R.id.titulofimdejogocasual);
    titulofimdejogocasual.setTypeface(tf2);
    
    
    Button botao_abre_chat = (Button) findViewById(R.id.botao_abre_chat);
    botao_abre_chat.setTypeface(tf2);
    Button botao_menu_principal = (Button) findViewById(R.id.botao_menu_principal);
    botao_menu_principal.setTypeface(tf2);
	
}

private void mudarFonteTextoPopupFimDeJogoCasual(Dialog dialog)
{
	String fontPath2 = "fonts/Wonton.ttf";
    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
    
    Button sendBtn = (Button) dialog.findViewById(R.id.sendMessageChat);
    sendBtn.setTypeface(tf2);
    
    Button botao_fechar_chat_final_modo_casual = (Button) dialog.findViewById(R.id.botao_fechar_chat_final_modo_casual);
    botao_fechar_chat_final_modo_casual.setTypeface(tf2);
}

private void mudarFonteTextoMostrarDescricaoItensCasual()
{
	String fontPath2 = "fonts/Wonton.ttf";
    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
    
    TextView descricao_modo_casual = (TextView) findViewById(R.id.descricao_modo_casual);
    descricao_modo_casual.setTypeface(tf2);
    
    TextView descricao_trovao_tira_carta_aleatoria = (TextView) findViewById(R.id.descricao_trovao_tira_carta_aleatoria);
    descricao_trovao_tira_carta_aleatoria.setTypeface(tf2);
    
    TextView descricao_parar_tempo = (TextView) findViewById(R.id.descricao_parar_tempo);
    descricao_parar_tempo.setTypeface(tf2);
    
    TextView descricao_trovao_tira_carta = (TextView) findViewById(R.id.descricao_trovao_tira_carta);
    descricao_trovao_tira_carta.setTypeface(tf2);
    
    TextView descricao_misturar_cartas = (TextView) findViewById(R.id.descricao_misturar_cartas);
    descricao_misturar_cartas.setTypeface(tf2);
    
    TextView descricao_nao_espere_mais = (TextView) findViewById(R.id.descricao_nao_espere_mais);
    descricao_nao_espere_mais.setTypeface(tf2);
    
    TextView descricao_mudar_dica = (TextView) findViewById(R.id.descricao_mudar_dica);
    descricao_mudar_dica.setTypeface(tf2);
    
    TextView descricao_dois_x = (TextView) findViewById(R.id.descricao_dois_x);
    descricao_dois_x.setTypeface(tf2);
    
    TextView descricao_reviver_carta = (TextView) findViewById(R.id.descricao_reviver_carta);
    descricao_reviver_carta.setTypeface(tf2);
    
    TextView descricao_cartas_douradas = (TextView) findViewById(R.id.descricao_cartas_douradas);
    descricao_cartas_douradas.setTypeface(tf2);
    
    TextView texto_nao_mostrar_novamente = (TextView) findViewById(R.id.texto_nao_mostrar_novamente);
    texto_nao_mostrar_novamente.setTypeface(tf2);
    
    TextView textoTituloCasualMostrarItens = (TextView) findViewById(R.id.textoTituloCasualMostrarItens);
    textoTituloCasualMostrarItens.setTypeface(tf2);
}

@Override
public void onNothingSelected(AdapterView<?> arg0) 
{
	// TODO Auto-generated method stub
	
}

public void usuarioClicouNoBotaoRevanche(View v)
{
	//um tem de mandar uma mensagem ao outro
	this.mandarMensagemMultiplayer("quero jogar uma revanche");
	
	//agora eh so esperar...
	this.loadingEsperaRevanche = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.por_favor_aguarde));
}

private synchronized void realizarRevanche()
{
	if(this.criouUmaSala == true)
	{
		//eh o criador da sala
		CriarSalaModoCasualTask armazenaNoBdNovaSala = new CriarSalaModoCasualTask(this);
		armazenaNoBdNovaSala.execute(this.salaAtual);
	}
	else
	{
		//tem de esperar ate o adversario criar a sala.
		this.loadingEsperaRevanche = ProgressDialog.show(ModoCasual.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.por_favor_aguarde));
	}
}

/*reiniciar as variaveis eh importante ao fazer uma revanche entre os jogadores*/
private void fazerVariaveisDoJogoFicaremComValoresNullIniciais()
{
	/*this.kanjiDaDica = null;
	this.kanjisDasCartasNaTela = null;
	this.kanjisDasCartasNaTelaQueJaSeTornaramDicas = null;
	this.kanjisQuePodemVirarCartas = null;
	this.palavrasAcertadas = null;
	this.palavrasErradas = null;
	this.palavrasJogadas = null;
	this.quaisCartasEstaoDouradas = null;
	this.salasAbertas = null;
	this.posicoesUltimosKanjisQueSairamDeKanjisQuePodemVirarCartas = null;
	this.naCriacaoDeSalaModoCasual = false;*/
	//SingletonGuardaDadosDaPartida.getInstance().limparCategoriasEKanjis();
}

@Override
public void onRestart()
{
	super.onRestart();
	
	Intent criaTelaInicial =
			new Intent(ModoCasual.this, MainActivity.class);
	criaTelaInicial.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);	
	startActivity(criaTelaInicial);
}

@Override
public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) 
{
	// TODO Auto-generated method stub
	int firstVisibleItem = arg1;
	int totalItemCount = arg3;
	
	ListView listViewSalas = (ListView) findViewById(R.id.lista_salas_abertas);
	int indiceObjetoDeBaixo = listViewSalas.getLastVisiblePosition();
	
	final int lastItem = firstVisibleItem + arg2;
	
	
	ImageView seta_cima_lobby_casual = (ImageView) findViewById(R.id.seta_cima_lobby_casual);
    ImageView seta_baixo_lobby_casual = (ImageView) findViewById(R.id.seta_baixo_lobby_casual);
	
    boolean usuarioEstahNoComecoDaLista = false;
	if (listViewSalas.getFirstVisiblePosition() == 0)
	{
		View childView = listViewSalas.getChildAt(0);
		if(childView != null && childView.getTop() >= 0)
		usuarioEstahNoComecoDaLista = true;
	}
	boolean usuarioEstahNoFimDaLista = false;
	if (listViewSalas.getAdapter() != null && listViewSalas.getLastVisiblePosition() == listViewSalas.getAdapter().getCount() -1)
		
	{
		View childView = listViewSalas.getChildAt(listViewSalas.getChildCount() - 1);
		if(childView != null && childView.getBottom() <= listViewSalas.getHeight())
		{
			usuarioEstahNoFimDaLista = true;
		}
	}
    
	if(usuarioEstahNoComecoDaLista == true && usuarioEstahNoFimDaLista == false)
	{
		//antes era firstVisibleItem == 0
		seta_cima_lobby_casual.setVisibility(View.GONE);
		seta_baixo_lobby_casual.setVisibility(View.VISIBLE);
	}
	else if(usuarioEstahNoComecoDaLista == false && usuarioEstahNoFimDaLista == false)
	{
		seta_cima_lobby_casual.setVisibility(View.VISIBLE);
		seta_baixo_lobby_casual.setVisibility(View.VISIBLE);
	}
	else if(usuarioEstahNoFimDaLista == true && usuarioEstahNoComecoDaLista == false)
	{
		//antes era if(lastItem == totalItemCount)
		//a seta para baixo nao aparece
		seta_cima_lobby_casual.setVisibility(View.VISIBLE);
		seta_baixo_lobby_casual.setVisibility(View.GONE);
	}
	else
	{
		//ambas as setas aparecem
		seta_cima_lobby_casual.setVisibility(View.GONE);
		seta_baixo_lobby_casual.setVisibility(View.GONE);
	}
}

@Override
public void onScrollStateChanged(AbsListView arg0, int arg1) {
	// TODO Auto-generated method stub
	
}

private void fazerMascoteFicarFelizPorUmTempo()
{
	 final ImageView imageViewMascote = (ImageView) findViewById(R.id.mascote);
		imageViewMascote.setImageResource(R.drawable.mascote_feliz);
		
		//a mascote vai ficar com cara de feliz por um tempo e depois volta ao normal
		new Timer().schedule(new TimerTask() 
		 { 
			    @Override
			    public void run() 
			    {
			        //If you want to operate UI modifications, you must run ui stuff on UiThread.
			        ModoCasual.this.runOnUiThread(new Runnable() 
			        {
			            @Override
			            public void run() 
			            {
			            	imageViewMascote.setImageResource(R.drawable.mascote);
			            }
			        });
			    }
			}, 1000);
}

private void fazerMascoteFicarZangadaPorUmTempo()
{
	final ImageView imageViewMascote = (ImageView) findViewById(R.id.mascote);
	imageViewMascote.setImageResource(R.drawable.mascote_zangada);
	
	//a mascote vai ficar com cara de zangada por um tempo e depois volta ao normal
	new Timer().schedule(new TimerTask() 
	 { 
		    @Override
		    public void run() 
		    {
		        //If you want to operate UI modifications, you must run ui stuff on UiThread.
		        ModoCasual.this.runOnUiThread(new Runnable() 
		        {
		            @Override
		            public void run() 
		            {
		            	imageViewMascote.setImageResource(R.drawable.mascote);
		            }
		        });
		    }
		}, 1000);
}

/*funcao chamada toda vez que o verso da carta deve aparecer e os dois textos (hiragana e kanji) devem aparecer.
 * O indice vai de 0 ate 7*/
//A FUNCAO ABAIXO ESTA SENDO CHAMADA DUAS VEZES PELA PESSOA QUE ACERTA A CARTA. NA SEGUNDA VEZ, EH SEMPRE A CARTA 8 O INDICE
private void fazerTextoVersoDaCartaAparecer(KanjiTreinar umKanjiParaReferencia, int indice_qual_carta_para_verso)
{
	//o verso deve ficar com a traducao em hiragana dela e seu kanji
	String hiraganaDoKanji = umKanjiParaReferencia.getHiraganaDoKanji();
	TextView textViewQueVaiDesaparecer = null;
	TextView texto_karuta_esquerda = null;
	TextView texto_karuta_direita = null;
	RelativeLayout textoDuploKaruta = null;
	
	if(indice_qual_carta_para_verso == 0)
	{
		textViewQueVaiDesaparecer = (TextView) findViewById(R.id.texto_karuta1);
		texto_karuta_esquerda = (TextView) findViewById(R.id.texto_karuta1_esquerda);
		texto_karuta_direita = (TextView) findViewById(R.id.texto_karuta1_direita);
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta1); 
	}
	else if(indice_qual_carta_para_verso == 1)
	{
		textViewQueVaiDesaparecer = (TextView) findViewById(R.id.texto_karuta2);
		texto_karuta_esquerda = (TextView) findViewById(R.id.texto_karuta2_esquerda);
		texto_karuta_direita = (TextView) findViewById(R.id.texto_karuta2_direita);
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta2); 
	}
	else if(indice_qual_carta_para_verso == 2)
	{
		textViewQueVaiDesaparecer = (TextView) findViewById(R.id.texto_karuta3);
		texto_karuta_esquerda = (TextView) findViewById(R.id.texto_karuta3_esquerda);
		texto_karuta_direita = (TextView) findViewById(R.id.texto_karuta3_direita);
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta3); 
	}
	else if(indice_qual_carta_para_verso == 3)
	{
		textViewQueVaiDesaparecer = (TextView) findViewById(R.id.texto_karuta4);
		texto_karuta_esquerda = (TextView) findViewById(R.id.texto_karuta4_esquerda);
		texto_karuta_direita = (TextView) findViewById(R.id.texto_karuta4_direita);
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta4); 
	}
	else if(indice_qual_carta_para_verso == 4)
	{
		textViewQueVaiDesaparecer = (TextView) findViewById(R.id.texto_karuta5);
		texto_karuta_esquerda = (TextView) findViewById(R.id.texto_karuta5_esquerda);
		texto_karuta_direita = (TextView) findViewById(R.id.texto_karuta5_direita);
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta5); 
	}
	if(indice_qual_carta_para_verso == 5)
	{
		textViewQueVaiDesaparecer = (TextView) findViewById(R.id.texto_karuta6);
		texto_karuta_esquerda = (TextView) findViewById(R.id.texto_karuta6_esquerda);
		texto_karuta_direita = (TextView) findViewById(R.id.texto_karuta6_direita);
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta6); 
	}
	else if(indice_qual_carta_para_verso == 6)
	{
		textViewQueVaiDesaparecer = (TextView) findViewById(R.id.texto_karuta7);
		texto_karuta_esquerda = (TextView) findViewById(R.id.texto_karuta7_esquerda);
		texto_karuta_direita = (TextView) findViewById(R.id.texto_karuta7_direita);
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta7); 
	}
	else if(indice_qual_carta_para_verso == 7)
	{
		textViewQueVaiDesaparecer = (TextView) findViewById(R.id.texto_karuta8);
		texto_karuta_esquerda = (TextView) findViewById(R.id.texto_karuta8_esquerda);
		texto_karuta_direita = (TextView) findViewById(R.id.texto_karuta8_direita);
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta8); 
	}
		
	
	textViewQueVaiDesaparecer.setVisibility(View.GONE);
	textoDuploKaruta.setVisibility(View.VISIBLE);
	colocarTextoVerticalNaCarta(texto_karuta_esquerda, umKanjiParaReferencia.getKanji());
	colocarTextoVerticalNaCarta(texto_karuta_direita, hiraganaDoKanji);
}

/*funcao que ocorre quando a carta deixa de mostrar apenas o verso*/
private void fazerTextoVersoDaCartaDesaparecer(int indice_qual_carta_para_verso)
{
	RelativeLayout textoDuploKaruta = null;
	TextView textViewReaparece = null;
	
	if(indice_qual_carta_para_verso == 0)
	{
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta1);
		textViewReaparece = (TextView) findViewById(R.id.texto_karuta1); 
	}
	else if(indice_qual_carta_para_verso == 1)
	{
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta2);
		textViewReaparece = (TextView) findViewById(R.id.texto_karuta2); 
	}
	else if(indice_qual_carta_para_verso == 2)
	{
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta3);
		textViewReaparece = (TextView) findViewById(R.id.texto_karuta3); 
	}
	else if(indice_qual_carta_para_verso == 3)
	{
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta4);
		textViewReaparece = (TextView) findViewById(R.id.texto_karuta4); 
	}
	else if(indice_qual_carta_para_verso == 4)
	{
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta5);
		textViewReaparece = (TextView) findViewById(R.id.texto_karuta5); 
	}
	else if(indice_qual_carta_para_verso == 5)
	{
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta6);
		textViewReaparece = (TextView) findViewById(R.id.texto_karuta6); 
	}
	else if(indice_qual_carta_para_verso == 6)
	{
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta7);
		textViewReaparece = (TextView) findViewById(R.id.texto_karuta7); 
	}
	else if(indice_qual_carta_para_verso == 7)
	{
		textoDuploKaruta = (RelativeLayout) findViewById(R.id.textoDuploKaruta8);
		textViewReaparece = (TextView) findViewById(R.id.texto_karuta8); 
	}
	
	textoDuploKaruta.setVisibility(View.GONE);
	textViewReaparece.setVisibility(View.VISIBLE);
	
}

}

  
 
