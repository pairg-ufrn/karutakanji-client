package com.karutakanji;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import lojinha.ConcreteDAOAcessaDinheiroDoJogador;
import lojinha.DAOAcessaDinheiroDoJogador;
import lojinha.TransformaPontosEmCredito;

import bancodedados.ArmazenaKanjisPorCategoria;
import bancodedados.ArmazenaTudoParaJogoOffline;
import bancodedados.InserirPartidaTreinamentoNoLogTask;
import bancodedados.KanjiTreinar;
import bancodedados.PegaKanjisTreinamentoErrouTask;
import bancodedados.PegaKanjisTreinamentoMenosTreinouTask;
import bancodedados.SingletonArmazenaCategoriasDoJogo;
import br.ufrn.dimap.pairg.karutakanji.android.R;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ModoTreinamento extends ActivityDoJogoComSom implements OnClickListener
{
	private int nivel; 
	private int vidas; //jogador comeca com 4 vidas e vai diminuindo
	private int suaPontuacao;
	private LinkedList<KanjiTreinar> kanjisDasCartasNaTela;
	private LinkedList<KanjiTreinar> kanjisDasCartasNaTelaQueJaSeTornaramDicas;
	private LinkedList<KanjiTreinar> kanjisQueUsuarioErrou;
	private LinkedList<KanjiTreinar> kanjisQueUsuarioAcertou;
	private KanjiTreinar kanjiDaDica;
	
	private int quantosNiveisPassaram; //numero que so vai de 0 a 2. De 2 em 2 niveis, o usuario perceberah uma mudanca nas cartas ou nos kanjis 
	private LinkedList<KanjiTreinar> kanjisQuePodemVirarCartasNovas;
	private LinkedList<KanjiTreinar> kanjisQueJaViraramCartas; //no jogo inteiro
	private LinkedList<KanjiTreinar> ultimosKanjis; //os kanjis serao ensinados de 4 em 4. Esses 4 ensinados deveriam aparecer com maior frequencia na tela p o usuario memoriza-los
	
	private int quantasCartasHaveraoNaTela; //no nivel 5,9,13,17,21, 25, 29 e 33  esse numero aumenta
	private boolean naoHaMaisNovosKanjisParaSeCriar;
	
	private boolean mostrarDicas; //deve-se mostrar os 4 ultimos kanjis ou nao? Esse valor vem dos extras do intent da tela anterior usada para criar essa tela daqui
	
	final static int[] SCREENS = {R.id.tela_modo_treinamento,R.id.tela_observacao_novos_kanjis,R.id.tela_fim_do_modo_treinamento,R.id.tela_sem_nada_treinamento};
	
	private LinkedList<KanjiTreinar> kanjisUsuarioErrouTreinamentoEmOrdemDoMaisErradoParaOMenos;
	private LinkedList<KanjiTreinar> kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento; //no começo, é igual a lista acima, mas depois os kanjis vao desaparecendo a medida que vao sendo escolhidos p dica
	private boolean usuarioQuerTreinarComAsPalavrasQueEleMaisErrou;
	
	private LinkedList<KanjiTreinar> kanjisUsuarioMenosTreinouTreinamentoQuePodemSerUsadosNoTreinamento;
	private boolean usuarioQuerTreinarComAsPalavrasQueEleMenosTreinou;
	
	private Runnable runnableThreadAumentaTempoNoJogo; //precisamos armazenar esse carinha como variavel pq precisaremos mata-lo assim que o jogo terminar
	private Handler handlerParaThreadAumentaTempoDoJogo; //mesmo do acima
	private boolean runnableThreadAumentaTempoNoJogoDeveriaMorrer;
	private boolean runnableThreadAumentaTempoNoJogoJaFoiCriada;
	private int tempoDecorrido; //em segundos
	private boolean tempoEstahParado; //o cronometro parou ou nao
	
	//coisas para o cronometro que fica mostrando
	private boolean perguntarAoUsuarioSeQuerTerminarJogoDepoisDeUmTempo; //a mascote vai perguntar de x em x segundos se o usuario quer terminar o jogo. Mas se ele n quiser mais ser incomodado, basta dizer p ela parar de perguntar
	//fim das coisas do cronometro que fica mostrando
	
	private boolean usuarioNoMeioDeUmTreinamento; //esse booleano eh importante p caso o user aperte back, aidna envie dados pro bd. Ele so irah enviar se estiver no meio de uma partida
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		//coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
		super.getGameHelper().setMaxAutoSignInAttempts(0);
		//fim das coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modo_treinamento);
		switchToScreen(R.id.tela_sem_nada_treinamento);
		
		this.usuarioNoMeioDeUmTreinamento = false;
		this.mostrarDicas = SingletonConfiguracoesModoTreinamento.getInstance().getMostrarDicas();
		this.usuarioQuerTreinarComAsPalavrasQueEleMaisErrou = SingletonConfiguracoesModoTreinamento.getInstance().getTreinarPalavrasErradas();
		//antes de comecar o jogo, caso o usuario tenha internet, iremos criar uma task para pegar os kanjis que ele mais errou, pois serao os usados inicialmente na geracao da dica
		
		this.usuarioQuerTreinarComAsPalavrasQueEleMenosTreinou = SingletonConfiguracoesModoTreinamento.getInstance().getTreinarPalavrasMenosTreinadas();
		//antes de comecar o jogo, caso o usuario tenha internet, iremos criar uma task para pegar os kanjis que ele menos treinou, pois serao os usados inicialmente na geracao da dica
		
		boolean temConexaoInternet = this.temConexaoComInternet();
		if(temConexaoInternet == false || (usuarioQuerTreinarComAsPalavrasQueEleMaisErrou == false && usuarioQuerTreinarComAsPalavrasQueEleMenosTreinou == false))
		{
			this.comecarJogoPelaPrimeiraVez();
			if(this.mostrarDicas == true)
			{
				this.mostrarTelaObservacaoNovosKanjis();
			}
			else
			{
				this.mostrarTelaModoTreinamento(null);
			}
		}
		else if(temConexaoInternet == true && usuarioQuerTreinarComAsPalavrasQueEleMaisErrou == true)
		{
			LinkedList<String> nomeEEMailUsuario = ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(getApplicationContext());
			String nome_usuario = nomeEEMailUsuario.get(0);
			
			//vou so pegar os nomes das categorias escolhidas e juntar esses nomes numa string gigante separada por virgulas
			HashMap<String,LinkedList<KanjiTreinar>> categoriasEscolhidasEKanjisDelas = SingletonGuardaDadosDaPartida.getInstance().getCategoriasEscolhidasEKanjisDelas();
			 
			 Iterator<String> iteradorCategoriasEKanjis = categoriasEscolhidasEKanjisDelas.keySet().iterator();
			 String categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula = "";
			 while(iteradorCategoriasEKanjis.hasNext() == true)
			 {
				 String umaCategoria = iteradorCategoriasEKanjis.next();
				 categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula = categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula + umaCategoria;
				 
				 if(iteradorCategoriasEKanjis.hasNext() == true)
				 {
					 categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula = categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula + ",";
				 }
			 }
			
			PegaKanjisTreinamentoErrouTask pegaKanjisQueUsuarioErrou = new PegaKanjisTreinamentoErrouTask(this);
			pegaKanjisQueUsuarioErrou.execute(nome_usuario,categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula);
		}
		else if(temConexaoInternet == true && usuarioQuerTreinarComAsPalavrasQueEleMenosTreinou == true)
		{
			LinkedList<String> nomeEEMailUsuario = ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(getApplicationContext());
			String nome_usuario = nomeEEMailUsuario.get(0);
			
			//vou so pegar os nomes das categorias escolhidas e juntar esses nomes numa string gigante separada por virgulas
			HashMap<String,LinkedList<KanjiTreinar>> categoriasEscolhidasEKanjisDelas = SingletonGuardaDadosDaPartida.getInstance().getCategoriasEscolhidasEKanjisDelas();
			 
			 Iterator<String> iteradorCategoriasEKanjis = categoriasEscolhidasEKanjisDelas.keySet().iterator();
			 String categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula = "";
			 while(iteradorCategoriasEKanjis.hasNext() == true)
			 {
				 String umaCategoria = iteradorCategoriasEKanjis.next();
				 categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula = categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula + umaCategoria;
				 
				 if(iteradorCategoriasEKanjis.hasNext() == true)
				 {
					 categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula = categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula + ",";
				 }
			 }
			
			PegaKanjisTreinamentoMenosTreinouTask pegaKanjisQueUsuarioMenosTreinou = new PegaKanjisTreinamentoMenosTreinouTask(this);
			pegaKanjisQueUsuarioMenosTreinou.execute(nome_usuario,categoriasUsuarioEscolheuParaJogarSeparadasPorVirgula);
		}
		
	}
	
	/*metodo chamado apos a PegaKanjisTreinamentoErrouTask terminar seu servico*/
	public void terminouDeEncontrarTodosOskanjisUsuarioErrouNoTreinamento(LinkedList<KanjiTreinar> kanjisErrados)
	{
		this.kanjisUsuarioErrouTreinamentoEmOrdemDoMaisErradoParaOMenos = kanjisErrados;
		this.kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento = new LinkedList<KanjiTreinar>();
		for(int i = 0; i < this.kanjisUsuarioErrouTreinamentoEmOrdemDoMaisErradoParaOMenos.size(); i++)
		{
			this.kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento.add(this.kanjisUsuarioErrouTreinamentoEmOrdemDoMaisErradoParaOMenos.get(i));
		}
		
		this.comecarJogoPelaPrimeiraVez();
		if(this.mostrarDicas == true)
		{
			this.mostrarTelaObservacaoNovosKanjis();
		}
		else
		{
			this.mostrarTelaModoTreinamento(null);
		}
	}
	
	/*metodo chamado apos a PegaKanjisTreinamentoMenosTreinouTask terminar seu servico*/
	public void terminouDeEncontrarTodosOskanjisUsuarioMenosTreinouNoTreinamento(LinkedList<KanjiTreinar> kanjisMenosTreinados)
	{
		this.kanjisUsuarioMenosTreinouTreinamentoQuePodemSerUsadosNoTreinamento = new LinkedList<KanjiTreinar>();
		
		for(int i = 0; i < kanjisMenosTreinados.size(); i++)
		{
			this.kanjisUsuarioMenosTreinouTreinamentoQuePodemSerUsadosNoTreinamento.add(kanjisMenosTreinados.get(i));
		}
		
		this.comecarJogoPelaPrimeiraVez();
		if(this.mostrarDicas == true)
		{
			this.mostrarTelaObservacaoNovosKanjis();
		}
		else
		{
			this.mostrarTelaModoTreinamento(null);
		}
	}
	
	private void comecarJogoPelaPrimeiraVez()
	{	
		this.nivel = 1;
		this.vidas = 4;
		this.suaPontuacao = 0;
		this.quantosNiveisPassaram = 0;
		quantasCartasHaveraoNaTela = 4;
		kanjisQueJaViraramCartas = new LinkedList<KanjiTreinar>();
		ultimosKanjis = new LinkedList<KanjiTreinar>();
		kanjisQueUsuarioErrou = new LinkedList<KanjiTreinar>();
		kanjisQueUsuarioAcertou = new LinkedList<KanjiTreinar>();
		this.kanjisDasCartasNaTela = new LinkedList<KanjiTreinar>();
		this.kanjisDasCartasNaTelaQueJaSeTornaramDicas = new LinkedList<KanjiTreinar>();
		
		naoHaMaisNovosKanjisParaSeCriar = false;
		this.runnableThreadAumentaTempoNoJogoJaFoiCriada = false; //a runnablethread soh serah criada quando o user sair da tela das 4 palavras
		
		this.usuarioNoMeioDeUmTreinamento = true;
		
		TextView textNivel = (TextView) findViewById(R.id.nivel);
		String stringNivel = getResources().getString(R.string.nivel_sem_dois_pontos);
		stringNivel = stringNivel + " " + this.nivel;
		textNivel.setText(stringNivel);
		
		pegarTodosOsKanjisQuePodemVirarCartas();
		
		findViewById(R.id.karuta1_imageview).setOnClickListener(this);
		findViewById(R.id.karuta2_imageview).setOnClickListener(this);
		findViewById(R.id.karuta3_imageview).setOnClickListener(this);
		findViewById(R.id.karuta4_imageview).setOnClickListener(this);
		findViewById(R.id.karuta5_imageview).setOnClickListener(this);
		findViewById(R.id.karuta6_imageview).setOnClickListener(this);
		findViewById(R.id.karuta7_imageview).setOnClickListener(this);
		findViewById(R.id.karuta8_imageview).setOnClickListener(this);
		findViewById(R.id.karuta9_imageview).setOnClickListener(this);
		findViewById(R.id.karuta10_imageview).setOnClickListener(this);
		findViewById(R.id.karuta11_imageview).setOnClickListener(this);
		findViewById(R.id.karuta12_imageview).setOnClickListener(this);
		this.mudarFonteDosKanjis();
		
		TextView textoPontuacao = (TextView) findViewById(R.id.pontuacao);
		String pontuacao = getResources().getString(R.string.pontos);
		textoPontuacao.setText(pontuacao + String.valueOf(this.suaPontuacao));
		
		this.escolherKanjisParaONivel();
		this.gerarKanjiDaDica();
		this.tornarCartasNaTelaClicaveisEVaziasNovamente();
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.modo_treinamento, menu);
		return true;
	}
	
	
	private void escolherNovosKanjis()
	{
		this.ultimosKanjis.clear();
		
		for(int i = 0; i < 4; i++)
		{
			KanjiTreinar kanjiTreinar = this.escolherUmNovoKanjiParaTreinar();
			
			 if(kanjiTreinar == null)
			 {
				 //acabaram-se os kanjis que posso usar na tela
				 this.naoHaMaisNovosKanjisParaSeCriar = true;
				 break;
				 
			 }
			 else
			 {
				 this.ultimosKanjis.add(kanjiTreinar);
			 }
		}
		
		//faltou renovar os kanjisusuarioerrou
		if(this.kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento != null 
				&& kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento.size() == 0)
		{
			//hora de renovar esses kanjis
			for(int j = 0; j < this.kanjisUsuarioErrouTreinamentoEmOrdemDoMaisErradoParaOMenos.size(); j++)
			{
				this.kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento.add(this.kanjisUsuarioErrouTreinamentoEmOrdemDoMaisErradoParaOMenos.get(j));
			}
		
		}
	}
	
	public void mostrarTelaModoTreinamento(View v)
	{
		this.switchToScreen(R.id.tela_modo_treinamento);
		
		/*if(this.nivel == 1)
		{
			//no primeiro nivel, iremos iniciar a musica de fundo
			this.mudarMusicaDeFundo(R.raw.japan_in_spring);
		}*/
		
		//assim que a tela do modo treinamento for mostrada, o cronômetro do modo treinamento deveria reiniciar/iniciar
		if(runnableThreadAumentaTempoNoJogoJaFoiCriada == false)
		{
			//a runnablethread n foi criada? Pois vamos criar! Isso ocorre no inicio do jogo 
			this.tempoEstahParado = false;
			this.tempoDecorrido = 0;
			
			this.perguntarAoUsuarioSeQuerTerminarJogoDepoisDeUmTempo = true;
			this.handlerParaThreadAumentaTempoDoJogo = new Handler();
			 this.runnableThreadAumentaTempoNoJogoDeveriaMorrer = false;
			 this.runnableThreadAumentaTempoNoJogo = new Runnable() {
		         @Override
		         public void run() 
		         {
		        	 if(tempoEstahParado == false)
		        	 {
		        		 ModoTreinamento.this.runOnUiThread(new Runnable() 
			 		        {
			 		            @Override
			 		            public void run() 
			 		            {
			 		            	if(tempoDecorrido < 3600)
			 		            	{
			 		            		passarUmSegundo();
				 	 		            if(tempoDecorrido % 300 == 0 && perguntarAoUsuarioSeQuerTerminarJogoDepoisDeUmTempo == true)
				 	 		            {
				 	 		            		//perguntar se o usuario quer terminar a partida
				 	 		            		perguntarSeUsuarioQuerTerminarPartida();
				 		            	}
				 	 		            
				 	 		            if(tempoDecorrido == 3600)
				 	 		            {
				 	 		            	tempoDecorrido = 0;
				 	 		            }
			 		            	}
			 		            }
			 		        });
		        		 
		        	 }
		        	 
		        	 if(runnableThreadAumentaTempoNoJogoDeveriaMorrer == false)
		             {
		            	 handlerParaThreadAumentaTempoDoJogo.postDelayed(this, 1000);  
		             }
   
		         }
		     };
		     
		     handlerParaThreadAumentaTempoDoJogo.postDelayed(runnableThreadAumentaTempoNoJogo, 1000);
		     this.runnableThreadAumentaTempoNoJogoJaFoiCriada = true;
		}
	}
	
	private void passarUmSegundo()
	 {
		 this.tempoDecorrido = this.tempoDecorrido + 1;
		 String tempoParaMostrar = ""; //n irei mostrar 90s, irei mostrar 1:30,1:29,0:30...
		 
		 String tempoDecorridoEmMinutosESegundos = converterTempoDecorridoEmMinutosESegundos();
		 String[] minutosESegundosELabels = tempoDecorridoEmMinutosESegundos.split(";");
		 String[] minutosELabel = minutosESegundosELabels[0].split(":");
		 String[] segundosELabel = minutosESegundosELabels[1].split(":");
		 String minutos = minutosELabel[1];
		 String segundos = segundosELabel[1];
		 
		 tempoParaMostrar = minutos;
		 
		 if(segundos.length() == 1)
		 {
			 tempoParaMostrar = minutos + ":0" + String.valueOf(segundos); 
		 }
		 else
		 {
			 tempoParaMostrar = minutos + ":" + String.valueOf(segundos);
		 }
	 }
	
	/*transforma o tempo decorrido em minutos e segundos*/
	private String converterTempoDecorridoEmMinutosESegundos() 
	{
	    final int SECONDS_IN_A_MINUTE = 60;
	    final int MINUTES_IN_AN_HOUR = 60;

	    int seconds = this.tempoDecorrido % SECONDS_IN_A_MINUTE;
	    int totalMinutes = this.tempoDecorrido / SECONDS_IN_A_MINUTE;
	    int minutes = totalMinutes % MINUTES_IN_AN_HOUR;

	    return  "minutes:" + minutes + ";seconds:" + seconds;
	}
	
	
	private void perguntarSeUsuarioQuerTerminarPartida()
	 {
		 this.tempoEstahParado = true; //enquanto a pergunta estiver na tela, o tempo n passa
		 
		 final TextView dica_kanji = (TextView) findViewById(R.id.dica_kanji);
		 final TextView dica_kanji_traducao = (TextView) findViewById(R.id.dica_kanji_traducao);
		 final RelativeLayout botoesPararTreinamento = (RelativeLayout) findViewById(R.id.botoesPararTreinamento);
		 final Button botaoPararTreinamentoNao = (Button) findViewById(R.id.botaoPararTreinamentoNao);
		 final String textoAnterior_dica_kanji = dica_kanji.getText().toString();
		 final String textoAnterior_dica_kanji_traducao = dica_kanji_traducao.getText().toString();
		 
		 String voce_ja_treinou_muito = getResources().getString(R.string.voce_ja_treinou_muito);
		 String quer_terminar = getResources().getString(R.string.quer_terminar);
		 
		 dica_kanji.setText(voce_ja_treinou_muito);
		 dica_kanji_traducao.setText(quer_terminar);
		 dica_kanji.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		 
		 botoesPararTreinamento.setVisibility(View.VISIBLE);
		 
		 boolean usuarioUsandoVersaoJaponesa = false; //se o usuario estiver usando a versao japonesa, a traducao n eh necessaria
		 Resources res = this.getResources();
         Locale myLocale = res.getConfiguration().locale;
         if(myLocale != null)
 		 {
 			String language = myLocale.getLanguage();
 			if(myLocale.getLanguage().compareTo("jp") == 0)
 			{
 				usuarioUsandoVersaoJaponesa = true;
 			}
 		 }
         
         final boolean usuarioUsandoVersaoJaponesaFinal = usuarioUsandoVersaoJaponesa;
		 
		 botaoPararTreinamentoNao.setOnClickListener(new Button.OnClickListener() 
		  {
			  public void onClick(View v) 
		      {
				  //volta tudo ao normal
				  dica_kanji.setText(textoAnterior_dica_kanji);
				  if(usuarioUsandoVersaoJaponesaFinal == false)
				  {
					  dica_kanji_traducao.setText(textoAnterior_dica_kanji_traducao);
				  }
				  else
				  {
					  dica_kanji_traducao.setText("");
				  }
			      dica_kanji.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
				  botoesPararTreinamento.setVisibility(View.GONE);
				  tempoEstahParado = false;

			  }
		  });
	 }
	
	public void mostrarTelaObservacaoNovosKanjis()
	{
		this.switchToScreen(R.id.tela_observacao_novos_kanjis);
		this.mudarFonte4UltimosKanjisETitulo();
		TextView textoUltimoKanji1 = (TextView) findViewById(R.id.ultimokanji1);
		TextView textoUltimoKanji2 = (TextView) findViewById(R.id.ultimokanji2);
		TextView textoUltimoKanji3 = (TextView) findViewById(R.id.ultimokanji3);
		TextView textoUltimoKanji4 = (TextView) findViewById(R.id.ultimokanji4);
		TextView textoUltimoKanji1Hiragana = (TextView) findViewById(R.id.ultimoKanji1Hiragana);
		TextView textoUltimoKanji2Hiragana = (TextView) findViewById(R.id.ultimoKanji2Hiragana);
		TextView textoUltimoKanji3Hiragana = (TextView) findViewById(R.id.ultimoKanji3Hiragana);
		TextView textoUltimoKanji4Hiragana = (TextView) findViewById(R.id.ultimoKanji4Hiragana);
		TextView textoUltimoKanji1Traducao = (TextView) findViewById(R.id.ultimoKanji1Traducao);
		TextView textoUltimoKanji2Traducao = (TextView) findViewById(R.id.ultimoKanji2Traducao);
		TextView textoUltimoKanji3Traducao = (TextView) findViewById(R.id.ultimoKanji3Traducao);
		TextView textoUltimoKanji4Traducao = (TextView) findViewById(R.id.ultimoKanji4Traducao);
		TextView textoUltimoKanji1Igual = (TextView) findViewById(R.id.ultimokanji1Igual);
		TextView textoUltimoKanji2Igual = (TextView) findViewById(R.id.ultimokanji2Igual);
		TextView textoUltimoKanji3Igual = (TextView) findViewById(R.id.ultimokanji3Igual);
		TextView textoUltimoKanji4Igual = (TextView) findViewById(R.id.ultimokanji4Igual);
		
		this.mudarFonteTraducoesQuatroUltimosKanjis();
		
		if(this.ultimosKanjis.size() == 0)
		{
			mostrarTelaModoTreinamento(null);
		}
		else if(this.ultimosKanjis.size() == 1)
		{
			KanjiTreinar ultimoKanji1 = this.ultimosKanjis.get(0); 
			String textoDicaKanji1 = ultimoKanji1.getKanji(); 
			textoUltimoKanji1.setText(textoDicaKanji1);
			textoUltimoKanji1Hiragana.setText(ultimoKanji1.getHiraganaDoKanji());
			textoUltimoKanji1Traducao.setText(ultimoKanji1.getTraducaoEmPortugues());
			textoUltimoKanji1Igual.setText(" = ");
			
			textoUltimoKanji2.setText("");
			textoUltimoKanji3.setText("");
			textoUltimoKanji4.setText("");
			textoUltimoKanji2Hiragana.setText("");
			textoUltimoKanji3Hiragana.setText("");
			textoUltimoKanji4Hiragana.setText("");
			textoUltimoKanji2Traducao.setText("");
			textoUltimoKanji3Traducao.setText("");
			textoUltimoKanji4Traducao.setText("");
			textoUltimoKanji2Igual.setText("");
			textoUltimoKanji3Igual.setText("");
			textoUltimoKanji4Igual.setText("");
			
		}
		else if(this.ultimosKanjis.size() == 2)
		{
			KanjiTreinar ultimoKanji1 = this.ultimosKanjis.get(0); 
			KanjiTreinar ultimoKanji2 = this.ultimosKanjis.get(1); 
			
			String textoDicaKanji1 = ultimoKanji1.getKanji(); 
			textoUltimoKanji1.setText(textoDicaKanji1);
			textoUltimoKanji1Hiragana.setText(ultimoKanji1.getHiraganaDoKanji());
			textoUltimoKanji1Traducao.setText(ultimoKanji1.getTraducaoEmPortugues());
			String textoDicaKanji2 = ultimoKanji2.getKanji(); 
			textoUltimoKanji2.setText(textoDicaKanji2);
			textoUltimoKanji2Hiragana.setText(ultimoKanji2.getHiraganaDoKanji());
			textoUltimoKanji2Traducao.setText(ultimoKanji2.getTraducaoEmPortugues());
			textoUltimoKanji1Igual.setText(" = ");
			textoUltimoKanji2Igual.setText(" = ");
			
			textoUltimoKanji3.setText("");
			textoUltimoKanji4.setText("");
			textoUltimoKanji3Hiragana.setText("");
			textoUltimoKanji4Hiragana.setText("");
			textoUltimoKanji3Traducao.setText("");
			textoUltimoKanji4Traducao.setText("");
			textoUltimoKanji3Igual.setText("");
			textoUltimoKanji4Igual.setText("");
		}
		else if(this.ultimosKanjis.size() == 3)
		{
			KanjiTreinar ultimoKanji1 = this.ultimosKanjis.get(0); 
			KanjiTreinar ultimoKanji2 = this.ultimosKanjis.get(1); 
			KanjiTreinar ultimoKanji3 = this.ultimosKanjis.get(2); 
			
			String textoDicaKanji1 = ultimoKanji1.getKanji(); 
			textoUltimoKanji1.setText(textoDicaKanji1);
			textoUltimoKanji1Hiragana.setText(ultimoKanji1.getHiraganaDoKanji());
			textoUltimoKanji1Traducao.setText(ultimoKanji1.getTraducaoEmPortugues());
			String textoDicaKanji2 = ultimoKanji2.getKanji(); 
			textoUltimoKanji2.setText(textoDicaKanji2);
			textoUltimoKanji2Hiragana.setText(ultimoKanji2.getHiraganaDoKanji());
			textoUltimoKanji2Traducao.setText(ultimoKanji2.getTraducaoEmPortugues());
			String textoDicaKanji3 = ultimoKanji3.getKanji(); 
			textoUltimoKanji3.setText(textoDicaKanji3);
			textoUltimoKanji3Hiragana.setText(ultimoKanji3.getHiraganaDoKanji());
			textoUltimoKanji3Traducao.setText(ultimoKanji3.getTraducaoEmPortugues());
			textoUltimoKanji1Igual.setText(" = ");
			textoUltimoKanji2Igual.setText(" = ");
			textoUltimoKanji3Igual.setText(" = ");
			
			textoUltimoKanji4.setText("");
			textoUltimoKanji4Hiragana.setText("");
			textoUltimoKanji4Traducao.setText("");
			textoUltimoKanji4Igual.setText("");
		}
		else if(this.ultimosKanjis.size() == 4)
		{
			KanjiTreinar ultimoKanji1 = this.ultimosKanjis.get(0); 
			KanjiTreinar ultimoKanji2 = this.ultimosKanjis.get(1); 
			KanjiTreinar ultimoKanji3 = this.ultimosKanjis.get(2); 
			KanjiTreinar ultimoKanji4 = this.ultimosKanjis.get(3); 
			
			String textoDicaKanji1 = ultimoKanji1.getKanji(); 
			textoUltimoKanji1.setText(textoDicaKanji1);
			textoUltimoKanji1Hiragana.setText(ultimoKanji1.getHiraganaDoKanji());
			textoUltimoKanji1Traducao.setText(ultimoKanji1.getTraducaoEmPortugues());
			String textoDicaKanji2 = ultimoKanji2.getKanji(); 
			textoUltimoKanji2.setText(textoDicaKanji2);
			textoUltimoKanji2Hiragana.setText(ultimoKanji2.getHiraganaDoKanji());
			textoUltimoKanji2Traducao.setText(ultimoKanji2.getTraducaoEmPortugues());
			String textoDicaKanji3 = ultimoKanji3.getKanji(); 
			textoUltimoKanji3.setText(textoDicaKanji3);
			textoUltimoKanji3Hiragana.setText(ultimoKanji3.getHiraganaDoKanji());
			textoUltimoKanji3Traducao.setText(ultimoKanji3.getTraducaoEmPortugues());
			String textoDicaKanji4 = ultimoKanji4.getKanji(); 
			textoUltimoKanji4.setText(textoDicaKanji4);
			textoUltimoKanji4Hiragana.setText(ultimoKanji4.getHiraganaDoKanji());
			textoUltimoKanji4Traducao.setText(ultimoKanji4.getTraducaoEmPortugues());
			textoUltimoKanji1Igual.setText(" = ");
			textoUltimoKanji2Igual.setText(" = ");
			textoUltimoKanji3Igual.setText(" = ");
			textoUltimoKanji4Igual.setText(" = ");
		}
		
		TextView textoObservacao = (TextView) findViewById(R.id.observacao);
		if(this.nivel == 5 || this.nivel == 9 || this.nivel == 13 || this.nivel == 17 || this.nivel == 21 || this.nivel == 25 || this.nivel == 29 || this.nivel == 33)
		{
			textoObservacao.setVisibility(View.VISIBLE);
		}
		else
		{
			textoObservacao.setVisibility(View.INVISIBLE);
		}
		
		if(this.ultimosKanjis.size() == 1)
		{
			TextView textViewTituloDaTela = (TextView) findViewById(R.id.titulo_tela_observacao_novos_kanjis);
			String texto = getResources().getString(R.string.aviso_ultimos_kanjis_modo_treinamento_uma_palavra);
			textViewTituloDaTela.setText(texto);
		}
		else if(this.ultimosKanjis.size() > 0)
		{
			TextView textViewTituloDaTela = (TextView) findViewById(R.id.titulo_tela_observacao_novos_kanjis);
			String texto = getResources().getString(R.string.aviso_ultimos_kanjis_modo_treinamento);
			textViewTituloDaTela.setText(texto);
		}
		
	}
	
	private void escolherKanjisParaONivel()
	{	 
		 this.kanjisDasCartasNaTela.clear();
		 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.clear();
		 
		 
		 if(this.quantosNiveisPassaram >= 2 || nivel == 1)
		 {
			 //hora de mais novos kanjis
			 this.escolherNovosKanjis();
			 if(this.mostrarDicas == true)
			 {
				this.mostrarTelaObservacaoNovosKanjis(); //aquela tela que o usuario visualiza os novos kanjis
			 }
			 else
			 {
				this.mostrarTelaModoTreinamento(null);
			 }
			 
			 this.quantosNiveisPassaram = 0;
		 }
		  
		 if(naoHaMaisNovosKanjisParaSeCriar == true && this.quantosNiveisPassaram >= 2)
		 {
			 //nao tem mais utilidade esses ultimos kanjis
			 this.ultimosKanjis.clear();
		 }
		 
		//escolher kanjis velhos
		 LinkedList<KanjiTreinar> velhosKanjis = this.escolherKanjisNaoNovos(this.quantasCartasHaveraoNaTela - this.ultimosKanjis.size());
		 
		 LinkedList<KanjiTreinar> kanjisDaTela = new LinkedList<KanjiTreinar>();
		 
		 for(int g = 0; g < ultimosKanjis.size(); g++)
		 {
			 kanjisDaTela.add(ultimosKanjis.get(g));
		 }
		 for(int h = 0; h < velhosKanjis.size(); h++)
		 {
			 kanjisDaTela.add(velhosKanjis.get(h));
		 }
		 
		 Collections.shuffle(kanjisDaTela);
		 
		 
		 for(int i = 0; i < this.quantasCartasHaveraoNaTela; i++)
		 {
			 KanjiTreinar kanjiParaUmaCarta = kanjisDaTela.get(i);
			 
			 this.kanjisDasCartasNaTela.add(kanjiParaUmaCarta);
				 
				 if(i == 0)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta1);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 1)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta2);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 2)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta3);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 3)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta4);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 4)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta5);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 5)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta6);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 6)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta7);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 7)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta8);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 } 
				 else if(i == 8)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta9);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 9)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta10);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 10)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta11);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
				 else if(i == 11)
				 {
					 TextView texto = (TextView) findViewById(R.id.texto_karuta12);
					 this.colocarTextoVerticalNaCarta(texto, kanjiParaUmaCarta.getKanji());
				 }
			 }
		 
	 }
	
	private KanjiTreinar escolherUmNovoKanjiParaTreinar()
	 {
		if(this.kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento != null && 
				this.kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento.size() > 0 &&
				this.usuarioQuerTreinarComAsPalavrasQueEleMaisErrou == true)
		{
			//usuario quer treinar com as palavras que ele errou
			//se ele so errou 3 kanjis, sem problema! Pegaremos kanjis quaisquer
			KanjiTreinar kanjiTreinar = this.kanjisUsuarioErrouTreinamentoQuePodemSerUsadosNoTreinamento.remove(0);
			return kanjiTreinar;
		}
		else if(this.kanjisUsuarioMenosTreinouTreinamentoQuePodemSerUsadosNoTreinamento != null &&
				this.kanjisUsuarioMenosTreinouTreinamentoQuePodemSerUsadosNoTreinamento.size() > 0 &&
				this.usuarioQuerTreinarComAsPalavrasQueEleMenosTreinou == true)
		{
			KanjiTreinar kanjiTreinar = this.kanjisUsuarioMenosTreinouTreinamentoQuePodemSerUsadosNoTreinamento.remove(0);
			return kanjiTreinar;
		}
		else
		{
			//nao iremos usar os kanjis que o usuario tem errado muito nem os menos treinados
			if(kanjisQuePodemVirarCartasNovas.size() <= 0)
			{
				return null;
			}
			else
			{
				Random geraNumAleatorio = new Random();
				 int posicaoKanjiEscolhido = geraNumAleatorio.nextInt(this.kanjisQuePodemVirarCartasNovas.size());
				 
				 KanjiTreinar kanjiEscolhido = this.kanjisQuePodemVirarCartasNovas.remove(posicaoKanjiEscolhido); 
				 
				 this.kanjisQueJaViraramCartas.add(kanjiEscolhido);
				 
				 return kanjiEscolhido;
			} 
		}
	 }
	
	private void pegarTodosOsKanjisQuePodemVirarCartas()
	 {
		 this.kanjisQuePodemVirarCartasNovas = new LinkedList<KanjiTreinar>();
		 HashMap<String,LinkedList<KanjiTreinar>> categoriasEscolhidasEKanjisDelas = SingletonGuardaDadosDaPartida.getInstance().getCategoriasEscolhidasEKanjisDelas();
		 
		 Iterator<String> iteradorCategoriasEKanjis = categoriasEscolhidasEKanjisDelas.keySet().iterator();
		 while(iteradorCategoriasEKanjis.hasNext() == true)
		 {
			 String umaCategoria = iteradorCategoriasEKanjis.next();
			 LinkedList<KanjiTreinar> kanjisDaCategoria = categoriasEscolhidasEKanjisDelas.get(umaCategoria);
			 
			 for(int i = 0; i < kanjisDaCategoria.size(); i++)
			 {
				 this.kanjisQuePodemVirarCartasNovas.add(kanjisDaCategoria.get(i));
			 }
		 }
	 }
	
	public void gerarKanjiDaDica()
	{
		LinkedList<KanjiTreinar> kanjisQueAindaNaoViraramDicas = new LinkedList<KanjiTreinar>();
		 
		 for(int i = 0; i < this.kanjisDasCartasNaTela.size(); i++)
		 {
			 KanjiTreinar umKanji = this.kanjisDasCartasNaTela.get(i);
			 
			 boolean kanjiJaVirouDica = false;
			 for(int j = 0; j < this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size(); j++)
			 {
				 KanjiTreinar umKanjiQueVirouDica = this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.get(j);
				 
				 if((umKanjiQueVirouDica.getKanji().compareTo(umKanji.getKanji()) == 0) 
						 && (umKanjiQueVirouDica.getCategoriaAssociada().compareTo(umKanji.getCategoriaAssociada()) == 0))
				 {
					 kanjiJaVirouDica = true;
				 }
			 }
			 
			 if(kanjiJaVirouDica == false)
			 {
				 kanjisQueAindaNaoViraramDicas.add(umKanji);
			 }
		 }
		 
		 
		 Random geraNumAleatorio = new Random(); 
		 int indiceKanjiDaDica = geraNumAleatorio.nextInt(kanjisQueAindaNaoViraramDicas.size());
		 
		 KanjiTreinar umKanji = kanjisQueAindaNaoViraramDicas.get(indiceKanjiDaDica);
		 this.kanjiDaDica = umKanji;
		 
		 this.alterarTextoDicaComBaseNoKanjiDaDica();
		 this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.add(umKanji);
	}
	
	private void alterarTextoDicaComBaseNoKanjiDaDica()
	{
		 TextView textoDica = (TextView) findViewById(R.id.dica_kanji);
		 String hiraganaDoKanji = this.kanjiDaDica.getHiraganaDoKanji();
		 String traducaoDoKanji = this.kanjiDaDica.getTraducaoEmPortugues();
		 textoDica.setText(hiraganaDoKanji);
		 TextView textoTraducao = (TextView) findViewById(R.id.dica_kanji_traducao);
		 
		 boolean usuarioUsandoVersaoJaponesa = false; //se o usuario estiver usando a versao japonesa, a traducao n eh necessaria
		 Resources res = this.getResources();
         Locale myLocale = res.getConfiguration().locale;
         if(myLocale != null)
 		 {
 			String language = myLocale.getLanguage();
 			if(myLocale.getLanguage().compareTo("jp") == 0)
 			{
 				usuarioUsandoVersaoJaponesa = true;
 			}
 		 }
         
         final boolean usuarioUsandoVersaoJaponesaFinal = usuarioUsandoVersaoJaponesa;
		 
		 if(usuarioUsandoVersaoJaponesaFinal == false)
		 {
			 textoTraducao.setText(traducaoDoKanji);
		 }
		 else
		 {
			 textoTraducao.setText("");
		 }
	}
	
	private void gerarKanjiDaDicaOuIniciarNovoNivel()
	{
		 if(this.kanjisDasCartasNaTelaQueJaSeTornaramDicas.size() == this.kanjisDasCartasNaTela.size())
		 {
			//deve-se passar para o proximo nivel
		    this.realizarProcedimentoPassarParaProximoNivel();
		    this.gerarKanjiDaDica();
		 }
		 else
		 {
			 this.gerarKanjiDaDica();
		 }
	}
	 
	private void realizarProcedimentoPassarParaProximoNivel()
	{
		this.nivel = this.nivel + 1;
		if(nivel == 5)
		{
			this.quantasCartasHaveraoNaTela = this.quantasCartasHaveraoNaTela + 1;
		}
		else if(nivel == 9)
		{
			this.quantasCartasHaveraoNaTela = this.quantasCartasHaveraoNaTela + 1;
		}
		else if(nivel == 13)
		{
			this.quantasCartasHaveraoNaTela = this.quantasCartasHaveraoNaTela + 1;
		}
		else if(nivel == 17)
		{
			this.quantasCartasHaveraoNaTela = this.quantasCartasHaveraoNaTela + 1;
		}
		else if(nivel == 21)
		{
			this.quantasCartasHaveraoNaTela = this.quantasCartasHaveraoNaTela + 1;
		}
		else if(nivel == 25)
		{
			this.quantasCartasHaveraoNaTela = this.quantasCartasHaveraoNaTela + 1;
		}
		else if(nivel == 29)
		{
			this.quantasCartasHaveraoNaTela = this.quantasCartasHaveraoNaTela + 1;
		}
		else if(nivel == 33)
		{
			this.quantasCartasHaveraoNaTela = this.quantasCartasHaveraoNaTela + 1;
		}
		

		this.quantosNiveisPassaram = this.quantosNiveisPassaram + 1;
		 TextView textViewNivel = (TextView) findViewById(R.id.nivel);
		 String nivel = getResources().getString(R.string.nivel_sem_dois_pontos);
		 textViewNivel.setText(nivel + " " +String.valueOf(this.nivel));
		 
		 this.escolherKanjisParaONivel();
		 this.tornarCartasNaTelaClicaveisEVaziasNovamente();
	     
	}
	
	private void tornarCartasNaTelaClicaveisEVaziasNovamente()
	{
		 ImageView imageViewKaruta1 = (ImageView) findViewById(R.id.karuta1_imageview);
		 findViewById(R.id.karuta1).setClickable(true);
		 imageViewKaruta1.setVisibility(View.VISIBLE);
		 this.fazerImageViewVoltarACartaNormal(0);
		 
		 ImageView imageViewKaruta2 = (ImageView) findViewById(R.id.karuta2_imageview);
		 findViewById(R.id.karuta2).setClickable(true);
		 imageViewKaruta2.setVisibility(View.VISIBLE);
		 this.fazerImageViewVoltarACartaNormal(1);
		 
		 ImageView imageViewKaruta3 = (ImageView) findViewById(R.id.karuta3_imageview);
		 findViewById(R.id.karuta3).setClickable(true);
		 imageViewKaruta3.setVisibility(View.VISIBLE);
		 this.fazerImageViewVoltarACartaNormal(2);
		 
		 ImageView imageViewKaruta4 = (ImageView) findViewById(R.id.karuta4_imageview);
		 findViewById(R.id.karuta4).setClickable(true);
		 imageViewKaruta4.setVisibility(View.VISIBLE);
		 this.fazerImageViewVoltarACartaNormal(3);
		 
		 if(quantasCartasHaveraoNaTela < 5)
		 { 
			 TextView textoKaruta5 = (TextView) findViewById(R.id.texto_karuta5);
			 textoKaruta5.setText("");
			 TextView textoKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
			 textoKaruta6.setText("");
			 TextView textoKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
			 textoKaruta7.setText("");
			 TextView textoKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
			 textoKaruta8.setText("");
			 TextView textoKaruta9 = (TextView) findViewById(R.id.texto_karuta9);
			 textoKaruta9.setText("");
			 TextView textoKaruta10 = (TextView) findViewById(R.id.texto_karuta10);
			 textoKaruta10.setText("");
			 TextView textoKaruta11 = (TextView) findViewById(R.id.texto_karuta11);
			 textoKaruta11.setText("");
			 TextView textoKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
			 textoKaruta12.setText("");
			 
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 imageViewKaruta5.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 imageViewKaruta6.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 imageViewKaruta7.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 imageViewKaruta8.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 imageViewKaruta9.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 imageViewKaruta10.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 imageViewKaruta11.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 imageViewKaruta12.setVisibility(View.INVISIBLE);
		 }
		 else if(this.quantasCartasHaveraoNaTela == 5)
		 {
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 findViewById(R.id.karuta5).setClickable(true);
			 imageViewKaruta5.setVisibility(View.VISIBLE);
			 this.fazerImageViewVoltarACartaNormal(4);
			 
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 imageViewKaruta6.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 imageViewKaruta7.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 imageViewKaruta8.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 imageViewKaruta9.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 imageViewKaruta10.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 imageViewKaruta11.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 imageViewKaruta12.setVisibility(View.INVISIBLE);
			 
			 TextView textoKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
			 textoKaruta6.setText("");
			 TextView textoKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
			 textoKaruta7.setText("");
			 TextView textoKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
			 textoKaruta8.setText("");
			 TextView textoKaruta9 = (TextView) findViewById(R.id.texto_karuta9);
			 textoKaruta9.setText("");
			 TextView textoKaruta10 = (TextView) findViewById(R.id.texto_karuta10);
			 textoKaruta10.setText("");
			 TextView textoKaruta11 = (TextView) findViewById(R.id.texto_karuta11);
			 textoKaruta11.setText("");
			 TextView textoKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
			 textoKaruta12.setText("");
		 }
		 else if(this.quantasCartasHaveraoNaTela == 6)
		 {
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 findViewById(R.id.karuta5).setClickable(true);
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 findViewById(R.id.karuta6).setClickable(true);
			 imageViewKaruta5.setVisibility(View.VISIBLE);
			 imageViewKaruta6.setVisibility(View.VISIBLE);
			 this.fazerImageViewVoltarACartaNormal(4);
			 this.fazerImageViewVoltarACartaNormal(5);
			 
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 imageViewKaruta7.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 imageViewKaruta8.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 imageViewKaruta9.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 imageViewKaruta10.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 imageViewKaruta11.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 imageViewKaruta12.setVisibility(View.INVISIBLE);
			 
			 TextView textoKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
			 textoKaruta7.setText("");
			 TextView textoKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
			 textoKaruta8.setText("");
			 TextView textoKaruta9 = (TextView) findViewById(R.id.texto_karuta9);
			 textoKaruta9.setText("");
			 TextView textoKaruta10 = (TextView) findViewById(R.id.texto_karuta10);
			 textoKaruta10.setText("");
			 TextView textoKaruta11 = (TextView) findViewById(R.id.texto_karuta11);
			 textoKaruta11.setText("");
			 TextView textoKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
			 textoKaruta12.setText("");
		 }
		 else if(this.quantasCartasHaveraoNaTela == 7)
		 {
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 findViewById(R.id.karuta5).setClickable(true);
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 findViewById(R.id.karuta6).setClickable(true);
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 findViewById(R.id.karuta7).setClickable(true);
			 imageViewKaruta5.setVisibility(View.VISIBLE);
			 imageViewKaruta6.setVisibility(View.VISIBLE);
			 imageViewKaruta7.setVisibility(View.VISIBLE);
			 this.fazerImageViewVoltarACartaNormal(4);
			 this.fazerImageViewVoltarACartaNormal(5);
			 this.fazerImageViewVoltarACartaNormal(6);
			 
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 imageViewKaruta8.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 imageViewKaruta9.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 imageViewKaruta10.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 imageViewKaruta11.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 imageViewKaruta12.setVisibility(View.INVISIBLE);
			 
			 TextView textoKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
			 textoKaruta8.setText("");
			 TextView textoKaruta9 = (TextView) findViewById(R.id.texto_karuta9);
			 textoKaruta9.setText("");
			 TextView textoKaruta10 = (TextView) findViewById(R.id.texto_karuta10);
			 textoKaruta10.setText("");
			 TextView textoKaruta11 = (TextView) findViewById(R.id.texto_karuta11);
			 textoKaruta11.setText("");
			 TextView textoKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
			 textoKaruta12.setText("");
			 
			 
		 }
		 else if(this.quantasCartasHaveraoNaTela == 8)
		 {
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 findViewById(R.id.karuta5).setClickable(true);
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 findViewById(R.id.karuta6).setClickable(true);
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 findViewById(R.id.karuta7).setClickable(true);
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 findViewById(R.id.karuta8).setClickable(true);
			 imageViewKaruta5.setVisibility(View.VISIBLE);
			 imageViewKaruta6.setVisibility(View.VISIBLE);
			 imageViewKaruta7.setVisibility(View.VISIBLE);
			 imageViewKaruta8.setVisibility(View.VISIBLE);
			 this.fazerImageViewVoltarACartaNormal(4);
			 this.fazerImageViewVoltarACartaNormal(5);
			 this.fazerImageViewVoltarACartaNormal(6);
			 this.fazerImageViewVoltarACartaNormal(7);
			 
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 imageViewKaruta9.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 imageViewKaruta10.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 imageViewKaruta11.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 imageViewKaruta12.setVisibility(View.INVISIBLE);
			 TextView textoKaruta9 = (TextView) findViewById(R.id.texto_karuta9);
			 textoKaruta9.setText("");
			 TextView textoKaruta10 = (TextView) findViewById(R.id.texto_karuta10);
			 textoKaruta10.setText("");
			 TextView textoKaruta11 = (TextView) findViewById(R.id.texto_karuta11);
			 textoKaruta11.setText("");
			 TextView textoKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
			 textoKaruta12.setText("");
		 }
		 else if(this.quantasCartasHaveraoNaTela == 9)
		 {
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 findViewById(R.id.karuta5).setClickable(true);
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 findViewById(R.id.karuta6).setClickable(true);
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 findViewById(R.id.karuta7).setClickable(true);
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 findViewById(R.id.karuta8).setClickable(true);
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 findViewById(R.id.karuta9).setClickable(true);
			 imageViewKaruta5.setVisibility(View.VISIBLE);
			 imageViewKaruta6.setVisibility(View.VISIBLE);
			 imageViewKaruta7.setVisibility(View.VISIBLE);
			 imageViewKaruta8.setVisibility(View.VISIBLE);
			 imageViewKaruta9.setVisibility(View.VISIBLE);
			 this.fazerImageViewVoltarACartaNormal(4);
			 this.fazerImageViewVoltarACartaNormal(5);
			 this.fazerImageViewVoltarACartaNormal(6);
			 this.fazerImageViewVoltarACartaNormal(7);
			 this.fazerImageViewVoltarACartaNormal(8);
			 
			 
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 imageViewKaruta10.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 imageViewKaruta11.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 imageViewKaruta12.setVisibility(View.INVISIBLE);
			 TextView textoKaruta10 = (TextView) findViewById(R.id.texto_karuta10);
			 textoKaruta10.setText("");
			 TextView textoKaruta11 = (TextView) findViewById(R.id.texto_karuta11);
			 textoKaruta11.setText("");
			 TextView textoKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
			 textoKaruta12.setText("");
		 }
		 else if(this.quantasCartasHaveraoNaTela == 10)
		 {
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 findViewById(R.id.karuta5).setClickable(true);
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 findViewById(R.id.karuta6).setClickable(true);
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 findViewById(R.id.karuta7).setClickable(true);
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 findViewById(R.id.karuta8).setClickable(true);
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 findViewById(R.id.karuta9).setClickable(true);
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 findViewById(R.id.karuta10).setClickable(true);
			 imageViewKaruta5.setVisibility(View.VISIBLE);
			 imageViewKaruta6.setVisibility(View.VISIBLE);
			 imageViewKaruta7.setVisibility(View.VISIBLE);
			 imageViewKaruta8.setVisibility(View.VISIBLE);
			 imageViewKaruta9.setVisibility(View.VISIBLE);
			 imageViewKaruta10.setVisibility(View.VISIBLE);
			 this.fazerImageViewVoltarACartaNormal(4);
			 this.fazerImageViewVoltarACartaNormal(5);
			 this.fazerImageViewVoltarACartaNormal(6);
			 this.fazerImageViewVoltarACartaNormal(7);
			 this.fazerImageViewVoltarACartaNormal(8);
			 this.fazerImageViewVoltarACartaNormal(9);
			 
			 
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 imageViewKaruta11.setVisibility(View.INVISIBLE);
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 imageViewKaruta12.setVisibility(View.INVISIBLE);
			 TextView textoKaruta11 = (TextView) findViewById(R.id.texto_karuta11);
			 textoKaruta11.setText("");
			 TextView textoKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
			 textoKaruta12.setText("");
		 }
		 else if(this.quantasCartasHaveraoNaTela == 11)
		 {
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 findViewById(R.id.karuta5).setClickable(true);
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 findViewById(R.id.karuta6).setClickable(true);
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 findViewById(R.id.karuta7).setClickable(true);
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 findViewById(R.id.karuta8).setClickable(true);
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 findViewById(R.id.karuta9).setClickable(true);
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 findViewById(R.id.karuta10).setClickable(true);
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 findViewById(R.id.karuta11).setClickable(true);
			 imageViewKaruta5.setVisibility(View.VISIBLE);
			 imageViewKaruta6.setVisibility(View.VISIBLE);
			 imageViewKaruta7.setVisibility(View.VISIBLE);
			 imageViewKaruta8.setVisibility(View.VISIBLE);
			 imageViewKaruta9.setVisibility(View.VISIBLE);
			 imageViewKaruta10.setVisibility(View.VISIBLE);
			 imageViewKaruta11.setVisibility(View.VISIBLE);
			 this.fazerImageViewVoltarACartaNormal(4);
			 this.fazerImageViewVoltarACartaNormal(5);
			 this.fazerImageViewVoltarACartaNormal(6);
			 this.fazerImageViewVoltarACartaNormal(7);
			 this.fazerImageViewVoltarACartaNormal(8);
			 this.fazerImageViewVoltarACartaNormal(9);
			 this.fazerImageViewVoltarACartaNormal(10);
			 
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 imageViewKaruta12.setVisibility(View.INVISIBLE);
			 TextView textoKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
			 textoKaruta12.setText("");
		 }
		 else
		 {
			 ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
			 findViewById(R.id.karuta5).setClickable(true);
			 ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
			 findViewById(R.id.karuta6).setClickable(true);
			 ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
			 findViewById(R.id.karuta7).setClickable(true);
			 ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
			 findViewById(R.id.karuta8).setClickable(true);
			 ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
			 findViewById(R.id.karuta9).setClickable(true);
			 ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
			 findViewById(R.id.karuta10).setClickable(true);
			 ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
			 findViewById(R.id.karuta11).setClickable(true);
			 ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
			 findViewById(R.id.karuta12).setClickable(true);
			 imageViewKaruta5.setVisibility(View.VISIBLE);
			 imageViewKaruta6.setVisibility(View.VISIBLE);
			 imageViewKaruta7.setVisibility(View.VISIBLE);
			 imageViewKaruta8.setVisibility(View.VISIBLE);
			 imageViewKaruta9.setVisibility(View.VISIBLE);
			 imageViewKaruta10.setVisibility(View.VISIBLE);
			 imageViewKaruta11.setVisibility(View.VISIBLE);
			 imageViewKaruta12.setVisibility(View.VISIBLE);
			 this.fazerImageViewVoltarACartaNormal(4);
			 this.fazerImageViewVoltarACartaNormal(5);
			 this.fazerImageViewVoltarACartaNormal(6);
			 this.fazerImageViewVoltarACartaNormal(7);
			 this.fazerImageViewVoltarACartaNormal(8);
			 this.fazerImageViewVoltarACartaNormal(9);
			 this.fazerImageViewVoltarACartaNormal(10);
			 this.fazerImageViewVoltarACartaNormal(11);
		 }
	 }
	
	private LinkedList<KanjiTreinar> escolherKanjisNaoNovos(int quantosKanjis)
	{
		LinkedList<KanjiTreinar> kanjisVelhos = new LinkedList<KanjiTreinar>();
		
		for(int i= 0; i < quantosKanjis; i++)
		{	
			Random geraNumAleatorio = new Random();
			boolean kanjiVelhoEhRepetido = true;
			
			KanjiTreinar kanjiNaoNovoEscolhido = null;
			
			while(kanjiVelhoEhRepetido == true)
			{
				int posicaoNovoKanjiNosKanjisJaViraramCartas = geraNumAleatorio.nextInt(this.kanjisQueJaViraramCartas.size());
				KanjiTreinar umKanji = this.kanjisQueJaViraramCartas.get(posicaoNovoKanjiNosKanjisJaViraramCartas);
				
				boolean umKanjiJaExisteNosKanjisVelhos = false;
				for(int j = 0; j < kanjisVelhos.size(); j++)
				{
					KanjiTreinar umKanjiVelho = kanjisVelhos.get(j);
					if(umKanjiVelho.getKanji().compareTo(umKanji.getKanji()) == 0 &&
							umKanjiVelho.getCategoriaAssociada().compareTo(umKanji.getCategoriaAssociada()) == 0)
					{
						//umKanji ja existe nos velhos
						umKanjiJaExisteNosKanjisVelhos = true;
					}
					
				}
				
				//esse kanji velho nao pode pertencer aos ultimos kanjis
				for(int k = 0; k < this.ultimosKanjis.size(); k++)
				{
					KanjiTreinar umDosUltimos = this.ultimosKanjis.get(k);
					if((umDosUltimos.getKanji().compareTo(umKanji.getKanji()) == 0)
							&& (umDosUltimos.getCategoriaAssociada().compareTo(umKanji.getCategoriaAssociada()) == 0))
					{
						//o kanjivelho eh um dos ultimos! nao pode! Devemos escolher outro kanji velho, por isso vamos obrigar que outro kanji seja escolhido
						umKanjiJaExisteNosKanjisVelhos = true;
					}
				}
				
				
				if(umKanjiJaExisteNosKanjisVelhos == true)
				{
					kanjiVelhoEhRepetido = true;
				}
				else
				{
					kanjiVelhoEhRepetido = false;
					kanjiNaoNovoEscolhido = umKanji;
				}
			}
			
			kanjisVelhos.add(kanjiNaoNovoEscolhido);
			
		}
		
		return kanjisVelhos;
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.karuta1_imageview:
	    		TextView textViewKaruta1 = (TextView) findViewById(R.id.texto_karuta1);
	        	String textoKaruta1 = this.kanjisDasCartasNaTela.get(0).getKanji();
	        	String textoKanjiDaDica = this.kanjiDaDica.getKanji();
	        	
	        	if(textoKaruta1.compareTo(textoKanjiDaDica) == 0)
	        	{
	        		//usuario acertou o kanji.
	        		super.reproduzirSfx("acertou_carta");
	        		this.fazerMascoteFicarFelizPorUmTempo();
	        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
	        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!!!
	        		ImageView imageViewKaruta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta1); //mudei a figura da carta
	        		findViewById(R.id.karuta1).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	        		textViewKaruta1.setText("");
	        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
	        		
	        	}
	        	else
	        	{
	        		//errou
	        		if(textViewKaruta1.getText().length() > 0)
	        		{
	        			this.realizarProcedimentoUsuarioErrouCarta();
	        		}
	        	}
	    	
	    	break;
	    case R.id.karuta2_imageview:
	    		TextView textViewKaruta2 = (TextView) findViewById(R.id.texto_karuta2);
	        	String textoKaruta2 = this.kanjisDasCartasNaTela.get(1).getKanji();
	        	String textoKanjiDaDica2 = this.kanjiDaDica.getKanji();
	        	
	        	if(textoKaruta2.compareTo(textoKanjiDaDica2) == 0)
	        	{
	        		//usuario acertou o kanji
	        		super.reproduzirSfx("acertou_carta");
	        		this.fazerMascoteFicarFelizPorUmTempo();
	        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
	        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
	        		ImageView imageViewKaruta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta2); //mudei a figura da carta
	        		findViewById(R.id.karuta2).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	        		textViewKaruta2.setText("");
	        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
	        	}
	        	else
	        	{
	        		//errou
	        		if(textViewKaruta2.getText().length() > 0)
	        		{
	        			this.realizarProcedimentoUsuarioErrouCarta();
	        		}
	        	}
	    	break;
	    case R.id.karuta3_imageview:
	    		TextView textViewKaruta3 = (TextView) findViewById(R.id.texto_karuta3);
	        	String textoKaruta3 = this.kanjisDasCartasNaTela.get(2).getKanji();
	        	String textoKanjiDaDica3 = this.kanjiDaDica.getKanji();
	        	
	        	if(textoKaruta3.compareTo(textoKanjiDaDica3) == 0)
	        	{
	        		//usuario acertou o kanji.
	        		super.reproduzirSfx("acertou_carta");
	        		this.fazerMascoteFicarFelizPorUmTempo();
	        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
	        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
	        		ImageView imageViewKaruta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta3); //mudei a figura da carta
	        		findViewById(R.id.karuta3).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	        		textViewKaruta3.setText("");
	        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
	        	}
	        	else
	        	{
	        		//errou
	        		if(textViewKaruta3.getText().length() > 0)
	        		{
	        			this.realizarProcedimentoUsuarioErrouCarta();
	        		}
	        	}
	    	break;
	    case R.id.karuta4_imageview:
	    		TextView textViewKaruta4 = (TextView) findViewById(R.id.texto_karuta4);
	        	String textoKaruta4 = this.kanjisDasCartasNaTela.get(3).getKanji();
	        	String textoKanjiDaDica4 = this.kanjiDaDica.getKanji();
	        	
	        	if(textoKaruta4.compareTo(textoKanjiDaDica4) == 0)
	        	{
	        		//usuario acertou o kanji. 
	        		super.reproduzirSfx("acertou_carta");
	        		this.fazerMascoteFicarFelizPorUmTempo();
	        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
	        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
	        		ImageView imageViewKaruta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta4); //mudei a figura da carta
	        		findViewById(R.id.karuta4).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	        		textViewKaruta4.setText("");
	        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
	        	}
	        	else
	        	{
	        		//errou
	        		if(textViewKaruta4.getText().length() > 0)
	        		{
	        			this.realizarProcedimentoUsuarioErrouCarta();
	        		}
	        	}
	    	break;
	    case R.id.karuta5_imageview:
	    		TextView textViewKaruta5 = (TextView) findViewById(R.id.texto_karuta5);
	        	String textoKaruta5 = this.kanjisDasCartasNaTela.get(4).getKanji();
	        	String textoKanjiDaDica5 = this.kanjiDaDica.getKanji();
	        	
	        	if(textoKaruta5.compareTo(textoKanjiDaDica5) == 0)
	        	{
	        		//usuario acertou o kanji.
	        		super.reproduzirSfx("acertou_carta");
	        		this.fazerMascoteFicarFelizPorUmTempo();
	        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
	        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
	        		ImageView imageViewKaruta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta5); //mudei a figura da carta
	        		findViewById(R.id.karuta5).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	        		textViewKaruta5.setText("");
	        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
	        	}
	        	else
	        	{
	        		//errou
	        		if(textViewKaruta5.getText().length() > 0)
	        		{
	        			this.realizarProcedimentoUsuarioErrouCarta();
	        		}
	        	}
	    	break;
	    case R.id.karuta6_imageview:
	    		TextView textViewKaruta6 = (TextView) findViewById(R.id.texto_karuta6);
	        	String textoKaruta6 = this.kanjisDasCartasNaTela.get(5).getKanji();
	        	String textoKanjiDaDica6 = this.kanjiDaDica.getKanji();
	        	
	        	if(textoKaruta6.compareTo(textoKanjiDaDica6) == 0)
	        	{
	        		//usuario acertou o kanji.
	        		super.reproduzirSfx("acertou_carta");
	        		this.fazerMascoteFicarFelizPorUmTempo();
	        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
	        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
	        		ImageView imageViewKaruta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta6); //mudei a figura da carta
	        		findViewById(R.id.karuta6).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	        		textViewKaruta6.setText("");
	        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
	        	}
	        	else
	        	{
	        		//errou
	        		if(textViewKaruta6.getText().length() > 0)
	        		{
	        			this.realizarProcedimentoUsuarioErrouCarta();
	        		}
	        	}
	    	break;
	    case R.id.karuta7_imageview:
	    		TextView textViewKaruta7 = (TextView) findViewById(R.id.texto_karuta7);
	        	String textoKaruta7 = this.kanjisDasCartasNaTela.get(6).getKanji();
	        	String textoKanjiDaDica7 = this.kanjiDaDica.getKanji();
	        	
	        	if(textoKaruta7.compareTo(textoKanjiDaDica7) == 0)
	        	{
	        		//usuario acertou o kanji.
	        		super.reproduzirSfx("acertou_carta");
	        		this.fazerMascoteFicarFelizPorUmTempo();
	        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
	        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
	        		ImageView imageViewKaruta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta7); //mudei a figura da carta
	        		findViewById(R.id.karuta7).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	        		textViewKaruta7.setText("");
	        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
	        	}
	        	else
	        	{
	        		//errou
	        		if(textViewKaruta7.getText().length() > 0)
	        		{
	        			this.realizarProcedimentoUsuarioErrouCarta();
	        		}
	        	}
	    	break;
	    case R.id.karuta8_imageview:
	    		TextView textViewKaruta8 = (TextView) findViewById(R.id.texto_karuta8);
	        	String textoKaruta8 = this.kanjisDasCartasNaTela.get(7).getKanji();
	        	String textoKanjiDaDica8 = this.kanjiDaDica.getKanji();
	        	
	        	if(textoKaruta8.compareTo(textoKanjiDaDica8) == 0)
	        	{
	        		//usuario acertou o kanji.
	        		super.reproduzirSfx("acertou_carta");
	        		this.fazerMascoteFicarFelizPorUmTempo();
	        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
	        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
	        		ImageView imageViewKaruta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta8); //mudei a figura da carta
	        		findViewById(R.id.karuta8).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
	        		textViewKaruta8.setText("");
	        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
	        	}
	        	else
	        	{
	        		//errou
	        		if(textViewKaruta8.getText().length() > 0)
	        		{
	        			this.realizarProcedimentoUsuarioErrouCarta();
	        		}
	        	}
	    	break;
	    case R.id.karuta9_imageview:
    		TextView textViewKaruta9 = (TextView) findViewById(R.id.texto_karuta9);
        	String textoKaruta9 = this.kanjisDasCartasNaTela.get(8).getKanji();
        	String textoKanjiDaDica9 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta9.compareTo(textoKanjiDaDica9) == 0)
        	{
        		//usuario acertou o kanji.
        		super.reproduzirSfx("acertou_carta");
        		this.fazerMascoteFicarFelizPorUmTempo();
        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
        		ImageView imageViewKaruta9 = (ImageView) findViewById(R.id.karuta9_imageview);
        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta9); //mudei a figura da carta
        		findViewById(R.id.karuta9).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
        		textViewKaruta9.setText("");
        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
        	}
        	else
        	{
        		//errou
        		if(textViewKaruta9.getText().length() > 0)
        		{
        			this.realizarProcedimentoUsuarioErrouCarta();
        		}
        	}
    	break;
	    case R.id.karuta10_imageview:
    		TextView textViewKaruta10 = (TextView) findViewById(R.id.texto_karuta10);
        	String textoKaruta10 = this.kanjisDasCartasNaTela.get(9).getKanji();
        	String textoKanjiDaDica10 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta10.compareTo(textoKanjiDaDica10) == 0)
        	{
        		//usuario acertou o kanji.
        		super.reproduzirSfx("acertou_carta");
        		this.fazerMascoteFicarFelizPorUmTempo();
        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
        		ImageView imageViewKaruta10 = (ImageView) findViewById(R.id.karuta10_imageview);
        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta10); //mudei a figura da carta
        		findViewById(R.id.karuta10).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
        		textViewKaruta10.setText("");
        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
        	}
        	else
        	{
        		//errou
        		if(textViewKaruta10.getText().length() > 0)
        		{
        			this.realizarProcedimentoUsuarioErrouCarta();
        		}
        	}
    	break;
	    case R.id.karuta11_imageview:
    		TextView textViewKaruta11 = (TextView) findViewById(R.id.texto_karuta11);
        	String textoKaruta11 = this.kanjisDasCartasNaTela.get(10).getKanji();
        	String textoKanjiDaDica11 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta11.compareTo(textoKanjiDaDica11) == 0)
        	{
        		//usuario acertou o kanji.
        		super.reproduzirSfx("acertou_carta");
        		this.fazerMascoteFicarFelizPorUmTempo();
        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
        		ImageView imageViewKaruta11 = (ImageView) findViewById(R.id.karuta11_imageview);
        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta11); //mudei a figura da carta
        		findViewById(R.id.karuta11).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
        		textViewKaruta11.setText("");
        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
        	}
        	else
        	{
        		//errou
        		if(textViewKaruta11.getText().length() > 0)
        		{
        			this.realizarProcedimentoUsuarioErrouCarta();
        		}
        	}
    	break;
	    case R.id.karuta12_imageview:
    		TextView textViewKaruta12 = (TextView) findViewById(R.id.texto_karuta12);
        	String textoKaruta12 = this.kanjisDasCartasNaTela.get(11).getKanji();
        	String textoKanjiDaDica12 = this.kanjiDaDica.getKanji();
        	
        	if(textoKaruta12.compareTo(textoKanjiDaDica12) == 0)
        	{
        		//usuario acertou o kanji.
        		super.reproduzirSfx("acertou_carta");
        		this.fazerMascoteFicarFelizPorUmTempo();
        		aumentarPontuacaoComBaseNaDificuldadeDoKanji();
        		this.kanjisQueUsuarioAcertou.add(this.kanjiDaDica); //usuario acertou mais um kanji!
        		ImageView imageViewKaruta12 = (ImageView) findViewById(R.id.karuta12_imageview);
        		this.fazerImageViewMudarParaVersoDeCarta(imageViewKaruta12); //mudei a figura da carta
        		findViewById(R.id.karuta12).setClickable(false); //a carta nao esta mais clicavel ate o final da rodada
        		textViewKaruta12.setText("");
        		this.gerarKanjiDaDicaOuIniciarNovoNivel();
        	}
        	else
        	{
        		//errou
        		if(textViewKaruta12.getText().length() > 0)
        		{
        			this.realizarProcedimentoUsuarioErrouCarta();
        		}
        	}
    	break;
	    	
		}
	}
	
	public void realizarProcedimentoUsuarioErrouCarta()
	{
		super.reproduzirSfx("errou_carta");
		
		kanjisQueUsuarioErrou.add(kanjiDaDica);
		this.vidas = this.vidas - 1;
		
		if(vidas == 3)
		{
			ImageView coracao1 = (ImageView) findViewById(R.id.coracao1);
			coracao1.setImageResource(R.drawable.heartquebrado);
		}
		else if(vidas == 2)
		{
			ImageView coracao2 = (ImageView) findViewById(R.id.coracao2);
			coracao2.setImageResource(R.drawable.heartquebrado);
		}
		else if(vidas == 1)
		{
			ImageView coracao3 = (ImageView) findViewById(R.id.coracao3);
			coracao3.setImageResource(R.drawable.heartquebrado);
		}
		else if(vidas == 0)
		{
			ImageView coracao4 = (ImageView) findViewById(R.id.coracao4);
			coracao4.setImageResource(R.drawable.heartquebrado);
			this.terminarModoTreinamento(null);
		}
		
		final ImageView imageViewMascote = (ImageView) findViewById(R.id.mascote);
		imageViewMascote.setImageResource(R.drawable.mascotetreinamento_zangada);
		
		//a mascote vai ficar com cara de zangada por um tempo e depois volta ao normal
		new Timer().schedule(new TimerTask() 
		 { 
			    @Override
			    public void run() 
			    {
			        //If you want to operate UI modifications, you must run ui stuff on UiThread.
			        ModoTreinamento.this.runOnUiThread(new Runnable() 
			        {
			            @Override
			            public void run() 
			            {
			            	imageViewMascote.setImageResource(R.drawable.mascotetreinamento);
			            }
			        });
			    }
			}, 1000);
		
	}
	
	
	void switchToScreen(int screenId) 
	{
		// make the requested screen visible; hide all others.
		for (int id : SCREENS) {
		    findViewById(id).setVisibility(screenId == id ? View.VISIBLE : View.GONE);
		}
	}
	
	private void aumentarPontuacaoComBaseNaDificuldadeDoKanji()
	{
		int dificuldade = this.kanjiDaDica.getDificuldadeDoKanji();
		
		if(dificuldade == 1)
		{
			this.suaPontuacao = this.suaPontuacao + 1;
		}
		else if(dificuldade == 2)
		{
			this.suaPontuacao = this.suaPontuacao + 2;
		}
		else
		{
			this.suaPontuacao = this.suaPontuacao + 3;
		}
		
		TextView textoPontuacao = (TextView) findViewById(R.id.pontuacao);
    	String pontuacao = getResources().getString(R.string.pontos);
    	
		if(suaPontuacao < 100)
    	{
    		textoPontuacao.setText(pontuacao + "0" + String.valueOf(suaPontuacao));
    	}
    	else
    	{
    		textoPontuacao.setText(pontuacao + String.valueOf(suaPontuacao));
    	}
		
		//this.realizarAnimacaoAumentaPontuacao(dificuldade);
	}
	
	private void realizarAnimacaoAumentaPontuacao(int dificuldadeDoKanji)
	{
		/*final AnimationDrawable animacaoAumentaPontuacao = new AnimationDrawable(); 
		int idImagemAnimacaoAumentaPontos1 = 0;
		int idImagemAnimacaoAumentaPontos2 = 0;
		int idImagemAnimacaoAumentaPontos3 = 0;
		int idImagemAnimacaoAumentaPontos4 = 0;
		int idImagemAnimacaoAumentaPontos5 = 0;
		int idImagemAnimacaoAumentaPontos6 = 0;
		int idImagemAnimacaoAumentaPontos7 = 0;
		int idImagemAnimacaoAumentaPontos8 = 0;
		int idImagemAnimacaoAumentaPontos9 = 0;
		int idImagemAnimacaoAumentaPontos10 = 0;
		int idImagemAnimacaoAumentaPontos11 = 0;
		int idImagemAnimacaoAumentaPontos12 = 0;
		int idImagemAnimacaoAumentaPontos13 = 0;
		int idImagemAnimacaoAumentaPontos14 = 0;
		int idImagemAnimacaoAumentaPontos15 = 0;
		int idImagemAnimacaoAumentaPontos16 = 0;
		int idImagemAnimacaoAumentaPontos17 = 0;
		
		
		if(dificuldadeDoKanji == 1)
		{
			idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("animacao10_1", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("animacao10_2", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("animacao10_3", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("animacao10_4", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("animacao10_5", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("animacao10_6", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("animacao10_7", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("animacao10_8", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("animacao10_9", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("animacao10_10", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("animacao10_11", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("animacao10_12", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("animacao10_13", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("animacao10_14", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("animacao10_15", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("animacao10_16", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("animacao10_17", "drawable", getPackageName());
		}
		else if(dificuldadeDoKanji == 2)
		{
			idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("animacao20_1", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("animacao20_2", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("animacao20_3", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("animacao20_4", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("animacao20_5", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("animacao20_6", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("animacao20_7", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("animacao20_8", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("animacao20_9", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("animacao20_10", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("animacao20_11", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("animacao20_12", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("animacao20_13", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("animacao20_14", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("animacao20_15", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("animacao20_16", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("animacao20_17", "drawable", getPackageName());
		}
		else if(dificuldadeDoKanji == 3)
		{
			idImagemAnimacaoAumentaPontos1 = getResources().getIdentifier("animacao30_1", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos2 = getResources().getIdentifier("animacao30_2", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos3 = getResources().getIdentifier("animacao30_3", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos4 = getResources().getIdentifier("animacao30_4", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos5 = getResources().getIdentifier("animacao30_5", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos6 = getResources().getIdentifier("animacao30_6", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos7 = getResources().getIdentifier("animacao30_7", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos8 = getResources().getIdentifier("animacao30_8", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos9 = getResources().getIdentifier("animacao30_9", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos10 = getResources().getIdentifier("animacao30_10", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos11 = getResources().getIdentifier("animacao30_11", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos12 = getResources().getIdentifier("animacao30_12", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos13 = getResources().getIdentifier("animacao30_13", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos14 = getResources().getIdentifier("animacao30_14", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos15 = getResources().getIdentifier("animacao30_15", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos16 = getResources().getIdentifier("animacao30_16", "drawable", getPackageName());
			idImagemAnimacaoAumentaPontos17 = getResources().getIdentifier("animacao30_17", "drawable", getPackageName());
		}
		
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos1), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos2), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos3), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos4), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos5), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos6), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos7), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos8), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos9), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos10), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos11), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos12), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos13), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos14), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos15), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos16), 50);
		 animacaoAumentaPontuacao.addFrame(getResources().getDrawable(idImagemAnimacaoAumentaPontos17), 50);
		 animacaoAumentaPontuacao.setOneShot(true);
		 
		 ImageView imageViewMaisPontos = (ImageView) findViewById(R.id.maispontos);
		 imageViewMaisPontos.setImageDrawable(animacaoAumentaPontuacao);
		 
		 imageViewMaisPontos.post(new Runnable() {
			@Override
			public void run() {
				animacaoAumentaPontuacao.start();
			}
		 	});
		 
		 new Timer().schedule(new TimerTask() 
		 { 
			    @Override
			    public void run() 
			    {
			        //If you want to operate UI modifications, you must run ui stuff on UiThread.
			        ModoTreinamento.this.runOnUiThread(new Runnable() 
			        {
			            @Override
			            public void run() 
			            {
			            	TextView textoPontuacao = (TextView) findViewById(R.id.pontuacao);
			            	String pontuacao = getResources().getString(R.string.pontuacao);
			            	
			            	if(suaPontuacao < 100)
			            	{
			            		textoPontuacao.setText(pontuacao + "0" + String.valueOf(suaPontuacao));
			            	}
			            	else
			            	{
			            		textoPontuacao.setText(pontuacao + String.valueOf(suaPontuacao));
			            	}
			            }
			        });
			    }
			}, 900);*/
	}

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	public void terminarModoTreinamento(View v)
	{
		this.tempoEstahParado = true;
		this.runnableThreadAumentaTempoNoJogoDeveriaMorrer = true;
		this.usuarioNoMeioDeUmTreinamento = false;
		this.mudarMusicaDeFundo(R.raw.tradicional_japanese_ethnic_music_pack);
		
		switchToScreen(R.id.tela_fim_do_modo_treinamento);
		this.mudarFonteDeTodoOTextoDaTelaFinal();
		
		TextView quantosPontosVoceFez = (TextView) findViewById(R.id.quantosPontosVoceFez);
		String textoVoceFez = getResources().getString(R.string.voceFez);
		quantosPontosVoceFez.setText(textoVoceFez + " " + this.suaPontuacao + " ");
		
		TextView textViewNivel = (TextView) findViewById(R.id.chegouAteONivel);
		String textoNivel = getResources().getString(R.string.chegouAteQueNivel);
		textViewNivel.setText(textoNivel + " " + this.nivel);
		
		
		//faltou calcular quanto dinheiro o usuário recebeu
		int creditosAdicionarAoJogador = TransformaPontosEmCredito.converterPontosEmCredito(this.suaPontuacao);
		DAOAcessaDinheiroDoJogador daoDinheiroJogador = ConcreteDAOAcessaDinheiroDoJogador.getInstance();
		daoDinheiroJogador.adicionarCredito(creditosAdicionarAoJogador, this);
		TextView textViewCreditos = (TextView) findViewById(R.id.japanPointsAdquiridos);
		textViewCreditos.setText("x" + String.valueOf(creditosAdicionarAoJogador));
		
		this.mudarFalaEMascoteDeAcordoComDesempenho();
		
		boolean temConexaoComNet = this.temConexaoComInternet();
		boolean categoriasESeusNomesEhVazio = SingletonArmazenaCategoriasDoJogo.getInstance().getCategoriasESeusIds().isEmpty();
		if(temConexaoComNet == true && categoriasESeusNomesEhVazio == false)
		{
			//pode mandar os dados para o bd se tem conexão e conseguiu pegar as actegorias do jogo normal
			this.mandarDadosDaPartidaParaOBancoDeDados();
		}
		
	}
	
	private void mudarFalaEMascoteDeAcordoComDesempenho()
	{
		TextView textViewFrase = (TextView) findViewById(R.id.fraseDeAcordoComDesempenho);
		RelativeLayout layoutFimTreinamento = (RelativeLayout) findViewById(R.id.tela_fim_do_modo_treinamento);
		ImageView mascote = (ImageView) layoutFimTreinamento.findViewById(R.id.mascote);
		
		String fraseFinal;
		int idImagemMascote;
		if(this.nivel == 1)
		{
			fraseFinal = getResources().getString(R.string.frase_final_treinamento_1);
			idImagemMascote = R.drawable.mascotetreinamento_zangada;
		}
		else if(this.nivel > 1 && this.nivel < 8)
		{
			fraseFinal = getResources().getString(R.string.frase_final_treinamento_2);
			idImagemMascote = R.drawable.mascotetreinamento;
		}
		else if(this.nivel >= 8 && this.nivel < 18)
		{
			fraseFinal = getResources().getString(R.string.frase_final_treinamento_3);
			idImagemMascote = R.drawable.mascotetreinamento_feliz;
		}
		else if(this.nivel >= 8 && this.nivel < 18)
		{
			fraseFinal = getResources().getString(R.string.frase_final_treinamento_3);
			idImagemMascote = R.drawable.mascotetreinamento_feliz;
		}
		else if(this.nivel >= 18 && this.nivel < 33)
		{
			fraseFinal = getResources().getString(R.string.frase_final_treinamento_4);
			idImagemMascote = R.drawable.mascotetreinamento_feliz;
		}
		else
		{
			fraseFinal = getResources().getString(R.string.frase_final_treinamento_5);
			idImagemMascote = R.drawable.mascotetreinamento_zangada;
		}
		
		textViewFrase.setText(fraseFinal);
		mascote.setImageResource(idImagemMascote);
	}
	
	public void voltarAoMenuPrincipal(View v)
	{
		SingletonDeveMostrarTelaLoginNaMainActivity.getInstance().setDeveMostrarTelaLogin(false); //n queremos mostrar a tela de lgin de novo, nao eh?
		Intent irMenuInicial =
				new Intent(ModoTreinamento.this, MainActivity.class);
		irMenuInicial.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);	
		startActivity(irMenuInicial);
	}
	
	public void treinarNovamente(View v)
	{
		mudarMusicaDeFundo(R.raw.japan_in_spring);
		Intent comecarModoTreinamento =
				new Intent(ModoTreinamento.this, ModoTreinamento.class);
		comecarModoTreinamento.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);	
		startActivity(comecarModoTreinamento);
	}
	
	 /*coloca o texto verticalmente numa carta*/
	 private void colocarTextoVerticalNaCarta(TextView textViewUmaCarta, String texto)
	 {
		 //cada caractere do texto deve vir seguido de um \n
		 String textoComBarrasN = "";
		 
		 for(int i = 0; i < texto.length(); i++)
		 {
			 String umaLetra = String.valueOf(texto.charAt(i));
			 textoComBarrasN = textoComBarrasN + umaLetra + "\n";
		 }
		 
		 textViewUmaCarta.setText(textoComBarrasN);
	 }
	 
	 /*é o que eu uso para escurecer as cartas*/
	 private void fazerImageViewMudarParaVersoDeCarta(ImageView imageView)
	 {
		 //imageView.setColorFilter(Color.rgb(123, 123, 123), android.graphics.PorterDuff.Mode.MULTIPLY);
		 imageView.setImageResource(R.drawable.karutaversotreinamento);
	 }
	 
	 private void fazerImageViewVoltarACartaNormal(int indiceCarta)
	 {
		 //imageView.setColorFilter(null);
		 //imageView.setAlpha(255);
		 this.colorirBordaDaCartaDeAcordoComCategoria(indiceCarta);
	 }
	 
	 private void fazerMascoteFicarFelizPorUmTempo()
	 {
		 final ImageView imageViewMascote = (ImageView) findViewById(R.id.mascote);
			imageViewMascote.setImageResource(R.drawable.mascotetreinamento_feliz);
			
			//a mascote vai ficar com cara de feliz por um tempo e depois volta ao normal
			new Timer().schedule(new TimerTask() 
			 { 
				    @Override
				    public void run() 
				    {
				        //If you want to operate UI modifications, you must run ui stuff on UiThread.
				        ModoTreinamento.this.runOnUiThread(new Runnable() 
				        {
				            @Override
				            public void run() 
				            {
				            	imageViewMascote.setImageResource(R.drawable.mascotetreinamento);
				            }
				        });
				    }
				}, 1000);
	 }
	 
	 private void mudarFonteDosKanjis()
	 {
	 	String fontPath = "fonts/KaoriGel.ttf";
	 	 
	     // text view label
	     TextView txtkaruta1 = (TextView) findViewById(R.id.texto_karuta1);
	     TextView txtkaruta2 = (TextView) findViewById(R.id.texto_karuta2);
	     TextView txtkaruta3 = (TextView) findViewById(R.id.texto_karuta3);
	     TextView txtkaruta4 = (TextView) findViewById(R.id.texto_karuta4);
	     TextView txtkaruta5 = (TextView) findViewById(R.id.texto_karuta5);
	     TextView txtkaruta6 = (TextView) findViewById(R.id.texto_karuta6);
	     TextView txtkaruta7 = (TextView) findViewById(R.id.texto_karuta7);
	     TextView txtkaruta8 = (TextView) findViewById(R.id.texto_karuta8);
	     TextView txtkaruta9 = (TextView) findViewById(R.id.texto_karuta9);
	     TextView txtkaruta10 = (TextView) findViewById(R.id.texto_karuta10);
	     TextView txtkaruta11 = (TextView) findViewById(R.id.texto_karuta11);
	     TextView txtkaruta12 = (TextView) findViewById(R.id.texto_karuta12);
	     
	     // Loading Font Face
	     Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

	     // Applying font
	     txtkaruta1.setTypeface(tf);
	     txtkaruta2.setTypeface(tf);
	     txtkaruta3.setTypeface(tf);
	     txtkaruta4.setTypeface(tf);
	     txtkaruta5.setTypeface(tf);
	     txtkaruta6.setTypeface(tf);
	     txtkaruta7.setTypeface(tf);
	     txtkaruta8.setTypeface(tf);
	     txtkaruta9.setTypeface(tf);
	     txtkaruta10.setTypeface(tf);
	     txtkaruta11.setTypeface(tf);
	     txtkaruta12.setTypeface(tf);
	 }
	 
	 private void mudarFonteDeTodoOTextoDaTelaFinal()
	 {
		 	String fontPath = "fonts/Wonton.ttf";
		 	String fontPath2 = "fonts/gilles_comic_br.ttf";
		 	// Loading Font Face
		    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
		    
		    // Applying font
		    TextView japanPointsAdquiridos = (TextView) findViewById(R.id.japanPointsAdquiridos);
		    TextView quantosPontosFez = (TextView) findViewById(R.id.quantosPontosVoceFez);
		    TextView pontos = (TextView) findViewById(R.id.pontos);
		    TextView chegouAteONivel = (TextView) findViewById(R.id.chegouAteONivel);
		    TextView fraseDeAcordoComDesempenho = (TextView) findViewById(R.id.fraseDeAcordoComDesempenho);
		    
		    japanPointsAdquiridos.setTypeface(tf);
		    
		    quantosPontosFez.setTypeface(tf2);
		    pontos.setTypeface(tf2);
		    chegouAteONivel.setTypeface(tf2);
		    fraseDeAcordoComDesempenho.setTypeface(tf2);
		    
		    
		    Button botaoVerPalavrasErradas = (Button) findViewById(R.id.botaoVerPalavrasErradas);
		    Button botaoTreinarNovamente = (Button) findViewById(R.id.botaoTreinarNovamente);
		    Button botaoVoltarAoMenuPrincipal = (Button) findViewById(R.id.botaoVoltarAoMenuPrincipal);
		    
		    botaoVerPalavrasErradas.setTypeface(tf);
		    botaoTreinarNovamente.setTypeface(tf);
		    botaoVoltarAoMenuPrincipal.setTypeface(tf);
		    
		    TextView textoTituloTreinamentoFimDeJogo = (TextView) findViewById(R.id.textoTituloTreinamentoFimDeJogo);
		    textoTituloTreinamentoFimDeJogo.setTypeface(tf);
	 }
	 
	 private void mudarFonte4UltimosKanjisETitulo()
	 {
		 String fontPath = "fonts/Wonton.ttf";
		 // Loading Font Face
		 Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		 
		 TextView textoUltimoKanji1 = (TextView) findViewById(R.id.ultimokanji1);
		 TextView textoUltimoKanji2 = (TextView) findViewById(R.id.ultimokanji2);
		 TextView textoUltimoKanji3 = (TextView) findViewById(R.id.ultimokanji3);
		 TextView textoUltimoKanji4 = (TextView) findViewById(R.id.ultimokanji4);
		 
		 textoUltimoKanji1.setTypeface(tf);
		 textoUltimoKanji2.setTypeface(tf);
		 textoUltimoKanji3.setTypeface(tf);
		 textoUltimoKanji4.setTypeface(tf);
		 
		 TextView titulo = (TextView) findViewById(R.id.titulo_tela_observacao_novos_kanjis);
		 titulo.setTypeface(tf);
		 
		 TextView observacao = (TextView) findViewById(R.id.observacao);
		 observacao.setTypeface(tf);
		 
		 TextView textoNaPlaquinha = (TextView) findViewById(R.id.textoTituloTreinamento);
		 textoNaPlaquinha.setTypeface(tf);
		 
	 }
	 
	 private void mudarFonteTraducoesQuatroUltimosKanjis()
	 {
		 String fontPath = "fonts/Wonton.ttf";
		 // Loading Font Face
		 Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		 
		 TextView textoUltimoKanji1Traducao = (TextView) findViewById(R.id.ultimoKanji1Traducao);
		 TextView textoUltimoKanji2Traducao = (TextView) findViewById(R.id.ultimoKanji2Traducao);
		 TextView textoUltimoKanji3Traducao = (TextView) findViewById(R.id.ultimoKanji3Traducao);
		 TextView textoUltimoKanji4Traducao = (TextView) findViewById(R.id.ultimoKanji4Traducao);
		 
		 textoUltimoKanji1Traducao.setTypeface(tf);
		 textoUltimoKanji2Traducao.setTypeface(tf);
		 textoUltimoKanji3Traducao.setTypeface(tf);
		 textoUltimoKanji4Traducao.setTypeface(tf);
	 }
	 
	 
	 /*indiceDaCarta vai de 0 ate 11*/
	 private void colorirBordaDaCartaDeAcordoComCategoria(int indiceDaCarta)
	 {
	 	ImageView karuta1 = (ImageView) findViewById(R.id.karuta1_imageview);
	     ImageView karuta2 = (ImageView) findViewById(R.id.karuta2_imageview);
	     ImageView karuta3 = (ImageView) findViewById(R.id.karuta3_imageview);
	     ImageView karuta4 = (ImageView) findViewById(R.id.karuta4_imageview);
	     ImageView karuta5 = (ImageView) findViewById(R.id.karuta5_imageview);
	     ImageView karuta6 = (ImageView) findViewById(R.id.karuta6_imageview);
	     ImageView karuta7 = (ImageView) findViewById(R.id.karuta7_imageview);
	     ImageView karuta8 = (ImageView) findViewById(R.id.karuta8_imageview);
	     ImageView karuta9 = (ImageView) findViewById(R.id.karuta9_imageview);
	     ImageView karuta10 = (ImageView) findViewById(R.id.karuta10_imageview);
	     ImageView karuta11 = (ImageView) findViewById(R.id.karuta11_imageview);
	     ImageView karuta12 = (ImageView) findViewById(R.id.karuta12_imageview);
	     
	     	KanjiTreinar umKanjiCartaNaTela = this.kanjisDasCartasNaTela.get(indiceDaCarta);
	     	ImageView cartaASerMudada;
	     	
	     	if(indiceDaCarta == 0)
	     	{
	     		cartaASerMudada = karuta1;
	     	}
	     	else if(indiceDaCarta == 1)
	     	{
	     		cartaASerMudada = karuta2;
	     	}
	     	else if(indiceDaCarta == 2)
	     	{
	     		cartaASerMudada = karuta3;
	     	}
	     	else if(indiceDaCarta == 3)
	     	{
	     		cartaASerMudada = karuta4;
	     	}
	     	else if(indiceDaCarta == 4)
	     	{
	     		cartaASerMudada = karuta5;
	     	}
	     	else if(indiceDaCarta == 5)
	     	{
	     		cartaASerMudada = karuta6;
	     	}
	     	else if(indiceDaCarta == 6)
	     	{
	     		cartaASerMudada = karuta7;
	     	}
	     	else if(indiceDaCarta == 7)
	     	{
	     		cartaASerMudada = karuta8;
	     	}
	     	else if(indiceDaCarta == 8)
	     	{
	     		cartaASerMudada = karuta9;
	     	}
	     	else if(indiceDaCarta == 9)
	     	{
	     		cartaASerMudada = karuta10;
	     	}
	     	else if(indiceDaCarta == 10)
	     	{
	     		cartaASerMudada = karuta11;
	     	}
	     	else
	     	{
	     		cartaASerMudada = karuta12;
	     	}
	     	
	     	String categoria = umKanjiCartaNaTela.getCategoriaAssociada();
	     	String cotidiano = getResources().getString(R.string.cotidiano);
	     	String lugar = getResources().getString(R.string.lugar);
	     	String natureza = getResources().getString(R.string.natureza);
	     	String verbos = getResources().getString(R.string.verbos);
	     	String adjetivos = getResources().getString(R.string.adjetivos);
	     	String tempo = getResources().getString(R.string.tempo);
	     	String supermercado = getResources().getString(R.string.supermercado);
	     	String lazer = getResources().getString(R.string.lazer);
	     	String educacao = getResources().getString(R.string.educacao);
	     	String trabalho = getResources().getString(R.string.trabalho);
	     	String geografia = getResources().getString(R.string.geografia);
	     	
	     	if(categoria.compareTo(cotidiano) == 0)
	     	{
	     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaamarelo);
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziaverdeescurotreinamento);
	     	}
	     	else if(categoria.compareTo(lugar) == 0)
	     	{
	     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaazul);
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziaazulmaisescurotreinamento);
	     	}
	     	else if(categoria.compareTo(natureza) == 0)
	     	{
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziaverdeclarotreinamento);
	     	}
	     	else if(categoria.compareTo(verbos) == 0)
	     	{
	     		//cartaASerMudada.setImageResource(R.drawable.karutavaziacinza);
	     		cartaASerMudada.setImageResource(R.drawable.karutavazialaranjatreinamento);
	     	}
	     	else if(categoria.compareTo(adjetivos) == 0)
	     	{
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziaroxoclarotreinamento);
	     	}
	     	else if(categoria.compareTo(tempo) == 0)
	     	{
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziaroxotreinamento);
	     	}
	     	else if(categoria.compareTo(supermercado) == 0)
	     	{
	     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaroxo);
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziaazulescurotreinamento);
	     	}
	     	else if(categoria.compareTo(lazer) == 0)
	     	{
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziarosatreinamento);
	     	}
	     	else if(categoria.compareTo(educacao) == 0)
	     	{
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziaazulclarotreinamento);
	     	}
	     	else if(categoria.compareTo(trabalho) == 0)
	     	{
	     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaverdeclaro);
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziamarromtreinamento);
	     	}
	     	else if(categoria.compareTo(geografia) == 0)
	     	{
	     		//cartaASerMudada.setImageResource(R.drawable.karutavaziaverdeescuro);
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziacinzatreinamento);
	     	}
	     	else
	     	{
	     		//cartaASerMudada.setImageResource(R.drawable.karutavaziavermelho);
	     		cartaASerMudada.setImageResource(R.drawable.karutavaziamarromclarotreinamento);
	     	}
	 }
	 
	 @Override
	 public void onBackPressed() 
	 {
		 super.onBackPressed();
		 boolean temConexaoComNet = this.temConexaoComInternet();
		 boolean categoriasESeusNomesEhVazio = SingletonArmazenaCategoriasDoJogo.getInstance().getCategoriasESeusIds().isEmpty();
			
		 if(this.usuarioNoMeioDeUmTreinamento == true && temConexaoComNet == true && categoriasESeusNomesEhVazio == false)
		 {
			 //usuario desistiu no meio de uma partida? Pelo menos envia coisas pro BD!!!
			 this.mandarDadosDaPartidaParaOBancoDeDados();
		 }
		 
		 mudarMusicaDeFundo(R.raw.tradicional_japanese_ethnic_music_pack);
	 }
	 
	 private boolean temConexaoComInternet() {
		    ConnectivityManager connectivityManager 
		          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
		}
	 
	 private void mandarDadosDaPartidaParaOBancoDeDados()
	 {
		 String data_partida = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		 String hora_partida = new SimpleDateFormat("HH:mm").format(new Date());
		 
		 LinkedList<String> nomeEEMailUsuario = ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(getApplicationContext());
		 String nome_usuario = nomeEEMailUsuario.get(0);
		 String ate_que_nivel_chegou = String.valueOf(this.nivel);
		 
		 String idsKanjisSeparadosPorVirgula = "";
		 String idsCategoriasKanjisSeparadosPorVirgula = "";
		 String seAcertouKanjisSeparadosPorVirgula = "";
		 
		 int quantosErrou = this.kanjisQueUsuarioErrou.size();
		 int quantosAcertou = this.kanjisQueUsuarioAcertou.size();
		 
		 for(int i = 0; i < this.kanjisQueUsuarioAcertou.size(); i++)
		 {
			 KanjiTreinar umKanji = this.kanjisQueUsuarioAcertou.get(i);
			 idsKanjisSeparadosPorVirgula = idsKanjisSeparadosPorVirgula + umKanji.getIdDoKanji() + ",";
			 String categoriaDesseKanji = umKanji.getCategoriaAssociada();
			 int idCategoria = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(categoriaDesseKanji);
			 idsCategoriasKanjisSeparadosPorVirgula = idsCategoriasKanjisSeparadosPorVirgula + String.valueOf(idCategoria) + ",";
			 seAcertouKanjisSeparadosPorVirgula = seAcertouKanjisSeparadosPorVirgula + "sim" + ",";
		 }
		 
		 for(int j = 0; j < this.kanjisQueUsuarioErrou.size(); j++)
		 {
			 KanjiTreinar umKanji = this.kanjisQueUsuarioErrou.get(j);
			 idsKanjisSeparadosPorVirgula = idsKanjisSeparadosPorVirgula + umKanji.getIdDoKanji();
			 String categoriaDesseKanji = umKanji.getCategoriaAssociada();
			 int idCategoria = SingletonArmazenaCategoriasDoJogo.getInstance().pegarIdDaCategoria(categoriaDesseKanji);
			 idsCategoriasKanjisSeparadosPorVirgula = idsCategoriasKanjisSeparadosPorVirgula + String.valueOf(idCategoria);
			 seAcertouKanjisSeparadosPorVirgula = seAcertouKanjisSeparadosPorVirgula + "não";
			 
			 if(j < this.kanjisQueUsuarioErrou.size() - 1)
			 {
				 idsKanjisSeparadosPorVirgula = idsKanjisSeparadosPorVirgula + ",";
				 idsCategoriasKanjisSeparadosPorVirgula = idsCategoriasKanjisSeparadosPorVirgula + ",";
				 seAcertouKanjisSeparadosPorVirgula = seAcertouKanjisSeparadosPorVirgula + ",";
			 }
		 }
		 
		 InserirPartidaTreinamentoNoLogTask taskInserePartidaNoLogDoTreinamento = 
				 new InserirPartidaTreinamentoNoLogTask(nome_usuario,data_partida,hora_partida,
					ate_que_nivel_chegou, idsKanjisSeparadosPorVirgula, idsCategoriasKanjisSeparadosPorVirgula, 
																	seAcertouKanjisSeparadosPorVirgula);
		 taskInserePartidaNoLogDoTreinamento.execute("");
	 }
	 
	 public void verPalavrasErradas(View v)
	 {
		 final Dialog popupPalavrasErradasFimTreinamento = new Dialog(this);
			
		 popupPalavrasErradasFimTreinamento.requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			  // Include dialog.xml file
		 popupPalavrasErradasFimTreinamento.setContentView(R.layout.popup_palavras_erradas_fim_treinamento);
		 popupPalavrasErradasFimTreinamento.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		 this.mudarFonteTextoPopupPalavrasErradas(popupPalavrasErradasFimTreinamento);


			//criar acao para botao de fechar dialog
			Button botao_fechar_popup_palavras_erradas = (Button) popupPalavrasErradasFimTreinamento.findViewById(R.id.botao_fechar_popup_palavras_erradas);
			botao_fechar_popup_palavras_erradas.setOnClickListener(new Button.OnClickListener() 
			{
			 	public void onClick(View v) 
			 	{
			 		 popupPalavrasErradasFimTreinamento.dismiss();
			 	}
			}); 
			
			LinkedList<KanjiTreinar> kanjisErradosSemRepeticao = new LinkedList<KanjiTreinar>();
			for(int i = 0; i < this.kanjisQueUsuarioErrou.size(); i++)
			{
				KanjiTreinar umKanjiErrado = kanjisQueUsuarioErrou.get(i);
				
				boolean kanjiJaPertenceAoSemRepeticao = false;
				for(int j = 0; j < kanjisErradosSemRepeticao.size(); j++)
				{
					KanjiTreinar umKanjiErradoSemRepeticao = kanjisErradosSemRepeticao.get(j);
					
					if((umKanjiErrado.getKanji().compareTo(umKanjiErradoSemRepeticao.getKanji()) == 0)
							&& (umKanjiErrado.getCategoriaAssociada().compareTo(umKanjiErradoSemRepeticao.getCategoriaAssociada()) == 0))
					{
						kanjiJaPertenceAoSemRepeticao = true;
					}
				}
				
				if(kanjiJaPertenceAoSemRepeticao == false)
				{
					kanjisErradosSemRepeticao.add(umKanjiErrado);
				}
			}
			
			TextView textViewPalavraErrada1 = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada1);
			TextView textViewPalavraErrada2 = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada2);
			TextView textViewPalavraErrada3 = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada3);
			TextView textViewPalavraErrada4 = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada4);
			TextView textViewPalavraErrada1Furigana = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada1Furigana);
			TextView textViewPalavraErrada2Furigana = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada2Furigana);
			TextView textViewPalavraErrada3Furigana = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada3Furigana);
			TextView textViewPalavraErrada4Furigana = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada4Furigana);
			TextView textViewPalavraErrada1Traducao = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada1Traducao);
			TextView textViewPalavraErrada2Traducao = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada2Traducao);
			TextView textViewPalavraErrada3Traducao = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada3Traducao);
			TextView textViewPalavraErrada4Traducao = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada4Traducao);
			TextView textViewPalavraErrada1Igual = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada1Igual);
			TextView textViewPalavraErrada2Igual = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada2Igual);
			TextView textViewPalavraErrada3Igual = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada3Igual);
			TextView textViewPalavraErrada4Igual = (TextView) popupPalavrasErradasFimTreinamento.findViewById(R.id.palavraErrada4Igual);
			
			//LinearLayout layoutPalavraErrada1 = (LinearLayout) findViewById(R.id.palavraErrada1TudoJunto);
			//LinearLayout layoutPalavraErrada2 = (LinearLayout) findViewById(R.id.palavraErrada2TudoJunto);
			//LinearLayout layoutPalavraErrada3 = (LinearLayout) findViewById(R.id.palavraErrada3TudoJunto);
			//LinearLayout layoutPalavraErrada4 = (LinearLayout) findViewById(R.id.palavraErrada4TudoJunto);
			
			int qualTextViewIrahColocarTexto = 1;
			for(int k = 0; k < kanjisErradosSemRepeticao.size(); k++)
			{
				KanjiTreinar umKanjiTreinarSemRepeticao = kanjisErradosSemRepeticao.get(k);
				String textoParaOTextView = umKanjiTreinarSemRepeticao.getKanji();
				String textoParaOTextViewFurigana = umKanjiTreinarSemRepeticao.getHiraganaDoKanji();
				String textoParaTextViewTraducao = umKanjiTreinarSemRepeticao.getTraducaoEmPortugues(); 
				
				if(qualTextViewIrahColocarTexto == 1)
				{
					textViewPalavraErrada1.setText(textoParaOTextView);
					textViewPalavraErrada1Furigana.setText(textoParaOTextViewFurigana);
					textViewPalavraErrada1Traducao.setText(textoParaTextViewTraducao);
					textViewPalavraErrada1Igual.setText(" = ");
				}
				else if(qualTextViewIrahColocarTexto == 2)
				{
					textViewPalavraErrada2.setText(textoParaOTextView);
					textViewPalavraErrada2Furigana.setText(textoParaOTextViewFurigana);
					textViewPalavraErrada2Traducao.setText(textoParaTextViewTraducao);
					textViewPalavraErrada2Igual.setText(" = ");
				}
				else if(qualTextViewIrahColocarTexto == 3)
				{
					textViewPalavraErrada3.setText(textoParaOTextView);
					textViewPalavraErrada3Furigana.setText(textoParaOTextViewFurigana);
					textViewPalavraErrada3Traducao.setText(textoParaTextViewTraducao);
					textViewPalavraErrada3Igual.setText(" = ");
				}
				else
				{
					textViewPalavraErrada4.setText(textoParaOTextView);
					textViewPalavraErrada4Furigana.setText(textoParaOTextViewFurigana);
					textViewPalavraErrada4Traducao.setText(textoParaTextViewTraducao);
					textViewPalavraErrada4Igual.setText(" = ");
				}
				
				qualTextViewIrahColocarTexto = qualTextViewIrahColocarTexto + 1;
			}
			
			if(qualTextViewIrahColocarTexto != 5)
			{
				//alguns textviews devem ficar em branco
				while(qualTextViewIrahColocarTexto != 5)
				{
					if(qualTextViewIrahColocarTexto == 1)
					{
						textViewPalavraErrada1.setText("");
						textViewPalavraErrada1Furigana.setText("");
						textViewPalavraErrada1Traducao.setText("");
						textViewPalavraErrada1Igual.setText("");
						/*layoutComPalavrasErradas.removeView(textViewPalavraErrada1);
						layoutComPalavrasErradas.removeView(textViewPalavraErrada1Furigana);
						layoutComPalavrasErradas.removeView(textViewPalavraErrada1Traducao);
						layoutComPalavrasErradas.removeView(layoutPalavraErrada1);*/
					}
					else if(qualTextViewIrahColocarTexto == 2)
					{
						textViewPalavraErrada2.setText("");
						textViewPalavraErrada2Furigana.setText("");
						textViewPalavraErrada2Traducao.setText("");
						textViewPalavraErrada2Igual.setText("");
						/*layoutComPalavrasErradas.removeView(textViewPalavraErrada2);
						layoutComPalavrasErradas.removeView(textViewPalavraErrada2Furigana);
						layoutComPalavrasErradas.removeView(textViewPalavraErrada2Traducao);
						layoutComPalavrasErradas.removeView(layoutPalavraErrada2);*/
					}
					else if(qualTextViewIrahColocarTexto == 3)
					{
						textViewPalavraErrada3.setText("");
						textViewPalavraErrada3Furigana.setText("");
						textViewPalavraErrada3Traducao.setText("");
						textViewPalavraErrada3Igual.setText("");
						/*layoutComPalavrasErradas.removeView(textViewPalavraErrada3);
						layoutComPalavrasErradas.removeView(textViewPalavraErrada3Furigana);
						layoutComPalavrasErradas.removeView(textViewPalavraErrada3Traducao);
						layoutComPalavrasErradas.removeView(layoutPalavraErrada3);*/
					}
					else
					{
						textViewPalavraErrada4.setText("");
						textViewPalavraErrada4Furigana.setText("");
						textViewPalavraErrada4Traducao.setText("");
						textViewPalavraErrada4Igual.setText("");
						/*layoutComPalavrasErradas.removeView(textViewPalavraErrada4);
						layoutComPalavrasErradas.removeView(textViewPalavraErrada4Furigana);
						layoutComPalavrasErradas.removeView(textViewPalavraErrada4Traducao);
						layoutComPalavrasErradas.removeView(layoutPalavraErrada4);*/
					}
					qualTextViewIrahColocarTexto = qualTextViewIrahColocarTexto + 1;
				}
			}
			
			
			popupPalavrasErradasFimTreinamento.show();
	 }
	 
	 private void mudarFonteTextoPopupPalavrasErradas(Dialog dialog)
	 {
		 String fontPath = "fonts/Wonton.ttf";
	     Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
	     
		 TextView palavraErrada1 = (TextView) dialog.findViewById(R.id.palavraErrada1Traducao);
		    TextView palavraErrada2 = (TextView) dialog.findViewById(R.id.palavraErrada2Traducao);
		    TextView palavraErrada3 = (TextView) dialog.findViewById(R.id.palavraErrada3Traducao);
		    TextView palavraErrada4 = (TextView) dialog.findViewById(R.id.palavraErrada4Traducao);
		    TextView tituloPalavrasQueErrou = (TextView) dialog.findViewById(R.id.palavrasQueVoceErrou);
		    palavraErrada1.setTypeface(tf);
		    palavraErrada2.setTypeface(tf);
		    palavraErrada3.setTypeface(tf);
		    palavraErrada4.setTypeface(tf);
		    tituloPalavrasQueErrou.setTypeface(tf);
	 }
	 
	 

}

