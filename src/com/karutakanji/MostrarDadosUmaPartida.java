package com.karutakanji;

import java.util.LinkedList;

import bancodedados.DadosPartidaParaOLog;
import bancodedados.KanjiTreinar;
import bancodedados.SingletonArmazenaCategoriasDoJogo;
import br.ufrn.dimap.pairg.karutakanji.android.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MostrarDadosUmaPartida extends ActivityDoJogoComSom implements View.OnClickListener
{
	private DadosPartidaParaOLog dadosUmaPartida;
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mostrar_dados_uma_partida);
		
		SingletonDadosDeUmaPartidaASerMostrada conheceDadosUmaPartida = SingletonDadosDeUmaPartidaASerMostrada.getInstance();
		this.dadosUmaPartida = conheceDadosUmaPartida.getDadosUmaPartida();
		
		this.mostrarTodosOsDadosDaPartidaExcetoAsPalavras();
		
		this.mudarFonteDosTextosDaTela();
		
		Button botao_palavras_treinadas = (Button) findViewById(R.id.botao_palavras_treinadas);
		botao_palavras_treinadas.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mostrar_dados_uma_partida, menu);
		return true;
	}
	
	
	private void mostrarTodosOsDadosDaPartidaExcetoAsPalavras()
	{
		TextView textViewDataPartida = (TextView) findViewById(R.id.dataDaPartida);
		TextView textViewEmailAdversario = (TextView) findViewById(R.id.emailDoAdversario);
		TextView textViewQuemVenceu = (TextView) findViewById(R.id.quemVenceu);
		TextView textViewPontuacao = (TextView) findViewById(R.id.suaPontuacao);
		
		String labelDataPartida = getResources().getString(R.string.em_dois_pontos);
		String labelEmailAdversario = getResources().getString(R.string.adversario_dois_pontos);
		String labelSuaPontuacao = getResources().getString(R.string.seus_pontos_dois_pontos);
		
		textViewDataPartida.setText(labelDataPartida + " " + this.dadosUmaPartida.getData());
		
		String como_aparece_nome_adversario = this.dadosUmaPartida.getnome_adversario();
    	if(como_aparece_nome_adversario.length() >= 20)
    	{
    		como_aparece_nome_adversario = como_aparece_nome_adversario.substring(0,20);
    	}
		
		textViewEmailAdversario.setText(labelEmailAdversario + " " + como_aparece_nome_adversario);
		
		String ganhou = "ganhou";
		String perdeu = "perdeu";
		String empatou = "empatou";
		
		String labelGanhou = getResources().getString(R.string.ganhou);
		String labelPerdeu = getResources().getString(R.string.perdeu);
		String labelEmpatou = getResources().getString(R.string.empatou);
		ImageView mascote_dados_uma_partida = (ImageView) findViewById(R.id.mascote_dados_uma_partida);
		
		if(this.dadosUmaPartida.getVoceGanhouOuPerdeu().compareTo(ganhou) == 0)
		{
			textViewQuemVenceu.setText(labelGanhou);
			mascote_dados_uma_partida.setImageResource(R.drawable.mascote_feliz_competicao_menor);
		
		}
		else if(this.dadosUmaPartida.getVoceGanhouOuPerdeu().compareTo(perdeu) == 0)
		{
			textViewQuemVenceu.setText(labelPerdeu);
			mascote_dados_uma_partida.setImageResource(R.drawable.mascote_triste_competicao_menor);
		}
		else
		{
			textViewQuemVenceu.setText(labelEmpatou);
			mascote_dados_uma_partida.setImageResource(R.drawable.mascote_pensativa_competicao_menor);
		}
		
		textViewPontuacao.setText(labelSuaPontuacao + " " + String.valueOf(this.dadosUmaPartida.getPontuacao()));
		
		/*String ids_categorias = 
    			this.dadosUmaPartida.getid_categorias_tudo_junto_separado_por_virgula();
    	//precisamos pegar os nomes dessas categorias
    	String categoriasSeparadasPorVirgulas = "";
		String[] idsCategoriasArray = ids_categorias.split(",");
    	SingletonArmazenaCategoriasDoJogo conheceCategoriasDoJogo 
    									= SingletonArmazenaCategoriasDoJogo.getInstance();
    	for(int j = 0; j < idsCategoriasArray.length; j++)
    	{
    		String umIdCategoria = idsCategoriasArray[j];
    		String umaCategoria = conheceCategoriasDoJogo.pegarCategoriaPorId(umIdCategoria);
    		categoriasSeparadasPorVirgulas = categoriasSeparadasPorVirgulas + umaCategoria;
    		
    		if(j < idsCategoriasArray.length - 1)
    		{
    			categoriasSeparadasPorVirgulas = categoriasSeparadasPorVirgulas + ",";
    		}
    	}
		String ultimoCaractereDasCategorias = String.valueOf(categoriasSeparadasPorVirgulas.charAt(categoriasSeparadasPorVirgulas.length() - 1));
		if(ultimoCaractereDasCategorias.compareTo(",") == 0)
    	{
			categoriasSeparadasPorVirgulas = categoriasSeparadasPorVirgulas.substring(0,categoriasSeparadasPorVirgulas.length() - 1); //vamos tirar o ultimo caractere da string
    	}*/
		
		//textViewCategorias.setText(labelCategorias + " " + categoriasSeparadasPorVirgulas);
	}
	
	

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	
	 
	 private void mudarFonteDosTextosDaTela()
	 {
		 String fontPath2 = "fonts/Wonton.ttf";
		    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
		    TextView textoTituloCompeticao = (TextView) findViewById(R.id.textoTituloCompeticao);
		    TextView tituloDadosDaPartida = (TextView) findViewById(R.id.tituloDadosDaPartida);
		    TextView quemVenceu = (TextView) findViewById(R.id.quemVenceu);
		    TextView emailDoAdversario = (TextView) findViewById(R.id.emailDoAdversario);
		    TextView suaPontuacao = (TextView) findViewById(R.id.suaPontuacao);
		    TextView dataDaPartida = (TextView) findViewById(R.id.dataDaPartida);
		    Button botao_palavras_treinadas = (Button) findViewById(R.id.botao_palavras_treinadas);
		    
		    textoTituloCompeticao.setTypeface(tf2);
		    tituloDadosDaPartida.setTypeface(tf2);
		    quemVenceu.setTypeface(tf2);
		    emailDoAdversario.setTypeface(tf2);
		    suaPontuacao.setTypeface(tf2);
		    dataDaPartida.setTypeface(tf2);
		    botao_palavras_treinadas.setTypeface(tf2);
	 }

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) {
	    case R.id.botao_palavras_treinadas:
	    	Intent i = new Intent(getApplicationContext(), MostrarPalavrasUmaPartida.class);
            startActivity(i);
	    	break;
		}
		
	}
	
}
