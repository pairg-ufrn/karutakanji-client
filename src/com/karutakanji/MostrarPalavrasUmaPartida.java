package com.karutakanji;

import java.util.LinkedList;

import bancodedados.AdapterListViewPalavrasUmaPartidaAnterior;
import bancodedados.DadosPartidaParaOLog;
import bancodedados.KanjiTreinar;
import br.ufrn.dimap.pairg.karutakanji.android.R;
import br.ufrn.dimap.pairg.karutakanji.android.R.layout;
import br.ufrn.dimap.pairg.karutakanji.android.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MostrarPalavrasUmaPartida extends ActivityDoJogoComSom implements View.OnClickListener, OnScrollListener 
{
	private DadosPartidaParaOLog dadosUmaPartida;
	private String oQueAtualmenteListViewMostra; //pode ter 3 valores: palavrastreinadas,palavrasacertadas e palavraserradas

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mostrar_palavras_uma_partida);
		
		SingletonDadosDeUmaPartidaASerMostrada conheceDadosUmaPartida = SingletonDadosDeUmaPartidaASerMostrada.getInstance();
		this.dadosUmaPartida = conheceDadosUmaPartida.getDadosUmaPartida();
		
		this.fazerListViewMostrarPalavrasTreinadas();
		
		
		findViewById(R.id.setaEsquerda).setOnClickListener(this);
		findViewById(R.id.setaDireita).setOnClickListener(this);
		
		this.mudarFonteTextosDaTela();
		
		ListView listViewPalavrasAcertadasErradasTreinadas = (ListView) findViewById(R.id.listViewPalavrasAcertadasErradasTreinadas);
		listViewPalavrasAcertadasErradasTreinadas.setOnScrollListener(this);
		
		ImageView botaoVoltarPalavrasUmaPartida = (ImageView) findViewById(R.id.botaoVoltarPalavrasUmaPartida);
		botaoVoltarPalavrasUmaPartida.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mostrar_palavras_uma_partida, menu);
		return true;
	}

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	private void fazerListViewMostrarPalavrasTreinadas()
	{
		this.oQueAtualmenteListViewMostra = "palavrastreinadas";
		
		TextView textViewTituloListView = (TextView)findViewById(R.id.tituloListViewPalavrasUmaPartida);
		String stringPalavrasTreinadas = getResources().getString(R.string.palavrasTreinadas);
		textViewTituloListView.setText(stringPalavrasTreinadas);
		
		LinkedList<KanjiTreinar> palavrasTreinadas = this.dadosUmaPartida.getPalavrasJogadas();
		
		//o usuario nao vai querer ver que o mesmo kanji foi treinado mais de uma vez, por isso vamos remover os repetidos
		palavrasTreinadas = obterSomenteOsKanjisNaoRepetidosDalista(palavrasTreinadas);
		
		int tamanhoValues = palavrasTreinadas.size();
		String[] values = new String[tamanhoValues];
        int percorredorValues = 0;
        
        for(int i = 0; i < palavrasTreinadas.size(); i++)
        {
        	KanjiTreinar umaPalavraTreinada = palavrasTreinadas.get(i);
        	String oQueApareceraComoItemNaLista = umaPalavraTreinada.getKanji() + " - " +
        					umaPalavraTreinada.getHiraganaDoKanji() + " - " +
        					umaPalavraTreinada.getTraducaoEmPortugues();
        	values[percorredorValues] = oQueApareceraComoItemNaLista;
        	percorredorValues = percorredorValues + 1;
        }
        
        AdapterListViewPalavrasUmaPartidaAnterior adapter = new AdapterListViewPalavrasUmaPartidaAnterior
        (this,android.R.layout.simple_list_item_1, values){
            @Override
            public boolean isEnabled(int position) {
                return false;
            }
        };
;


        // Assign adapter to ListView
        ListView listView = (ListView) findViewById(R.id.listViewPalavrasAcertadasErradasTreinadas);
        listView.setAdapter(adapter);
	}
	
	private void fazerListViewMostrarPalavrasAcertadas()
	{
		this.oQueAtualmenteListViewMostra = "palavrasacertadas";
		
		TextView textViewTituloListView = (TextView)findViewById(R.id.tituloListViewPalavrasUmaPartida);
		String stringPalavrasAcertadas = getResources().getString(R.string.palavrasAcertadas);
		textViewTituloListView.setText(stringPalavrasAcertadas);
		
		LinkedList<KanjiTreinar> palavrasAcertadas = this.dadosUmaPartida.getPalavrasAcertadas();
		//o usuario nao vai querer ver que o mesmo kanji foi acertado mais de uma vez, por isso vamos remover os repetidos
		palavrasAcertadas = obterSomenteOsKanjisNaoRepetidosDalista(palavrasAcertadas);
		
		int tamanhoValues = palavrasAcertadas.size();
		String[] values = new String[tamanhoValues];
        int percorredorValues = 0;
        
        for(int i = 0; i < palavrasAcertadas.size(); i++)
        {
        	KanjiTreinar umaPalavraAcertada = palavrasAcertadas.get(i);
        	String oQueApareceraComoItemNaLista = umaPalavraAcertada.getKanji() + " - " +
        					umaPalavraAcertada.getHiraganaDoKanji() + " - " +
        					umaPalavraAcertada.getTraducaoEmPortugues();
        	values[percorredorValues] = oQueApareceraComoItemNaLista;
        	percorredorValues = percorredorValues + 1;
        }
        
        AdapterListViewPalavrasUmaPartidaAnterior adapter = new AdapterListViewPalavrasUmaPartidaAnterior
        (this,android.R.layout.simple_list_item_1, values){
            @Override
            public boolean isEnabled(int position) {
                return false;
            }
        };
;


        // Assign adapter to ListView
        ListView listView = (ListView) findViewById(R.id.listViewPalavrasAcertadasErradasTreinadas);
        listView.setAdapter(adapter); 
	}
	
	private void fazerListViewMostrarPalavrasErradas()
	{
		this.oQueAtualmenteListViewMostra = "palavraserradas";
		
		TextView textViewTituloListView = (TextView)findViewById(R.id.tituloListViewPalavrasUmaPartida);
		String stringPalavrasErradas = getResources().getString(R.string.palavrasErradas);
		textViewTituloListView.setText(stringPalavrasErradas);
		
		LinkedList<KanjiTreinar> palavrasErradas = this.dadosUmaPartida.getPalavrasErradas();
		//o usuario nao vai querer ver que o mesmo kanji foi errado mais de uma vez, por isso vamos remover os repetidos
		palavrasErradas = obterSomenteOsKanjisNaoRepetidosDalista(palavrasErradas);
		
		int tamanhoValues = palavrasErradas.size();
		String[] values = new String[tamanhoValues];
        int percorredorValues = 0;
        
        for(int i = 0; i < palavrasErradas.size(); i++)
        {
        	KanjiTreinar umaPalavraErrada = palavrasErradas.get(i);
        	String oQueApareceraComoItemNaLista = umaPalavraErrada.getKanji() + " - " +
        					umaPalavraErrada.getHiraganaDoKanji() + " - " +
        					umaPalavraErrada.getTraducaoEmPortugues();
        	values[percorredorValues] = oQueApareceraComoItemNaLista;
        	percorredorValues = percorredorValues + 1;
        }
        
        AdapterListViewPalavrasUmaPartidaAnterior adapter = new AdapterListViewPalavrasUmaPartidaAnterior
        (this,android.R.layout.simple_list_item_1, values){
            @Override
            public boolean isEnabled(int position) {
                return false;
            }
        };
;


        // Assign adapter to ListView
        ListView listView = (ListView) findViewById(R.id.listViewPalavrasAcertadasErradasTreinadas);
        listView.setAdapter(adapter); 
	}
	
	private void usuarioClicouSetaEsquerda()
	{
		if(this.oQueAtualmenteListViewMostra.compareTo("palavrastreinadas") == 0)
		{
			this.fazerListViewMostrarPalavrasErradas();
		}
		else if(this.oQueAtualmenteListViewMostra.compareTo("palavrasacertadas") == 0)
		{
			this.fazerListViewMostrarPalavrasTreinadas();
		}
		else
		{
			this.fazerListViewMostrarPalavrasAcertadas();
		}
	}
	
	private void usuarioClicouSetaDireita()
	{
		if(this.oQueAtualmenteListViewMostra.compareTo("palavrastreinadas") == 0)
		{
			this.fazerListViewMostrarPalavrasAcertadas();
		}
		else if(this.oQueAtualmenteListViewMostra.compareTo("palavrasacertadas") == 0)
		{
			this.fazerListViewMostrarPalavrasErradas();
		}
		else
		{
			this.fazerListViewMostrarPalavrasTreinadas();
		}
	}
	
	/*a lista retornada sao todos os kanjis da lista de kanjis passada mas sem repeticao de kanji*/
	 private LinkedList<KanjiTreinar> obterSomenteOsKanjisNaoRepetidosDalista(LinkedList<KanjiTreinar> listaKanjis)
	 {
		 LinkedList<KanjiTreinar> copiaListaKanjis = new LinkedList<KanjiTreinar>();
		 
		 //primeiro vou copiar toda a lista passada como parametro
		 for(int i = 0; i < listaKanjis.size(); i++)
		 {
			 copiaListaKanjis.add(listaKanjis.get(i));
		 }
		 
		 //vou sair esvaiando essa copia ate que nenhum kanji sobre nela e apenas uma instancia de cada kanji apareca na lista listaKanjisSemRepeticao
		 LinkedList<KanjiTreinar> listaKanjisSemRepeticao = new LinkedList<KanjiTreinar>();
		 while(copiaListaKanjis.size() > 0)
		 {
			 KanjiTreinar kanjiRemovidoCopiaListaKanjis = copiaListaKanjis.removeFirst();
			 LinkedList<Integer> indicesKanjisASeremRemovidosDeCopiaListaKanjis = new LinkedList<Integer>();
			 for(int j = 0; j < copiaListaKanjis.size(); j++)
			 {
				 KanjiTreinar umKanjiDaCopia = copiaListaKanjis.get(j);
				 if((umKanjiDaCopia.getKanji().compareTo(kanjiRemovidoCopiaListaKanjis.getKanji()) == 0) && 
						 (umKanjiDaCopia.getCategoriaAssociada().compareTo(kanjiRemovidoCopiaListaKanjis.getCategoriaAssociada()) == 0))
				 {
					 //achamos um kanji repetido!
					 indicesKanjisASeremRemovidosDeCopiaListaKanjis.add(j);
				 }
			 }
			 
			 //agora iremos realmente tirar os kanjis repetidos da copia
			 for(int k = 0; k < indicesKanjisASeremRemovidosDeCopiaListaKanjis.size(); k++)
			 {
				 int umIndiceKanjiRemover = indicesKanjisASeremRemovidosDeCopiaListaKanjis.get(k);
				 copiaListaKanjis.remove(umIndiceKanjiRemover);
			 }
			 
			 //por fim, o kanji que removemos do inicio da lista que eh a copia sera colocado na lista de retorno
			 listaKanjisSemRepeticao.add(kanjiRemovidoCopiaListaKanjis);
		 }
		 
		 return listaKanjisSemRepeticao;
	 }
	
	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) {
	    case R.id.setaEsquerda:
	    	usuarioClicouSetaEsquerda();
	    	break;
	    case R.id.setaDireita:
	    	usuarioClicouSetaDireita();
	    	break;
	    case R.id.botaoVoltarPalavrasUmaPartida:
	    	Intent i = new Intent(getApplicationContext(), MostrarDadosUmaPartida.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
	    	break;
		}
		
	}
	
	private void mudarFonteTextosDaTela()
	{
		 String fontPath2 = "fonts/Wonton.ttf";
		    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
		    TextView textoTituloCompeticao = (TextView) findViewById(R.id.textoTituloCompeticao);
		    TextView tituloListView = (TextView) findViewById(R.id.tituloListViewPalavrasUmaPartida);
		    
		    textoTituloCompeticao.setTypeface(tf2);
		    tituloListView.setTypeface(tf2);
	}
	
	@Override
	public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) 
	{
		// TODO Auto-generated method stub
		int firstVisibleItem = arg1;
		int totalItemCount = arg3;
		
		ListView listViewPalavrasAcertadasErradasTreinadas = (ListView) findViewById(R.id.listViewPalavrasAcertadasErradasTreinadas);
		int indiceObjetoDeBaixo = listViewPalavrasAcertadasErradasTreinadas.getLastVisiblePosition();
		
		final int lastItem = firstVisibleItem + arg2;
		
		ImageView setas_palavras_treinadas_acertadas_erradas = (ImageView) findViewById(R.id.setas_palavras_treinadas_acertadas_erradas);
		
		if(firstVisibleItem == 0)
		{
			setas_palavras_treinadas_acertadas_erradas.setImageResource(R.drawable.seta_baixo_listviews);
		}
		else if(lastItem == totalItemCount)
		{
			//a seta para baixo nao aparece
			setas_palavras_treinadas_acertadas_erradas.setImageResource(R.drawable.seta_cima_listviews);
		}
		else if(totalItemCount <= 4)
		{
			//as setas nao precisam aparecer
			setas_palavras_treinadas_acertadas_erradas.setVisibility(View.GONE);
		}
		else
		{
			//ambas as setas aparecem
			setas_palavras_treinadas_acertadas_erradas.setImageResource(R.drawable.seta_cima_e_baixo_listviews);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}

}
