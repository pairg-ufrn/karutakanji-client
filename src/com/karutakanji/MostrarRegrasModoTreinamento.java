package com.karutakanji;

import bancodedados.ArmazenaTudoParaJogoOffline;
import bancodedados.ChecaVersaoAtualDoSistemaTask;
import br.ufrn.dimap.pairg.karutakanji.android.R;

import com.google.example.games.basegameutils.GameHelper;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MostrarRegrasModoTreinamento extends ActivityDoJogoComSom implements View.OnClickListener 
{
	private boolean mostrarRegrasTreinamento; //pegarei da memoria do android o valor desse booleano e aqui eu modificarei
	private ChecaVersaoAtualDoSistemaTask checaVersaoAtual;
	final static int[] SCREENS = {R.id.telaatualizeojogo, R.id.telamostrarregrasnormal};
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		//coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
		super.getGameHelper().setMaxAutoSignInAttempts(0);
		//fim das coisas necessarias para o jogador nao precisar fazer login da Google play no modo treinamento
				
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mostrar_regras_modo_treinamento);
		switchToScreen(R.id.telamostrarregrasnormal);
		
		/*PARTE DEVE SER RETIRADA ASSIM QUE O JOGO VOLTAR A TER OS OUTROS MODOS*/
		/*ArmazenaTudoParaJogoOffline armazenaQuantasVezesAAplicacaoFoiIniciada = 
				ArmazenaTudoParaJogoOffline.getInstance();
		armazenaQuantasVezesAAplicacaoFoiIniciada.aumentarQuantasVezesAAplicacaoFoiReiniciada(this);
		
		boolean haConexaoComInternet = this.temConexaoComInternet();
		if(haConexaoComInternet == true)
		{
			this.checaVersaoAtual = new ChecaVersaoAtualDoSistemaTask(this);
			this.checaVersaoAtual.execute("");
		}*/
		
		/*FIM DE PARTE DEVE SER RETIRADA ASSIM QUE O JOGO VOLTAR A TER OS OUTROS MODOS*/
		
		ImageView imageViewCheckbox = (ImageView) findViewById(R.id.checkbox_aparecer_novamente);
		imageViewCheckbox.setClickable(true);
		imageViewCheckbox.setOnClickListener(this);
		
		
		ArmazenaConfiguracoesDoJogo armazenaMostrarRegras = 
							ArmazenaConfiguracoesDoJogo.getInstance();
		this.mostrarRegrasTreinamento = 
				armazenaMostrarRegras.getMostrarRegrasDoTreinamento(this);
		
		if(this.mostrarRegrasTreinamento == false)
		{
			imageViewCheckbox.setImageResource(R.drawable.checkbox_desmarcada_regras_treinamento);
		}
		else
		{
			imageViewCheckbox.setImageResource(R.drawable.checkbox_marcada_regras_treinamento);
		}
		
		this.mudarFonteTodosOsTextosDessaTela();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mostrar_regras_modo_treinamento, menu);
		return true;
	}
	
	public void escolherCategoriasTreinamento(View v)
	{
		//antes disso, armazenar o valor da checkbox
		ArmazenaConfiguracoesDoJogo armazenaMostrarRegras = 
				ArmazenaConfiguracoesDoJogo.getInstance();
		armazenaMostrarRegras.alterarMostrarRegrasDoTreinamento(this, this.mostrarRegrasTreinamento);
		
		Intent criaTelaModoTreinamento =
				new Intent(this, EscolherCategoriasModoTreinamento.class);
		startActivity(criaTelaModoTreinamento);
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
			case R.id.checkbox_aparecer_novamente:
				ImageView imageViewCheckbox = (ImageView) findViewById(R.id.checkbox_aparecer_novamente);
				if(this.mostrarRegrasTreinamento == true)
				{
					mostrarRegrasTreinamento = false;
					imageViewCheckbox.setImageResource(R.drawable.checkbox_desmarcada_regras_treinamento);
				}
				else
				{
					mostrarRegrasTreinamento = true;
					imageViewCheckbox.setImageResource(R.drawable.checkbox_marcada_regras_treinamento);
				}
			break;
		}
		
	}
	
	private void mudarFonteTodosOsTextosDessaTela()
	{
		String fontPath = "fonts/Wonton.ttf";
		// Loading Font Face
	    Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

	    // Applying font
	    TextView textoTitulo = (TextView) findViewById(R.id.textoTituloTreinamento);
	    TextView instrucoes = (TextView) findViewById(R.id.instrucoes);
	    TextView descricaoTreinamento = (TextView) findViewById(R.id.descricao_modo_treinamento);
	    TextView textoNaoMostrarNovamente = (TextView) findViewById(R.id.texto_nao_mostrar_novamente);
	    
	    textoTitulo.setTypeface(tf);
	    instrucoes.setTypeface(tf);
	    descricaoTreinamento.setTypeface(tf);
	    textoNaoMostrarNovamente.setTypeface(tf);
	    
	}

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	private boolean temConexaoComInternet() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	public void mudarParaTelaAtualizeOJogo(String versaoMaisAtual)
	{
		switchToScreen(R.id.telaatualizeojogo);
		
		String stringMensagemAtualizeJogo = getResources().getString(R.string.mensagem_por_favor_atualize_o_jogo);
		stringMensagemAtualizeJogo = stringMensagemAtualizeJogo + versaoMaisAtual;
		
		TextView textViewAtualize = (TextView) findViewById(R.id.mensagemAtualizeOJogo);
		textViewAtualize.setText(stringMensagemAtualizeJogo);
	}
	
	void switchToScreen(int screenId) {
		// make the requested screen visible; hide all others.
		for (int id : SCREENS) {
		    findViewById(id).setVisibility(screenId == id ? View.VISIBLE : View.GONE);
		}
	}

}
