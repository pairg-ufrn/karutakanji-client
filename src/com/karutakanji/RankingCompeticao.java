package com.karutakanji;

import java.util.ArrayList;
import java.util.LinkedList;

import bancodedados.AdapterListViewRanking;
import bancodedados.AdapterListViewSalasCriadas;
import bancodedados.ArmazenaTudoParaJogoOffline;
import bancodedados.DadosRankingCompeticao;
import bancodedados.Pega20MelhoresJogadoresRankingTask;
import bancodedados.PegaTodoORankingTask;
import br.ufrn.dimap.pairg.karutakanji.android.R;
import br.ufrn.dimap.pairg.karutakanji.android.R.layout;
import br.ufrn.dimap.pairg.karutakanji.android.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class RankingCompeticao extends ActivityDoJogoComSom implements OnScrollListener
{
	private ProgressDialog loadingPegaRanking;
	private String qualVisualizacaoDeRankingEstaSendoMostrada; //2 valores: '20melhores' ou 'todooranking'
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ranking_competicao);
		
		this.loadingPegaRanking = ProgressDialog.show(RankingCompeticao.this, getResources().getString(R.string.carregando_ranking), getResources().getString(R.string.por_favor_aguarde));
		
		Pega20MelhoresJogadoresRankingTask pega20MelhoresJogadores = new Pega20MelhoresJogadoresRankingTask(this);
		pega20MelhoresJogadores.execute("");
		
		this.qualVisualizacaoDeRankingEstaSendoMostrada = "20melhores";
		this.mudarFonteTextosDaTela();
		
		final ListView lista_ranking = (ListView) findViewById(R.id.lista_ranking);
		//View header = getLayoutInflater().inflate(R.layout.headerlistviewranking, null); TIREI O CABELHO DA LISTA!
		//lista_ranking.addHeaderView(header);
		
		//this.mudarFonteTextosHeader();
		
		lista_ranking.setOnScrollListener(this);
		ImageView seta_cima_ranking_competicao = (ImageView) findViewById(R.id.seta_cima_ranking_competicao);
	    ImageView seta_baixo_ranking_competicao = (ImageView) findViewById(R.id.seta_baixo_ranking_competicao);
		
	    seta_cima_ranking_competicao.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub
				lista_ranking.smoothScrollBy(-40, 20);
				return true;
				
			}
		});
		
	    seta_baixo_ranking_competicao.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub
				lista_ranking.smoothScrollBy(40, 20);
				return true;
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ranking_competicao, menu);
		return true;
	}

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	public void atualizarListViewComRankingUsuarios(ArrayList<DadosRankingCompeticao> dadosRanking)
	{
		
		AdapterListViewRanking adapterRanking = new AdapterListViewRanking
				(this, br.ufrn.dimap.pairg.karutakanji.android.R.layout.item_lista_ranking, dadosRanking);
		ListView listViewRanking = (ListView) findViewById(R.id.lista_ranking);
		listViewRanking.setAdapter(adapterRanking); 
		
		if(this.qualVisualizacaoDeRankingEstaSendoMostrada.compareTo("todooranking") == 0)
		{
			//temos de achar a posicao do usuario na arraylist
			LinkedList<String> nomeEEMail = 
					ArmazenaTudoParaJogoOffline.getInstance().getLoginUsuarioLocalmente(this);
			String nome_usuario = nomeEEMail.get(0);
			for(int i = 0; i < dadosRanking.size(); i++)
			{
				DadosRankingCompeticao umDados = dadosRanking.get(i);
				if(umDados.getNome_usuario().compareTo(nome_usuario) == 0)
				{
					//achamos o usuario! Vamos posicionar a visao da lista nele!
					listViewRanking.setSelection(i);
					break;
				}
			}
		}
		
		this.loadingPegaRanking.dismiss();
		
	}
	
	public void mudarVisualizacaoRanking(View v)
	{
		if(this.qualVisualizacaoDeRankingEstaSendoMostrada.compareTo("20melhores") == 0)
		{
			this.qualVisualizacaoDeRankingEstaSendoMostrada = "todooranking";
			Button botao = (Button) findViewById(R.id.botaoMudarVisualizacaoRanking);
			String vinteMelhores = getResources().getString(R.string.vinteMelhores);
			botao.setText(vinteMelhores);
			
			this.loadingPegaRanking = ProgressDialog.show(RankingCompeticao.this, getResources().getString(R.string.carregando_ranking), getResources().getString(R.string.por_favor_aguarde));
			
			PegaTodoORankingTask pegaTodoORanking = new PegaTodoORankingTask(this);
			pegaTodoORanking.execute("");
		}
		else
		{
			this.qualVisualizacaoDeRankingEstaSendoMostrada = "20melhores";
			Button botao = (Button) findViewById(R.id.botaoMudarVisualizacaoRanking);
			String minhaPosicao = getResources().getString(R.string.minhaPosicao);
			botao.setText(minhaPosicao);
			
			this.loadingPegaRanking = ProgressDialog.show(RankingCompeticao.this, getResources().getString(R.string.carregando_ranking), getResources().getString(R.string.por_favor_aguarde));
			
			Pega20MelhoresJogadoresRankingTask pega20MelhoresJogadores = new Pega20MelhoresJogadoresRankingTask(this);
			pega20MelhoresJogadores.execute("");
		}
	}
	
	private void mudarFonteTextosDaTela()
	{
		String fontPath2 = "fonts/Wonton.ttf";
	    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
	    TextView textoTituloCompeticao = (TextView) findViewById(R.id.textoTituloCompeticao);
	    textoTituloCompeticao.setTypeface(tf2);
	    
	    TextView textTituloRankingCompeticao = (TextView) findViewById(R.id.textTituloRankingCompeticao);
	    textTituloRankingCompeticao.setTypeface(tf2);
	    
	}
	
	/*private void mudarFonteTextosHeader()
	{
		String fontPath2 = "fonts/mvboli.ttf";
	    Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
	    TextView label_posicao_do_jogador = (TextView) findViewById(R.id.label_posicao_do_jogador);
	    label_posicao_do_jogador.setTypeface(tf2);
	    TextView label_usuario = (TextView) findViewById(R.id.label_usuario);
	    label_usuario.setTypeface(tf2);
	    TextView label_pontuacao = (TextView) findViewById(R.id.label_pontuacao);
	    label_pontuacao.setTypeface(tf2);
	    TextView label_vitorias = (TextView) findViewById(R.id.label_vitorias);
	    label_vitorias.setTypeface(tf2);
	    TextView label_derrotas = (TextView) findViewById(R.id.label_derrotas);
	    label_derrotas.setTypeface(tf2);
	}*/
	
	@Override
	public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) 
	{
		// TODO Auto-generated method stub
		int firstVisibleItem = arg1;
		int totalItemCount = arg3;
		
		ListView lista_ranking = (ListView) findViewById(R.id.lista_ranking);
		int indiceObjetoDeBaixo = lista_ranking.getLastVisiblePosition();
		
		final int lastItem = firstVisibleItem + arg2;
		
		
		ImageView seta_cima_ranking_competicao = (ImageView) findViewById(R.id.seta_cima_ranking_competicao);
	    ImageView seta_baixo_ranking_competicao = (ImageView) findViewById(R.id.seta_baixo_ranking_competicao);
		
	    boolean usuarioEstahNoComecoDaLista = false;
		if (lista_ranking.getFirstVisiblePosition() == 0)
		{
			View childView = lista_ranking.getChildAt(0);
			if(childView != null && childView.getTop() >= 0)
			usuarioEstahNoComecoDaLista = true;
		}
		boolean usuarioEstahNoFimDaLista = false;
		if (lista_ranking.getAdapter() != null && lista_ranking.getLastVisiblePosition() == lista_ranking.getAdapter().getCount() -1)
			
		{
			View childView = lista_ranking.getChildAt(lista_ranking.getChildCount() - 1);
			if(childView != null && childView.getBottom() <= lista_ranking.getHeight())
			{
				usuarioEstahNoFimDaLista = true;
			}
		}
	    
		if(usuarioEstahNoComecoDaLista == true && usuarioEstahNoFimDaLista == false)
		{
			//antes era firstVisibleItem == 0
			seta_cima_ranking_competicao.setVisibility(View.GONE);
			seta_baixo_ranking_competicao.setVisibility(View.VISIBLE);
		}
		else if(usuarioEstahNoComecoDaLista == false && usuarioEstahNoFimDaLista == false)
		{
			seta_cima_ranking_competicao.setVisibility(View.VISIBLE);
			seta_baixo_ranking_competicao.setVisibility(View.VISIBLE);
		}
		else if(usuarioEstahNoFimDaLista == true && usuarioEstahNoComecoDaLista == false)
		{
			//antes era if(lastItem == totalItemCount)
			//a seta para baixo nao aparece
			seta_cima_ranking_competicao.setVisibility(View.VISIBLE);
			seta_baixo_ranking_competicao.setVisibility(View.GONE);
		}
		else
		{
			//ambas as setas aparecem
			seta_cima_ranking_competicao.setVisibility(View.GONE);
			seta_baixo_ranking_competicao.setVisibility(View.GONE);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

}
