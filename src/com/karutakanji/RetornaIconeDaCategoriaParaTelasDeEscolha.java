package com.karutakanji;

import android.app.Activity;
import br.ufrn.dimap.pairg.karutakanji.android.R;

public class RetornaIconeDaCategoriaParaTelasDeEscolha 
{
	public static int retornarIdIconeDaCategoria(String categoria, Activity activity)
	{
     	String cotidiano = activity.getResources().getString(R.string.cotidiano);
     	String lugar = activity.getResources().getString(R.string.lugar);
     	String natureza = activity.getResources().getString(R.string.natureza);
     	String verbos = activity.getResources().getString(R.string.verbos);
     	String adjetivos = activity.getResources().getString(R.string.adjetivos);
     	String tempo = activity.getResources().getString(R.string.tempo);
     	String supermercado = activity.getResources().getString(R.string.supermercado);
     	String lazer = activity.getResources().getString(R.string.lazer);
     	String educacao = activity.getResources().getString(R.string.educacao);
     	String trabalho = activity.getResources().getString(R.string.trabalho);
     	String geografia = activity.getResources().getString(R.string.geografia);
     	String saude = activity.getResources().getString(R.string.saude);
     	
		if(categoria.compareTo(adjetivos) == 0)
		{
			return R.drawable.categoria_adjetivo;
		}
		else if(categoria.compareTo(cotidiano) == 0)
		{
			return R.drawable.categoria_cotidiano;
		}
		else if(categoria.compareTo(educacao) == 0)
		{
			return R.drawable.categoria_educacao;
		}
		else if(categoria.compareTo(geografia) == 0)
		{
			return R.drawable.categoria_geografia;
		}
		else if(categoria.compareTo(lazer) == 0)
		{
			return R.drawable.categoria_lazer;
		}
		else if(categoria.compareTo(lugar) == 0)
		{
			return R.drawable.categoria_lugar;
		}
		else if(categoria.compareTo(natureza) == 0)
		{
			return R.drawable.categoria_natureza;
		}
		else if(categoria.compareTo(saude) == 0)
		{
			return R.drawable.categoria_saude;
		}
		else if(categoria.compareTo(supermercado) == 0)
		{
			return R.drawable.categoria_supermercado;
		}
		else if(categoria.compareTo(tempo) == 0)
		{
			return R.drawable.categoria_tempo;
		}
		else if(categoria.compareTo(trabalho) == 0)
		{
			return R.drawable.categoria_trabalho;
		}
		else
		{
			return R.drawable.categoria_verbo;
		}
	}

}
