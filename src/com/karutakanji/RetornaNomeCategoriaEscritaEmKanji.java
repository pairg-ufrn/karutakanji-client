package com.karutakanji;

import br.ufrn.dimap.pairg.karutakanji.android.R;
import android.app.Activity;

public class RetornaNomeCategoriaEscritaEmKanji 
{
	public static String retornarNomeCategoriaEscritaEmKanji(String categoria, Activity activity)
	{
		String cotidiano = activity.getResources().getString(R.string.cotidiano);
     	String lugar = activity.getResources().getString(R.string.lugar);
     	String natureza = activity.getResources().getString(R.string.natureza);
     	String verbos = activity.getResources().getString(R.string.verbos);
     	String adjetivos = activity.getResources().getString(R.string.adjetivos);
     	String tempo = activity.getResources().getString(R.string.tempo);
     	String supermercado = activity.getResources().getString(R.string.supermercado);
     	String lazer = activity.getResources().getString(R.string.lazer);
     	String educacao = activity.getResources().getString(R.string.educacao);
     	String trabalho = activity.getResources().getString(R.string.trabalho);
     	String geografia = activity.getResources().getString(R.string.geografia);
     	String saude = activity.getResources().getString(R.string.saude);
     	
		if(categoria.compareTo(adjetivos) == 0)
		{
			return "形容詞";
		}
		else if(categoria.compareTo(cotidiano) == 0)
		{
			return "日常";
		}
		else if(categoria.compareTo(educacao) == 0)
		{
			return "教育";
		}
		else if(categoria.compareTo(geografia) == 0)
		{
			return "地理学";
		}
		else if(categoria.compareTo(lazer) == 0)
		{
			return "余暇";
		}
		else if(categoria.compareTo(lugar) == 0)
		{
			return "場所";
		}
		else if(categoria.compareTo(natureza) == 0)
		{
			return "自然";
		}
		else if(categoria.compareTo(saude) == 0)
		{
			return "健康";
		}
		else if(categoria.compareTo(supermercado) == 0)
		{
			return "スーパー";
		}
		else if(categoria.compareTo(tempo) == 0)
		{
			return "時間";
		}
		else if(categoria.compareTo(trabalho) == 0)
		{
			return "仕事";
		}
		else if(categoria.compareTo(verbos) == 0)
		{
			return "動詞";
		}
		else
		{
			return "";
		}
	}

}
