package com.karutakanji;

/*esse singleton soh serve para armazenar se deve ser mostrada as dicas entre a tela de
 * escolha das categorias e a do jogo em si, se o usuario quer treinar as palavras erradas no modo treinamento etc
 * Nao poderia ser um extra passado no intent porque qd o usuario restarta o treino com as msm
 * categorias, o metodo onCreate() seria chamado novamente*/
public class SingletonConfiguracoesModoTreinamento 
{
	private boolean mostrarDicas;
	private boolean treinarPalavrasErradas;
	private boolean treinarPalavrasMenosTreinadas;
	
	private static SingletonConfiguracoesModoTreinamento  instancia;
	
	private SingletonConfiguracoesModoTreinamento()
	{
		
	}
	
	public static SingletonConfiguracoesModoTreinamento getInstance()
	{
		if(instancia == null)
		{
			instancia = new SingletonConfiguracoesModoTreinamento();
		}
		
		return instancia;
	}

	public boolean getMostrarDicas() {
		return mostrarDicas;
	}

	public void setMostrarDicas(boolean mostrarDicas) {
		this.mostrarDicas = mostrarDicas;
	}

	public boolean getTreinarPalavrasErradas() {
		return treinarPalavrasErradas;
	}

	public void setTreinarPalavrasErradas(boolean treinarPalavrasErradas) {
		this.treinarPalavrasErradas = treinarPalavrasErradas;
	}

	public boolean getTreinarPalavrasMenosTreinadas() {
		return treinarPalavrasMenosTreinadas;
	}

	public void setTreinarPalavrasMenosTreinadas(
			boolean treinarPalavrasMenosTreinadas) {
		this.treinarPalavrasMenosTreinadas = treinarPalavrasMenosTreinadas;
	}
	
	
}
