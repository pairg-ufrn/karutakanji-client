package com.karutakanji;

public class SingletonDeveMostrarTelaLoginNaMainActivity 
{
	private boolean deveMostrarTelaLogin;
	private static SingletonDeveMostrarTelaLoginNaMainActivity instancia;
	
	private SingletonDeveMostrarTelaLoginNaMainActivity()
	{
		deveMostrarTelaLogin = true;
	}
	
	public static SingletonDeveMostrarTelaLoginNaMainActivity getInstance()
	{
		if(instancia == null)
		{
			instancia = new SingletonDeveMostrarTelaLoginNaMainActivity();
		}
		
		return instancia;
	}

	public boolean getDeveMostrarTelaLogin() {
		return deveMostrarTelaLogin;
	}

	public void setDeveMostrarTelaLogin(boolean deveMostrar) {
		this.deveMostrarTelaLogin = deveMostrar;
	}

}
